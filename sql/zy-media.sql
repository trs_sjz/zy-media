/*
 Navicat Premium Data Transfer

 Source Server         : mysql5.7.29
 Source Server Type    : MySQL
 Source Server Version : 50729
 Source Host           : localhost:3308
 Source Schema         : zy-media

 Target Server Type    : MySQL
 Target Server Version : 50729
 File Encoding         : 65001

 Date: 24/03/2022 11:59:29
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作 sub主子表操作）',
  `package_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '代码生成业务表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
  `column_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '代码生成业务表字段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for mam_picture
-- ----------------------------
DROP TABLE IF EXISTS `mam_picture`;
CREATE TABLE `mam_picture`  (
  `picture_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `group_id` int(10) NULL DEFAULT NULL COMMENT '分组ID',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题',
  `filterwords` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '关键词',
  `categoryids` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '分类ID',
  `picture_sort` int(8) NULL DEFAULT NULL COMMENT '图片排序',
  `tag_make` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '相机制造商',
  `tag_model` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '相机型号',
  `tag_fnumber` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '光圈值',
  `tag_exposure_time` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '曝光时间',
  `tag_iso_equivalent` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '感光度',
  `tag_focal_length` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '焦距',
  `tag_max_aperture` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '最大光圈',
  `tag_exif_image_width` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '宽度',
  `tag_exif_image_height` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '高度',
  `tag_x_resolution` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '水平分辨率',
  `tag_y_resolution` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '垂直分辨率',
  `tag_x_resolution_print` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '水平打印分辨率',
  `tag_y_resolution_print` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '垂直打印分辨率',
  `tag_software` varchar(125) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '显示固件Firmware版本',
  `tag_35mm_film_equiv_focal_length` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '35纳米孔径',
  `tag_aperture` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '孔径(图片分辨率单位)',
  `tag_body_serial_number` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '序列号',
  `tag_metering_mode` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '点测光值',
  `tag_resolution_unit` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '分辨率单位',
  `tag_exposure_bias` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '曝光补偿',
  `tag_color_space` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '色域、色彩空间',
  `tag_ycbcr_coefficients` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '色相系数',
  `tag_ycbcr_positioning` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '色相定位',
  `tag_ycbcr_subsampling` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '色相抽样',
  `tag_exif_version` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'exif版本号',
  `mime_type` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'mime类型',
  `subpath` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '目录',
  `fileext` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '后缀类型',
  `filename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件名称',
  `filenamebase` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件基础名称',
  `fileoriginalname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件原始名称',
  `filepathbase` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件基础路径',
  `filesize` double NULL DEFAULT NULL COMMENT '文件大小',
  `thumb_middle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '中图缩略图',
  `thumb_small` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '小图缩略图',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by_user_id` int(10) NULL DEFAULT NULL COMMENT '创建用户ID',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `delete_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '删除者',
  `delete_time` datetime(0) NULL DEFAULT NULL COMMENT '删除时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`picture_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '图片信息' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for mam_picture_category
-- ----------------------------
DROP TABLE IF EXISTS `mam_picture_category`;
CREATE TABLE `mam_picture_category`  (
  `category_id` bigint(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '中文名称',
  `name_en` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '英文名称',
  `parent_id` bigint(10) NULL DEFAULT 0 COMMENT '父节点ID',
  `visible` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单状态（0显示 1隐藏）',
  `order_num` int(6) NULL DEFAULT NULL COMMENT '排序字段',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`category_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '图片分类信息' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for mam_picture_group
-- ----------------------------
DROP TABLE IF EXISTS `mam_picture_group`;
CREATE TABLE `mam_picture_group`  (
  `group_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `dept_id` int(10) NULL DEFAULT NULL COMMENT '部门ID',
  `leader` tinyint(1) NULL DEFAULT NULL COMMENT '1是领导人 0非领导人',
  `group_title` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '分组标题',
  `group_keywords` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '分组关键字',
  `photographer` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '摄影师',
  `filming_place` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '拍摄地',
  `filming_date` datetime(0) NULL DEFAULT NULL COMMENT '拍摄日期',
  `picture_quantity` int(10) NULL DEFAULT 0 COMMENT '图片数量',
  `categorys` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '组图分类',
  `cover_id` int(10) NULL DEFAULT 0 COMMENT '封面ID',
  `dept_visible` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '部门可见 0不可见 1 可见',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `owner_userid` int(11) NULL DEFAULT NULL COMMENT '拥有者id',
  `create_by_userid` int(11) NULL DEFAULT NULL COMMENT '创建者ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `delete_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '删除者',
  `delete_time` datetime(0) NULL DEFAULT NULL COMMENT '删除时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`group_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '图组信息' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for mam_picture_view
-- ----------------------------
DROP TABLE IF EXISTS `mam_picture_view`;
CREATE TABLE `mam_picture_view`  (
  `view_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问量id',
  `view_num` int(200) NULL DEFAULT 0 COMMENT '当天访问量',
  `view_data` datetime(0) NULL DEFAULT NULL COMMENT '日期',
  PRIMARY KEY (`view_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '图片库访问量' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for mam_video
-- ----------------------------
DROP TABLE IF EXISTS `mam_video`;
CREATE TABLE `mam_video`  (
  `video_id` int(11) NOT NULL AUTO_INCREMENT,
  `dept_id` int(11) NULL DEFAULT NULL COMMENT '部门ID',
  `category_id` int(11) NULL DEFAULT NULL COMMENT '分类ID',
  `groups` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '视频分组',
  `source_video_id` int(11) NULL DEFAULT NULL COMMENT '源视频ID',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题',
  `subtitle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '副标题',
  `keywords` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '关键字',
  `subpath` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '路径',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '视频名称',
  `originfilename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '视频原名称',
  `playcount` bigint(20) NULL DEFAULT NULL COMMENT '播放次数',
  `processstatus` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '处理状态',
  `status` int(5) NULL DEFAULT NULL COMMENT '视频状态',
  `size` bigint(20) NULL DEFAULT NULL COMMENT '大小',
  `source` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '来源',
  `streams` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '码流',
  `thumbname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '视频截图',
  `thumbs` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '视频截图缩略图',
  `audiobitrate` int(11) NULL DEFAULT NULL COMMENT '音频比特率',
  `audiochannels` int(11) NULL DEFAULT NULL COMMENT '声道',
  `audiocodec` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '音频编码器',
  `audioformat` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '音频格式',
  `audiosamplerate` int(11) NULL DEFAULT NULL COMMENT '音频采样率',
  `bitrate` int(11) NULL DEFAULT NULL COMMENT '比特率',
  `demuxer` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '视频格式',
  `duration` int(11) NULL DEFAULT NULL COMMENT '播放时长',
  `fps` int(11) NULL DEFAULT NULL COMMENT '每秒传输帧数',
  `framerate` double NULL DEFAULT NULL COMMENT '帧速率',
  `width` int(11) NULL DEFAULT NULL COMMENT '视频宽度',
  `height` int(11) NULL DEFAULT NULL COMMENT '视频高度',
  `mediatype` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '视频类型',
  `nbframes` int(11) NULL DEFAULT NULL COMMENT '丢帧数',
  `pixelformat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '像素',
  `videocodec` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '视频编码',
  `videoformat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '视频格式',
  `videolevel` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '视频级别',
  `videoprofile` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '视频属性',
  `elapsedseconds` int(11) NULL DEFAULT NULL COMMENT '响应时间',
  `fileext` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '后缀',
  `filename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件名称',
  `filenamebase` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件名称基础',
  `filepathbase` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件基础路径',
  `filterwords` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '过滤关键字',
  `author` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '视频作者',
  `leader` tinyint(1) NULL DEFAULT NULL COMMENT '1是领导人 0非领导人',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除状态',
  `createdmanner` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建方式',
  `create_user_id` int(11) NULL DEFAULT NULL COMMENT '创建用户ID',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `delete_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '删除人',
  `delete_time` datetime(0) NULL DEFAULT NULL COMMENT '删除时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `shooting_time` datetime(0) NULL DEFAULT NULL COMMENT '视频拍摄时间',
  PRIMARY KEY (`video_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '视频信息' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for mam_video_category
-- ----------------------------
DROP TABLE IF EXISTS `mam_video_category`;
CREATE TABLE `mam_video_category`  (
  `category_id` bigint(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '中文名称',
  `name_en` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '英文名称',
  `parent_id` bigint(10) NULL DEFAULT NULL COMMENT '父节点ID',
  `visible` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单状态（0显示 1隐藏）',
  `order_num` int(6) NULL DEFAULT NULL COMMENT '排序字段',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`category_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '视频分类信息' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for mam_video_config
-- ----------------------------
DROP TABLE IF EXISTS `mam_video_config`;
CREATE TABLE `mam_video_config`  (
  `config_id` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `config_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '参数名称',
  `config_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '参数键名',
  `config_value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '参数键值',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`) USING BTREE,
  UNIQUE INDEX `参数主键唯一`(`config_key`) USING BTREE COMMENT '参数'
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '视频转码配置表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of mam_video_config
-- ----------------------------
INSERT INTO `mam_video_config` VALUES (1, NULL, 'streamformat', 'H264', NULL, NULL, 'admin', '2022-03-11 15:28:57', NULL);
INSERT INTO `mam_video_config` VALUES (2, NULL, 'streamquality', '3', NULL, NULL, 'admin', '2022-03-11 15:28:57', NULL);
INSERT INTO `mam_video_config` VALUES (3, NULL, 'stream1bitrate', '300k', NULL, NULL, 'admin', '2022-03-11 15:28:57', NULL);
INSERT INTO `mam_video_config` VALUES (4, NULL, 'stream1resolution', '360', NULL, NULL, 'admin', '2022-03-11 15:28:57', NULL);
INSERT INTO `mam_video_config` VALUES (5, NULL, 'stream1audioBitrate', '32', NULL, NULL, 'admin', '2022-03-11 15:28:57', NULL);
INSERT INTO `mam_video_config` VALUES (6, NULL, 'stream1audioChannels', '2', NULL, NULL, 'admin', '2022-03-11 15:28:57', NULL);
INSERT INTO `mam_video_config` VALUES (7, NULL, 'stream2bitrate', '600k', NULL, NULL, 'admin', '2022-03-11 15:28:57', NULL);
INSERT INTO `mam_video_config` VALUES (8, NULL, 'stream2resolution', '576', NULL, NULL, 'admin', '2022-03-11 15:28:57', NULL);
INSERT INTO `mam_video_config` VALUES (9, NULL, 'stream2audioBitrate', '64', NULL, NULL, 'admin', '2022-03-11 15:28:57', NULL);
INSERT INTO `mam_video_config` VALUES (10, NULL, 'stream2audioChannels', '2', NULL, NULL, 'admin', '2022-03-11 15:28:57', NULL);
INSERT INTO `mam_video_config` VALUES (11, NULL, 'stream3bitrate', '1200k', NULL, NULL, 'admin', '2022-03-11 15:28:57', NULL);
INSERT INTO `mam_video_config` VALUES (12, NULL, 'stream3resolution', '810', NULL, NULL, 'admin', '2022-03-11 15:28:57', NULL);
INSERT INTO `mam_video_config` VALUES (13, NULL, 'stream3audioBitrate', '98', NULL, NULL, 'admin', '2022-03-11 15:28:57', NULL);
INSERT INTO `mam_video_config` VALUES (14, NULL, 'stream3audioChannels', '2', NULL, NULL, 'admin', '2022-03-11 15:28:57', NULL);
INSERT INTO `mam_video_config` VALUES (15, NULL, 'stream4bitrate', '5M', NULL, NULL, 'admin', '2022-03-11 15:28:57', NULL);
INSERT INTO `mam_video_config` VALUES (16, NULL, 'stream4resolution', '1080', NULL, NULL, 'admin', '2022-03-11 15:28:57', NULL);
INSERT INTO `mam_video_config` VALUES (17, NULL, 'stream4audioBitrate', '128', NULL, NULL, 'admin', '2022-03-11 15:28:58', NULL);
INSERT INTO `mam_video_config` VALUES (18, NULL, 'stream4audioChannels', '2', NULL, NULL, 'admin', '2022-03-11 15:28:58', NULL);
INSERT INTO `mam_video_config` VALUES (19, NULL, 'stream5bitrate', '10M', NULL, NULL, 'admin', '2022-03-11 15:28:58', NULL);
INSERT INTO `mam_video_config` VALUES (20, NULL, 'stream5resolution', '1600', NULL, NULL, 'admin', '2022-03-11 15:28:58', NULL);
INSERT INTO `mam_video_config` VALUES (21, NULL, 'stream5audioBitrate', '192', NULL, NULL, 'admin', '2022-03-11 15:28:58', NULL);
INSERT INTO `mam_video_config` VALUES (22, NULL, 'stream5audioChannels', '2', NULL, NULL, 'admin', '2022-03-11 15:28:58', NULL);
INSERT INTO `mam_video_config` VALUES (23, NULL, 'stream6bitrate', '18M', NULL, NULL, 'admin', '2022-03-11 15:28:58', NULL);
INSERT INTO `mam_video_config` VALUES (24, NULL, 'stream6resolution', '2160', NULL, NULL, 'admin', '2022-03-11 15:28:58', NULL);
INSERT INTO `mam_video_config` VALUES (25, NULL, 'stream6audioBitrate', '256', NULL, NULL, 'admin', '2022-03-11 15:28:58', NULL);
INSERT INTO `mam_video_config` VALUES (26, NULL, 'stream6audioChannels', '2', NULL, NULL, 'admin', '2022-03-11 15:28:58', NULL);
INSERT INTO `mam_video_config` VALUES (27, NULL, 'thumbMiddleWidth', '640', NULL, NULL, 'admin', '2022-03-11 15:28:58', NULL);
INSERT INTO `mam_video_config` VALUES (28, NULL, 'thumbMiddleHeight', '360', NULL, NULL, 'admin', '2022-03-11 15:28:58', NULL);
INSERT INTO `mam_video_config` VALUES (29, NULL, 'thumbSmallWidth', '192', NULL, NULL, 'admin', '2022-03-11 15:28:58', NULL);
INSERT INTO `mam_video_config` VALUES (30, NULL, 'thumbSmallHeight', '146', NULL, NULL, 'admin', '2022-03-11 15:28:58', NULL);

-- ----------------------------
-- Table structure for mam_video_frament
-- ----------------------------
DROP TABLE IF EXISTS `mam_video_frament`;
CREATE TABLE `mam_video_frament`  (
  `frament_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '片段ID',
  `video_id` int(11) NULL DEFAULT NULL COMMENT '视频ID',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '片段名称',
  `thumb_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '片段截图',
  `start` double NULL DEFAULT NULL COMMENT '起始时间点',
  `end` double NULL DEFAULT NULL COMMENT '结束时间点',
  `create_user_id` int(11) NULL DEFAULT NULL COMMENT '创建者ID',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`frament_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '视频片段表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for mam_video_lable
-- ----------------------------
DROP TABLE IF EXISTS `mam_video_lable`;
CREATE TABLE `mam_video_lable`  (
  `lable_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `video_id` int(11) NULL DEFAULT NULL COMMENT '视频ID',
  `person` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '人物',
  `object` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '物体',
  `address` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '地点',
  `action` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '动作事件',
  `mark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标识',
  `scene` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '场景',
  `organization` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '组织机构',
  `keyword` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '关键词',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建信息',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`lable_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '视频标签表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for mam_video_log
-- ----------------------------
DROP TABLE IF EXISTS `mam_video_log`;
CREATE TABLE `mam_video_log`  (
  `log_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '日志ID',
  `object_id` int(11) NULL DEFAULT NULL COMMENT '对象ID',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '操作用户',
  `create_user_id` int(11) NULL DEFAULT NULL COMMENT '创建该视频的用户id',
  `category_id` int(11) NULL DEFAULT NULL COMMENT '视频分类id',
  `action_media_type` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作媒资类型',
  `action` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作action',
  `action_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作名词',
  `action_des` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作详情',
  `app_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作APP编码',
  `source` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '来源',
  `type` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作类型',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`log_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for mam_video_process
-- ----------------------------
DROP TABLE IF EXISTS `mam_video_process`;
CREATE TABLE `mam_video_process`  (
  `process_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流程ID',
  `workflow_id` int(11) NULL DEFAULT NULL COMMENT '工作流ID',
  `video_id` int(11) NULL DEFAULT NULL COMMENT '视频ID',
  `start_time` datetime(0) NULL DEFAULT NULL COMMENT '起始时间',
  `end_time` datetime(0) NULL DEFAULT NULL COMMENT '结束时间',
  `start_user_id` int(11) NULL DEFAULT NULL COMMENT '起草人',
  `procinst` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '流程序列ID',
  `start_act_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '开始节点ID',
  `end_act_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '结束节点ID',
  `prev_act_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '上一个节点',
  `execution_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '当前执行节点',
  `next_act_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '下一个节点',
  `message` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '审核意见',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`process_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '视频审核表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for mam_video_processjob
-- ----------------------------
DROP TABLE IF EXISTS `mam_video_processjob`;
CREATE TABLE `mam_video_processjob`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creatornodekey` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建服务器名称',
  `markernodekey` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '执行服务器名称',
  `detail` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '详情',
  `domainobjid` int(11) NULL DEFAULT NULL COMMENT '对象ID',
  `marktime` datetime(0) NULL DEFAULT NULL COMMENT '开始时间',
  `processorder` int(11) NULL DEFAULT NULL COMMENT '排序',
  `state` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '执行的状态',
  `status` int(11) NULL DEFAULT NULL COMMENT '状态',
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '类型',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_by_userid` int(11) NULL DEFAULT NULL COMMENT '创建用户ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_by_userid` int(11) NULL DEFAULT NULL COMMENT '更新者ID',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for mam_video_source
-- ----------------------------
DROP TABLE IF EXISTS `mam_video_source`;
CREATE TABLE `mam_video_source`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attachedpic` int(11) NULL DEFAULT NULL COMMENT '附件ID',
  `audiobitrate` int(11) NULL DEFAULT NULL COMMENT '音频比特率',
  `audiochannels` int(11) NULL DEFAULT NULL COMMENT '声道',
  `audiocodec` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '音频编码',
  `audioformat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '音频格式',
  `audiosamplerate` int(11) NULL DEFAULT NULL COMMENT '音频采样率',
  `bitrate` int(11) NULL DEFAULT NULL COMMENT '比特率',
  `demuxer` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '格式',
  `duration` int(11) NULL DEFAULT NULL COMMENT '时长',
  `fps` int(11) NULL DEFAULT NULL COMMENT 'fps',
  `framerate` double NULL DEFAULT NULL COMMENT '帧',
  `height` int(11) NULL DEFAULT NULL COMMENT '高度',
  `mediatype` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '媒资类型',
  `nbframes` int(11) NULL DEFAULT NULL COMMENT '丢帧数',
  `pixelformat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '像素',
  `videocodec` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '视频编码',
  `videoformat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '视频格式',
  `videolevel` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '视频级别',
  `videoprofile` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '视频属性',
  `width` int(11) NULL DEFAULT NULL COMMENT '宽度',
  `autosegment` bit(1) NULL DEFAULT NULL,
  `categoryid` int(11) NULL DEFAULT NULL,
  `createdip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建IP',
  `createdmanner` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建方式',
  `fileext` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件后缀',
  `filename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件名称',
  `filepath` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件路径',
  `fsroot` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `lang` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '语言',
  `md5sum` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'md5值',
  `mtype` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '类型',
  `originname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '原始名称',
  `sampleid` int(11) NULL DEFAULT NULL COMMENT '样本ID',
  `size` bigint(20) NULL DEFAULT NULL COMMENT '文件大小',
  `source` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '来源',
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '状态',
  `subpath` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '目录',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题',
  `typeids` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `year` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `create_user_id` int(11) NULL DEFAULT NULL COMMENT '创建用户ID',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '源视频表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for mam_video_stream
-- ----------------------------
DROP TABLE IF EXISTS `mam_video_stream`;
CREATE TABLE `mam_video_stream`  (
  `stream_id` int(11) NOT NULL AUTO_INCREMENT,
  `videoid` int(11) NULL DEFAULT NULL COMMENT '视频ID',
  `attachedpic` int(11) NULL DEFAULT NULL,
  `audiobitrate` int(11) NULL DEFAULT NULL COMMENT '音频比特率',
  `audiochannels` int(11) NULL DEFAULT NULL COMMENT '音频声道',
  `audiocodec` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '音频编解码器',
  `audioformat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '音频格式',
  `audiosamplerate` int(11) NULL DEFAULT NULL COMMENT '音频采样速率',
  `bitrate` int(11) NULL DEFAULT NULL COMMENT '码率',
  `demuxer` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '视音频分离器',
  `duration` int(11) NULL DEFAULT NULL COMMENT '时长',
  `fps` int(11) NULL DEFAULT NULL COMMENT '平均帧率',
  `framerate` double NULL DEFAULT NULL COMMENT '帧速率',
  `height` int(11) NULL DEFAULT NULL COMMENT '高度',
  `mediatype` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '媒体类型',
  `nbframes` int(11) NULL DEFAULT NULL COMMENT '帧数',
  `pixelformat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '分辨率',
  `videocodec` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '视频编码',
  `videoformat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '视频格式',
  `videolevel` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '音视频编码器的级',
  `videoprofile` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '基本画质',
  `width` int(11) NULL DEFAULT NULL COMMENT '视频宽度',
  `consequent` int(11) NULL DEFAULT NULL,
  `consoleonly` bit(1) NULL DEFAULT NULL,
  `consumerappid` int(11) NULL DEFAULT NULL,
  `cputime` double NULL DEFAULT NULL,
  `elapsedseconds` int(11) NULL DEFAULT NULL COMMENT '转码时长',
  `fileext` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '后缀',
  `filename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件名',
  `filepath` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件路径',
  `format` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '格式',
  `fsroot` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'root权限',
  `isretranscode` bit(1) NULL DEFAULT NULL COMMENT '是否重新转码',
  `maxmenkb` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `operator` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作者\r\n',
  `progressivestatus` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '任务状态',
  `repositoryid` int(11) NULL DEFAULT NULL COMMENT '仓库ID',
  `size` bigint(20) NULL DEFAULT NULL COMMENT '大小',
  `subpath` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '目录',
  `transcodecmd` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '转码命令',
  `type` int(11) NULL DEFAULT NULL COMMENT '类型',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`stream_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for mam_video_thumb
-- ----------------------------
DROP TABLE IF EXISTS `mam_video_thumb`;
CREATE TABLE `mam_video_thumb`  (
  `thumb_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `video_id` int(11) NULL DEFAULT NULL COMMENT '视频ID',
  `subpath` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图片目录',
  `intercept_time` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '当前截取时间',
  `filename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件名称',
  `filepath` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件路径',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人名称',
  `create_user_id` int(11) NULL DEFAULT NULL COMMENT '创建者ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`thumb_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `blob_data` blob NULL COMMENT '存放持久化Trigger对象',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'Blob类型的触发器表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `calendar_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '日历名称',
  `calendar` blob NOT NULL COMMENT '存放持久化calendar对象',
  PRIMARY KEY (`sched_name`, `calendar_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '日历信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `cron_expression` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'cron表达式',
  `time_zone_id` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '时区',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'Cron类型的触发器表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `entry_id` varchar(95) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度器实例id',
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `instance_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度器实例名',
  `fired_time` bigint(13) NOT NULL COMMENT '触发的时间',
  `sched_time` bigint(13) NOT NULL COMMENT '定时器制定的时间',
  `priority` int(11) NOT NULL COMMENT '优先级',
  `state` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态',
  `job_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '任务名称',
  `job_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '任务组名',
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否并发',
  `requests_recovery` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否接受恢复执行',
  PRIMARY KEY (`sched_name`, `entry_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '已触发的触发器表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `job_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务组名',
  `description` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '相关介绍',
  `job_class_name` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '执行任务类名称',
  `is_durable` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否持久化',
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否并发',
  `is_update_data` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否更新数据',
  `requests_recovery` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否接受恢复执行',
  `job_data` blob NULL COMMENT '存放持久化job对象',
  PRIMARY KEY (`sched_name`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '任务详细信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `lock_name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '悲观锁名称',
  PRIMARY KEY (`sched_name`, `lock_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '存储的悲观锁信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  PRIMARY KEY (`sched_name`, `trigger_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '暂停的触发器表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `instance_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '实例名称',
  `last_checkin_time` bigint(13) NOT NULL COMMENT '上次检查时间',
  `checkin_interval` bigint(13) NOT NULL COMMENT '检查间隔时间',
  PRIMARY KEY (`sched_name`, `instance_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '调度器状态表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `repeat_count` bigint(7) NOT NULL COMMENT '重复的次数统计',
  `repeat_interval` bigint(12) NOT NULL COMMENT '重复的间隔时间',
  `times_triggered` bigint(10) NOT NULL COMMENT '已经触发的次数',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '简单触发器的信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `str_prop_1` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第一个参数',
  `str_prop_2` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第二个参数',
  `str_prop_3` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第三个参数',
  `int_prop_1` int(11) NULL DEFAULT NULL COMMENT 'int类型的trigger的第一个参数',
  `int_prop_2` int(11) NULL DEFAULT NULL COMMENT 'int类型的trigger的第二个参数',
  `long_prop_1` bigint(20) NULL DEFAULT NULL COMMENT 'long类型的trigger的第一个参数',
  `long_prop_2` bigint(20) NULL DEFAULT NULL COMMENT 'long类型的trigger的第二个参数',
  `dec_prop_1` decimal(13, 4) NULL DEFAULT NULL COMMENT 'decimal类型的trigger的第一个参数',
  `dec_prop_2` decimal(13, 4) NULL DEFAULT NULL COMMENT 'decimal类型的trigger的第二个参数',
  `bool_prop_1` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Boolean类型的trigger的第一个参数',
  `bool_prop_2` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Boolean类型的trigger的第二个参数',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '同步机制的行锁表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '触发器的名字',
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '触发器所属组的名字',
  `job_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_job_details表job_name的外键',
  `job_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_job_details表job_group的外键',
  `description` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '相关介绍',
  `next_fire_time` bigint(13) NULL DEFAULT NULL COMMENT '上一次触发时间（毫秒）',
  `prev_fire_time` bigint(13) NULL DEFAULT NULL COMMENT '下一次触发时间（默认为-1表示不触发）',
  `priority` int(11) NULL DEFAULT NULL COMMENT '优先级',
  `trigger_state` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '触发器状态',
  `trigger_type` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '触发器的类型',
  `start_time` bigint(13) NOT NULL COMMENT '开始时间',
  `end_time` bigint(13) NULL DEFAULT NULL COMMENT '结束时间',
  `calendar_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日程表名称',
  `misfire_instr` smallint(2) NULL DEFAULT NULL COMMENT '补偿执行的策略',
  `job_data` blob NULL COMMENT '存放持久化job对象',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  INDEX `sched_name`(`sched_name`, `job_name`, `job_group`) USING BTREE,
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `job_name`, `job_group`) REFERENCES `qrtz_job_details` (`sched_name`, `job_name`, `job_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '触发器详细信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '参数配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2022-03-23 15:03:03', '', NULL, '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2022-03-23 15:03:03', '', NULL, '初始化密码 123456');
INSERT INTO `sys_config` VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2022-03-23 15:03:03', '', NULL, '深黑主题theme-dark，浅色主题theme-light，深蓝主题theme-blue');
INSERT INTO `sys_config` VALUES (4, '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'false', 'Y', 'admin', '2022-03-23 15:03:03', '', NULL, '是否开启注册用户功能（true开启，false关闭）');
INSERT INTO `sys_config` VALUES (5, '用户管理-密码字符范围', 'sys.account.chrtype', '0', 'Y', 'admin', '2022-03-23 15:03:03', '', NULL, '默认任意字符范围，0任意（密码可以输入任意字符），1数字（密码只能为0-9数字），2英文字母（密码只能为a-z和A-Z字母），3字母和数字（密码必须包含字母，数字）,4字母数字和特殊字符（目前支持的特殊字符包括：~!@#$%^&*()-=_+）');
INSERT INTO `sys_config` VALUES (6, '用户管理-初始密码修改策略', 'sys.account.initPasswordModify', '0', 'Y', 'admin', '2022-03-23 15:03:03', '', NULL, '0：初始密码修改策略关闭，没有任何提示，1：提醒用户，如果未修改初始密码，则在登录时就会提醒修改密码对话框');
INSERT INTO `sys_config` VALUES (7, '用户管理-账号密码更新周期', 'sys.account.passwordValidateDays', '0', 'Y', 'admin', '2022-03-23 15:03:03', '', NULL, '密码更新周期（填写数字，数据初始化值为0不限制，若修改必须为大于0小于365的正整数），如果超过这个周期登录系统时，则在登录时就会提醒修改密码对话框');
INSERT INTO `sys_config` VALUES (8, '主框架页-菜单导航显示风格', 'sys.index.menuStyle', 'default', 'Y', 'admin', '2022-03-23 15:03:03', '', NULL, '菜单导航显示风格（default为左侧导航菜单，topnav为顶部导航菜单）');
INSERT INTO `sys_config` VALUES (9, '主框架页-是否开启页脚', 'sys.index.footer', 'true', 'Y', 'admin', '2022-03-23 15:03:03', '', NULL, '是否开启底部页脚显示（true显示，false隐藏）');
INSERT INTO `sys_config` VALUES (10, '主框架页-是否开启页签', 'sys.index.tagsView', 'true', 'Y', 'admin', '2022-03-23 15:03:03', '', NULL, '是否开启菜单多页签显示（true显示，false隐藏）');
INSERT INTO `sys_config` VALUES (11, '允许图片上传格式', 'sys.picture.extensions', 'jpg,gif,png,bmp,jpeg,zip,rar,7z', 'Y', 'admin', '2020-04-03 16:43:52', '', NULL, '加入zip,rar类型用来批量上传');
INSERT INTO `sys_config` VALUES (12, '允许图片上传媒体格式', 'sys.picture.mimeTypes', '.jpg,.gif,.png,.bmp,.jpeg,.zip,.7z', 'Y', 'admin', '2020-04-03 16:47:55', 'admin', '2020-06-01 16:43:47', '单位MB');
INSERT INTO `sys_config` VALUES (13, '允许单个图片上传大小', 'sys.picture.fileSingleSizeLimit', '10', 'Y', 'admin', '2020-04-03 16:49:01', 'admin', '2020-04-03 16:50:30', 'MB');
INSERT INTO `sys_config` VALUES (14, '允许单次上传文件总大小', 'sys.picture.fileSizeLimit', '100', 'Y', 'admin', '2020-04-03 16:52:48', '', NULL, 'MB');
INSERT INTO `sys_config` VALUES (15, '图库缩略图小图宽度', 'sys.picture.smallWidth', '450', 'Y', 'admin', '2020-04-07 16:09:37', 'admin', '2020-04-07 16:10:32', '');
INSERT INTO `sys_config` VALUES (16, '图库缩略图小图高度', 'sys.picture.smallHeight', '300', 'Y', 'admin', '2020-04-07 16:10:27', '', NULL, NULL);
INSERT INTO `sys_config` VALUES (17, '图库缩略图中图宽度', 'sys.picture.middleWidth', '860', 'Y', 'admin', '2020-04-07 16:12:43', '', NULL, NULL);
INSERT INTO `sys_config` VALUES (18, '图库缩略图中图高度', 'sys.picture.middleHeight', '540', 'Y', 'admin', '2020-04-07 16:13:09', 'admin', '2020-04-07 16:14:08', '');
INSERT INTO `sys_config` VALUES (19, 'ffmpeg和ffprobe所在目录', 'sys.video.ffMpegPath', 'D:\\TRS\\win32\\ffmpeg\\', 'Y', 'admin', '2018-03-16 11:33:02', 'admin', '2020-09-08 16:08:09', 'ffmpeg用于转码和抽帧，ffprobe用于抽取元数据');
INSERT INTO `sys_config` VALUES (20, '允许上传的单个视频文件大小(MB)', 'sys.video.maxFileSize', '2000', 'Y', 'admin', '2020-03-19 17:23:28', 'admin', '2020-09-11 11:36:31', '单位MB');
INSERT INTO `sys_config` VALUES (21, '允许上传的视频文件后缀', 'sys.video.allowedExtensions', 'mpg,mp4,rmvb,rm,mpeg,avi,wmv,mov,flv', 'Y', 'admin', '2020-03-19 17:24:09', 'admin', '2020-03-25 09:21:18', '');
INSERT INTO `sys_config` VALUES (22, '上传文件分块的大小(MB)', 'sys.video.uploadChunkSize', '100', 'Y', 'admin', '2020-03-25 15:36:18', 'admin', '2020-09-11 11:36:22', '用于上传时将视频分为较小的片段上传，以支持大文件和断点续传,单位MB');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父部门id',
  `ancestors` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 200 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (100, 0, '0', '若依科技', 0, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-03-23 15:02:57', '', NULL);
INSERT INTO `sys_dept` VALUES (101, 100, '0,100', '深圳总公司', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-03-23 15:02:57', '', NULL);
INSERT INTO `sys_dept` VALUES (102, 100, '0,100', '长沙分公司', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-03-23 15:02:57', '', NULL);
INSERT INTO `sys_dept` VALUES (103, 101, '0,100,101', '研发部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-03-23 15:02:57', '', NULL);
INSERT INTO `sys_dept` VALUES (104, 101, '0,100,101', '市场部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-03-23 15:02:57', '', NULL);
INSERT INTO `sys_dept` VALUES (105, 101, '0,100,101', '测试部门', 3, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-03-23 15:02:57', '', NULL);
INSERT INTO `sys_dept` VALUES (106, 101, '0,100,101', '财务部门', 4, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-03-23 15:02:57', '', NULL);
INSERT INTO `sys_dept` VALUES (107, 101, '0,100,101', '运维部门', 5, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-03-23 15:02:57', '', NULL);
INSERT INTO `sys_dept` VALUES (108, 102, '0,100,102', '市场部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-03-23 15:02:57', '', NULL);
INSERT INTO `sys_dept` VALUES (109, 102, '0,100,102', '财务部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-03-23 15:02:57', '', NULL);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2022-03-23 15:03:02', '', NULL, '性别男');
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2022-03-23 15:03:02', '', NULL, '性别女');
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2022-03-23 15:03:02', '', NULL, '性别未知');
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2022-03-23 15:03:02', '', NULL, '显示菜单');
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2022-03-23 15:03:02', '', NULL, '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2022-03-23 15:03:02', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2022-03-23 15:03:02', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2022-03-23 15:03:02', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2022-03-23 15:03:02', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2022-03-23 15:03:02', '', NULL, '默认分组');
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2022-03-23 15:03:02', '', NULL, '系统分组');
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2022-03-23 15:03:02', '', NULL, '系统默认是');
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2022-03-23 15:03:02', '', NULL, '系统默认否');
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2022-03-23 15:03:02', '', NULL, '通知');
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2022-03-23 15:03:02', '', NULL, '公告');
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2022-03-23 15:03:02', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2022-03-23 15:03:02', '', NULL, '关闭状态');
INSERT INTO `sys_dict_data` VALUES (18, 99, '其他', '0', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2022-03-23 15:03:02', '', NULL, '其他操作');
INSERT INTO `sys_dict_data` VALUES (19, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2022-03-23 15:03:02', '', NULL, '新增操作');
INSERT INTO `sys_dict_data` VALUES (20, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2022-03-23 15:03:02', '', NULL, '修改操作');
INSERT INTO `sys_dict_data` VALUES (21, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-03-23 15:03:02', '', NULL, '删除操作');
INSERT INTO `sys_dict_data` VALUES (22, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2022-03-23 15:03:02', '', NULL, '授权操作');
INSERT INTO `sys_dict_data` VALUES (23, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-03-23 15:03:02', '', NULL, '导出操作');
INSERT INTO `sys_dict_data` VALUES (24, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-03-23 15:03:02', '', NULL, '导入操作');
INSERT INTO `sys_dict_data` VALUES (25, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-03-23 15:03:02', '', NULL, '强退操作');
INSERT INTO `sys_dict_data` VALUES (26, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-03-23 15:03:02', '', NULL, '生成操作');
INSERT INTO `sys_dict_data` VALUES (27, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-03-23 15:03:02', '', NULL, '清空操作');
INSERT INTO `sys_dict_data` VALUES (28, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2022-03-23 15:03:02', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (29, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2022-03-23 15:03:02', '', NULL, '停用状态');

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2022-03-23 15:03:02', '', NULL, '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2022-03-23 15:03:02', '', NULL, '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2022-03-23 15:03:02', '', NULL, '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2022-03-23 15:03:02', '', NULL, '任务状态列表');
INSERT INTO `sys_dict_type` VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', '2022-03-23 15:03:02', '', NULL, '任务分组列表');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2022-03-23 15:03:02', '', NULL, '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', '2022-03-23 15:03:02', '', NULL, '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2022-03-23 15:03:02', '', NULL, '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2022-03-23 15:03:02', '', NULL, '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2022-03-23 15:03:02', '', NULL, '登录状态列表');

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '定时任务调度表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2022-03-23 15:03:04', '', NULL, '');
INSERT INTO `sys_job` VALUES (2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2022-03-23 15:03:04', '', NULL, '');
INSERT INTO `sys_job` VALUES (3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2022-03-23 15:03:04', '', NULL, '');
INSERT INTO `sys_job` VALUES (100, '视频转码调度', 'DEFAULT', 'videoTask.process', '0/20 * * * * ?', '1', '1', '0', 'admin', '2022-03-24 09:49:28', 'admin', '2022-03-24 09:55:29', '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log`  (
  `job_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日志信息',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '异常信息',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 193 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '定时任务调度日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------
INSERT INTO `sys_job_log` VALUES (1, '视频转码调度', 'DEFAULT', 'VideoTask.process', '视频转码调度 总共耗时：86毫秒', '1', 'org.springframework.beans.factory.NoSuchBeanDefinitionException: No bean named \'VideoTask\' available\r\n	at org.springframework.beans.factory.support.DefaultListableBeanFactory.getBeanDefinition(DefaultListableBeanFactory.java:872)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.getMergedLocalBeanDefinition(AbstractBeanFactory.java:1344)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.doGetBean(AbstractBeanFactory.java:309)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:208)\r\n	at com.media.common.utils.spring.SpringUtils.getBean(SpringUtils.java:49)\r\n	at com.media.quartz.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:32)\r\n	at com.media.quartz.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:19)\r\n	at com.media.quartz.util.AbstractQuartzJob.execute(AbstractQuartzJob.java:44)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\n', '2022-03-24 09:49:35');
INSERT INTO `sys_job_log` VALUES (2, '视频转码调度', 'DEFAULT', 'VideoTask.process', '视频转码调度 总共耗时：0毫秒', '1', 'org.springframework.beans.factory.NoSuchBeanDefinitionException: No bean named \'VideoTask\' available\r\n	at org.springframework.beans.factory.support.DefaultListableBeanFactory.getBeanDefinition(DefaultListableBeanFactory.java:872)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.getMergedLocalBeanDefinition(AbstractBeanFactory.java:1344)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.doGetBean(AbstractBeanFactory.java:309)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:208)\r\n	at com.media.common.utils.spring.SpringUtils.getBean(SpringUtils.java:49)\r\n	at com.media.quartz.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:32)\r\n	at com.media.quartz.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:19)\r\n	at com.media.quartz.util.AbstractQuartzJob.execute(AbstractQuartzJob.java:44)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\n', '2022-03-24 09:49:40');
INSERT INTO `sys_job_log` VALUES (3, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：169毫秒', '0', '', '2022-03-24 09:55:29');
INSERT INTO `sys_job_log` VALUES (4, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：21毫秒', '0', '', '2022-03-24 09:55:29');
INSERT INTO `sys_job_log` VALUES (5, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：6毫秒', '0', '', '2022-03-24 09:55:29');
INSERT INTO `sys_job_log` VALUES (6, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：5毫秒', '0', '', '2022-03-24 09:55:29');
INSERT INTO `sys_job_log` VALUES (7, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：4毫秒', '0', '', '2022-03-24 09:55:29');
INSERT INTO `sys_job_log` VALUES (8, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：5毫秒', '0', '', '2022-03-24 09:55:29');
INSERT INTO `sys_job_log` VALUES (9, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：6毫秒', '0', '', '2022-03-24 09:55:30');
INSERT INTO `sys_job_log` VALUES (10, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：42毫秒', '0', '', '2022-03-24 09:55:32');
INSERT INTO `sys_job_log` VALUES (11, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：6毫秒', '0', '', '2022-03-24 09:55:40');
INSERT INTO `sys_job_log` VALUES (12, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：6毫秒', '0', '', '2022-03-24 09:56:00');
INSERT INTO `sys_job_log` VALUES (13, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 09:56:20');
INSERT INTO `sys_job_log` VALUES (14, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：2毫秒', '0', '', '2022-03-24 09:56:40');
INSERT INTO `sys_job_log` VALUES (15, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：2毫秒', '0', '', '2022-03-24 09:57:00');
INSERT INTO `sys_job_log` VALUES (16, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：5毫秒', '0', '', '2022-03-24 09:57:20');
INSERT INTO `sys_job_log` VALUES (17, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：4毫秒', '0', '', '2022-03-24 09:57:40');
INSERT INTO `sys_job_log` VALUES (18, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：32毫秒', '0', '', '2022-03-24 09:58:03');
INSERT INTO `sys_job_log` VALUES (19, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：6毫秒', '0', '', '2022-03-24 09:58:20');
INSERT INTO `sys_job_log` VALUES (20, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 09:58:40');
INSERT INTO `sys_job_log` VALUES (21, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：5毫秒', '0', '', '2022-03-24 09:59:00');
INSERT INTO `sys_job_log` VALUES (22, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：5毫秒', '0', '', '2022-03-24 09:59:20');
INSERT INTO `sys_job_log` VALUES (23, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：4毫秒', '0', '', '2022-03-24 09:59:40');
INSERT INTO `sys_job_log` VALUES (24, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：16毫秒', '0', '', '2022-03-24 10:00:00');
INSERT INTO `sys_job_log` VALUES (25, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:00:20');
INSERT INTO `sys_job_log` VALUES (26, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：4毫秒', '0', '', '2022-03-24 10:00:40');
INSERT INTO `sys_job_log` VALUES (27, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：8毫秒', '0', '', '2022-03-24 10:01:00');
INSERT INTO `sys_job_log` VALUES (28, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：2毫秒', '0', '', '2022-03-24 10:01:20');
INSERT INTO `sys_job_log` VALUES (29, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：2毫秒', '0', '', '2022-03-24 10:01:40');
INSERT INTO `sys_job_log` VALUES (30, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:02:00');
INSERT INTO `sys_job_log` VALUES (31, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：2毫秒', '0', '', '2022-03-24 10:02:20');
INSERT INTO `sys_job_log` VALUES (32, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:02:40');
INSERT INTO `sys_job_log` VALUES (33, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：4毫秒', '0', '', '2022-03-24 10:03:00');
INSERT INTO `sys_job_log` VALUES (34, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:03:20');
INSERT INTO `sys_job_log` VALUES (35, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:03:40');
INSERT INTO `sys_job_log` VALUES (36, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：4毫秒', '0', '', '2022-03-24 10:04:00');
INSERT INTO `sys_job_log` VALUES (37, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:04:20');
INSERT INTO `sys_job_log` VALUES (38, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：5毫秒', '0', '', '2022-03-24 10:04:40');
INSERT INTO `sys_job_log` VALUES (39, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：7毫秒', '0', '', '2022-03-24 10:05:00');
INSERT INTO `sys_job_log` VALUES (40, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:05:20');
INSERT INTO `sys_job_log` VALUES (41, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：2毫秒', '0', '', '2022-03-24 10:05:40');
INSERT INTO `sys_job_log` VALUES (42, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:06:00');
INSERT INTO `sys_job_log` VALUES (43, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：5毫秒', '0', '', '2022-03-24 10:06:20');
INSERT INTO `sys_job_log` VALUES (44, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:06:40');
INSERT INTO `sys_job_log` VALUES (45, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：2毫秒', '0', '', '2022-03-24 10:07:00');
INSERT INTO `sys_job_log` VALUES (46, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:07:20');
INSERT INTO `sys_job_log` VALUES (47, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：4毫秒', '0', '', '2022-03-24 10:07:40');
INSERT INTO `sys_job_log` VALUES (48, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：4毫秒', '0', '', '2022-03-24 10:08:00');
INSERT INTO `sys_job_log` VALUES (49, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：4毫秒', '0', '', '2022-03-24 10:08:20');
INSERT INTO `sys_job_log` VALUES (50, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:08:40');
INSERT INTO `sys_job_log` VALUES (51, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：146毫秒', '0', '', '2022-03-24 10:09:20');
INSERT INTO `sys_job_log` VALUES (52, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：4毫秒', '0', '', '2022-03-24 10:09:40');
INSERT INTO `sys_job_log` VALUES (53, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：5毫秒', '0', '', '2022-03-24 10:10:00');
INSERT INTO `sys_job_log` VALUES (54, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：36毫秒', '0', '', '2022-03-24 10:10:20');
INSERT INTO `sys_job_log` VALUES (55, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：5毫秒', '0', '', '2022-03-24 10:10:40');
INSERT INTO `sys_job_log` VALUES (56, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：4毫秒', '0', '', '2022-03-24 10:11:00');
INSERT INTO `sys_job_log` VALUES (57, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:11:20');
INSERT INTO `sys_job_log` VALUES (58, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：5毫秒', '0', '', '2022-03-24 10:11:40');
INSERT INTO `sys_job_log` VALUES (59, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:12:00');
INSERT INTO `sys_job_log` VALUES (60, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：101毫秒', '0', '', '2022-03-24 10:12:20');
INSERT INTO `sys_job_log` VALUES (61, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：2毫秒', '0', '', '2022-03-24 10:12:40');
INSERT INTO `sys_job_log` VALUES (62, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：4毫秒', '0', '', '2022-03-24 10:13:00');
INSERT INTO `sys_job_log` VALUES (63, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:13:20');
INSERT INTO `sys_job_log` VALUES (64, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：6毫秒', '0', '', '2022-03-24 10:13:40');
INSERT INTO `sys_job_log` VALUES (65, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:14:00');
INSERT INTO `sys_job_log` VALUES (66, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:14:20');
INSERT INTO `sys_job_log` VALUES (67, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:14:40');
INSERT INTO `sys_job_log` VALUES (68, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：6毫秒', '0', '', '2022-03-24 10:15:00');
INSERT INTO `sys_job_log` VALUES (69, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：4毫秒', '0', '', '2022-03-24 10:15:20');
INSERT INTO `sys_job_log` VALUES (70, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：5毫秒', '0', '', '2022-03-24 10:15:40');
INSERT INTO `sys_job_log` VALUES (71, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：6毫秒', '0', '', '2022-03-24 10:16:00');
INSERT INTO `sys_job_log` VALUES (72, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：30毫秒', '0', '', '2022-03-24 10:17:01');
INSERT INTO `sys_job_log` VALUES (73, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：5毫秒', '0', '', '2022-03-24 10:17:20');
INSERT INTO `sys_job_log` VALUES (74, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：4毫秒', '0', '', '2022-03-24 10:17:40');
INSERT INTO `sys_job_log` VALUES (75, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:18:00');
INSERT INTO `sys_job_log` VALUES (76, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:18:20');
INSERT INTO `sys_job_log` VALUES (77, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：7毫秒', '0', '', '2022-03-24 10:18:40');
INSERT INTO `sys_job_log` VALUES (78, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:19:00');
INSERT INTO `sys_job_log` VALUES (79, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：5毫秒', '0', '', '2022-03-24 10:19:20');
INSERT INTO `sys_job_log` VALUES (80, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：7毫秒', '0', '', '2022-03-24 10:19:40');
INSERT INTO `sys_job_log` VALUES (81, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:20:00');
INSERT INTO `sys_job_log` VALUES (82, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：7毫秒', '0', '', '2022-03-24 10:20:20');
INSERT INTO `sys_job_log` VALUES (83, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：2毫秒', '0', '', '2022-03-24 10:20:40');
INSERT INTO `sys_job_log` VALUES (84, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：5毫秒', '0', '', '2022-03-24 10:21:00');
INSERT INTO `sys_job_log` VALUES (85, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：6毫秒', '0', '', '2022-03-24 10:21:20');
INSERT INTO `sys_job_log` VALUES (86, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:21:40');
INSERT INTO `sys_job_log` VALUES (87, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：4毫秒', '0', '', '2022-03-24 10:22:00');
INSERT INTO `sys_job_log` VALUES (88, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：33毫秒', '0', '', '2022-03-24 10:24:22');
INSERT INTO `sys_job_log` VALUES (89, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:24:40');
INSERT INTO `sys_job_log` VALUES (90, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：4毫秒', '0', '', '2022-03-24 10:25:00');
INSERT INTO `sys_job_log` VALUES (91, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：5毫秒', '0', '', '2022-03-24 10:25:20');
INSERT INTO `sys_job_log` VALUES (92, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：2毫秒', '0', '', '2022-03-24 10:25:40');
INSERT INTO `sys_job_log` VALUES (93, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：43毫秒', '0', '', '2022-03-24 10:28:20');
INSERT INTO `sys_job_log` VALUES (94, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：9毫秒', '0', '', '2022-03-24 10:28:40');
INSERT INTO `sys_job_log` VALUES (95, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：6毫秒', '0', '', '2022-03-24 10:29:00');
INSERT INTO `sys_job_log` VALUES (96, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：6毫秒', '0', '', '2022-03-24 10:29:20');
INSERT INTO `sys_job_log` VALUES (97, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：6毫秒', '0', '', '2022-03-24 10:29:40');
INSERT INTO `sys_job_log` VALUES (98, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：7毫秒', '0', '', '2022-03-24 10:30:00');
INSERT INTO `sys_job_log` VALUES (99, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：6毫秒', '0', '', '2022-03-24 10:30:20');
INSERT INTO `sys_job_log` VALUES (100, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：6毫秒', '0', '', '2022-03-24 10:30:40');
INSERT INTO `sys_job_log` VALUES (101, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:31:00');
INSERT INTO `sys_job_log` VALUES (102, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：30毫秒', '0', '', '2022-03-24 10:31:25');
INSERT INTO `sys_job_log` VALUES (103, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：6毫秒', '0', '', '2022-03-24 10:31:40');
INSERT INTO `sys_job_log` VALUES (104, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：4毫秒', '0', '', '2022-03-24 10:32:00');
INSERT INTO `sys_job_log` VALUES (105, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:32:20');
INSERT INTO `sys_job_log` VALUES (106, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:32:40');
INSERT INTO `sys_job_log` VALUES (107, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：4毫秒', '0', '', '2022-03-24 10:33:00');
INSERT INTO `sys_job_log` VALUES (108, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:33:20');
INSERT INTO `sys_job_log` VALUES (109, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:33:40');
INSERT INTO `sys_job_log` VALUES (110, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：4毫秒', '0', '', '2022-03-24 10:34:00');
INSERT INTO `sys_job_log` VALUES (111, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:34:20');
INSERT INTO `sys_job_log` VALUES (112, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：4毫秒', '0', '', '2022-03-24 10:34:40');
INSERT INTO `sys_job_log` VALUES (113, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：4毫秒', '0', '', '2022-03-24 10:35:00');
INSERT INTO `sys_job_log` VALUES (114, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:35:20');
INSERT INTO `sys_job_log` VALUES (115, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：2毫秒', '0', '', '2022-03-24 10:35:40');
INSERT INTO `sys_job_log` VALUES (116, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：5毫秒', '0', '', '2022-03-24 10:36:00');
INSERT INTO `sys_job_log` VALUES (117, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：2毫秒', '0', '', '2022-03-24 10:36:20');
INSERT INTO `sys_job_log` VALUES (118, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:36:40');
INSERT INTO `sys_job_log` VALUES (119, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：4毫秒', '0', '', '2022-03-24 10:37:00');
INSERT INTO `sys_job_log` VALUES (120, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:37:20');
INSERT INTO `sys_job_log` VALUES (121, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:37:40');
INSERT INTO `sys_job_log` VALUES (122, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:38:00');
INSERT INTO `sys_job_log` VALUES (123, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:38:20');
INSERT INTO `sys_job_log` VALUES (124, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：2毫秒', '0', '', '2022-03-24 10:38:40');
INSERT INTO `sys_job_log` VALUES (125, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:39:00');
INSERT INTO `sys_job_log` VALUES (126, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：2毫秒', '0', '', '2022-03-24 10:39:20');
INSERT INTO `sys_job_log` VALUES (127, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:39:40');
INSERT INTO `sys_job_log` VALUES (128, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：5毫秒', '0', '', '2022-03-24 10:40:00');
INSERT INTO `sys_job_log` VALUES (129, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：2毫秒', '0', '', '2022-03-24 10:40:20');
INSERT INTO `sys_job_log` VALUES (130, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:40:40');
INSERT INTO `sys_job_log` VALUES (131, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:41:00');
INSERT INTO `sys_job_log` VALUES (132, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：2毫秒', '0', '', '2022-03-24 10:41:20');
INSERT INTO `sys_job_log` VALUES (133, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:41:40');
INSERT INTO `sys_job_log` VALUES (134, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:42:00');
INSERT INTO `sys_job_log` VALUES (135, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:42:20');
INSERT INTO `sys_job_log` VALUES (136, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：4毫秒', '0', '', '2022-03-24 10:42:40');
INSERT INTO `sys_job_log` VALUES (137, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：4毫秒', '0', '', '2022-03-24 10:43:00');
INSERT INTO `sys_job_log` VALUES (138, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：2毫秒', '0', '', '2022-03-24 10:43:20');
INSERT INTO `sys_job_log` VALUES (139, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:43:40');
INSERT INTO `sys_job_log` VALUES (140, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:44:00');
INSERT INTO `sys_job_log` VALUES (141, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:44:20');
INSERT INTO `sys_job_log` VALUES (142, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：2毫秒', '0', '', '2022-03-24 10:44:40');
INSERT INTO `sys_job_log` VALUES (143, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:45:00');
INSERT INTO `sys_job_log` VALUES (144, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：1毫秒', '0', '', '2022-03-24 10:45:20');
INSERT INTO `sys_job_log` VALUES (145, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:45:40');
INSERT INTO `sys_job_log` VALUES (146, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：4毫秒', '0', '', '2022-03-24 10:46:00');
INSERT INTO `sys_job_log` VALUES (147, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：2毫秒', '0', '', '2022-03-24 10:46:20');
INSERT INTO `sys_job_log` VALUES (148, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：2毫秒', '0', '', '2022-03-24 10:46:40');
INSERT INTO `sys_job_log` VALUES (149, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:47:00');
INSERT INTO `sys_job_log` VALUES (150, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：2毫秒', '0', '', '2022-03-24 10:47:20');
INSERT INTO `sys_job_log` VALUES (151, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：2毫秒', '0', '', '2022-03-24 10:47:40');
INSERT INTO `sys_job_log` VALUES (152, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:48:00');
INSERT INTO `sys_job_log` VALUES (153, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:48:20');
INSERT INTO `sys_job_log` VALUES (154, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:48:40');
INSERT INTO `sys_job_log` VALUES (155, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：4毫秒', '0', '', '2022-03-24 10:49:00');
INSERT INTO `sys_job_log` VALUES (156, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:49:20');
INSERT INTO `sys_job_log` VALUES (157, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：2毫秒', '0', '', '2022-03-24 10:49:40');
INSERT INTO `sys_job_log` VALUES (158, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:50:00');
INSERT INTO `sys_job_log` VALUES (159, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：4毫秒', '0', '', '2022-03-24 10:50:20');
INSERT INTO `sys_job_log` VALUES (160, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：2毫秒', '0', '', '2022-03-24 10:50:40');
INSERT INTO `sys_job_log` VALUES (161, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：11毫秒', '0', '', '2022-03-24 10:51:00');
INSERT INTO `sys_job_log` VALUES (162, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：2毫秒', '0', '', '2022-03-24 10:51:20');
INSERT INTO `sys_job_log` VALUES (163, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：2毫秒', '0', '', '2022-03-24 10:51:40');
INSERT INTO `sys_job_log` VALUES (164, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:52:00');
INSERT INTO `sys_job_log` VALUES (165, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：2毫秒', '0', '', '2022-03-24 10:52:20');
INSERT INTO `sys_job_log` VALUES (166, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:52:40');
INSERT INTO `sys_job_log` VALUES (167, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：2毫秒', '0', '', '2022-03-24 10:53:00');
INSERT INTO `sys_job_log` VALUES (168, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：2毫秒', '0', '', '2022-03-24 10:53:20');
INSERT INTO `sys_job_log` VALUES (169, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：2毫秒', '0', '', '2022-03-24 10:53:40');
INSERT INTO `sys_job_log` VALUES (170, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：4毫秒', '0', '', '2022-03-24 10:54:00');
INSERT INTO `sys_job_log` VALUES (171, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：2毫秒', '0', '', '2022-03-24 10:54:20');
INSERT INTO `sys_job_log` VALUES (172, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:54:40');
INSERT INTO `sys_job_log` VALUES (173, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:55:00');
INSERT INTO `sys_job_log` VALUES (174, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：8毫秒', '0', '', '2022-03-24 10:55:20');
INSERT INTO `sys_job_log` VALUES (175, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：4毫秒', '0', '', '2022-03-24 10:55:41');
INSERT INTO `sys_job_log` VALUES (176, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：4毫秒', '0', '', '2022-03-24 10:56:00');
INSERT INTO `sys_job_log` VALUES (177, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：4毫秒', '0', '', '2022-03-24 10:56:20');
INSERT INTO `sys_job_log` VALUES (178, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：4毫秒', '0', '', '2022-03-24 10:56:40');
INSERT INTO `sys_job_log` VALUES (179, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:57:00');
INSERT INTO `sys_job_log` VALUES (180, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：4毫秒', '0', '', '2022-03-24 10:57:20');
INSERT INTO `sys_job_log` VALUES (181, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:57:40');
INSERT INTO `sys_job_log` VALUES (182, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:58:00');
INSERT INTO `sys_job_log` VALUES (183, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：2毫秒', '0', '', '2022-03-24 10:58:20');
INSERT INTO `sys_job_log` VALUES (184, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：2毫秒', '0', '', '2022-03-24 10:58:40');
INSERT INTO `sys_job_log` VALUES (185, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 10:59:00');
INSERT INTO `sys_job_log` VALUES (186, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：2毫秒', '0', '', '2022-03-24 10:59:20');
INSERT INTO `sys_job_log` VALUES (187, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：2毫秒', '0', '', '2022-03-24 10:59:40');
INSERT INTO `sys_job_log` VALUES (188, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：2毫秒', '0', '', '2022-03-24 11:00:00');
INSERT INTO `sys_job_log` VALUES (189, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：2毫秒', '0', '', '2022-03-24 11:00:20');
INSERT INTO `sys_job_log` VALUES (190, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：2毫秒', '0', '', '2022-03-24 11:00:40');
INSERT INTO `sys_job_log` VALUES (191, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：3毫秒', '0', '', '2022-03-24 11:01:00');
INSERT INTO `sys_job_log` VALUES (192, '视频转码调度', 'DEFAULT', 'videoTask.process', '视频转码调度 总共耗时：2毫秒', '0', '', '2022-03-24 11:01:20');

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor`  (
  `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `login_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录账号',
  `ipaddr` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作系统',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '提示消息',
  `login_time` datetime(0) NULL DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 126 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统访问记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------
INSERT INTO `sys_logininfor` VALUES (100, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-23 15:44:24');
INSERT INTO `sys_logininfor` VALUES (101, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-23 16:47:06');
INSERT INTO `sys_logininfor` VALUES (102, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-23 16:51:11');
INSERT INTO `sys_logininfor` VALUES (103, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-23 16:54:38');
INSERT INTO `sys_logininfor` VALUES (104, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-23 16:59:58');
INSERT INTO `sys_logininfor` VALUES (105, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-23 17:04:37');
INSERT INTO `sys_logininfor` VALUES (106, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-24 09:02:06');
INSERT INTO `sys_logininfor` VALUES (107, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2022-03-24 09:09:40');
INSERT INTO `sys_logininfor` VALUES (108, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-24 09:09:42');
INSERT INTO `sys_logininfor` VALUES (109, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-24 09:13:11');
INSERT INTO `sys_logininfor` VALUES (110, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-24 09:14:38');
INSERT INTO `sys_logininfor` VALUES (111, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-24 09:18:59');
INSERT INTO `sys_logininfor` VALUES (112, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-24 09:21:48');
INSERT INTO `sys_logininfor` VALUES (113, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2022-03-24 09:23:49');
INSERT INTO `sys_logininfor` VALUES (114, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-24 09:23:51');
INSERT INTO `sys_logininfor` VALUES (115, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-24 09:30:43');
INSERT INTO `sys_logininfor` VALUES (116, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-24 09:35:13');
INSERT INTO `sys_logininfor` VALUES (117, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-24 09:36:56');
INSERT INTO `sys_logininfor` VALUES (118, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-24 09:38:52');
INSERT INTO `sys_logininfor` VALUES (119, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-24 09:55:16');
INSERT INTO `sys_logininfor` VALUES (120, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-24 09:59:24');
INSERT INTO `sys_logininfor` VALUES (121, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-24 10:10:24');
INSERT INTO `sys_logininfor` VALUES (122, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-24 10:17:26');
INSERT INTO `sys_logininfor` VALUES (123, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-24 10:24:41');
INSERT INTO `sys_logininfor` VALUES (124, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-24 10:28:40');
INSERT INTO `sys_logininfor` VALUES (125, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-24 10:46:30');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父菜单ID',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '#' COMMENT '请求地址',
  `target` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '打开方式（menuItem页签 menuBlank新窗口）',
  `menu_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `is_refresh` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '是否刷新（0刷新 1不刷新）',
  `perms` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2070 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, 10, '#', 'menuItem', 'M', '0', '1', '', 'fa fa-gear', 'admin', '2022-03-23 15:02:59', 'admin', '2022-03-24 09:15:12', '系统管理目录');
INSERT INTO `sys_menu` VALUES (2, '系统监控', 0, 11, '#', 'menuItem', 'M', '0', '1', '', 'fa fa-video-camera', 'admin', '2022-03-23 15:02:59', 'admin', '2022-03-24 09:15:19', '系统监控目录');
INSERT INTO `sys_menu` VALUES (3, '系统工具', 0, 12, '#', 'menuItem', 'M', '0', '1', '', 'fa fa-bars', 'admin', '2022-03-23 15:02:59', 'admin', '2022-03-24 09:15:23', '系统工具目录');
INSERT INTO `sys_menu` VALUES (4, '若依官网', 0, 13, 'http://ruoyi.vip', 'menuBlank', 'C', '1', '1', '', 'fa fa-location-arrow', 'admin', '2022-03-23 15:02:59', 'admin', '2022-03-24 09:15:43', '若依官网地址');
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1, 1, '/system/user', '', 'C', '0', '1', 'system:user:view', 'fa fa-user-o', 'admin', '2022-03-23 15:02:59', '', NULL, '用户管理菜单');
INSERT INTO `sys_menu` VALUES (101, '角色管理', 1, 2, '/system/role', '', 'C', '0', '1', 'system:role:view', 'fa fa-user-secret', 'admin', '2022-03-23 15:02:59', '', NULL, '角色管理菜单');
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 1, 3, '/system/menu', '', 'C', '0', '1', 'system:menu:view', 'fa fa-th-list', 'admin', '2022-03-23 15:02:59', '', NULL, '菜单管理菜单');
INSERT INTO `sys_menu` VALUES (103, '部门管理', 1, 4, '/system/dept', '', 'C', '0', '1', 'system:dept:view', 'fa fa-outdent', 'admin', '2022-03-23 15:02:59', '', NULL, '部门管理菜单');
INSERT INTO `sys_menu` VALUES (104, '岗位管理', 1, 5, '/system/post', '', 'C', '0', '1', 'system:post:view', 'fa fa-address-card-o', 'admin', '2022-03-23 15:02:59', '', NULL, '岗位管理菜单');
INSERT INTO `sys_menu` VALUES (105, '字典管理', 1, 6, '/system/dict', '', 'C', '0', '1', 'system:dict:view', 'fa fa-bookmark-o', 'admin', '2022-03-23 15:02:59', '', NULL, '字典管理菜单');
INSERT INTO `sys_menu` VALUES (106, '参数设置', 1, 7, '/system/config', '', 'C', '0', '1', 'system:config:view', 'fa fa-sun-o', 'admin', '2022-03-23 15:02:59', '', NULL, '参数设置菜单');
INSERT INTO `sys_menu` VALUES (107, '通知公告', 1, 8, '/system/notice', '', 'C', '0', '1', 'system:notice:view', 'fa fa-bullhorn', 'admin', '2022-03-23 15:02:59', '', NULL, '通知公告菜单');
INSERT INTO `sys_menu` VALUES (108, '日志管理', 1, 9, '#', '', 'M', '0', '1', '', 'fa fa-pencil-square-o', 'admin', '2022-03-23 15:02:59', '', NULL, '日志管理菜单');
INSERT INTO `sys_menu` VALUES (109, '在线用户', 2, 1, '/monitor/online', '', 'C', '0', '1', 'monitor:online:view', 'fa fa-user-circle', 'admin', '2022-03-23 15:02:59', '', NULL, '在线用户菜单');
INSERT INTO `sys_menu` VALUES (110, '定时任务', 2, 2, '/monitor/job', '', 'C', '0', '1', 'monitor:job:view', 'fa fa-tasks', 'admin', '2022-03-23 15:02:59', '', NULL, '定时任务菜单');
INSERT INTO `sys_menu` VALUES (111, '数据监控', 2, 3, '/monitor/data', '', 'C', '0', '1', 'monitor:data:view', 'fa fa-bug', 'admin', '2022-03-23 15:02:59', '', NULL, '数据监控菜单');
INSERT INTO `sys_menu` VALUES (112, '服务监控', 2, 4, '/monitor/server', '', 'C', '0', '1', 'monitor:server:view', 'fa fa-server', 'admin', '2022-03-23 15:02:59', '', NULL, '服务监控菜单');
INSERT INTO `sys_menu` VALUES (113, '缓存监控', 2, 5, '/monitor/cache', '', 'C', '0', '1', 'monitor:cache:view', 'fa fa-cube', 'admin', '2022-03-23 15:02:59', '', NULL, '缓存监控菜单');
INSERT INTO `sys_menu` VALUES (114, '表单构建', 3, 1, '/tool/build', '', 'C', '0', '1', 'tool:build:view', 'fa fa-wpforms', 'admin', '2022-03-23 15:02:59', '', NULL, '表单构建菜单');
INSERT INTO `sys_menu` VALUES (115, '代码生成', 3, 2, '/tool/gen', '', 'C', '0', '1', 'tool:gen:view', 'fa fa-code', 'admin', '2022-03-23 15:02:59', '', NULL, '代码生成菜单');
INSERT INTO `sys_menu` VALUES (116, '系统接口', 3, 3, '/tool/swagger', '', 'C', '0', '1', 'tool:swagger:view', 'fa fa-gg', 'admin', '2022-03-23 15:02:59', '', NULL, '系统接口菜单');
INSERT INTO `sys_menu` VALUES (500, '操作日志', 108, 1, '/monitor/operlog', '', 'C', '0', '1', 'monitor:operlog:view', 'fa fa-address-book', 'admin', '2022-03-23 15:02:59', '', NULL, '操作日志菜单');
INSERT INTO `sys_menu` VALUES (501, '登录日志', 108, 2, '/monitor/logininfor', '', 'C', '0', '1', 'monitor:logininfor:view', 'fa fa-file-image-o', 'admin', '2022-03-23 15:02:59', '', NULL, '登录日志菜单');
INSERT INTO `sys_menu` VALUES (1000, '用户查询', 100, 1, '#', '', 'F', '0', '1', 'system:user:list', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1001, '用户新增', 100, 2, '#', '', 'F', '0', '1', 'system:user:add', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1002, '用户修改', 100, 3, '#', '', 'F', '0', '1', 'system:user:edit', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1003, '用户删除', 100, 4, '#', '', 'F', '0', '1', 'system:user:remove', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1004, '用户导出', 100, 5, '#', '', 'F', '0', '1', 'system:user:export', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1005, '用户导入', 100, 6, '#', '', 'F', '0', '1', 'system:user:import', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1006, '重置密码', 100, 7, '#', '', 'F', '0', '1', 'system:user:resetPwd', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1007, '角色查询', 101, 1, '#', '', 'F', '0', '1', 'system:role:list', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1008, '角色新增', 101, 2, '#', '', 'F', '0', '1', 'system:role:add', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1009, '角色修改', 101, 3, '#', '', 'F', '0', '1', 'system:role:edit', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1010, '角色删除', 101, 4, '#', '', 'F', '0', '1', 'system:role:remove', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1011, '角色导出', 101, 5, '#', '', 'F', '0', '1', 'system:role:export', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1012, '菜单查询', 102, 1, '#', '', 'F', '0', '1', 'system:menu:list', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1013, '菜单新增', 102, 2, '#', '', 'F', '0', '1', 'system:menu:add', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1014, '菜单修改', 102, 3, '#', '', 'F', '0', '1', 'system:menu:edit', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1015, '菜单删除', 102, 4, '#', '', 'F', '0', '1', 'system:menu:remove', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1016, '部门查询', 103, 1, '#', '', 'F', '0', '1', 'system:dept:list', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1017, '部门新增', 103, 2, '#', '', 'F', '0', '1', 'system:dept:add', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1018, '部门修改', 103, 3, '#', '', 'F', '0', '1', 'system:dept:edit', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1019, '部门删除', 103, 4, '#', '', 'F', '0', '1', 'system:dept:remove', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1020, '岗位查询', 104, 1, '#', '', 'F', '0', '1', 'system:post:list', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1021, '岗位新增', 104, 2, '#', '', 'F', '0', '1', 'system:post:add', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1022, '岗位修改', 104, 3, '#', '', 'F', '0', '1', 'system:post:edit', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1023, '岗位删除', 104, 4, '#', '', 'F', '0', '1', 'system:post:remove', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1024, '岗位导出', 104, 5, '#', '', 'F', '0', '1', 'system:post:export', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1025, '字典查询', 105, 1, '#', '', 'F', '0', '1', 'system:dict:list', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1026, '字典新增', 105, 2, '#', '', 'F', '0', '1', 'system:dict:add', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1027, '字典修改', 105, 3, '#', '', 'F', '0', '1', 'system:dict:edit', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1028, '字典删除', 105, 4, '#', '', 'F', '0', '1', 'system:dict:remove', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1029, '字典导出', 105, 5, '#', '', 'F', '0', '1', 'system:dict:export', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1030, '参数查询', 106, 1, '#', '', 'F', '0', '1', 'system:config:list', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1031, '参数新增', 106, 2, '#', '', 'F', '0', '1', 'system:config:add', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1032, '参数修改', 106, 3, '#', '', 'F', '0', '1', 'system:config:edit', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1033, '参数删除', 106, 4, '#', '', 'F', '0', '1', 'system:config:remove', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1034, '参数导出', 106, 5, '#', '', 'F', '0', '1', 'system:config:export', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1035, '公告查询', 107, 1, '#', '', 'F', '0', '1', 'system:notice:list', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1036, '公告新增', 107, 2, '#', '', 'F', '0', '1', 'system:notice:add', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1037, '公告修改', 107, 3, '#', '', 'F', '0', '1', 'system:notice:edit', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1038, '公告删除', 107, 4, '#', '', 'F', '0', '1', 'system:notice:remove', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1039, '操作查询', 500, 1, '#', '', 'F', '0', '1', 'monitor:operlog:list', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1040, '操作删除', 500, 2, '#', '', 'F', '0', '1', 'monitor:operlog:remove', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1041, '详细信息', 500, 3, '#', '', 'F', '0', '1', 'monitor:operlog:detail', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1042, '日志导出', 500, 4, '#', '', 'F', '0', '1', 'monitor:operlog:export', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1043, '登录查询', 501, 1, '#', '', 'F', '0', '1', 'monitor:logininfor:list', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1044, '登录删除', 501, 2, '#', '', 'F', '0', '1', 'monitor:logininfor:remove', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1045, '日志导出', 501, 3, '#', '', 'F', '0', '1', 'monitor:logininfor:export', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1046, '账户解锁', 501, 4, '#', '', 'F', '0', '1', 'monitor:logininfor:unlock', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1047, '在线查询', 109, 1, '#', '', 'F', '0', '1', 'monitor:online:list', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1048, '批量强退', 109, 2, '#', '', 'F', '0', '1', 'monitor:online:batchForceLogout', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1049, '单条强退', 109, 3, '#', '', 'F', '0', '1', 'monitor:online:forceLogout', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1050, '任务查询', 110, 1, '#', '', 'F', '0', '1', 'monitor:job:list', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1051, '任务新增', 110, 2, '#', '', 'F', '0', '1', 'monitor:job:add', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1052, '任务修改', 110, 3, '#', '', 'F', '0', '1', 'monitor:job:edit', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1053, '任务删除', 110, 4, '#', '', 'F', '0', '1', 'monitor:job:remove', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1054, '状态修改', 110, 5, '#', '', 'F', '0', '1', 'monitor:job:changeStatus', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1055, '任务详细', 110, 6, '#', '', 'F', '0', '1', 'monitor:job:detail', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1056, '任务导出', 110, 7, '#', '', 'F', '0', '1', 'monitor:job:export', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1057, '生成查询', 115, 1, '#', '', 'F', '0', '1', 'tool:gen:list', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1058, '生成修改', 115, 2, '#', '', 'F', '0', '1', 'tool:gen:edit', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1059, '生成删除', 115, 3, '#', '', 'F', '0', '1', 'tool:gen:remove', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1060, '预览代码', 115, 4, '#', '', 'F', '0', '1', 'tool:gen:preview', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1061, '生成代码', 115, 5, '#', '', 'F', '0', '1', 'tool:gen:code', '#', 'admin', '2022-03-23 15:02:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2004, '图片管理', 0, 1, '#', 'menuItem', 'M', '0', '1', NULL, 'fa fa-picture-o', 'admin', '2021-04-08 14:52:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2005, '视频管理', 0, 2, '#', 'menuItem', 'M', '0', '1', '', 'fa fa-video-camera', 'admin', '2021-04-08 14:53:13', 'admin', '2021-04-08 14:55:00', '');
INSERT INTO `sys_menu` VALUES (2006, '个人图库', 2004, 1, '/picture/group', 'menuItem', 'C', '0', '0', 'picture:group:view', 'fa fa-camera-retro', 'admin', '2021-04-08 14:55:32', 'admin', '2021-04-21 09:54:16', '');
INSERT INTO `sys_menu` VALUES (2007, '图片分类', 2004, 2, '/picture/category', 'menuItem', 'C', '0', '0', 'picture:category:view', 'fa fa-calendar', 'admin', '2021-04-08 14:56:38', 'admin', '2021-04-08 15:37:20', '');
INSERT INTO `sys_menu` VALUES (2009, '部门视频', 2005, 2, '/video/video/deptView', 'menuItem', 'C', '0', '0', 'video:video:deptView', 'fa fa-users', 'admin', '2021-04-08 14:57:44', 'admin', '2022-03-21 16:32:42', '');
INSERT INTO `sys_menu` VALUES (2011, '图片回收站', 2004, 3, '/picture/recycle', 'menuItem', 'C', '0', '0', 'picture:recycle:view', 'fa fa-trash', 'admin', '2021-04-08 14:58:53', 'admin', '2021-04-28 11:47:54', '');
INSERT INTO `sys_menu` VALUES (2012, '视频回收站', 2005, 7, '/video/recycle', 'menuItem', 'C', '0', '0', 'video:recycle:view', 'fa fa-shopping-basket', 'admin', '2021-04-08 14:59:24', 'admin', '2022-03-18 16:20:11', '');
INSERT INTO `sys_menu` VALUES (2013, '新增图片分类', 2007, 1, '#', 'menuItem', 'F', '0', '1', 'picture:category:add', '#', 'admin', '2021-04-21 09:54:56', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2014, '编辑图片分类', 2007, 2, '#', 'menuItem', 'F', '0', '1', 'picture:category:edit', '#', 'admin', '2021-04-21 09:55:19', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2015, '删除图片分类', 2007, 3, '#', 'menuItem', 'F', '0', '1', 'picture:category:remove', '#', 'admin', '2021-04-21 09:56:50', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2016, '新增图组信息', 2006, 1, '#', 'menuItem', 'F', '0', '1', 'picture:group:add', '#', 'admin', '2021-04-21 09:59:21', 'admin', '2021-04-21 10:00:43', '');
INSERT INTO `sys_menu` VALUES (2017, '编辑图组信息', 2006, 2, '#', 'menuItem', 'F', '0', '1', 'picture:group:edit', '#', 'admin', '2021-04-21 09:59:42', 'admin', '2021-04-21 10:00:53', '');
INSERT INTO `sys_menu` VALUES (2018, '删除图组信息', 2006, 3, '#', 'menuItem', 'F', '0', '1', 'picture:group:remove', '#', 'admin', '2021-04-21 09:59:58', 'admin', '2021-04-21 10:00:35', '');
INSERT INTO `sys_menu` VALUES (2019, '导出图组信息', 2006, 4, '#', 'menuItem', 'F', '0', '1', 'picture:group:export', '#', 'admin', '2021-04-21 10:00:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2026, '视频分类', 2005, 3, '/video/category', 'menuItem', 'C', '0', '1', 'video:category:view', 'fa fa-calendar', 'admin', '2021-12-17 15:59:21', 'admin', '2021-12-17 16:10:05', '视频分类信息菜单');
INSERT INTO `sys_menu` VALUES (2027, '视频分类信息查询', 2026, 1, '#', '', 'F', '0', '1', 'video:category:list', '#', 'admin', '2021-12-17 15:59:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2028, '视频分类信息新增', 2026, 2, '#', '', 'F', '0', '1', 'video:category:add', '#', 'admin', '2021-12-17 15:59:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2029, '视频分类信息修改', 2026, 3, '#', '', 'F', '0', '1', 'video:category:edit', '#', 'admin', '2021-12-17 15:59:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2030, '视频分类信息删除', 2026, 4, '#', '', 'F', '0', '1', 'video:category:remove', '#', 'admin', '2021-12-17 15:59:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2031, '视频分类信息导出', 2026, 5, '#', '', 'F', '0', '1', 'video:category:export', '#', 'admin', '2021-12-17 15:59:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2032, '个人视频', 2005, 1, '/video/video', 'menuItem', 'C', '0', '1', 'video:video:view', 'fa fa-file-movie-o', 'admin', '2022-03-11 14:30:50', 'admin', '2022-03-11 14:38:00', '视频信息菜单');
INSERT INTO `sys_menu` VALUES (2033, '视频信息查询', 2032, 1, '#', '', 'F', '0', '1', 'video:video:list', '#', 'admin', '2022-03-11 14:30:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2034, '视频信息新增', 2032, 2, '#', '', 'F', '0', '1', 'video:video:add', '#', 'admin', '2022-03-11 14:30:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2035, '视频信息修改', 2032, 3, '#', '', 'F', '0', '1', 'video:video:edit', '#', 'admin', '2022-03-11 14:30:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2036, '视频信息删除', 2032, 4, '#', '', 'F', '0', '1', 'video:video:remove', '#', 'admin', '2022-03-11 14:30:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2037, '视频信息导出', 2032, 5, '#', '', 'F', '0', '1', 'video:video:export', '#', 'admin', '2022-03-11 14:30:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2038, '视频转码配置', 2005, 4, '/video/config', 'menuItem', 'C', '0', '1', 'video:config:view', 'fa fa-wrench', 'admin', '2022-03-11 15:13:02', 'admin', '2022-03-11 15:31:02', '视频转码配置菜单');
INSERT INTO `sys_menu` VALUES (2039, '视频转码配置查询', 2038, 1, '#', '', 'F', '0', '1', 'video:config:list', '#', 'admin', '2022-03-11 15:13:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2040, '视频转码配置新增', 2038, 2, '#', '', 'F', '0', '1', 'video:config:add', '#', 'admin', '2022-03-11 15:13:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2041, '视频转码配置修改', 2038, 3, '#', '', 'F', '0', '1', 'video:config:edit', '#', 'admin', '2022-03-11 15:13:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2042, '视频转码配置删除', 2038, 4, '#', '', 'F', '0', '1', 'video:config:remove', '#', 'admin', '2022-03-11 15:13:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2043, '视频转码配置导出', 2038, 5, '#', '', 'F', '0', '1', 'video:config:export', '#', 'admin', '2022-03-11 15:13:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2044, '视频加工任务', 2005, 6, '/video/processjob', 'menuItem', 'C', '0', '1', 'video:processjob:view', 'fa fa-calendar', 'admin', '2022-03-15 16:44:27', 'admin', '2022-03-15 16:46:42', '视频加工任务菜单');
INSERT INTO `sys_menu` VALUES (2045, '视频加工任务查询', 2044, 1, '#', '', 'F', '0', '1', 'video:processjob:list', '#', 'admin', '2022-03-15 16:44:27', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2046, '视频加工任务新增', 2044, 2, '#', '', 'F', '0', '1', 'video:processjob:add', '#', 'admin', '2022-03-15 16:44:27', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2047, '视频加工任务修改', 2044, 3, '#', '', 'F', '0', '1', 'video:processjob:edit', '#', 'admin', '2022-03-15 16:44:27', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2048, '视频加工任务删除', 2044, 4, '#', '', 'F', '0', '1', 'video:processjob:remove', '#', 'admin', '2022-03-15 16:44:27', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2049, '视频加工任务导出', 2044, 5, '#', '', 'F', '0', '1', 'video:processjob:export', '#', 'admin', '2022-03-15 16:44:27', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2050, '源视频', 2005, 5, '/video/source', 'menuItem', 'C', '0', '1', 'video:source:view', 'fa fa-caret-square-o-right', 'admin', '2022-03-15 16:44:38', 'admin', '2022-03-15 16:46:11', '源视频菜单');
INSERT INTO `sys_menu` VALUES (2051, '源视频查询', 2050, 1, '#', '', 'F', '0', '1', 'video:source:list', '#', 'admin', '2022-03-15 16:44:38', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2052, '源视频新增', 2050, 2, '#', '', 'F', '0', '1', 'video:source:add', '#', 'admin', '2022-03-15 16:44:38', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2053, '源视频修改', 2050, 3, '#', '', 'F', '0', '1', 'video:source:edit', '#', 'admin', '2022-03-15 16:44:38', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2054, '源视频删除', 2050, 4, '#', '', 'F', '0', '1', 'video:source:remove', '#', 'admin', '2022-03-15 16:44:38', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2055, '源视频导出', 2050, 5, '#', '', 'F', '0', '1', 'video:source:export', '#', 'admin', '2022-03-15 16:44:38', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2056, '视频日志', 2005, 8, '/video/log', 'menuItem', 'M', '0', '1', 'video:log:view', 'fa fa-calendar-minus-o', 'admin', '2022-03-18 09:16:04', 'admin', '2022-03-18 13:56:11', '视频日志菜单');
INSERT INTO `sys_menu` VALUES (2062, '下载日志', 2056, 1, '/video/log/download', 'menuItem', 'C', '0', '1', 'video:downloadLog:list', 'fa fa-cloud-download', 'admin', '2022-03-18 13:58:26', 'admin', '2022-03-18 16:20:29', '');
INSERT INTO `sys_menu` VALUES (2063, '播放日志', 2056, 2, '/video/log/play', 'menuItem', 'C', '0', '1', 'video:playLog:list', 'fa fa-toggle-right', 'admin', '2022-03-18 13:59:15', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2064, '系统设置', 1, 10, '/system/setting', 'menuItem', 'C', '0', '1', 'system:setting:view', 'fa fa-cogs', 'admin', '2022-03-23 10:50:28', 'admin', '2022-03-23 10:53:38', '系统设置菜单');
INSERT INTO `sys_menu` VALUES (2065, '系统设置查询', 2064, 1, '#', '', 'F', '0', '1', 'system:setting:list', '#', 'admin', '2022-03-23 10:50:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2066, '系统设置新增', 2064, 2, '#', '', 'F', '0', '1', 'system:setting:add', '#', 'admin', '2022-03-23 10:50:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2067, '系统设置修改', 2064, 3, '#', '', 'F', '0', '1', 'system:setting:edit', '#', 'admin', '2022-03-23 10:50:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2068, '系统设置删除', 2064, 4, '#', '', 'F', '0', '1', 'system:setting:remove', '#', 'admin', '2022-03-23 10:50:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2069, '系统设置导出', 2064, 5, '#', '', 'F', '0', '1', 'system:setting:export', '#', 'admin', '2022-03-23 10:50:28', '', NULL, '');

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `notice_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公告标题',
  `notice_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公告内容',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '通知公告表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES (1, '温馨提醒：2018-07-01 若依新版本发布啦', '2', '新版本内容', '0', 'admin', '2022-03-23 15:03:04', '', NULL, '管理员');
INSERT INTO `sys_notice` VALUES (2, '维护通知：2018-07-01 若依系统凌晨维护', '1', '维护内容', '0', 'admin', '2022-03-23 15:03:04', '', NULL, '管理员');

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
  `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求方式',
  `operator_type` int(1) NULL DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '返回参数',
  `status` int(1) NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`oper_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 135 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '操作日志记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO `sys_oper_log` VALUES (100, '菜单管理', 2, 'com.media.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"1\"],\"parentId\":[\"0\"],\"menuType\":[\"M\"],\"menuName\":[\"系统管理\"],\"url\":[\"#\"],\"target\":[\"menuItem\"],\"perms\":[\"\"],\"orderNum\":[\"10\"],\"icon\":[\"fa fa-gear\"],\"visible\":[\"0\"],\"isRefresh\":[\"1\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-03-24 09:15:12');
INSERT INTO `sys_oper_log` VALUES (101, '菜单管理', 2, 'com.media.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2\"],\"parentId\":[\"0\"],\"menuType\":[\"M\"],\"menuName\":[\"系统监控\"],\"url\":[\"#\"],\"target\":[\"menuItem\"],\"perms\":[\"\"],\"orderNum\":[\"11\"],\"icon\":[\"fa fa-video-camera\"],\"visible\":[\"0\"],\"isRefresh\":[\"1\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-03-24 09:15:19');
INSERT INTO `sys_oper_log` VALUES (102, '菜单管理', 2, 'com.media.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"3\"],\"parentId\":[\"0\"],\"menuType\":[\"M\"],\"menuName\":[\"系统工具\"],\"url\":[\"#\"],\"target\":[\"menuItem\"],\"perms\":[\"\"],\"orderNum\":[\"12\"],\"icon\":[\"fa fa-bars\"],\"visible\":[\"0\"],\"isRefresh\":[\"1\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-03-24 09:15:24');
INSERT INTO `sys_oper_log` VALUES (103, '菜单管理', 2, 'com.media.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"4\"],\"parentId\":[\"0\"],\"menuType\":[\"C\"],\"menuName\":[\"若依官网\"],\"url\":[\"http://ruoyi.vip\"],\"target\":[\"menuBlank\"],\"perms\":[\"\"],\"orderNum\":[\"13\"],\"icon\":[\"fa fa-location-arrow\"],\"visible\":[\"0\"],\"isRefresh\":[\"1\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-03-24 09:15:32');
INSERT INTO `sys_oper_log` VALUES (104, '菜单管理', 2, 'com.media.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"4\"],\"parentId\":[\"0\"],\"menuType\":[\"C\"],\"menuName\":[\"若依官网\"],\"url\":[\"http://ruoyi.vip\"],\"target\":[\"menuBlank\"],\"perms\":[\"\"],\"orderNum\":[\"13\"],\"icon\":[\"fa fa-location-arrow\"],\"visible\":[\"1\"],\"isRefresh\":[\"1\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-03-24 09:15:44');
INSERT INTO `sys_oper_log` VALUES (105, '系统设置表', 2, 'com.media.web.controller.system.SysSettingController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/setting/edit', '127.0.0.1', '内网IP', '{\"settingId\":[\"1\"],\"name\":[\"媒资管理系统\"],\"fullName\":[\"媒资管理系统\"],\"company\":[\"\"],\"address\":[\"河北省石家庄市\"],\"telephone\":[\"\"],\"watermarkMode\":[\"0\"],\"watermarkAlpha\":[\"50\"],\"watermarkPosition\":[\"9\"],\"videologoMode\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-03-24 09:24:07');
INSERT INTO `sys_oper_log` VALUES (106, '系统设置表', 2, 'com.media.web.controller.system.SysSettingController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/setting/edit', '127.0.0.1', '内网IP', '{\"settingId\":[\"1\"],\"name\":[\"媒资管理系统\"],\"fullName\":[\"媒资管理系统\"],\"company\":[\"\"],\"address\":[\"河北省石家庄市\"],\"telephone\":[\"\"],\"watermarkMode\":[\"0\"],\"watermarkAlpha\":[\"50\"],\"watermarkPosition\":[\"9\"],\"videologoMode\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-03-24 09:24:10');
INSERT INTO `sys_oper_log` VALUES (107, '上传视频', 1, 'com.media.video.controller.MamVideoSourceController.fileUpload()', 'POST', 1, 'admin', '研发部门', '/video/source/fileUpload', '127.0.0.1', '内网IP', '{\"id\":[\"WU_FILE_0\"],\"name\":[\"demo.mp4\"],\"type\":[\"video/mp4\"],\"lastModifiedDate\":[\"Tue Mar 15 2022 09:18:41 GMT+0800 (中国标准时间)\"],\"size\":[\"3268585\"]}', '{\"path\":\"\",\"status\":1}', 0, NULL, '2022-03-24 09:24:26');
INSERT INTO `sys_oper_log` VALUES (108, '上传视频', 1, 'com.media.video.controller.MamVideoSourceController.fileUpload()', 'POST', 1, 'admin', '研发部门', '/video/source/fileUpload', '127.0.0.1', '内网IP', '{\"id\":[\"WU_FILE_0\"],\"name\":[\"demo.mp4\"],\"type\":[\"video/mp4\"],\"lastModifiedDate\":[\"Tue Mar 15 2022 09:18:41 GMT+0800 (中国标准时间)\"],\"size\":[\"3268585\"]}', '{\"path\":\"\",\"status\":1}', 0, NULL, '2022-03-24 09:39:01');
INSERT INTO `sys_oper_log` VALUES (109, '视频信息', 1, 'com.media.video.controller.MamVideoController.addSave()', 'POST', 1, 'admin', '研发部门', '/video/video//add', '127.0.0.1', '内网IP', '{\"sourceVideoIds\":[\"2\"],\"title\":[\"1111\"],\"keywords\":[\"1111\"],\"categoryId\":[\"2\"],\"name\":[\"学生活动\"],\"author\":[\"若依\"],\"shootingTime\":[\"2022-03-24\"],\"leader\":[\"0\"],\"remark\":[\"11111\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-03-24 09:39:12');
INSERT INTO `sys_oper_log` VALUES (110, '个人信息', 2, 'com.media.web.controller.system.SysProfileController.update()', 'POST', 1, 'admin', '研发部门', '/system/user/profile/update', '127.0.0.1', '内网IP', '{\"id\":[\"\"],\"userName\":[\"紫夜\"],\"phonenumber\":[\"15888888888\"],\"email\":[\"ry@163.com\"],\"sex\":[\"1\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-03-24 09:41:58');
INSERT INTO `sys_oper_log` VALUES (111, '定时任务', 1, 'com.media.quartz.controller.SysJobController.addSave()', 'POST', 1, 'admin', '研发部门', '/monitor/job/add', '127.0.0.1', '内网IP', '{\"createBy\":[\"admin\"],\"jobName\":[\"视频转码调度\"],\"jobGroup\":[\"DEFAULT\"],\"invokeTarget\":[\"com.media.video.task.VideoTask.process\"],\"cronExpression\":[\"0/20 * * * * ?\"],\"misfirePolicy\":[\"1\"],\"concurrent\":[\"1\"],\"status\":[\"0\"],\"remark\":[\"20秒监听调度\"]}', '{\"msg\":\"新增任务\'视频转码调度\'失败，目标字符串不在白名单内\",\"code\":500}', 0, NULL, '2022-03-24 09:48:14');
INSERT INTO `sys_oper_log` VALUES (112, '定时任务', 1, 'com.media.quartz.controller.SysJobController.addSave()', 'POST', 1, 'admin', '研发部门', '/monitor/job/add', '127.0.0.1', '内网IP', '{\"createBy\":[\"admin\"],\"jobName\":[\"视频转码调度\"],\"jobGroup\":[\"DEFAULT\"],\"invokeTarget\":[\"com.media.video.task.VideoTask.process\"],\"cronExpression\":[\"0/20 * * * * ?\"],\"misfirePolicy\":[\"1\"],\"concurrent\":[\"1\"],\"status\":[\"0\"],\"remark\":[\"20秒监听调度\"]}', '{\"msg\":\"新增任务\'视频转码调度\'失败，目标字符串不在白名单内\",\"code\":500}', 0, NULL, '2022-03-24 09:48:20');
INSERT INTO `sys_oper_log` VALUES (113, '定时任务', 1, 'com.media.quartz.controller.SysJobController.addSave()', 'POST', 1, 'admin', '研发部门', '/monitor/job/add', '127.0.0.1', '内网IP', '{\"createBy\":[\"admin\"],\"jobName\":[\"视频转码调度\"],\"jobGroup\":[\"DEFAULT\"],\"invokeTarget\":[\"com.media.video.task.VideoTask.process\"],\"cronExpression\":[\"0/20 * * * * ?\"],\"misfirePolicy\":[\"1\"],\"concurrent\":[\"1\"],\"status\":[\"0\"],\"remark\":[\"20秒监听调度\"]}', '{\"msg\":\"新增任务\'视频转码调度\'失败，目标字符串不在白名单内\",\"code\":500}', 0, NULL, '2022-03-24 09:48:35');
INSERT INTO `sys_oper_log` VALUES (114, '定时任务', 1, 'com.media.quartz.controller.SysJobController.addSave()', 'POST', 1, 'admin', '研发部门', '/monitor/job/add', '127.0.0.1', '内网IP', '{\"createBy\":[\"admin\"],\"jobName\":[\"视频转码调度\"],\"jobGroup\":[\"DEFAULT\"],\"invokeTarget\":[\"VideoTask.process\"],\"cronExpression\":[\"0/20 * * * * ?\"],\"misfirePolicy\":[\"1\"],\"concurrent\":[\"1\"],\"status\":[\"0\"],\"remark\":[\"\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-03-24 09:49:28');
INSERT INTO `sys_oper_log` VALUES (115, '定时任务', 2, 'com.media.quartz.controller.SysJobController.changeStatus()', 'POST', 1, 'admin', '研发部门', '/monitor/job/changeStatus', '127.0.0.1', '内网IP', '{\"jobId\":[\"100\"],\"jobGroup\":[\"DEFAULT\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-03-24 09:49:32');
INSERT INTO `sys_oper_log` VALUES (116, '定时任务', 2, 'com.media.quartz.controller.SysJobController.run()', 'POST', 1, 'admin', '研发部门', '/monitor/job/run', '127.0.0.1', '内网IP', '{\"jobId\":[\"100\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-03-24 09:49:35');
INSERT INTO `sys_oper_log` VALUES (117, '定时任务', 2, 'com.media.quartz.controller.SysJobController.changeStatus()', 'POST', 1, 'admin', '研发部门', '/monitor/job/changeStatus', '127.0.0.1', '内网IP', '{\"jobId\":[\"100\"],\"jobGroup\":[\"DEFAULT\"],\"status\":[\"1\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-03-24 09:49:54');
INSERT INTO `sys_oper_log` VALUES (118, '定时任务', 2, 'com.media.quartz.controller.SysJobController.editSave()', 'POST', 1, 'admin', '研发部门', '/monitor/job/edit', '127.0.0.1', '内网IP', '{\"jobId\":[\"100\"],\"updateBy\":[\"admin\"],\"jobName\":[\"视频转码调度\"],\"jobGroup\":[\"DEFAULT\"],\"invokeTarget\":[\"videoTask.process\"],\"cronExpression\":[\"0/20 * * * * ?\"],\"misfirePolicy\":[\"1\"],\"concurrent\":[\"1\"],\"status\":[\"1\"],\"remark\":[\"\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-03-24 09:52:23');
INSERT INTO `sys_oper_log` VALUES (119, '定时任务', 2, 'com.media.quartz.controller.SysJobController.changeStatus()', 'POST', 1, 'admin', '研发部门', '/monitor/job/changeStatus', '127.0.0.1', '内网IP', '{\"jobId\":[\"100\"],\"jobGroup\":[\"DEFAULT\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-03-24 09:55:29');
INSERT INTO `sys_oper_log` VALUES (120, '定时任务', 2, 'com.media.quartz.controller.SysJobController.run()', 'POST', 1, 'admin', '研发部门', '/monitor/job/run', '127.0.0.1', '内网IP', '{\"jobId\":[\"100\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-03-24 09:55:32');
INSERT INTO `sys_oper_log` VALUES (121, '视频片段', 1, 'com.media.video.controller.MamVideoFramentController.addSave()', 'POST', 1, 'admin', '研发部门', '/video/frament/add', '127.0.0.1', '内网IP', '{\"videoId\":[\"1\"],\"name\":[\"片段层1\"],\"thumbUrl\":[\"/profile/upload/admin/video/private/2022/03/24/eb8ebe68e9f50ec9d93e6fd72fef3fa4/1_11.jpg\"],\"start\":[\"10599\"],\"end\":[\"11662\"]}', '{\"msg\":\"操作成功\",\"code\":0,\"data\":{\"createTime\":1648086981828,\"end\":11662,\"framentId\":1,\"name\":\"片段层1\",\"params\":{},\"start\":10599,\"thumbUrl\":\"/profile/upload/admin/video/private/2022/03/24/eb8ebe68e9f50ec9d93e6fd72fef3fa4/1_11.jpg\",\"videoId\":1}}', 0, NULL, '2022-03-24 09:56:22');
INSERT INTO `sys_oper_log` VALUES (122, '系统设置', 10, 'com.media.web.controller.system.SysSettingController.fileVideoLogo()', 'POST', 1, 'admin', '研发部门', '/system/setting/fileVideoLogo', '127.0.0.1', '内网IP', '{\"fileId\":[\"38226_true.jpg\"],\"initialPreview\":[\"[]\"],\"initialPreviewConfig\":[\"[]\"],\"initialPreviewThumbTags\":[\"[]\"],\"settingId\":[\"1\"]}', '{\"msg\":\"操作成功\",\"code\":0,\"data\":{\"videologoPath\":\"resource/videologo/videologo.jpg\"}}', 0, NULL, '2022-03-24 10:11:41');
INSERT INTO `sys_oper_log` VALUES (123, '系统设置表', 2, 'com.media.web.controller.system.SysSettingController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/setting/edit', '127.0.0.1', '内网IP', '{\"settingId\":[\"1\"],\"name\":[\"媒资管理系统\"],\"fullName\":[\"媒资管理系统\"],\"company\":[\"\"],\"address\":[\"河北省石家庄市\"],\"telephone\":[\"\"],\"watermarkMode\":[\"0\"],\"watermarkAlpha\":[\"50\"],\"watermarkPosition\":[\"9\"],\"videologoMode\":[\"1\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-03-24 10:11:43');
INSERT INTO `sys_oper_log` VALUES (124, '上传视频', 1, 'com.media.video.controller.MamVideoSourceController.fileUpload()', 'POST', 1, 'admin', '研发部门', '/video/source/fileUpload', '127.0.0.1', '内网IP', '{\"id\":[\"WU_FILE_0\"],\"name\":[\"demo.mp4\"],\"type\":[\"video/mp4\"],\"lastModifiedDate\":[\"Tue Mar 15 2022 09:18:41 GMT+0800 (中国标准时间)\"],\"size\":[\"3268585\"]}', '{\"path\":\"\",\"status\":1}', 0, NULL, '2022-03-24 10:12:03');
INSERT INTO `sys_oper_log` VALUES (125, '视频信息', 1, 'com.media.video.controller.MamVideoController.addSave()', 'POST', 1, 'admin', '研发部门', '/video/video//add', '127.0.0.1', '内网IP', '{\"sourceVideoIds\":[\"3\"],\"title\":[\"2222\"],\"keywords\":[\"2222\"],\"categoryId\":[\"2\"],\"name\":[\"学生活动\"],\"author\":[\"紫夜\"],\"shootingTime\":[\"2022-03-24\"],\"leader\":[\"0\"],\"remark\":[\"22222\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-03-24 10:12:11');
INSERT INTO `sys_oper_log` VALUES (126, '视频信息', 3, 'com.media.video.controller.MamVideoController.remove()', 'POST', 1, 'admin', '研发部门', '/video/video/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"2\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-03-24 10:14:54');
INSERT INTO `sys_oper_log` VALUES (127, '还原视频管理', 3, 'com.media.video.controller.VideoRecycleController.restore()', 'POST', 1, 'admin', '研发部门', '/video/recycle/restore', '127.0.0.1', '内网IP', '{\"ids\":[\"2\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-03-24 10:17:32');
INSERT INTO `sys_oper_log` VALUES (128, '视频信息', 3, 'com.media.video.controller.MamVideoController.remove()', 'POST', 1, 'admin', '研发部门', '/video/video/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"2\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-03-24 10:17:37');
INSERT INTO `sys_oper_log` VALUES (129, '清空回收站', 3, 'com.media.video.controller.VideoRecycleController.clearRecycle()', 'POST', 1, 'admin', '研发部门', '/video/recycle/clearRecycle', '127.0.0.1', '内网IP', '', '{\"msg\":\"ok\",\"code\":0}', 0, NULL, '2022-03-24 10:17:42');
INSERT INTO `sys_oper_log` VALUES (130, '图片信息', 3, 'com.media.picture.controller.PictureController.softRemove()', 'POST', 1, 'admin', '研发部门', '/picture/picture/softRemove', '127.0.0.1', '内网IP', '{\"pictureId\":[\"5\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-03-24 10:19:37');
INSERT INTO `sys_oper_log` VALUES (131, '图片信息', 2, 'com.media.picture.controller.PictureRecycleController.recover()', 'POST', 1, 'admin', '研发部门', '/picture/recycle/recover', '127.0.0.1', '内网IP', '{\"ids\":[\"4\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-03-24 10:21:17');
INSERT INTO `sys_oper_log` VALUES (132, '图片信息', 3, 'com.media.picture.controller.PictureRecycleController.remove()', 'POST', 1, 'admin', '研发部门', '/picture/recycle/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"5\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-03-24 10:21:23');
INSERT INTO `sys_oper_log` VALUES (133, '图片信息', 2, 'com.media.picture.controller.PictureRecycleController.recover()', 'POST', 1, 'admin', '研发部门', '/picture/recycle/recover', '127.0.0.1', '内网IP', '{\"ids\":[\"6\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-03-24 10:21:31');
INSERT INTO `sys_oper_log` VALUES (134, '系统设置表', 2, 'com.media.web.controller.system.SysSettingController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/setting/edit', '127.0.0.1', '内网IP', '{\"settingId\":[\"1\"],\"name\":[\"媒资管理系统\"],\"fullName\":[\"媒资管理系统\"],\"company\":[\"\"],\"address\":[\"河北省石家庄市\"],\"telephone\":[\"\"],\"watermarkMode\":[\"0\"],\"watermarkAlpha\":[\"50\"],\"watermarkPosition\":[\"9\"],\"videologoMode\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-03-24 10:25:32');

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `post_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位名称',
  `post_sort` int(4) NOT NULL COMMENT '显示顺序',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '岗位信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, 'ceo', '董事长', 1, '0', 'admin', '2022-03-23 15:02:58', '', NULL, '');
INSERT INTO `sys_post` VALUES (2, 'se', '项目经理', 2, '0', 'admin', '2022-03-23 15:02:58', '', NULL, '');
INSERT INTO `sys_post` VALUES (3, 'hr', '人力资源', 3, '0', 'admin', '2022-03-23 15:02:58', '', NULL, '');
INSERT INTO `sys_post` VALUES (4, 'user', '普通员工', 4, '0', 'admin', '2022-03-23 15:02:58', '', NULL, '');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'admin', 1, '1', '0', '0', 'admin', '2022-03-23 15:02:58', '', NULL, '超级管理员');
INSERT INTO `sys_role` VALUES (2, '普通角色', 'common', 2, '2', '0', '0', 'admin', '2022-03-23 15:02:58', '', NULL, '普通角色');

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色和部门关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES (2, 100);
INSERT INTO `sys_role_dept` VALUES (2, 101);
INSERT INTO `sys_role_dept` VALUES (2, 105);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (2, 1);
INSERT INTO `sys_role_menu` VALUES (2, 2);
INSERT INTO `sys_role_menu` VALUES (2, 3);
INSERT INTO `sys_role_menu` VALUES (2, 4);
INSERT INTO `sys_role_menu` VALUES (2, 100);
INSERT INTO `sys_role_menu` VALUES (2, 101);
INSERT INTO `sys_role_menu` VALUES (2, 102);
INSERT INTO `sys_role_menu` VALUES (2, 103);
INSERT INTO `sys_role_menu` VALUES (2, 104);
INSERT INTO `sys_role_menu` VALUES (2, 105);
INSERT INTO `sys_role_menu` VALUES (2, 106);
INSERT INTO `sys_role_menu` VALUES (2, 107);
INSERT INTO `sys_role_menu` VALUES (2, 108);
INSERT INTO `sys_role_menu` VALUES (2, 109);
INSERT INTO `sys_role_menu` VALUES (2, 110);
INSERT INTO `sys_role_menu` VALUES (2, 111);
INSERT INTO `sys_role_menu` VALUES (2, 112);
INSERT INTO `sys_role_menu` VALUES (2, 113);
INSERT INTO `sys_role_menu` VALUES (2, 114);
INSERT INTO `sys_role_menu` VALUES (2, 115);
INSERT INTO `sys_role_menu` VALUES (2, 116);
INSERT INTO `sys_role_menu` VALUES (2, 500);
INSERT INTO `sys_role_menu` VALUES (2, 501);
INSERT INTO `sys_role_menu` VALUES (2, 1000);
INSERT INTO `sys_role_menu` VALUES (2, 1001);
INSERT INTO `sys_role_menu` VALUES (2, 1002);
INSERT INTO `sys_role_menu` VALUES (2, 1003);
INSERT INTO `sys_role_menu` VALUES (2, 1004);
INSERT INTO `sys_role_menu` VALUES (2, 1005);
INSERT INTO `sys_role_menu` VALUES (2, 1006);
INSERT INTO `sys_role_menu` VALUES (2, 1007);
INSERT INTO `sys_role_menu` VALUES (2, 1008);
INSERT INTO `sys_role_menu` VALUES (2, 1009);
INSERT INTO `sys_role_menu` VALUES (2, 1010);
INSERT INTO `sys_role_menu` VALUES (2, 1011);
INSERT INTO `sys_role_menu` VALUES (2, 1012);
INSERT INTO `sys_role_menu` VALUES (2, 1013);
INSERT INTO `sys_role_menu` VALUES (2, 1014);
INSERT INTO `sys_role_menu` VALUES (2, 1015);
INSERT INTO `sys_role_menu` VALUES (2, 1016);
INSERT INTO `sys_role_menu` VALUES (2, 1017);
INSERT INTO `sys_role_menu` VALUES (2, 1018);
INSERT INTO `sys_role_menu` VALUES (2, 1019);
INSERT INTO `sys_role_menu` VALUES (2, 1020);
INSERT INTO `sys_role_menu` VALUES (2, 1021);
INSERT INTO `sys_role_menu` VALUES (2, 1022);
INSERT INTO `sys_role_menu` VALUES (2, 1023);
INSERT INTO `sys_role_menu` VALUES (2, 1024);
INSERT INTO `sys_role_menu` VALUES (2, 1025);
INSERT INTO `sys_role_menu` VALUES (2, 1026);
INSERT INTO `sys_role_menu` VALUES (2, 1027);
INSERT INTO `sys_role_menu` VALUES (2, 1028);
INSERT INTO `sys_role_menu` VALUES (2, 1029);
INSERT INTO `sys_role_menu` VALUES (2, 1030);
INSERT INTO `sys_role_menu` VALUES (2, 1031);
INSERT INTO `sys_role_menu` VALUES (2, 1032);
INSERT INTO `sys_role_menu` VALUES (2, 1033);
INSERT INTO `sys_role_menu` VALUES (2, 1034);
INSERT INTO `sys_role_menu` VALUES (2, 1035);
INSERT INTO `sys_role_menu` VALUES (2, 1036);
INSERT INTO `sys_role_menu` VALUES (2, 1037);
INSERT INTO `sys_role_menu` VALUES (2, 1038);
INSERT INTO `sys_role_menu` VALUES (2, 1039);
INSERT INTO `sys_role_menu` VALUES (2, 1040);
INSERT INTO `sys_role_menu` VALUES (2, 1041);
INSERT INTO `sys_role_menu` VALUES (2, 1042);
INSERT INTO `sys_role_menu` VALUES (2, 1043);
INSERT INTO `sys_role_menu` VALUES (2, 1044);
INSERT INTO `sys_role_menu` VALUES (2, 1045);
INSERT INTO `sys_role_menu` VALUES (2, 1046);
INSERT INTO `sys_role_menu` VALUES (2, 1047);
INSERT INTO `sys_role_menu` VALUES (2, 1048);
INSERT INTO `sys_role_menu` VALUES (2, 1049);
INSERT INTO `sys_role_menu` VALUES (2, 1050);
INSERT INTO `sys_role_menu` VALUES (2, 1051);
INSERT INTO `sys_role_menu` VALUES (2, 1052);
INSERT INTO `sys_role_menu` VALUES (2, 1053);
INSERT INTO `sys_role_menu` VALUES (2, 1054);
INSERT INTO `sys_role_menu` VALUES (2, 1055);
INSERT INTO `sys_role_menu` VALUES (2, 1056);
INSERT INTO `sys_role_menu` VALUES (2, 1057);
INSERT INTO `sys_role_menu` VALUES (2, 1058);
INSERT INTO `sys_role_menu` VALUES (2, 1059);
INSERT INTO `sys_role_menu` VALUES (2, 1060);
INSERT INTO `sys_role_menu` VALUES (2, 1061);

-- ----------------------------
-- Table structure for sys_setting
-- ----------------------------
DROP TABLE IF EXISTS `sys_setting`;
CREATE TABLE `sys_setting`  (
  `setting_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '设置ID',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '系统名称',
  `full_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '系统全称',
  `company` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '单位名称',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '地址',
  `icp` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备案号',
  `telephone` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '电话',
  `watermark_mode` tinyint(1) NULL DEFAULT NULL COMMENT '1开启水印 0关闭水印',
  `watermark_alpha` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '透明度',
  `watermark_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '水印图片',
  `watermark_position` tinyint(1) NULL DEFAULT NULL COMMENT '水印位置(1-9)',
  `videologo_mode` tinyint(1) NULL DEFAULT NULL COMMENT '1开启台标0关闭台标',
  `videologo_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '台标图案',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`setting_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统设置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_setting
-- ----------------------------
INSERT INTO `sys_setting` VALUES (1, '媒资管理系统', '媒资管理系统', NULL, '河北省石家庄市', NULL, NULL, 0, '50', '', 9, 0, '/videologo/videologo.jpg', NULL);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint(20) NULL DEFAULT NULL COMMENT '部门ID',
  `login_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '登录账号',
  `user_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户昵称',
  `user_type` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '00' COMMENT '用户类型（00系统用户 01注册用户）',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '头像路径',
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '密码',
  `salt` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '盐加密',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime(0) NULL DEFAULT NULL COMMENT '最后登录时间',
  `pwd_update_date` datetime(0) NULL DEFAULT NULL COMMENT '密码最后更新时间',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 103, 'admin', '紫夜', '00', 'ry@163.com', '15888888888', '1', '', '29c67a30398638269fe600f73a054934', '111111', '0', '0', '127.0.0.1', '2022-03-24 10:46:30', '2022-03-23 15:02:58', 'admin', '2022-03-23 15:02:58', '', '2022-03-24 10:46:30', '管理员');
INSERT INTO `sys_user` VALUES (2, 105, 'ry', '若依', '00', 'ry@qq.com', '15666666666', '1', '', '8e6d98b90472783cc73c17047ddccf36', '222222', '0', '0', '127.0.0.1', '2022-03-23 15:02:58', '2022-03-23 15:02:58', 'admin', '2022-03-23 15:02:58', '', NULL, '测试员');

-- ----------------------------
-- Table structure for sys_user_online
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_online`;
CREATE TABLE `sys_user_online`  (
  `sessionId` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户会话id',
  `login_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录账号',
  `dept_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `ipaddr` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作系统',
  `status` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '在线状态on_line在线off_line离线',
  `start_timestamp` datetime(0) NULL DEFAULT NULL COMMENT 'session创建时间',
  `last_access_time` datetime(0) NULL DEFAULT NULL COMMENT 'session最后访问时间',
  `expire_time` int(5) NULL DEFAULT 0 COMMENT '超时时间，单位为分钟',
  PRIMARY KEY (`sessionId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '在线用户记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_online
-- ----------------------------
INSERT INTO `sys_user_online` VALUES ('495751f1-fe5a-43e2-8c62-ac7c3b585de5', 'admin', '研发部门', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', 'on_line', '2022-03-24 10:31:34', '2022-03-24 10:46:30', 1800000);

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户与岗位关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES (1, 1);
INSERT INTO `sys_user_post` VALUES (2, 2);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户和角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (2, 2);

SET FOREIGN_KEY_CHECKS = 1;
