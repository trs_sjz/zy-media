var checkType = 0;
var userId = config.userId;
var chunkSize = 5 * 1024 * 1024; //分块大小
var uniqueFileNameMap = new Map();
var filemd5MarkMap = new Map();
/*******获取速率使用*********/
var timeMap = new Map();
var sizeMap = new Map();
var otimeMap = new Map();
var ospeedMap = new Map();
var uploadInit = null;
var isFolder = false;
var uploadFinishStatus = false;

var OnairJsUtil = {
    /**获取uuid*/
    getUuid: function () {
        var s = [];
        var hexDigits = "0123456789abcdef";
        for (var i = 0; i < 36; i++) {
            s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
        }
        s[14] = "4"; // bits 12-15 of the time_hi_and_version field to 0010
        s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1); // bits 6-7 of the clock_seq_hi_and_reserved to 01
        s[8] = s[13] = s[18] = s[23] = "-";
        var uuid = s.join("");
        return uuid;
    },
}

var backEndUrl = config.uploadUrl;
/*************wu上传初始****************/
new WebUploader.Uploader.register(
    {
        "before-send-file": "beforeSendFile",
        "before-send": "beforeSend",
        "after-send-file": "afterSendFile"
    },
    {  //上传文件开始触发
        beforeSendFile: function (file) {
            //秒传验证
            var task = new $.Deferred();
            var start = new Date().getTime();
            var md5Mark;
            file.relativePath = (file.source && file.source.source && file.source.source.webkitRelativePath) ? file.source.source.webkitRelativePath : '';
            if (file.relativePath) {
                file.fid = OnairJsUtil.getUuid();
            }
            var relative_path = file.relativePath.split("/");
            file.sourceRelativePath = relative_path.slice(0, relative_path.length - 1);
            if (checkType == 1) {
                uploadInit.md5File(file).progress(
                    function (percentage) {
                        $("#" + file.id + " .progress .md5_pro .pro_color").width(percentage * 100 + "%");
                        $("#" + file.id + " .progress .md5_pro .jindu").text("MD5进度：" + Math.round(percentage * 100) + "%");
                        //console.log(percentage);
                    }).then(function (val) {
                    $("#" + file.id + " .operate .del_btn").show();
                    // $("#" + file.id + " .operate .pause_btn").show();
                    //console.log("总耗时: "+ ((new Date().getTime()) - start)/ 1E3);
                    md5Mark = val;
                    filemd5MarkMap.put(file.id, md5Mark);
                    $.ajax({
                        type: "POST",
                        url: backEndUrl + "fileUploadCheck",
                        data: {status: "md5Check", md5: val},
                        cache: false,
                        timeout: 3000000, //to do 超时的话，只能认为该文件不曾上传过
                        dataType: "json"
                    }).then(function (data, textStatus, jqXHR) {
                        var time = new Date().getTime();
                        timeMap.put(file.id, time);
                        if (data.ifExist) { //若存在，这返回失败给WebUploader，表明该文件不需要上传
                            task.reject();
                            uploadInit.skipFile(file);
                            file.path = data.path;
                            $("#" + file.id + " .progress .upload_pro .pro_color").width(100 + "%");
                            if ($("#" + file.id).attr("class").indexOf("batchEdit") > -1) {
                                UploadComlate2(file);
                            } else {
                                UploadComlate(file);
                            }
                        } else {
                            task.resolve();
                        }
                    }, function (jqXHR, textStatus, errorThrown) { //任何形式的验证失败，都触发重新上传
                        task.resolve();
                    });
                });
            } else {
                $("#" + file.id + " .operate .del_btn").show();
                sizeMap.put(file.id, 0);
                otimeMap.put(file.id, 0);
                //  $("#" + file.id + " .operate .pause_btn").show();
                task.resolve();
            }
            return $.when(task);
        },
        //开始上传分片触发
        beforeSend: function (block) {
            //分片验证是否已传过，用于断点续传
            var task = new $.Deferred();
            var file = block.file;
            var id = file.id;
            uploadInit.md5File(file, block.start, block.end).then(function (val) {
                $.ajax({
                    type: "POST",
                    url: backEndUrl + "fileUploadCheck",
                    data: {
                        status: "chunkCheck",
                        name: uniqueFileNameMap.get(id),
                        chunkIndex: block.chunk,
                        size: block.end - block.start,
                        blockmd5: val
                    },
                    cache: false,
                    timeout: 3000000, //to do 超时的话，只能认为该分片未上传过
                    dataType: "json"
                }).then(function (data, textStatus, jqXHR) {
                    var time = new Date().getTime();
                    timeMap.put(file.id, time);
                    if (data.ifExist == 1) { //若存在，返回失败给WebUploader，表明该分块不需要上传
                        task.reject();
                    } else {
                        task.resolve();
                    }
                }, function (jqXHR, textStatus, errorThrown) { //任何形式的验证失败，都触发重新上传
                    task.reject();
                    task.resolve();
                });
            });
            return $.when(task);
        },
        //上传文件完成触发
        afterSendFile: function (file) {
            var chunksTotal = Math.ceil(file.size / chunkSize);
            //		if ((chunksTotal = Math.ceil(file.size
            //				/ chunkSize)) > 1) {
            //合并请求
            var task = new $.Deferred();
            $.ajax({
                type: "POST",
                url: backEndUrl + "fileMergeChunks",
                data: {
                    status: "chunksMerge",
                    name: uniqueFileNameMap.get(file.id),
                    filename:file.name,
                    chunks: chunksTotal,
                    ext: file.ext,
                    md5: filemd5MarkMap.get(file.id)
                },
                cache: false,
                dataType: "json"
            }).then(function (data, textStatus, jqXHR) {
                //to do 检查响应是否正常
                var status = data.status;
                file.path = data.path;
                task.resolve();
                if (status == 1) {
                	UploadComlate(file,data);
                } else {
                    uploadStatus = false;
                    UploadFail(file);
                }
                uniqueFileNameMap.remove(file.id);
                filemd5MarkMap.remove(file.id);
            }, function (jqXHR, textStatus, errorThrown) {
                task.reject();
            });
            return $.when(task);
            //	} else {
            //		UploadComlate(file);
            //	}
        }
    });


/***************弹窗控制***********************/
$(function () {
   
   
});
var LOGTITLE = $(".treeview-menu li.active a").html();//用于统计日志名称
var uploadStatus = true;

var upload_count = 0;
var upload_success_count = 0;
var upload_fail_count = 0;
var upload_reset = 0;

var supportTransition = (function(){
    var s = document.createElement('p').style,
    r = 'transition' in s ||
          'WebkitTransition' in s ||
          'MozTransition' in s ||
          'msTransition' in s ||
          'OTransition' in s;
s = null;
return r;
})();

/***********上传配置*********************/
var uploader = WebUploader.create({
    swf: "js/Uploader.swf",//上传的flash
    server: backEndUrl + "fileUpload",//后台上传方法
    pick: "#picker",
    compress: false,//不启用压缩
    resize: false,//尺寸不改变
    // dnd : "#upload_btn",
    disableWidgets: "log",
    paste: document.body,
    disableGlobalDnd: true,
    multiple: true,
    thumb: {
        width: 200,
        height: 148,
        quality: 100,
        allowMagnify: false,
        crop: false,		//禁止剪裁，防止图片回显变形
    },
    compress: false,
    prepareNextFile: true,
    chunked: true,
    chunkSize: chunkSize,
    threads: "5",
    disableGlobalDnd: true,
    formData: {
    },
    fileSizeLimit: 100 * 1024 * 1024 * 1024,
    fileSingleSizeLimit: 100 * 1024 * 1024 * 1024,
    duplicate: true,
    auto: true,
    //自定义上传类型
    accept: {
        title: '',
        extensions: config.extensions,
        mimeTypes: config.mimeTypes
    },
});
uploader.addButton({
    id: '#pickerFolder',
});
uploadInit = uploader;
var file_path = {};
/************文件加入上传队列执行*******************/
uploader.on('uploadBeforeSend', function (object, data, headers) {
	uploadFinishStatus = false;
    var webkitRelativePath = (object.file.source && object.file.source.source && object.file.source.source.webkitRelativePath) ? object.file.source.source.webkitRelativePath : '';
    var relative_path = webkitRelativePath.split("/");
    var sourceRelativePath = relative_path.slice(0, relative_path.length - 1);
    relative_path.forEach(function (item, idx) {
        var name = sourceRelativePath.slice(0, relative_path.length - idx).join("/");
        if (!file_path[name]) {
            file_path[name] = OnairJsUtil.getUuid();
        }
    });
});
uploader.on("fileQueued", function (file) {
	if(file.ext == "zip" || file.ext == "rar" || file.ext == "7z"){
		parent.$.modal.msgError("暂不支持上传压缩包");
		return;
	}
	console.log("file.ext===>"+file.ext);
	var tagShootingTime = "";
    upload_count++;
    $('#upload_cont').css('visibility', 'visible');
    ospeedMap.put(file.id, 0);
    uploader.sort(function (a, b) {
        return a.size - b.size;
    });//多文件同时加入时小文件优先上传
    var uniqueFileName = md5(file.name + file.size + file.type + userId);//文件唯一标识
    uniqueFileNameMap.put(file.id, uniqueFileName);
    var fileSize = WebUploader.Base.formatSize(file.size, 2, ['B', 'KB', 'MB', 'G', 'TB']);
    var ext = file.ext;
    var lastModifiedDate = file.lastModifiedDate.format("yyyy-MM-dd");
    var title = getFileName(file.name);
    var tplData = {fileId:file.id,fileName:file.name,fileSize:fileSize,ext:ext,title:title,price:config.price,fileInfo:file._info,lastModifiedDate:lastModifiedDate};
    var html = template("imageTpl", tplData); 
    $("#image-list").append(html);
    
    var $img = $("#" + file.id).find("img");
    uploader.makeThumb(file, function (error, src) {
        if (error) {
            $img.replaceWith("");
        }
        var imgWidth = file._info.width;
        var imgHeight = file._info.height;
        $("#" + file.id).find(".imgWidth").text(imgWidth);
        $("#" + file.id).find(".imgHeight").text(imgHeight);
        $img.attr("src", src);
    });
    
});


/***********上传进度获取**************/
uploader.on("uploadProgress", function (file, percentage, uploaded) {
	//console.log(percentage)
    var num = percentage * 100;
    var osize = sizeMap.get(file.id); // 前大小
    var size = file.size * percentage;// 现大小
    sizeMap.put(file.id, size);
    var sizesp = file.size - size;// 剩余大小
    var sizedf = size - osize;//大小差
    
    var otime = timeMap.get(file.id);//前时间
    var time = new Date().getTime();//现时间
    timeMap.put(file.id, time);

    var timedf = (time - otime) / 1000;//时间差
    //console.log("N时间：" + time);
    //console.log("O时间：" + otime);
    //console.log("时间差：" + timedf);
    var timesp = 0;
    var speed = ospeedMap.get(file.id);
   
    if (timedf > 0.068 && timedf < 0.088 && sizedf != 0) {
        speed = Math.round(sizedf / timedf / 1024);
        ospeedMap.put(file.id, speed);
        //console.log("大小差：" + sizedf);
        //console.log("时间差：" + timedf);
        //console.log("速度：" + speed);
    }
   
    if (speed != 0) {
        timesp = Math.round(sizesp / speed / 1000);
    }
    $("#" + file.id + " .progress-bar").width(num + "%");
    $("#" + file.id + " .progress-bar").text(num.toFixed(2) + "%");
  
});



/************全部上传完成****************/
uploader.on("uploadFinished", function () {
    if(typeof(eval(uploadFinished))=="function"){
		 uploadFinished();
    }
    setTimeout(function () {
        if (uploadStatus) {
            $('#upload_cont').css('visibility', 'hidden');
        }
    }, 1000);
});

/***************上传之前*****************/
//上传之前获取 文件夹相对路径，
uploader.on('uploadBeforeSend', function(object, data, headers) {
	// 如果是webkitdirectory控件选择的文件，会包含webkitRelativePath属性，添加进formData发送
	console.log("uploadBeforeSend日志：");
	console.log(object);//打印此对象，可以查看webkitRelativePath值
	data.relativepath = object.file.source.source.webkitRelativePath?object.file.source.source.webkitRelativePath : '';
	console.log("webkitRelativePath:"+data.relativepath);
});

var post_file_data = [];
var code = "0";
/************上传成功触发*************/
function UploadComlate(file,data) {
	
    upload_success_count++;
    $("#" + file.id + " .progress-bar").width(
        100 + "%");
    $("#" + file.id + " .progress-bar").text("上传完成");
    $("#" + file.id).addClass("yc");
    timeMap.remove(file.id);
    sizeMap.remove(file.id);
    otimeMap.remove(file.id);
    ospeedMap.remove(file.id);
    if (upload_reset > 0) {
        upload_fail_count--;
        upload_reset--;
        uploadResetSucess(file);
        return;
    }
    $("#" + file.id).attr("data-pictureid",data.ids);  //设置图片ID
    //图片上传成功后获取该张图片信息
    if($("#filmingDate").val().length == 0 && code != "1"){
    	code = "1";
	    $.ajax({
	        type : 'GET',
	        url: ctx + "/picture/picture/getPictureInfo",
	     	cache : false,
	     	async : true,
	     	data: { pictureId: data.ids},
	     	dataType : "jsonp",
	     	timeout:30000,
	     	jsonpCallback:"callback",
	        success : function(result) {
	            //tagShootingTime = result.pictureInfo.tagShootingTime;
	            $("#filmingDate").val(result.pictureInfo.tagShootingTime);
	            code = "0";
	        },
	        error : function() {
	            console.log("请求失败");
	        }
		});
    }
}


//全部上传以后的回调
function uploadFinished(){
	uploadFinishStatus = true;
}


function uploadSucess(data) {
    console.log(data);
}

/**
 * 注册成功回调方法
 */
function uploadSucessCallback(data) {
    if (typeof queryAssets == "function" && data.code === 0 && data.data && data.data.insertMap) {
        setTimeout(function () {
            post_file_data = [];
            file_path = {};
            queryAssets();
            core_handle.get_structure();
        }, 1000);
    }
}

/************上传失败触发*************/
function UploadFail(file) {
    upload_fail_count++;
    $("#" + file.id + " .progress .upload_pro .pro_color").width(
        100 + "%");
    $("#" + file.id + " .progress .upload_pro .jindu").text("上传失败");
    //	$("#" + file.id + " .operate .play_btn").hide();
    //$("#" + file.id + " .operate .pause_btn").hide();
    timeMap.remove(file.id);
    sizeMap.remove(file.id);
    otimeMap.remove(file.id);
    ospeedMap.remove(file.id);
    $("#" + file.id + " .operate .reset_btn").show();
    var path_name = file.sourceRelativePath.join("/");
    $("#" + file.id + " .operate .reset_btn").attr("fpid", file_path[path_name]);
}





//进度条业务处理
function formatSeconds(time) {
    var total = time || 0, hour = Math.floor(total / 3600), minute = Math.floor((total - 3600 * hour) / 60),
        second = Math.floor(total - 3600 * hour - 60 * minute),
        hour = "" + (!isNaN(hour) && 0 < hour ? (hour < 10 ? ("0" + hour + ":") : (hour + ":")) : "00:"),
        hour = hour + (!isNaN(minute) && 0 < minute ? (minute < 10 ? ("0" + minute + ":") : minute + ":") : "00:");
    return hour += !isNaN(second) && 0 < second ? (second < 10 ? ("0" + second + "") : second) : "00";
}

function formatSpeed(a) {
    var b = 0;
    1024 <= Math.round(a / 1024)
        ? (b = Math.round(100 * (a / 1048576)) / 100, b = Math.max(0, b), b = isNaN(b) ? 0 : parseFloat(b).toFixed(2), a = b + "MB/s")
        : (b = Math.round(100 * (a / 1024)) / 100, b = Math.max(0, b), b = isNaN(b) ? 0 : parseFloat(b).toFixed(2), a = b + "KB/s");
    return a;
}

function getFileName(file){
    var filename=file;
    var index1=filename.lastIndexOf(".");
    var index2=filename.length;
    var type=filename.substring(0,index1);
    return type;
}

/************************************数组业务处理**************************************************/
function Map() {
    this.container = new Object();
}

Map.prototype.put = function (key, value) {
    this.container[key] = value;
};

Map.prototype.get = function (key) {
    return this.container[key];
};

Map.prototype.keySet = function () {
    var keyset = new Array();
    var count = 0;
    for (var key in this.container) {
        // 跳过object的extend函数
        if (key == 'extend') {
            continue;
        }
        keyset[count] = key;
        count++;
    }
    return keyset;
};

Map.prototype.size = function () {
    var count = 0;
    for (var key in this.container) {
        // 跳过object的extend函数
        if (key == 'extend') {
            continue;
        }
        count++;
    }
    return count;
};
Map.prototype.remove = function (key) {
    delete this.container[key];
};
//格式化时间
Date.prototype.format = function (fmt) {
    var o = {
        "M+": this.getMonth() + 1, //月份
        "d+": this.getDate(), //日
        "h+": this.getHours(), //小时
        "m+": this.getMinutes(), //分
        "s+": this.getSeconds(), //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S": this.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}