/**
*下载原图
*/
function downloadPic(id){
	$.modal.confirm("确定要下载原图吗?", function() {
	    location.href=ctx+"picture/picture/download/"+id;
	});
}

//编辑图片信息
function pictureEdit(pictureId){
	parent.layer.open({
		type: 2,
		area: ['70%', '80%'],
		fix: false,
		//不固定
		maxmin: true,
		shade: 0.3,
		title: "图片编辑",
		content: ctx + "picture/picture/edit/"+pictureId,
		btn: ['确定', '关闭'],
		shadeClose: true,
		yes: function(index, layero) {
			iframeIndex = index;
			var iframeWin = layero.find('iframe')[0];
  	        var post_data = iframeWin.contentWindow.submitHandler(index, layero);
  	        $.operate.save(ctx + "picture/picture/edit", post_data,editSuccessCallBack);
	    },
	    cancel: function(index) {
	        return true;
	    }
	});
}

//图片编辑成功回调
function editSuccessCallBack(result){
	 if (result.code == web_status.SUCCESS) {
  	     $.modal.closeLoading();
         $.modal.enable();
         parent.layer.closeAll('iframe'); //再执行关闭
     }
}
/**
 *删除图片
 */
 function deletePic(pictureId){
 	$.modal.confirm("确定要删除该图片吗", function() {
 		$.ajax({
	            type : "POST",
	            dataType: "json",
	            url : ctx + "picture/picture/remove",
	            data : {pictureId:pictureId},
	            success : function(result) {
	            	if(result.code==0){
	            		layer.msg('图片删除成功！',{
	                	    icon: $.modal.icon(modal_status.SUCCESS),
	                	    time: 500,
	                	    shade: [0.1, '#8F8F8F']
	                	}, function(){
	            			location.reload();
	            	    }); 
	            	}else{
	            		$.modal.msgError("封面删除失败!");
	            	}
	            	
	            },
	            //请求失败，包含具体的错误信息
	            error : function(e){
	               
	            }
	       });
 	});
 }

 