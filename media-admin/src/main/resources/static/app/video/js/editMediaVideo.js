(function ($) {
    $.transTime = function transTime(time) {
        var duration = parseInt(time);
        var hour = parseInt(duration / 60 / 60);
        var minute = parseInt((duration % 3600) / 60);
        var sec = duration % 60 % 60 + '';
        var isM0 = ':';
        if (hour == 0) {
            hour = '';
        } else if (hour < 10) {
            hour = '0' + hour;
        }
        if (minute == 0) {
            minute = '00';
        } else if (minute < 10) {
            minute = '0' + minute;
        }
        if (sec.length == 1) {
            sec = '0' + sec;
        }
        if (hour == 0) {
            return minute + isM0 + sec;
        } else {
            return hour + isM0 + minute + isM0 + sec;
        }
    }
})(jQuery);
/**
 * 初始化视频播放器
 * @returns
 */
function initVideo(){
    var in_point_time = "";
	var out_point_time = "";     	
	$("#preview").videoPlayer({
        id: "myVideo",//创建video id
        control: true,//视频支持  音频不支持
        autoPlay: false,
        width: 600,//视频音频的宽 最小宽度530
        height: 379,//视频的宽,音频设置无效
        source: setting.previewUrl,//播放源地址
        thumbnailUrl: "",//播放器缩略图
        playType: setting.playType,//可选值 视频：video 音频：audio
        timeFlag: false,//只有为true时才会触发currentTime函数
        flvAudio: setting.flvAudio,
        muted: false,
        videoSkin: "vjs-controlbar-fixed",//播放器样式
        playbackRates: [], //0.5-2速率范围
        currentTime: function (val) {
            console.log(val)
        },
        shootingFlag: false,//打点
        screenshotFlag: true,//截图
        pictureFlag: true,//画中画
        videoInfoFlag: true,//视频源信息
        videoHelpFlag: true,//帮助信息
        stripFlag: true,//拆条
        downLoadFlag: false,//下载按钮
        videoInfo: [
            {name: "分辨率", value: setting.detailData.resolution},
            {name: "格式", value: setting.detailData.format},
            {name: "码率", value: setting.detailData.bitRate},
            {name: "帧率", value: setting.detailData.frameRate}
        ],
        videoHelp: [{
            name: "快捷键",
            value: "播放/暂停：[空格]；快退：[左箭头]；快进：[右箭头]；加音量：[上箭头]；减音量：[下箭头]；打入点：[i]；打出点：[o]；清除点：[c]；截图：[enter]；生成片段：[p]；"
        }],
        screenshotsCallback: function (val) {
            var chosedId = $("#preview .vjs-customize-btnGround .vjs-screenshot").attr("chosed-id");
            cutImage(chosedId, val.vjsThumbnailOriginal);
        },
        pictureCallback: function (val) {
            var videoObj = $("#preview").find("video").get(0);
            if (!document.pictureInPictureElement) {//开启
                videoObj.requestPictureInPicture();
            } else {//关闭
                document.exitPictureInPicture();
            }
        },
        /* shootingFlag:false,*/
        shootingCallback: function (val) {
        },
        vjsInpointCallback: function (val) {
            in_point_time = val.targetTime;
            cutfirstImage(in_point_time);
        },
        vjsOutpointCallback: function (val) {
            out_point_time = val.targetTime;
        },
        vjsAllpointDelCallback: function () {
            in_point_time = "";
            out_point_time = "";
        },
        vjsAddFragmentCallback: function () {
        	console.log(in_point_time+""+out_point_time);
            addFragment(in_point_time, out_point_time);
            in_point_time = "";
            out_point_time = "";
        }
    });

    setTimeout(function () {
        var play = document.getElementById("myVideoPlay");
        var evt = document.createEvent("MouseEvents");
        if (play) {
            evt.initEvent("click", true, true);
            play.dispatchEvent(evt);
        }

    }, 500);
    //bindHotkey();
  
}
/**
 * 绑定快捷键
 */
function bindHotkey(type) {
    $("#preview").mouseover(function () {
        //layer.msg("开启快捷键使用模式");
        // $("#YSContent").css("overflow", "hidden").css("paddingLeft", "").addClass("compensate-for-scrollbar");
        $(document).unbind().on("keydown", function (e) {
        	
            var videoObj = $("#preview").find("video").get(0);
            //左倒退0.1秒
            if (e.keyCode == 37) {
                videoObj.pause();
                videoObj.currentTime = (videoObj.currentTime - 1 / 25);
            }
            //右前进0.1秒
            if (e.keyCode == 39) {
                videoObj.pause();
                videoObj.currentTime = (videoObj.currentTime + 1 / 25);
            }
            //截图键
            if (e.keyCode == 13) {
                videoObj.pause();
                cutImage();
            }
            //+音量
            if (e.keyCode == 38) {
                var first_mute_control = $("#myVideo").find(".vjs-volume-panel-vertical .vjs-mute-control");
                if (videoObj.volume === 1) {
                    if (first_mute_control.hasClass("vjs-vol-0")) {
                        $("#myVideo").find(".vjs-mute-control").click();
                    }
                    return;
                }
                videoObj.volume = (videoObj.volume + .1).toFixed(1);
            }
            //-音量
            if (e.keyCode == 40) {
                if (videoObj.volume === 0) {
                    return false;
                }
                videoObj.volume = (videoObj.volume - .1).toFixed(1);
                return false;
            }
            //空格
            if (e.keyCode == 32) {
                var active_element = document.activeElement.className;
                if (active_element.indexOf("vjs-play-control") >= 0) {
                    return;
                }
                $("#myVideo").find(".vjs-play-control").focus();
                if (navigator.userAgent.indexOf("Firefox") < 0) {
                    $("#myVideo").find(".vjs-play-control").click();
                }
            }
            if (!type) {
                //入点 i
                if (e.keyCode == 73) {
                    videoObj.pause();
                    videoPlayCallBack.videojsInpoint("myVideo");
                }
                //出点 o
                if (e.keyCode == 79) {
                    videoObj.pause();
                    videoPlayCallBack.videojsOutpoint("myVideo");
                }
                //删除点 c
                if (e.keyCode == 67) {
                    videoPlayCallBack.videojsAllpointDel("myVideo");
                }
                //生成片段 p
                if (e.keyCode == 80) {
                    videoObj.pause();
                    addFragment();
                }
            }
        });
    });
    $("#preview").mouseout(function () {
        $(document).unbind();
    });
}

$(function () {
	if(setting.processstatus=="SUC_DONE"){
		initVideo();  //初始化视频播放
	}

	initActionLog();    //初始化操作记录
	initFrament();      //初始化片段
	initThumbs();       //初始化缩略图
	$("#form-video-edit").validate({
        focusCleanup: true
    });
	//切换tab
	$(".nav-tabs li").click(function () {
	     $(this).addClass("active").siblings("li").removeClass("active");
	     var tabId = $(this).children("a").attr("href");
	     $(".tab-content .tab-pane").removeClass("active");
	     $(tabId).addClass("active");
	});
	//初始化标签
	$('.tagsinput').tagsInput({
		 'height':'aotu',
		 'width':'100%',
		 'defaultText':'添加标签',
		 'onAddTag':function(tag){
			 updateLabel(this);
		  },
		 'onRemoveTag':function(tag){
			 updateLabel(this);
		  }
	});
	
	 // 右键菜单实现
	$('#thumbList').contextMenu({
        selector: '.thumb', 
        items: {
        	 "set_cover": {
                 name: "设为封面",
                 callback: function(key, opt) {
                	 var thumbId = $(this).attr("id");
                	 $.operate.save(ctx + "video/thumb/setCover", {thumbId:thumbId},function(result){
                		  $.modal.closeLoading(); 
   	               		  if(result.code==0){
   	               	         	$.modal.msgSuccess(result.msg);
   	               		  }else{
   	               	           	$.modal.msgError(result.msg);
   	               		  }
   	               	 });

                 }
             },
             "download_image": {
                 name: "生成图片",
                 callback: function(key, opt) {
                 	
                 }
             },
             "add_remark": {
                 name: "添加描述",
                 callback: function(key, opt) {
                	 var that = this;
                	 var thumbId = $(that).attr("id");
                	 var defaultValue = $(that).find(".tag").text();
                	 layer.prompt({title: '添加关键帧描述', formType: 2, value: defaultValue}, function(text, index){
                		 layer.close(index);
                		 $.operate.save(ctx + "video/thumb/addRemark", {thumbId:thumbId,remark:text},function(result){
                       		  $.modal.closeLoading(); 
   	   	               		  if(result.code==0){
   	   	               			   $(that).find(".tag").text(text);
   	   	               	           $.modal.msgSuccess(result.msg);
   	   	               		  }else{
   	   	               	           	$.modal.msgError(result.msg);
   	   	               		  }
   	   	               	 });
                     });
                	
                 }
             },
          
        }
    });
	
	
});


/**
 * 保存视频信息
 */
function submitHandler() {
    if ($.validate.form()) {
        $.operate.save(setting.prefix + "/edit", $('#form-video-edit').serialize(),function successCallBack(result){
        	$.modal.closeLoading();
            if (result.code == web_status.SUCCESS) {
                $.modal.close();
                parent.$.modal.msgSuccess(result.msg);
                parent.$.treeTable.refresh();
            }else{
            	parent.$.modal.msgSuccess("保存失败");
            }
       });
    }
}
/**
 * 保存回调
 */
function successCallBack(result){
    if (result.code == web_status.SUCCESS) {
    	   $.modal.closeLoading();
           $.modal.enable();
    }
}
/**
 * 更新视频标签
 */
function updateLabel(obj){
	 var lableKey = $(obj).attr("id");
	 var  lableArray = [];
	 $("#"+lableKey+"_tagsinput >.tag>span").each(function(index){
		 lableArray.push($.trim($(this).text()));
		
	 });  
	 var lableInfo = lableArray.join(',');
	 var post_data = {};
	 post_data["videoId"] = setting.videoId;
	 post_data[lableKey] = lableInfo;
		 
	 $.ajax({
	        type: "POST",
	        url: ctx+"video/lable/update",
	        data: JSON.stringify(post_data),
	        contentType: "application/json; charset=utf-8",
	        success: function (data) {
	            console.log(data);
	        }
	 })
}


/**
 * 初始化缩略图
 * @returns
 */
function  initFrament(){
	var config = {
        url: ctx+"video/frament/list",
        type: "post",
        dataType: "json",
        data: {videoId:setting.videoId,pageSize:100,pageNum:1},
        success: function(result) {
        	var html="";
	       	 $.each(result.rows, function(index, val){
	       		var time = initPoint(val.interceptTime);
	       		 html+='<div class="frament_items clear" id="'+val.framentId+'" framentid="'+val.framentId+'" start-point="'+val.start+'" end-point="'+val.end+'">';
	       	 	 html+='<div class="frament_title col-md-8 pr5" onclick="choseFragment(this,'+val.framentId+')">'+val.name+'</div>';
	       		 html+=' <div class="frament_time col-md-4 text-right">'+time+'<span class="m"><a href="javascript:void(0)" onclick="javascript:delFrament('+val.framentId+',this)"><i class="glyphicon glyphicon-trash"></i></a> </span></div>';
	       	     html+=' </div>';
	         });
	       	$(".framentList").append(html);
        }
    };
    $.ajax(config)
}
/**
 * 初始化视频截图
 * @returns
 */
function initThumbs(){
	var config = {
	        url: ctx+"video/thumb/list",
	        type: "post",
	        dataType: "json",
	        data: {videoId:setting.videoId,pageSize:100,pageNum:1},
	        success: function(result) {
	        	var html="";
		       	 $.each(result.rows, function(index, val){
		       		 
		       		html+='<div class="thumb pull-left" id="'+val.thumbId+'">';
		       		html+='    <div class="del_mask" onclick="deleteFrame(this)"><em class="glyphicon glyphicon-remove-circle "></em></div>';
		       		html+='    <em class="tag" id="/profile'+val.filepathbase+"/"+val.filename+'" point-time="'+val.interceptTime+'"></em>';
		       		html+='    <img onclick="playCurrentTime('+val.interceptTime+');" src="'+ctx+'profile'+val.filepath+"/"+val.filename+'" alt="截图">';
		       		html+='    <div class="text-center">'+ $.transTime(val.interceptTime)+'</div>';
		       		html+='</div>';
		         });
		       	$("#thumbList").append(html);
	        }
	    };
	    $.ajax(config)
}
/**
 * 初始化操作日志
 * @returns
 */
function initActionLog(){
	 var post_data = {
         pageSize:100,
         pageNum:1,
         orderByColumn:"",
         isAsc:"desc",
         actionMediaType:"VIDEO",
         objectId:setting.videoId
	 };
	 $.ajax({
	        type: "POST",
	        url: ctx+"video/log/list",
	        data: JSON.stringify(post_data),
	        contentType: "application/json; charset=utf-8",
	        dataType: "json",
	        success: function (data) {
	        	var html="";
	        	 $.each(data.rows, function(index, val){
                      html += "<li>"+val.createTime+"  "+val.actionDes+" </li>";
                });
	        	$(".action-log").append(html);
	        }
	 })
}
/**
 * 播放当前视频
 * @param time
 * @returns
 */
function playCurrentTime(new_time){
	  var videoObj = $("#preview").find("video").get(0);
      videoObj.pause();
      videoObj.currentTime = new_time;
}
/**
 * 删除图片
 * @param obj
 * @returns
 */
function deleteFrame(obj){
	parent.$.modal.confirm("确认要删除吗?", function() {
		var thumbId = $(obj).parent().attr("id");
	   
		$.ajax({
	        type: "POST",
	        url: ctx+"video/thumb/remove",
	        data: {thumbId:thumbId},
	        //contentType: "application/json; charset=utf-8",
	        dataType: "json",
	        success: function (data) {
	        	if(data.code==0){
	        		$(obj).parent().remove();
	        	}
	        }
	    })
	});
}

//分段首帧截图
function cutfirstImage(in_point_time) {
    var video = $("#preview").find("video")[0];
    var pointTime = in_point_time*1000;
    var scale = 1;
    var canvas = document.createElement("canvas");
    if (video) {
        canvas.width = video.videoWidth * scale;
        canvas.height = video.videoHeight * scale;
        canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
        var data = canvas.toDataURL("image/png");
        //生成的文件中"data:image/jpeg;base64,"为多余数据，替换掉
        data = data.replace("data:image/png;base64,", "");
        //表单方式提交数据
        var formData = new FormData();
        formData.append("videoId",setting.videoId);
        formData.append("fileName", "cutImage.png");
        formData.append("pointTime", pointTime);
        //使用URI编码
        formData.append("base64Stream", encodeURIComponent(data));
        $.ajax({
            type: "POST",
            url: ctx+"video/thumb/cutImage",
            data: formData,
            async: true,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
            	if(data.code==0){
            		var html = "";
            		html+='<div class="thumb pull-left"  id="'+data.data.thumbId+'">';
            		html+=' <div class="del_mask" onclick="deleteFrame(this)"><em class="glyphicon glyphicon-remove-circle "></em></div>';
                    html+=' <em class="tag"  point-time='+data.data.interceptTime*100+' id="'+data.data.thumbSrc+'"></em>';
                   
                    html+='<img  onclick="playCurrentTime('+data.data.interceptTime+');" src="'+ctx+data.data.thumbSrc+'" alt="截图">';
                    html+='<div class="text-center" >'+data.data.pontTime+'</div>';
                    html+='</div>';
            		$(".thumbs").prepend(html);
            		$("#fragmentImgurl").val(ctx+data.data.thumbSrc);
            		
            	}
            }
        })
    }
}
/**
 * 截屏
 * @returns
 */
function cutImage(fid,data){
	setTab("tab-4");
	var video = $("#preview").find("video")[0];
	video.pause();
	var pointTime = video.currentTime * 1000;
	var scale = 1;
    if (!data) {
        var canvas = document.createElement("canvas");
        canvas.width = video.videoWidth * scale;
        canvas.height = video.videoHeight * scale;
        canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
        data = canvas.toDataURL("image/png");
    }
    //生成的文件中"data:image/jpeg;base64,"为多余数据，替换掉
   
    data = data.replace("data:image/png;base64,", "");
    
    //表单方式提交数据
    var formData = new FormData();
    formData.append("videoId",setting.videoId);
    formData.append("fileName", "cutImage.png");
    formData.append("pointTime", pointTime);
    //使用URI编码
    formData.append("base64Stream", encodeURIComponent(data));
    $.ajax({
        type: "POST",
        url: ctx+"video/thumb/cutImage",
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
        	if(data.code==0){
        		var html = "";
        		html+='<div class="thumb pull-left"  id="'+data.data.thumbId+'">';
        		html+=' <div class="del_mask" onclick="deleteFrame(this)"><em class="glyphicon glyphicon-remove-circle "></em></div>';
                html+=' <em class="tag"  point-time='+data.data.interceptTime*100+' id="'+data.data.thumbSrc+'"></em>';
               
                html+='<img  onclick="playCurrentTime('+data.data.interceptTime+');" src="'+ctx+data.data.thumbSrc+'" alt="截图">';
                html+='<div class="text-center" >'+data.data.pontTime+'</div>';
                html+='</div>';
        		$(".thumbs").prepend(html);
        	}
        }
    })
}

/**
 * 添加分段
 */
function addFragment(in_point_time, out_point_time) {
	setTab("tab-3");
    var url = $("#fragmentImgurl").val();
    if (!in_point_time && $("#myVideo .vjs-shoot-div .vjs-shoot-inpoint").length) {
        in_point_time = Number($("#myVideo .vjs-shoot-div .vjs-shoot-inpoint").attr("data-time"));
    }
    if (!out_point_time && $("#myVideo .vjs-shoot-div .vjs-shoot-outpoint").length) {
        out_point_time = Number($("#myVideo .vjs-shoot-div .vjs-shoot-outpoint").attr("data-time"));
    }
    if (in_point_time !== 0 && !in_point_time) {
        layer.msg("生成片段失败，未打入点");
        return;
    }
    if (!out_point_time) {
        layer.msg("生成片段失败，未打出点");
        return;
    }
    in_point_time > out_point_time ? [in_point_time, out_point_time] = [out_point_time, in_point_time] : "";
    if (out_point_time - in_point_time < 1) {
        layer.msg("生成片段失败，出入点时间间隔过短");
        return;
    }
    if (!url) {
        layer.msg("生成首帧失败，请重新生成片段");
        return;
    }
    var name = "片段层" + ($(".frament_items").length + 1);
    var data = { 
    		videoId: setting.videoId,
    		name:name,
    		thumbUrl:url,
    		start:Math.round(in_point_time * 1000),
    		end: Math.round(out_point_time * 1000)
    };
    $.operate.save(ctx + "video/frament/add",data,function(result){
	    $.modal.closeLoading();
	    if (result.code == web_status.SUCCESS) {
	    	 initFdetails(result.data);
             $.modal.msgSuccess(result.msg);
        }else{
   	         $.modal.msgError(result.msg);
        }
    });
  
}
/**
 * 删除分段
 */
function delFrament(ids, obj) {
    parent.$.modal.confirm("您确认要删除该片段吗?", function() {
		 $.operate.save(ctx + "video/frament/remove",{ids:ids},function(result){
		    $.modal.closeLoading();
		    if (result.code == web_status.SUCCESS) {
		      
		       $(obj).parents(".frament_items").remove();
	           $.modal.msgSuccess(result.msg);
	        }else{
	   	       $.modal.msgError(result.msg);
	        }
	    });
	 });
}
/**
 * 初始化片段信息
 */
function initFdetails(data){
	console.log(data);
	var time = initPoint(data.end - data.start).substring(0, 8);
	var str = "";
	str+='<div class="frament_items clear" id="'+data.framentId+'" framentid="'+data.framentId+'" start-point="'+data.start+'" end-point="'+data.end+'">';
	str+='<div class="frament_title col-md-8 pr5" onclick="choseFragment(this,'+data.framentId+')">'+data.name+'</div>';
    str+='<div class="frament_time col-md-4 text-right">'+time+'<span class="m"><a href="javascript:void(0)" onclick="javascript:delFrament('+data.framentId+',this)"><i class="glyphicon glyphicon-trash"></i></a> </span></div>';
    str+='</div>';
    $(".framentList").append(str);
}
function choseFragment(obj,id){
    var videoObj = $("#preview").find("video").get(0);
    var start_time = $(obj).parent().attr("start-point");
    var end_time = $(obj).parent().attr("end-point");
    var time_obj = [{type: 0, targetTime: Number(start_time) / 1000}, {type: 1, targetTime: Number(end_time) / 1000}];
  
    videoPlayCallBack.videojsVideoFragment("myVideo", time_obj);
    videoObj.pause();
}
/**
 * 切换tab
 * @param tabId
 * @returns
 */
function setTab(tabId){
	 $('a[href="#'+tabId+'"]').parent().addClass("active").siblings("li").removeClass("active");
	 $(".tab-content .tab-pane").removeClass("active");
     $("#"+tabId).addClass("active");
}
/**
 * 视频帧转换成时间
 */
function initPoint(time) {
    var currentTime = "00:00:00.000";
    var time = time / 1000;
    if (time > 0) {
        var s = parseInt(time / 60);
        if (s < 10) {
            currentTime = "00:0" + s + ":"
        } else {
            currentTime = "00:" + s + ":";
        }
        var ss = parseInt(time % 60);
        if (ss < 10) {
            currentTime += "0" + ss;
        } else {
            currentTime += ss;
        }
        var sss = String(time);
        sss = sss.substring(sss.indexOf("."), sss.indexOf(".") + 4);
        currentTime += sss;
    }
    return currentTime;
}

