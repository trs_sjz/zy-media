/**
 * 首页方法封装处理
 * Copyright (c) 2019 ruoyi
 */
layer.config({
    extend: 'moon/style.css',
    skin: 'layer-ext-moon'
});

$(function() {
    // MetsiMenu
    //$('.side-menu').metisMenu();

    //固定菜单栏
    $(function() {
        $('.sidebar-collapse').slimScroll({
        	 height: 'calc(100% - 53px)',
            railOpacity: 0.9,
            alwaysVisible: false
        });
    });
    
    $('.menuItem').on('click', menuItem);
    
    
    function menuItem(){
    	var id = $(this).parent().parent().attr('id');
    	$(this).parent().siblings().removeClass("active");
    	$(this).parent().parent().parent().siblings().removeClass("active");
    	//判断当前元素id是否被定义，若未定义则说明被点击的标签为二级标签
    	if(typeof(id) != "undefined"){
    		$(this).addClass("active");
    	}else{
    		$(this).parent().addClass("active");
    	}
        var modalid =  $(".nav-menu>ul>li[class='active']>a").data("modalid");
        var nodeid=$(this).parent("li").attr("id");
     
        location.hash=$(this).attr("href")+"?modalid="+modalid+"&nodeid="+nodeid;
       
      
    }
    
    
  
    //ios浏览器兼容性处理
    if (/(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent)) {
        $('#content-main').css('overflow-y', 'auto');
    }

});

$(window).bind("load resize",
function() {
    if ($(this).width() < 769) {
        $('body').addClass('mini-navbar');
        $('.navbar-static-side').fadeIn();
        $(".sidebar-collapse .logo").addClass("hide");
        $(".slimScrollDiv").css({ "overflow":"hidden" })
    }
});



/**
 * iframe处理
 */
$(function() {
    //计算元素集合的总宽度
    function calSumWidth(elements) {
        var width = 0;
        $(elements).each(function() {
            width += $(this).outerWidth(true);
        });
        return width;
    }
    
   
    window.onhashchange = function() {
    	var hash = location.hash;
        var url = hash.substring(1, hash.length);
        $('a[href$="' + url + '"]').click();
    };

});