var OnairJsUtil = {
    /**获取uuid*/
    getUuid: function () {
        var s = [];
        var hexDigits = "0123456789abcdef";
        for (var i = 0; i < 36; i++) {
            s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
        }
        s[14] = "4"; // bits 12-15 of the time_hi_and_version field to 0010
        s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1); // bits 6-7 of the clock_seq_hi_and_reserved to 01
        s[8] = s[13] = s[18] = s[23] = "-";
        var uuid = s.join("");
        return uuid;
    },
}

/***************弹窗控制***********************/
$(function () {
   
    $(".upload_cont h4 em.min_upload_icon").unbind().click(function () {
        $(this).toggleClass("max_upload_icon");
        $(this).parent().parent().toggleClass("max");
    });

    $(".upload_cont h4 em.close_upload_icon").unbind().click(function () {
        $(this).parent().parent().css("visibility", "hidden");
    });

    //MD5校验触发按钮
    $(".upload_btn p span em").unbind().click(function () {
        $(this).toggleClass("active");
    });
//	var UnloadConfirm = {};
//		UnloadConfirm.MSG_UNLOAD = "有附件正在上传\n\n您确定要离开吗？";
//		UnloadConfirm.set = function(a) {
//			window.onbeforeunload = function(b) {
//				b = b || window.event;
//				b.returnValue = a;
//				return a;
//			};
//		};
//
//		UnloadConfirm.clear = function() {
//			fckDraft.delDraftById();
//			window.onbeforeunload = function(b) {
//				b = b || window.event;
//				b.returnValue = a;
//				return a;
//			};
//		};
//		UnloadConfirm.set(UnloadConfirm.MSG_UNLOAD);
});
var LOGTITLE = $(".treeview-menu li.active a").html();//用于统计日志名称
var uploadStatus = true;

var upload_count = 0;
var upload_success_count = 0;
var upload_fail_count = 0;
var upload_reset = 0;

var supportTransition = (function(){
    var s = document.createElement('p').style,
    r = 'transition' in s ||
          'WebkitTransition' in s ||
          'MozTransition' in s ||
          'msTransition' in s ||
          'OTransition' in s;
s = null;
return r;
})();

/***********上传配置*********************/
var uploader = WebUploader.create({
    swf: "js/Uploader.swf",//上传的flash
    server: backEndUrl + "fileUpload",//后台上传方法
    pick: "#picker",
    resize: false,
    // dnd : "#upload_btn",
    disableWidgets: "log",
    paste: document.body,
    disableGlobalDnd: true,
    multiple: true,
    thumb: {
        width: 100,
        height: 100,
        quality: 100,
        allowMagnify: false,
        crop: true
    },
    compress: false,
    prepareNextFile: true,
    chunked: true,
    chunkSize: chunkSize,
    threads: "5",
    formData: {
    },
    fileSizeLimit: 100 * 1024 * 1024 * 1024,
    fileSingleSizeLimit: 100 * 1024 * 1024 * 1024,
    duplicate: true,
    auto: true,
    //自定义上传类型
    accept: {
        title: '',
        extensions: config.extensions,
        mimeTypes: config.mimeTypes
    }
});
uploader.addButton({
    id: '#pickerFolder',
});
uploadInit = uploader;
var file_path = {};
/************文件加入上传队列执行*******************/
uploader.on('uploadBeforeSend', function (object, data, headers) {
	uploadFinishStatus = false;
    var webkitRelativePath = (object.file.source && object.file.source.source && object.file.source.source.webkitRelativePath) ? object.file.source.source.webkitRelativePath : '';
    var relative_path = webkitRelativePath.split("/");
    var sourceRelativePath = relative_path.slice(0, relative_path.length - 1);
    relative_path.forEach(function (item, idx) {
        var name = sourceRelativePath.slice(0, relative_path.length - idx).join("/");
        if (!file_path[name]) {
            file_path[name] = OnairJsUtil.getUuid();
        }
    });
});
uploader.on("fileQueued", function (file) {
    upload_count++;
    if(file.ext == "zip" || file.ext == "rar" || file.ext == "7z"){
		//alert("暂不支持上传压缩包");
		parent.$.modal.msgError("暂不支持上传压缩包");
		return;
	}
    $('#upload_cont').css('visibility', 'visible');
    ospeedMap.put(file.id, 0);
    uploader.sort(function (a, b) {
        return a.size - b.size;
    });//多文件同时加入时小文件优先上传
    var uniqueFileName = md5(file.name + file.size + file.type + userId);//文件唯一标识
    uniqueFileNameMap.put(file.id, uniqueFileName);
    var fileSize = WebUploader.Base.formatSize(file.size, 2, ['B', 'KB', 'MB', 'G', 'TB']);
    $("#progress_cont").append('<div class="progress_div" id="' + file.id + '"><p class="imgWrap"><img /></p>'
    	+'<div style="float: right;width: 70%;"><span style="float: left;margin-top: 17px;">图片描述：</span><textarea class="form-control  input-remark" rows="1" placeholder="请输入图片说明" style="width: 80%;margin-top: 10px;"></textarea>'
    	+'<span style="float: left;margin-top: 17px;">图片价格：</span><input  placeholder="请输入价格（单位：元）" class="form-control input-price"  type="text" autocomplete="off" style="width: 60%;margin-top: 10px;"></div>'
    	+' <p style="float: right;width: 70%;margin-top: 10px;"">图片名称：' + file.name + '&nbsp;&nbsp;图片大小：' + fileSize + '</p><div class="progress" id="progress' + file.id + '"></div>');
    if (checkType == 1) {
        $("#progress" + file.id).append('<div class="md5_pro">'
            + '<span class="pro_color" style="width: 0%;"></span><span class="jindu"></span></div>'
            + '<div class="upload_pro"><span class="pro_color "  style="width: 0%;"></span><span class="jindu"></span></div>'
            + '</div>');
        //$("#" + file.id).append('<div class="operate"><em class="reset_btn" style="display:none" ></em><em class="del_btn" style="display:none" ></em></div><div class="clear"></div>');
    } else {
        $("#progress" + file.id).append('<div class="upload_pro"><span class="pro_color "  style="width: 0%;"></span><span class="jindu"></span></div>' + '</div>');
        //$("#" + file.id).append('<div class="operate"><em class="reset_btn" style="display:none" ></em><em class="del_btn"></em></div><div class="clear"></div>');
    }
    $progress_div = $("#" + file.id);
    $btns = $('<div class="file-panel">' +
            '<span class="cancel">删除</span>' +
            '<span class="rotateRight">向右旋转</span>' +
            '<span class="rotateLeft">向左旋转</span></div>').appendTo( $progress_div );
    
    $wrap = $progress_div.find( 'p.imgWrap' );
    
    var $img = $("#" + file.id).find("img");
    uploader.makeThumb(file, function (error, src) {
        if (error) {
            $img.replaceWith("");
        }
        $img.attr("src", src);
    });
    
   $("#" + file.id).on('mouseenter', function() {
        $(this).find(".file-panel").stop().animate({height: 30});
   });
   
   $("#" + file.id).on('mouseleave', function() {
	    $(this).find(".file-panel").stop().animate({height: 0});
   });
   file.rotation = 0;
   $("#" + file.id).on('click', '.file-panel span', function() {
	    var index = $(this).index(),
	        deg;
	   
	    switch ( index ) {
	        case 0:
	        	var id = $(this).parent().parent().attr("id");
	        	
	        	var pictureId = $("#"+id).attr("data-pictureid");
	        	deletePic(pictureId);//删除上传图片
	        	
	        	uniqueFileNameMap.remove(id);
        	    filemd5MarkMap.remove(id);
        	    timeMap.remove(id);
        	    sizeMap.remove(id);
        	    ospeedMap.remove(id);
        	    otimeMap.remove(id);
        	    uploader.removeFile(id, true); //从上传文件列表中删除
        	    $(this).parent().parent().remove(); //从上传列表dom中删除
	            return;

	        case 1:
	            file.rotation += 90;
	            break;

	        case 2:
	            file.rotation -= 90;
	            break;
	    }
	   
	    $img.rotate({animateTo:file.rotation});


	});
    
});




/***********上传进度获取**************/
uploader.on("uploadProgress", function (file, percentage, uploaded) {
	//console.log(percentage)
    var num = percentage * 100;
    var osize = sizeMap.get(file.id); // 前大小
    var size = file.size * percentage;// 现大小
    sizeMap.put(file.id, size);
    var sizesp = file.size - size;// 剩余大小
    var sizedf = size - osize;//大小差
    
    var otime = timeMap.get(file.id);//前时间
    var time = new Date().getTime();//现时间
    timeMap.put(file.id, time);

    var timedf = (time - otime) / 1000;//时间差
    //console.log("N时间：" + time);
    //console.log("O时间：" + otime);
    //console.log("时间差：" + timedf);
    var timesp = 0;
    var speed = ospeedMap.get(file.id);
   
    if (timedf > 0.068 && timedf < 0.088 && sizedf != 0) {
        speed = Math.round(sizedf / timedf / 1024);
        ospeedMap.put(file.id, speed);
        //console.log("大小差：" + sizedf);
        //console.log("时间差：" + timedf);
        //console.log("速度：" + speed);
    }
   
    if (speed != 0) {
        timesp = Math.round(sizesp / speed / 1000);
    }
    //console.log("大小差：" + sizedf);
    //console.log("剩余时长：" + timesp+"===");
    //console.log("剩余时长："+formatSeconds(timesp));
    // $("#" + file.id + " .operate .pause_btn").show();
    $("#" + file.id + " .operate .reset_btn").hide();
    $("#" + file.id + " .progress .upload_pro .pro_color").width(num + "%");
    $("#" + file.id + " .progress .upload_pro .jindu").text(
        "上传进度：" + num.toFixed(2) + "%" + "  速率：" + formatSpeed(speed * 1000)
        + "  剩余时长：" + formatSeconds(timesp));
});


/************执行操作*****************/
$("#progress_cont").on("click", " .progress_div .operate .play_btn",
    function () {
        var id = $(this).parent().parent().attr("id");
        var file = uploader.getFile(id);
        uploader.upload(file);
        $(this).removeClass("play_btn").addClass("pause_btn");
    });


/************暂停操作*****************/
$("#progress_cont ").on("click", " .progress_div .operate .pause_btn",
    function () {
        var id = $(this).parent().parent().attr("id");
        var file = uploader.getFile(id);
        uploader.stop(file);
        $(this).removeClass("pause_btn").addClass("play_btn");
});


/************删除操作***************/
$("#progress_cont ").on("click", " .progress_div .operate .del_btn", function () {
    var id = $(this).parent().parent().attr("id");
    uniqueFileNameMap.remove(id);
    filemd5MarkMap.remove(id);
    timeMap.remove(id);
    sizeMap.remove(id);
    ospeedMap.remove(id);
    otimeMap.remove(id);
    uploader.removeFile(id, true); //从上传文件列表中删除
    $(this).parent().parent().remove(); //从上传列表dom中删除
});

/************重试操作****************/
$("#progress_cont ").on("click", " .progress_div .operate .reset_btn", function () {
    upload_reset++;
    var id = $(this).parent().parent().attr("id");
    var file = uploader.getFile(id);
    var uniqueFileName = md5(file.name + file.size + file.type + userId);
    uniqueFileNameMap.put(file.id, uniqueFileName);
    uploader.retry(file);
});
/************全部上传完成****************/
uploader.on("uploadFinished", function () {
    if(typeof(eval(uploadFinished))=="function"){
		 uploadFinished();
    }
    setTimeout(function () {
        if (uploadStatus) {
            $('#upload_cont').css('visibility', 'hidden');
        }
    }, 1000);
});
/************清空列表****************/
$(".empty_btn").click(function () {
	 $(".progress_div").each(function(){
	      var pictureId = $(this).attr("data-pictureid");
	      deletePic(pictureId);
	 });
    $(".yc").remove();
});

var post_file_data = [];

/************上传成功触发*************/
function UploadComlate(file,data) {
    upload_success_count++;
    $("#" + file.id + " .progress .upload_pro .pro_color").width(
        100 + "%");
    $("#" + file.id + " .progress .upload_pro .jindu").text("上传完成");
    $("#" + file.id).addClass("yc");
    timeMap.remove(file.id);
    sizeMap.remove(file.id);
    otimeMap.remove(file.id);
    ospeedMap.remove(file.id);
    if (upload_reset > 0) {
        upload_fail_count--;
        upload_reset--;
        uploadResetSucess(file);
        return;
    }
    $("#" + file.id).attr("data-pictureid",data.ids);  //设置图片ID
    uploadComlateSucess(file,data);
}

function uploadResetSucess(file) {
	  //注册资产
    var isFolder = $(".webuploader-element-invisible").attr("webkitdirectory");    //是否上传文件夹
    if (isFolder != undefined) {
        var data = {
            fpid: $("#" + file.id + " .operate .reset_btn").attr("fpid"),
            src: "页面上传",
            name: file.name,
            mediaUrl: file.path
        };
        if (upload_count === (upload_success_count + upload_fail_count)) {
            uploadSucess([data]);
        } else {
            post_file_data.push(data);
        }
    } else {
        var fpid = sessionStorage.getItem("assetsFolderId");
        var file_data = {mediaUrl: file.path, name: file.name, src: "页面上传"};
        if (fpid) {
            file_data.fpid = fpid;
        }
        post_file_data = [file_data];
        uploadSucess();
    }
}



function uploadSucess(data) {
    
}

/**
 * 注册成功回调方法
 */
function uploadSucessCallback(data) {
    if (typeof queryAssets == "function" && data.code === 0 && data.data && data.data.insertMap) {
        setTimeout(function () {
            post_file_data = [];
            file_path = {};
            queryAssets();
            core_handle.get_structure();
        }, 1000);
    }
}

/************上传失败触发*************/
function UploadFail(file) {
    upload_fail_count++;
    $("#" + file.id + " .progress .upload_pro .pro_color").width(
        100 + "%");
    $("#" + file.id + " .progress .upload_pro .jindu").text("上传失败");
    //	$("#" + file.id + " .operate .play_btn").hide();
    //$("#" + file.id + " .operate .pause_btn").hide();
    timeMap.remove(file.id);
    sizeMap.remove(file.id);
    otimeMap.remove(file.id);
    ospeedMap.remove(file.id);
    $("#" + file.id + " .operate .reset_btn").show();
    var path_name = file.sourceRelativePath.join("/");
    $("#" + file.id + " .operate .reset_btn").attr("fpid", file_path[path_name]);
}

function check() {
    var checkbox = document.getElementById('md5');
    if (checkbox.checked) {
        checkType = 1;
    } else {
        checkType = 0;
    }
}

$(".upload_cont h4 i.close_upload_icon").unbind().click(function () {
    $(this).parent().parent().css("visibility", "hidden");
});


//MD5校验
$(".upload_btn p span i").unbind().click(function () {
    $(this).toggleClass("active");
});

function formatSeconds(time) {
    var total = time || 0, hour = Math.floor(total / 3600), minute = Math.floor((total - 3600 * hour) / 60),
        second = Math.floor(total - 3600 * hour - 60 * minute),
        hour = "" + (!isNaN(hour) && 0 < hour ? (hour < 10 ? ("0" + hour + ":") : (hour + ":")) : "00:"),
        hour = hour + (!isNaN(minute) && 0 < minute ? (minute < 10 ? ("0" + minute + ":") : minute + ":") : "00:");
    return hour += !isNaN(second) && 0 < second ? (second < 10 ? ("0" + second + "") : second) : "00";
}

function formatSpeed(a) {
    var b = 0;
    1024 <= Math.round(a / 1024)
        ? (b = Math.round(100 * (a / 1048576)) / 100, b = Math.max(0, b), b = isNaN(b) ? 0 : parseFloat(b).toFixed(2), a = b + "MB/s")
        : (b = Math.round(100 * (a / 1024)) / 100, b = Math.max(0, b), b = isNaN(b) ? 0 : parseFloat(b).toFixed(2), a = b + "KB/s");
    return a;
}
