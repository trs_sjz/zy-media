$("#md5").attr("checked", false);
var checkType = 0;
var userId = config.userId;
var groupId = config.groupId?config.groupId:0;
var chunkSize = 5 * 1024 * 1024; //分块大小
var uniqueFileNameMap = new Map();
var filemd5MarkMap = new Map();
/*******获取速率使用*********/
var timeMap = new Map();
var sizeMap = new Map();
var otimeMap = new Map();
var ospeedMap = new Map();
var uploadInit = null;
var isFolder = false;
var uploadFinishStatus = false;

var backEndUrl = config.uploadUrl;
/*************wu上传初始****************/
new WebUploader.Uploader.register(
    {
        "before-send-file": "beforeSendFile",
        "before-send": "beforeSend",
        "after-send-file": "afterSendFile"
    },
    {  //上传文件开始触发
        beforeSendFile: function (file) {
            //秒传验证
            var task = new $.Deferred();
            var start = new Date().getTime();
            var md5Mark;
            file.relativePath = (file.source && file.source.source && file.source.source.webkitRelativePath) ? file.source.source.webkitRelativePath : '';
            if (file.relativePath) {
                file.fid = OnairJsUtil.getUuid();
            }
            var relative_path = file.relativePath.split("/");
            file.sourceRelativePath = relative_path.slice(0, relative_path.length - 1);
            if (checkType == 1) {
                uploadInit.md5File(file).progress(
                    function (percentage) {
                        $("#" + file.id + " .progress .md5_pro .pro_color").width(percentage * 100 + "%");
                        $("#" + file.id + " .progress .md5_pro .jindu").text("MD5进度：" + Math.round(percentage * 100) + "%");
                        //console.log(percentage);
                    }).then(function (val) {
                    $("#" + file.id + " .operate .del_btn").show();
                    // $("#" + file.id + " .operate .pause_btn").show();
                    //console.log("总耗时: "+ ((new Date().getTime()) - start)/ 1E3);
                    md5Mark = val;
                    filemd5MarkMap.put(file.id, md5Mark);
                    $.ajax({
                        type: "POST",
                        url: backEndUrl + "fileUploadCheck",
                        data: {status: "md5Check", md5: val},
                        cache: false,
                        timeout: 3000000, //to do 超时的话，只能认为该文件不曾上传过
                        dataType: "json"
                    }).then(function (data, textStatus, jqXHR) {
                        var time = new Date().getTime();
                        timeMap.put(file.id, time);
                        if (data.ifExist) { //若存在，这返回失败给WebUploader，表明该文件不需要上传
                            task.reject();
                            uploadInit.skipFile(file);
                            file.path = data.path;
                            $("#" + file.id + " .progress .upload_pro .pro_color").width(100 + "%");
                            if ($("#" + file.id).attr("class").indexOf("batchEdit") > -1) {
                                UploadComlate2(file);
                            } else {
                                UploadComlate(file);
                            }
                        } else {
                            task.resolve();
                        }
                    }, function (jqXHR, textStatus, errorThrown) { //任何形式的验证失败，都触发重新上传
                        task.resolve();
                    });
                });
            } else {
                $("#" + file.id + " .operate .del_btn").show();
                sizeMap.put(file.id, 0);
                otimeMap.put(file.id, 0);
                //  $("#" + file.id + " .operate .pause_btn").show();
                task.resolve();
            }
            return $.when(task);
        },
        //开始上传分片触发
        beforeSend: function (block) {
            //分片验证是否已传过，用于断点续传
            var task = new $.Deferred();
            var file = block.file;
            var id = file.id;
            
            uploadInit.md5File(file, block.start, block.end).then(function (val) {
                $.ajax({
                    type: "POST",
                    url: backEndUrl + "fileUploadCheck",
                    data: {
                        status: "chunkCheck",
                        name: uniqueFileNameMap.get(id),
                        chunkIndex: block.chunk,
                        size: block.end - block.start,
                        blockmd5: val,
                    },
                    cache: false,
                    timeout: 3000000, //to do 超时的话，只能认为该分片未上传过
                    dataType: "json"
                }).then(function (data, textStatus, jqXHR) {
                    var time = new Date().getTime();
                    timeMap.put(file.id, time);
                    if (data.ifExist == 1) { //若存在，返回失败给WebUploader，表明该分块不需要上传
                        task.reject();
                    } else {
                        task.resolve();
                    }
                }, function (jqXHR, textStatus, errorThrown) { //任何形式的验证失败，都触发重新上传
                    task.reject();
                    task.resolve();
                });
            });
            return $.when(task);
        },
        //上传文件完成触发
        afterSendFile: function (file) {
            var chunksTotal = Math.ceil(file.size / chunkSize);
            //		if ((chunksTotal = Math.ceil(file.size
            //				/ chunkSize)) > 1) {
            //合并请求
            var task = new $.Deferred();
            $.ajax({
                type: "POST",
                url: backEndUrl + "fileMergeChunks",
                data: {
                    status: "chunksMerge",
                    name: uniqueFileNameMap.get(file.id),
                    filename:file.name,
                    chunks: chunksTotal,
                    ext: file.ext,
                    groupId:groupId,
                    md5: filemd5MarkMap.get(file.id),
                    type: uniqueFileNameMap.get(file.id+"type"),
                    size: uniqueFileNameMap.get(file.id+"size"),
                },
                cache: false,
                dataType: "json"
            }).then(function (data, textStatus, jqXHR) {
                //to do 检查响应是否正常
                var status = data.status;
                file.path = data.path;
                task.resolve();
                if (status == 1) {
                    if ($("#" + file.id).attr("class").indexOf("batchEdit") > -1) {
                        UploadComlate2(file);
                    } else {
                        UploadComlate(file,data);
                    }
                } else {
                    uploadStatus = false;
                    UploadFail(file);
                }
                uniqueFileNameMap.remove(file.id);
                filemd5MarkMap.remove(file.id);
            }, function (jqXHR, textStatus, errorThrown) {
                task.reject();
            });
            return $.when(task);
            //	} else {
            //		UploadComlate(file);
            //	}
        }
    });

function Map() {
    this.container = new Object();
}

Map.prototype.put = function (key, value) {
    this.container[key] = value;
};

Map.prototype.get = function (key) {
    return this.container[key];
};

Map.prototype.keySet = function () {
    var keyset = new Array();
    var count = 0;
    for (var key in this.container) {
        // 跳过object的extend函数
        if (key == 'extend') {
            continue;
        }
        keyset[count] = key;
        count++;
    }
    return keyset;
};

Map.prototype.size = function () {
    var count = 0;
    for (var key in this.container) {
        // 跳过object的extend函数
        if (key == 'extend') {
            continue;
        }
        count++;
    }
    return count;
};
Map.prototype.remove = function (key) {
    delete this.container[key];
};
