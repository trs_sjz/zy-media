package com.media.web.controller.system;

import com.media.common.annotation.Log;
import com.media.common.config.MediaConfig;
import com.media.common.core.controller.BaseController;
import com.media.common.core.domain.AjaxResult;
import com.media.common.enums.BusinessType;
import com.media.common.utils.file.FileUtils;
import com.media.system.domain.SysSetting;
import com.media.system.service.ISysSettingService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


/**
 * 系统设置表Controller
 * 
 * @author jason
 * @date 2020-07-21
 */
@Controller
@RequestMapping("/system/setting")
public class SysSettingController extends BaseController {
	private String prefix = "system/setting";

	@Autowired
	private ISysSettingService sysSettingService;

	@RequiresPermissions("system:setting:view")
	@GetMapping()
	public String setting(ModelMap mmap) {
		// 获取配置信息
		SysSetting sysSetting = sysSettingService.selectSysSettingById(1);
		mmap.put("setting", sysSetting);
		return prefix + "/setting";
	}

	/**
	 * 修改保存系统设置表
	 */
	@RequiresPermissions("system:setting:edit")
	@Log(title = "系统设置表", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(SysSetting sysSetting) {
		System.out.println(sysSetting);
		return toAjax(sysSettingService.updateSysSetting(sysSetting));
	}

	/**
	 * 上传水印图片
	 */
	@Log(title = "系统设置", businessType = BusinessType.UPLOAD)
	@PostMapping("/fileUpload")
	@ResponseBody
	public AjaxResult fileUpload(@RequestParam MultipartFile file, @RequestParam("settingId") Integer settingId) throws IOException {
		int result = 0;
		// 上传路径
		String waterImageDir = MediaConfig.getProfile() + File.separator + "watermark" + File.separator;
		File waterImagePath = new File(waterImageDir);
		if (!waterImagePath.exists()) {
			// 创建文件夹
			waterImagePath.mkdirs();
		}

		try {
			if (file != null) {
				String fileName = file.getOriginalFilename();
				String fileExt = FileUtils.getFileSuffix(fileName);
				String watermarkName = "watermark." + fileExt;
				//水印上传路径
				String waterImageFile =waterImageDir+ watermarkName;
				File desc = new File(waterImageFile);
				// 上传文件
				file.transferTo(desc);
				
				SysSetting sysSetting = new SysSetting();
				sysSetting.setSettingId(settingId);
				sysSetting.setWatermarkImage("/watermark/"+watermarkName);
				//更新水印
				sysSettingService.updateSysSetting(sysSetting);
				
				Map<String, String> resultMap = new HashMap<String, String>();
				resultMap.put("watermarkPath", "resource/watermark/watermark."+fileExt);
				return AjaxResult.success(resultMap);
			}

		} catch (IOException e) {
			result = 0;
		}
		return toAjax(result);
	}
	
	/**
	 * 上传台标
	 */
	@Log(title = "系统设置", businessType = BusinessType.UPLOAD)
	@PostMapping("/fileVideoLogo")
	@ResponseBody
	public AjaxResult fileVideoLogo(@RequestParam MultipartFile file,@RequestParam("settingId") Integer settingId) throws IOException {
		int result = 0;
		// 上传路径
		String waterImageDir = MediaConfig.getProfile() + File.separator + "videologo" + File.separator;
		File waterImagePath = new File(waterImageDir);
		if (!waterImagePath.exists()) {
			// 创建文件夹
			waterImagePath.mkdirs();
		}

		try {
			if (file != null) {
				String fileName = file.getOriginalFilename();
				String fileExt = FileUtils.getFileSuffix(fileName);
				String videologoName = "videologo." + fileExt;
				//水印上传路径
				String videologoFile =waterImageDir+ videologoName;
				File desc = new File(videologoFile);
				// 上传文件
				file.transferTo(desc);
				
				SysSetting sysSetting = new SysSetting();
				sysSetting.setSettingId(settingId);
				sysSetting.setVideologoImage("/videologo/"+videologoName);
				//更新台标
				sysSettingService.updateSysSetting(sysSetting);
				
				Map<String, String> resultMap = new HashMap<String, String>();
				resultMap.put("videologoPath", "resource/videologo/videologo."+fileExt);
				return AjaxResult.success(resultMap);
			}

		} catch (IOException e) {
			result = 0;
		}
		return toAjax(result);
	}
}
