package com.media.framework.web.domain.response;

/**
 * 返回文件状态
 * @program: media
 * @description: CheckChunkResult
 * @author: 庄霸.liziye
 * @create: 2022-03-15 09:50
 **/
public class CheckChunkResult {
    /**
     * 文件是否存在
     */
    public int ifExist;

    public CheckChunkResult(int ifExist) {
        super();
        this.ifExist = ifExist;
    }

    public CheckChunkResult() {
        super();
    }

    public int getIfExist() {
        return ifExist;
    }

    public void setIfExist(int ifExist) {
        this.ifExist = ifExist;
    }
}
