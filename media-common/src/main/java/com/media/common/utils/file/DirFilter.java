package com.media.common.utils.file;

import java.io.File;
import java.io.FilenameFilter;
import java.util.regex.Pattern;

public class DirFilter implements FilenameFilter {
	private Pattern pattern;

	public DirFilter(String regex) {
		pattern = Pattern.compile(regex);
	}

	public boolean accept(File dir, String name) // 这里貌似没有用到dir
	{
		// Strip path information, search for regex:
		String nameString = new File(name).getName();
		String postfix = nameString.substring(nameString.lastIndexOf(".") + 1);
		return pattern.matcher(postfix).matches();
	}
}
