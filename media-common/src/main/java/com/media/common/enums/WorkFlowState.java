package com.media.common.enums;

/**
 * 视频操作状态
 *
 * @author liziye
 */
public enum WorkFlowState {
    CREATE("1", "创建"), SUBMIT("2", "提交"), REJECT("3", "打回"), RESBMIT("4", "重新提交"), APPROVE("5", "通过"),
    REVOKE("6", "拒绝"),NONE("7","无需执行"), SUC_DONE("9", "流程结束");

    private final String code;
    private final String info;

    WorkFlowState(String code, String info) {
        this.code = code;
        this.info = info;
    }

    public String getCode() {
        return code;
    }

    public String getInfo() {
        return info;
    }
}
