package com.media.common.enums;

/**
 * 资源类型
 *
 * @author liziye
 */
public enum MediaType {
    /**
     * 视频
     */
    VIDEO,
    /**
     * 图片
     */
    PICTURE,
    /**
     * 文章
     */
    ARTICLE,
    /**
     * 数字报
     */
    EPAPER,
}

