package com.media.common.constant;
/**
 * 媒资常量信息
 * 
 * @author jason
 */
public class MediaConstrans {
	  /** 媒资上传分片缓存目录 */
	  public final static String MEDIA_UPLOAD_CHUNKS_PACH = "chunks";
	  /** 媒资上传源目录 */
	  public final static String MEDIA_UPLOAD_SOURCE_PACH = "source";
	  /** 媒资上传源转码目录 */
	  public final static String MEDIA_UPLOAD_PRIVATE_PACH = "private";
	  
	  /** 视频server库 */
	  public final static String VIDEO_TRSSERVER_TABLENAME = "视频库";
	  /** 图片server库 */
	  public final static String PICTURE_TRSSERVER_TABLENAME = "图片库";
	  
	  /** 图片组库server库 */
	  public final static String PICTURE_GROUP_TRSSERVER_TABLENAME = "图片组库";
}
