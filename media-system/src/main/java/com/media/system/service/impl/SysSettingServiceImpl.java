package com.media.system.service.impl;

import com.media.system.domain.SysSetting;
import com.media.system.mapper.SysSettingMapper;
import com.media.system.service.ISysSettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * 系统设置表Service业务层处理
 * 
 * @author jason
 * @date 2020-07-21
 */
@Service
public class SysSettingServiceImpl implements ISysSettingService
{
    @Autowired
    private SysSettingMapper sysSettingMapper;

    /**
     * 查询系统设置表
     * 
     * @param settingId 系统设置表ID
     * @return 系统设置表
     */
    @Override
    public SysSetting selectSysSettingById(Integer settingId)
    {
        return sysSettingMapper.selectSysSettingById(settingId);
    }

    
    /**
     * 修改系统设置表
     * 
     * @param sysSetting 系统设置表
     * @return 结果
     */
    @Override
    public int updateSysSetting(SysSetting sysSetting)
    {
        return sysSettingMapper.updateSysSetting(sysSetting);
    }
}
