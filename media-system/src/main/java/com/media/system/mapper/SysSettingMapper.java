package com.media.system.mapper;


import com.media.system.domain.SysSetting;

/**
 * 系统设置表Mapper接口
 * 
 * @author jason
 * @date 2020-07-21
 */
public interface SysSettingMapper 
{
    /**
     * 查询系统设置表
     * 
     * @param settingId 系统设置表ID
     * @return 系统设置表
     */
    public SysSetting selectSysSettingById(Integer settingId);

  

    /**
     * 修改系统设置表
     * 
     * @param sysSetting 系统设置表
     * @return 结果
     */
    public int updateSysSetting(SysSetting sysSetting);

   
}
