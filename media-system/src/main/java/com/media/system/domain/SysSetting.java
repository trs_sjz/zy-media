package com.media.system.domain;

import com.media.common.annotation.Excel;
import com.media.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 系统设置表对象 sys_setting
 * 
 * @author jason
 * @date 2020-07-21
 */
public class SysSetting extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 设置ID */
    private Integer settingId;

    /** 系统名称 */
    @Excel(name = "系统名称")
    private String name;

    /** 系统全称 */
    @Excel(name = "系统全称")
    private String fullName;

    /** 单位名称 */
    @Excel(name = "单位名称")
    private String company;

    /** 地址 */
    @Excel(name = "地址")
    private String address;

    /** 备案号 */
    @Excel(name = "备案号")
    private String icp;

    /** 地址 */
    @Excel(name = "地址")
    private String telephone;

    /** 1开启水印 0关闭水印 */
    @Excel(name = "1开启水印 0关闭水印")
    private Integer watermarkMode;

    /** 透明度 */
    @Excel(name = "透明度")
    private String watermarkAlpha;

    /** 水印图片 */
    @Excel(name = "水印图片")
    private String watermarkImage;

    /** 水印位置(1-9) */
    @Excel(name = "水印位置(1-9)")
    private Integer watermarkPosition;
    
    /** 1开启水印 0关闭水印 */
    @Excel(name = "1开启台标 0关闭台标")
    private Integer videologoMode;
    
    /** 台标图片 */
    @Excel(name = "台标图片")
    private String videologoImage;

    

    public void setSettingId(Integer settingId) 
    {
        this.settingId = settingId;
    }

    public Integer getSettingId() 
    {
        return settingId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setFullName(String fullName) 
    {
        this.fullName = fullName;
    }

    public String getFullName() 
    {
        return fullName;
    }
    public void setCompany(String company) 
    {
        this.company = company;
    }

    public String getCompany() 
    {
        return company;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setIcp(String icp) 
    {
        this.icp = icp;
    }

    public String getIcp() 
    {
        return icp;
    }
    public void setTelephone(String telephone) 
    {
        this.telephone = telephone;
    }

    public String getTelephone() 
    {
        return telephone;
    }


	public void setWatermarkMode(Integer watermarkMode) 
    {
        this.watermarkMode = watermarkMode;
    }

    public Integer getWatermarkMode() 
    {
        return watermarkMode;
    }
    public void setWatermarkAlpha(String watermarkAlpha) 
    {
        this.watermarkAlpha = watermarkAlpha;
    }

    public String getWatermarkAlpha() 
    {
        return watermarkAlpha;
    }
    public void setWatermarkImage(String watermarkImage) 
    {
        this.watermarkImage = watermarkImage;
    }

    public String getWatermarkImage() 
    {
        return watermarkImage;
    }
    public void setWatermarkPosition(Integer watermarkPosition) 
    {
        this.watermarkPosition = watermarkPosition;
    }

    public Integer getWatermarkPosition() 
    {
        return watermarkPosition;
    }

    
    public Integer getVideologoMode() {
		return videologoMode;
	}

	public void setVideologoMode(Integer videologoMode) {
		this.videologoMode = videologoMode;
	}

	public String getVideologoImage() {
		return videologoImage;
	}

	public void setVideologoImage(String videologoImage) {
		this.videologoImage = videologoImage;
	}


	@Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("settingId", getSettingId())
            .append("name", getName())
            .append("fullName", getFullName())
            .append("company", getCompany())
            .append("address", getAddress())
            .append("icp", getIcp())
            .append("telephone", getTelephone())
            .append("watermarkMode", getWatermarkMode())
            .append("watermarkAlpha", getWatermarkAlpha())
            .append("watermarkImage", getWatermarkImage())
            .append("watermarkPosition", getWatermarkPosition())
            .append("videologoMode", getVideologoMode())
            .append("videologoImage", getVideologoImage())
            .toString();
    }
}
