package com.media.picture.service.impl;

import com.media.common.core.text.Convert;
import com.media.picture.domain.MamPictureView;
import com.media.picture.mapper.MamPictureViewMapper;
import com.media.picture.service.IMamPictureViewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 图片库访问量Service业务层处理
 * 
 * @author liziye
 * @date 2021-12-15
 */
@Service
public class MamPictureViewServiceImpl implements IMamPictureViewService 
{
    @Autowired
    private MamPictureViewMapper mamPictureViewMapper;

    /**
     * 查询图片库访问量
     * 
     * @param viewId 图片库访问量ID
     * @return 图片库访问量
     */
    @Override
    public MamPictureView selectMamPictureViewById(Long viewId)
    {
        return mamPictureViewMapper.selectMamPictureViewById(viewId);
    }

    /**
     * 查询图片库访问量列表
     * 
     * @param mamPictureView 图片库访问量
     * @return 图片库访问量
     */
    @Override
    public List<MamPictureView> selectMamPictureViewList(MamPictureView mamPictureView)
    {
        return mamPictureViewMapper.selectMamPictureViewList(mamPictureView);
    }

    /**
     * 新增图片库访问量
     * 
     * @param mamPictureView 图片库访问量
     * @return 结果
     */
    @Override
    public int insertMamPictureView(MamPictureView mamPictureView)
    {
        return mamPictureViewMapper.insertMamPictureView(mamPictureView);
    }

    /**
     * 修改图片库访问量
     * 
     * @param mamPictureView 图片库访问量
     * @return 结果
     */
    @Override
    public int updateMamPictureView(MamPictureView mamPictureView)
    {
        return mamPictureViewMapper.updateMamPictureView(mamPictureView);
    }

    /**
     * 删除图片库访问量对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMamPictureViewByIds(String ids)
    {
        return mamPictureViewMapper.deleteMamPictureViewByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除图片库访问量信息
     * 
     * @param viewId 图片库访问量ID
     * @return 结果
     */
    @Override
    public int deleteMamPictureViewById(Long viewId)
    {
        return mamPictureViewMapper.deleteMamPictureViewById(viewId);
    }
}
