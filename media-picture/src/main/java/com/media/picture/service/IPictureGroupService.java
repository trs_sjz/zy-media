package com.media.picture.service;

import com.media.picture.domain.PictureGroup;

import java.util.List;

/**
 * 图组信息Service接口
 * 
 * @author liziye
 * @date 2021-04-21
 */
public interface IPictureGroupService 
{
    /**
     * 查询图组信息
     * 
     * @param groupId 图组信息ID
     * @return 图组信息
     */
    public PictureGroup selectPictureGroupById(Long groupId);

    /**
     * 查询图组信息列表
     * 
     * @param pictureGroup 图组信息
     * @return 图组信息集合
     */
    public List<PictureGroup> selectPictureGroupList(PictureGroup pictureGroup);

    /**
     * 新增图组信息
     * 
     * @param pictureGroup 图组信息
     * @return 结果
     */
    public int insertPictureGroup(PictureGroup pictureGroup);

    /**
     * 修改图组信息
     * 
     * @param pictureGroup 图组信息
     * @return 结果
     */
    public int updatePictureGroup(PictureGroup pictureGroup);

    /**
     * 批量删除图组信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deletePictureGroupByIds(String ids);

    /**
     * 删除图组信息信息
     * 
     * @param groupId 图组信息ID
     * @return 结果
     */
    public int deletePictureGroupById(Long groupId);

    /**
     * 设置分组封面
     *
     * @param pictureGroup 图片分组
     * @return 结果
     */
    public int updatePictureCoverId(PictureGroup pictureGroup);
}
