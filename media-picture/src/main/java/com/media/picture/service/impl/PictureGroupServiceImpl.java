package com.media.picture.service.impl;

import com.media.common.core.domain.entity.SysUser;
import com.media.common.core.text.Convert;
import com.media.common.utils.DateUtils;
import com.media.common.utils.ShiroUtils;
import com.media.picture.domain.PictureGroup;
import com.media.picture.mapper.PictureGroupMapper;
import com.media.picture.mapper.PictureMapper;
import com.media.picture.service.IPictureGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 图组信息Service业务层处理
 * 
 * @author liziye
 * @date 2021-04-21
 */
@Service
public class PictureGroupServiceImpl implements IPictureGroupService 
{
    @Autowired
    private PictureGroupMapper pictureGroupMapper;

    @Autowired
    private PictureMapper pictureMapper;

    /**
     * 查询图组信息
     * 
     * @param groupId 图组信息ID
     * @return 图组信息
     */
    @Override
    public PictureGroup selectPictureGroupById(Long groupId)
    {
        return pictureGroupMapper.selectPictureGroupById(groupId);
    }

    /**
     * 查询图组信息列表
     * 
     * @param pictureGroup 图组信息
     * @return 图组信息
     */
    @Override
    public List<PictureGroup> selectPictureGroupList(PictureGroup pictureGroup)
    {
        return pictureGroupMapper.selectPictureGroupList(pictureGroup);
    }

    /**
     * 新增图组信息
     * 
     * @param pictureGroup 图组信息
     * @return 结果
     */
    @Override
    public int insertPictureGroup(PictureGroup pictureGroup)
    {
        SysUser user = ShiroUtils.getSysUser();
        pictureGroup.setCoverId(0);
        pictureGroup.setDeptId(user.getDeptId().intValue());
        pictureGroup.setCreateBy(user.getLoginName());
        pictureGroup.setCreateTime(DateUtils.getNowDate());
        return pictureGroupMapper.insertPictureGroup(pictureGroup);
    }

    /**
     * 修改图组信息
     * 
     * @param pictureGroup 图组信息
     * @return 结果
     */
    @Override
    public int updatePictureGroup(PictureGroup pictureGroup)
    {
        pictureGroup.setUpdateBy(ShiroUtils.getLoginName());
        pictureGroup.setUpdateTime(DateUtils.getNowDate());
        return pictureGroupMapper.updatePictureGroup(pictureGroup);
    }

    /**
     * 删除图组信息对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deletePictureGroupByIds(String ids)
    {
        String[] groupIds = Convert.toStrArray(ids);
        for (int i = 0; i < groupIds.length; i++) {
            int picSize = pictureMapper.selectPictureByGroupId(Long.valueOf(groupIds[i])).size();
            if (picSize != 0){
                return 0;
            }
        }
        return pictureGroupMapper.deletePictureGroupByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除图组信息信息
     * 
     * @param groupId 图组信息ID
     * @return 结果
     */
    @Override
    public int deletePictureGroupById(Long groupId)
    {
        return pictureGroupMapper.deletePictureGroupById(groupId);
    }

    /**
     * 修改分组封面
     *
     * @param pictureGroup 图片分组
     * @return 结果
     */
    @Override
    public int updatePictureCoverId(PictureGroup pictureGroup)
    {
        return pictureGroupMapper.updatePictureCoverId(pictureGroup);
    }
}
