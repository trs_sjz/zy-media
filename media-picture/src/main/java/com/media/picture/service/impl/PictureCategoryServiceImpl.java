package com.media.picture.service.impl;

import com.media.common.core.domain.Ztree;
import com.media.common.core.text.Convert;
import com.media.common.utils.DateUtils;
import com.media.common.utils.ShiroUtils;
import com.media.picture.domain.PictureCategory;
import com.media.picture.mapper.PictureCategoryMapper;
import com.media.picture.service.IPictureCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 图片分类信息Service业务层处理
 * 
 * @author liziye
 * @date 2021-04-08
 */
@Service
public class PictureCategoryServiceImpl implements IPictureCategoryService 
{
    @Autowired
    private PictureCategoryMapper pictureCategoryMapper;

    /**
     * 查询图片分类信息
     * 
     * @param categoryId 图片分类信息ID
     * @return 图片分类信息
     */
    @Override
    public PictureCategory selectPictureCategoryById(Long categoryId)
    {
        return pictureCategoryMapper.selectPictureCategoryById(categoryId);
    }

    /**
     * 查询图片分类信息列表
     * 
     * @param pictureCategory 图片分类信息
     * @return 图片分类信息
     */
    @Override
    public List<PictureCategory> selectPictureCategoryList(PictureCategory pictureCategory)
    {
        return pictureCategoryMapper.selectPictureCategoryList(pictureCategory);
    }

    /**
     * 新增图片分类信息
     * 
     * @param pictureCategory 图片分类信息
     * @return 结果
     */
    @Override
    public int insertPictureCategory(PictureCategory pictureCategory)
    {
        pictureCategory.setCreateBy(ShiroUtils.getLoginName());
        pictureCategory.setCreateTime(DateUtils.getNowDate());
        return pictureCategoryMapper.insertPictureCategory(pictureCategory);
    }

    /**
     * 修改图片分类信息
     * 
     * @param pictureCategory 图片分类信息
     * @return 结果
     */
    @Override
    public int updatePictureCategory(PictureCategory pictureCategory)
    {
        pictureCategory.setUpdateBy(ShiroUtils.getLoginName());
        pictureCategory.setUpdateTime(DateUtils.getNowDate());
        return pictureCategoryMapper.updatePictureCategory(pictureCategory);
    }

    /**
     * 删除图片分类信息对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deletePictureCategoryByIds(String ids)
    {
        return pictureCategoryMapper.deletePictureCategoryByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除图片分类信息信息
     * 
     * @param categoryId 图片分类信息ID
     * @return 结果
     */
    @Override
    public int deletePictureCategoryById(Long categoryId)
    {
        return pictureCategoryMapper.deletePictureCategoryById(categoryId);
    }

    /**
     * 查询图片分类信息树列表
     * 
     * @return 所有图片分类信息信息
     */
    @Override
    public List<Ztree> selectPictureCategoryTree()
    {
        List<PictureCategory> pictureCategoryList = pictureCategoryMapper.selectPictureCategoryList(new PictureCategory());
        List<Ztree> ztrees = new ArrayList<Ztree>();
        for (PictureCategory pictureCategory : pictureCategoryList)
        {
            Ztree ztree = new Ztree();
            ztree.setId(pictureCategory.getCategoryId());
            ztree.setpId(pictureCategory.getParentId());
            ztree.setName(pictureCategory.getName());
            ztree.setTitle(pictureCategory.getName());
            ztrees.add(ztree);
        }
        return ztrees;
    }
}
