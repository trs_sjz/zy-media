package com.media.picture.service;

import com.media.picture.domain.Picture;
import com.media.picture.domain.PictureGroup;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * 图片信息Service接口
 * 
 * @author liziye
 * @date 2021-04-22
 */
public interface IPictureService 
{
    /**
     * 查询图片信息
     * 
     * @param pictureId 图片信息ID
     * @return 图片信息
     */
    public Picture selectPictureById(Long pictureId);

    /**
     * 查询图片信息列表
     * 
     * @param picture 图片信息
     * @return 图片信息集合
     */
    public List<Picture> selectPictureList(Picture picture);

    /**
     * 新增图片信息
     * 
     * @param picture 图片信息
     * @return 结果
     */
    public int insertPicture(Picture picture);

    /**
     * 修改图片信息
     * 
     * @param picture 图片信息
     * @return 结果
     */
    public int updatePicture(Picture picture);

    /**
     * 批量删除图片信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deletePictureByIds(String ids);

    /**
     * 删除图片信息信息
     * 
     * @param pictureId 图片信息ID
     * @return 结果
     */
    public int deletePictureById(Long pictureId);

    /**
     * 软删除图片信息
     *
     * @param pictureId 图片信息ID
     * @return 结果
     */
    public int softDeletePictureById(Long pictureId);

    /**
     * 上传图片
     *
     * @param file 图片文件
     * @param groupId 图片所属组id
     * @return 结果
     */
    public int uploadPicture(MultipartFile file, String groupId) throws IOException;

    /**
     * 查询图片信息
     *
     * @param groupId 图组ID
     * @return 图片信息
     */
    public List<Picture> selectPictureByGroupId(Long groupId);

    /**
     * 批量还原软删除图片信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int recoverPictureByIds(String ids);

}
