package com.media.picture.service.impl;

import com.media.common.config.MediaConfig;
import com.media.common.core.domain.entity.SysUser;
import com.media.common.core.text.Convert;
import com.media.common.utils.DateUtils;
import com.media.common.utils.ShiroUtils;
import com.media.common.utils.file.FileUploadUtils;
import com.media.picture.domain.Picture;
import com.media.picture.domain.PictureGroup;
import com.media.picture.mapper.PictureGroupMapper;
import com.media.picture.mapper.PictureMapper;
import com.media.picture.service.IPictureService;
import com.media.picture.utils.PictureUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 图片信息Service业务层处理
 * 
 * @author liziye
 * @date 2021-04-22
 */
@Service
public class PictureServiceImpl implements IPictureService 
{
    @Autowired
    private PictureMapper pictureMapper;

    @Autowired
    private PictureGroupMapper pictureGroupMapper;

    /**
     * 查询图片信息
     * 
     * @param pictureId 图片信息ID
     * @return 图片信息
     */
    @Override
    public Picture selectPictureById(Long pictureId)
    {
        return pictureMapper.selectPictureById(pictureId);
    }

    /**
     * 查询图片信息列表
     * 
     * @param picture 图片信息
     * @return 图片信息
     */
    @Override
    public List<Picture> selectPictureList(Picture picture)
    {
        return pictureMapper.selectPictureList(picture);
    }

    /**
     * 新增图片信息
     * 
     * @param picture 图片信息
     * @return 结果
     */
    @Override
    public int insertPicture(Picture picture)
    {
        picture.setCreateTime(DateUtils.getNowDate());
        return pictureMapper.insertPicture(picture);
    }

    /**
     * 修改图片信息
     * 
     * @param picture 图片信息
     * @return 结果
     */
    @Override
    public int updatePicture(Picture picture)
    {
        picture.setUpdateTime(DateUtils.getNowDate());
        return pictureMapper.updatePicture(picture);
    }

    /**
     * 删除图片信息对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deletePictureByIds(String ids)
    {
        return pictureMapper.deletePictureByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除图片信息信息
     * 
     * @param pictureId 图片信息ID
     * @return 结果
     */
    @Override
    public int deletePictureById(Long pictureId)
    {
        return pictureMapper.deletePictureById(pictureId);
    }

    @Override
    public int softDeletePictureById(Long pictureId) {
        //更新图片状态
        Picture picture = pictureMapper.selectPictureById(pictureId);
        picture.setDelFlag("2");
        picture.setDeleteBy(ShiroUtils.getSysUser().getLoginName());
        picture.setDeleteTime(DateUtils.getNowDate());
        pictureMapper.updatePicture(picture);
        //更新图片张数
        Long groupId = picture.getGroupId();
        PictureGroup pictureGroup = pictureGroupMapper.selectPictureGroupById(groupId);
        pictureGroup.setPictureQuantity(pictureGroup.getPictureQuantity()-1);
        return pictureGroupMapper.updatePictureGroup(pictureGroup);
    }

    /**
     * 还原软删除图片信息对象
     *
     * @param ids 需要还原的数据ID
     * @return 结果
     */
    @Override
    public int recoverPictureByIds(String ids)
    {
        return pictureMapper.recoverPictureByIds(Convert.toStrArray(ids));
    }


    @Override
    public int uploadPicture(MultipartFile file, String groupId){
        int result = 1;
        SysUser currentUser = ShiroUtils.getSysUser();
        try {
            Picture picture = new Picture();
            String picPath = FileUploadUtils.upload(MediaConfig.getUploadPath(), file);
            PictureUtils.getPictureMetadata(picPath,picture);
            picture.setGroupId(Long.valueOf(groupId));
            picture.setTitle(file.getOriginalFilename());
            picture.setFilename(file.getOriginalFilename());
            picture.setSubpath(picPath);
            picture.setFileext(file.getContentType());
            picture.setFilesize(file.getSize());
            picture.setCreateBy(currentUser.getLoginName());
            picture.setCreateTime(DateUtils.getNowDate());
            pictureMapper.insertPicture(picture);
        }catch (Exception e){
            result = 0;
            return result;
        }
        return result;
    }

    @Override
    public List<Picture> selectPictureByGroupId(Long groupId) {
        return pictureMapper.selectPictureByGroupId(groupId);
    }

}
