package com.media.picture.mapper;

import com.media.picture.domain.PictureGroup;

import java.util.List;

/**
 * 图组信息Mapper接口
 * 
 * @author liziye
 * @date 2021-04-21
 */
public interface PictureGroupMapper 
{
    /**
     * 查询图组信息
     * 
     * @param groupId 图组信息ID
     * @return 图组信息
     */
    public PictureGroup selectPictureGroupById(Long groupId);

    /**
     * 查询图组信息列表
     * 
     * @param pictureGroup 图组信息
     * @return 图组信息集合
     */
    public List<PictureGroup> selectPictureGroupList(PictureGroup pictureGroup);

    /**
     * 新增图组信息
     * 
     * @param pictureGroup 图组信息
     * @return 结果
     */
    public int insertPictureGroup(PictureGroup pictureGroup);

    /**
     * 修改图组信息
     * 
     * @param pictureGroup 图组信息
     * @return 结果
     */
    public int updatePictureGroup(PictureGroup pictureGroup);

    /**
     * 删除图组信息
     * 
     * @param groupId 图组信息ID
     * @return 结果
     */
    public int deletePictureGroupById(Long groupId);

    /**
     * 批量删除图组信息
     * 
     * @param groupIds 需要删除的数据ID
     * @return 结果
     */
    public int deletePictureGroupByIds(String[] groupIds);

    /**
     * 修改图片分组封面
     *
     * @param pictureGroup 图片分组
     * @return 结果
     */
    public int updatePictureCoverId(PictureGroup pictureGroup);
}
