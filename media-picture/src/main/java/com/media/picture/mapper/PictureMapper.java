package com.media.picture.mapper;

import com.media.picture.domain.Picture;
import com.media.picture.domain.PictureGroup;

import java.util.List;

/**
 * 图片信息Mapper接口
 * 
 * @author liziye
 * @date 2021-04-22
 */
public interface PictureMapper 
{
    /**
     * 查询图片信息
     * 
     * @param pictureId 图片信息ID
     * @return 图片信息
     */
    public Picture selectPictureById(Long pictureId);

    /**
     * 查询图片信息列表
     * 
     * @param picture 图片信息
     * @return 图片信息集合
     */
    public List<Picture> selectPictureList(Picture picture);

    /**
     * 新增图片信息
     * 
     * @param picture 图片信息
     * @return 结果
     */
    public int insertPicture(Picture picture);

    /**
     * 修改图片信息
     * 
     * @param picture 图片信息
     * @return 结果
     */
    public int updatePicture(Picture picture);

    /**
     * 删除图片信息
     * 
     * @param pictureId 图片信息ID
     * @return 结果
     */
    public int deletePictureById(Long pictureId);

    /**
     * 软删除图片信息
     *
     * @param pictureId 图片信息ID
     * @return 结果
     */
    public int softDeletePictureById(Long pictureId);

    /**
     * 批量删除图片信息
     * 
     * @param pictureIds 需要删除的数据ID
     * @return 结果
     */
    public int deletePictureByIds(String[] pictureIds);

    /**
     * 查询图片信息列表
     *
     * @param groupId 图组id
     * @return 图片信息集合
     */
    public List<Picture> selectPictureByGroupId(Long groupId);

    /**
     * 批量还原软删除图片
     *
     * @param pictureIds 需要还原的数据ID
     * @return 结果
     */
    public int recoverPictureByIds(String[] pictureIds);

}
