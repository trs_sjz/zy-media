package com.media.picture.mapper;

import com.media.picture.domain.MamPictureView;

import java.util.List;

/**
 * 图片库访问量Mapper接口
 * 
 * @author liziye
 * @date 2021-12-15
 */
public interface MamPictureViewMapper 
{
    /**
     * 查询图片库访问量
     * 
     * @param viewId 图片库访问量ID
     * @return 图片库访问量
     */
    public MamPictureView selectMamPictureViewById(Long viewId);

    /**
     * 查询图片库访问量列表
     * 
     * @param mamPictureView 图片库访问量
     * @return 图片库访问量集合
     */
    public List<MamPictureView> selectMamPictureViewList(MamPictureView mamPictureView);

    /**
     * 新增图片库访问量
     * 
     * @param mamPictureView 图片库访问量
     * @return 结果
     */
    public int insertMamPictureView(MamPictureView mamPictureView);

    /**
     * 修改图片库访问量
     * 
     * @param mamPictureView 图片库访问量
     * @return 结果
     */
    public int updateMamPictureView(MamPictureView mamPictureView);

    /**
     * 删除图片库访问量
     * 
     * @param viewId 图片库访问量ID
     * @return 结果
     */
    public int deleteMamPictureViewById(Long viewId);

    /**
     * 批量删除图片库访问量
     * 
     * @param viewIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteMamPictureViewByIds(String[] viewIds);
}
