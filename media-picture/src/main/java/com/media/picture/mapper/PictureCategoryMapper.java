package com.media.picture.mapper;

import com.media.picture.domain.PictureCategory;

import java.util.List;

/**
 * 图片分类信息Mapper接口
 * 
 * @author liziye
 * @date 2021-04-08
 */
public interface PictureCategoryMapper 
{
    /**
     * 查询图片分类信息
     * 
     * @param categoryId 图片分类信息ID
     * @return 图片分类信息
     */
    public PictureCategory selectPictureCategoryById(Long categoryId);

    /**
     * 查询图片分类信息列表
     * 
     * @param pictureCategory 图片分类信息
     * @return 图片分类信息集合
     */
    public List<PictureCategory> selectPictureCategoryList(PictureCategory pictureCategory);

    /**
     * 新增图片分类信息
     * 
     * @param pictureCategory 图片分类信息
     * @return 结果
     */
    public int insertPictureCategory(PictureCategory pictureCategory);

    /**
     * 修改图片分类信息
     * 
     * @param pictureCategory 图片分类信息
     * @return 结果
     */
    public int updatePictureCategory(PictureCategory pictureCategory);

    /**
     * 删除图片分类信息
     * 
     * @param categoryId 图片分类信息ID
     * @return 结果
     */
    public int deletePictureCategoryById(Long categoryId);

    /**
     * 批量删除图片分类信息
     * 
     * @param categoryIds 需要删除的数据ID
     * @return 结果
     */
    public int deletePictureCategoryByIds(String[] categoryIds);
}
