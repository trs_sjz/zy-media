package com.media.picture.controller;

import com.media.common.annotation.Log;
import com.media.common.core.controller.BaseController;
import com.media.common.core.domain.AjaxResult;
import com.media.common.core.page.TableDataInfo;
import com.media.common.enums.BusinessType;
import com.media.picture.domain.Picture;
import com.media.picture.service.IPictureService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 图片回收站Controller
 *
 * @program: ziye
 * @description: PictureRecycleController
 * @author: 庄霸.liziye
 * @create: 2021-04-27 15:56
 **/
@Controller
@RequestMapping("/picture/recycle")
public class PictureRecycleController extends BaseController {

    private String prefix = "picture/recycle";

    @Autowired
    private IPictureService pictureService;

    @RequiresPermissions("picture:recycle:view")
    @GetMapping()
    public String recycle()
    {
        return prefix + "/recycle";
    }

    /**
     * 查询回收站图片信息列表
     */
    @RequiresPermissions("picture:recycle:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Picture picture)
    {
        startPage();
        picture.setDelFlag("2");
        List<Picture> list = pictureService.selectPictureList(picture);
        return getDataTable(list);
    }

    /**
     * 还原软删除图片
     */
    @RequiresPermissions("picture:recycle:recover")
    @Log(title = "图片信息", businessType = BusinessType.UPDATE)
    @PostMapping("/recover")
    @ResponseBody
    public AjaxResult recover(String ids)
    {
        return toAjax(pictureService.recoverPictureByIds(ids));
    }

    /**
     * 删除图片信息
     */
    @RequiresPermissions("picture:picture:remove")
    @Log(title = "图片信息", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(pictureService.deletePictureByIds(ids));
    }
}
