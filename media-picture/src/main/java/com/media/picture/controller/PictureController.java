package com.media.picture.controller;

import com.media.common.annotation.Log;
import com.media.common.core.controller.BaseController;
import com.media.common.core.domain.AjaxResult;
import com.media.common.core.page.TableDataInfo;
import com.media.common.enums.BusinessType;
import com.media.common.utils.RedisUtil;
import com.media.common.utils.poi.ExcelUtil;
import com.media.picture.domain.Picture;
import com.media.picture.domain.PictureGroup;
import com.media.picture.service.IPictureGroupService;
import com.media.picture.service.IPictureService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 图片信息Controller
 * 
 * @author liziye
 * @date 2021-04-22
 */
@Controller
@RequestMapping("/picture/picture")
public class PictureController extends BaseController
{
    private static final Logger log = LoggerFactory.getLogger(PictureController.class);

    private String prefix = "picture/picture";

    @Autowired
    private IPictureService pictureService;

    @Autowired
    private IPictureGroupService pictureGroupService;

    @Autowired
    private RedisUtil redisUtil;

    @RequiresPermissions("picture:picture:view")
    @GetMapping()
    public String picture()
    {
        redisUtil.incr("count",1);
        System.out.println(redisUtil.get("count"));
        return prefix + "/com/media";
    }

    /**
     * 查询图片信息列表
     */
    @RequiresPermissions("picture:picture:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Picture picture)
    {
        startPage();
        List<Picture> list = pictureService.selectPictureList(picture);
        return getDataTable(list);
    }

    /**
     * 导出图片信息列表
     */
    @RequiresPermissions("picture:picture:export")
    @Log(title = "图片信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Picture picture)
    {
        List<Picture> list = pictureService.selectPictureList(picture);
        ExcelUtil<Picture> util = new ExcelUtil<Picture>(Picture.class);
        return util.exportExcel(list, "com/media");
    }

    /**
     * 新增图片信息
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存图片信息
     */
    @RequiresPermissions("picture:picture:add")
    @Log(title = "图片信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Picture picture)
    {
        return toAjax(pictureService.insertPicture(picture));
    }

    /**
     * 修改图片信息
     */
    @GetMapping("/edit/{pictureId}")
    public String edit(@PathVariable("pictureId") Long pictureId, ModelMap mmap)
    {
        Picture picture = pictureService.selectPictureById(pictureId);
        mmap.put("picture", picture);
        return prefix + "/edit";
    }

    /**
     * 修改保存图片信息
     */
    @RequiresPermissions("picture:picture:edit")
    @Log(title = "图片信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Picture picture)
    {
        return toAjax(pictureService.updatePicture(picture));
    }

    /**
     * 删除图片信息
     */
    @RequiresPermissions("picture:picture:remove")
    @Log(title = "图片信息", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(pictureService.deletePictureByIds(ids));
    }

    /**
     * 软删除图片信息
     */
    @RequiresPermissions("picture:picture:remove")
    @Log(title = "图片信息", businessType = BusinessType.DELETE)
    @PostMapping( "/softRemove")
    @ResponseBody
    public AjaxResult softRemove(String pictureId)
    {
        return toAjax(pictureService.softDeletePictureById(Long.valueOf(pictureId)));
    }

    /**
     * 上传图组图片
     */
    @RequiresPermissions("picture:picture:upload")
    @GetMapping("/upload/{groupId}")
    public String upload(@PathVariable("groupId") Long groupId, ModelMap mmap)
    {
        PictureGroup pictureGroup = pictureGroupService.selectPictureGroupById(groupId);
        mmap.put("pictureGroup", pictureGroup);
        return prefix + "/upload";
    }

    /**
     * 上传图片
     */
    @Log(title = "上传图片", businessType = BusinessType.UPLOAD)
    @PostMapping("/uploadPicture")
    @ResponseBody
    public AjaxResult uploadPicture(MultipartFile file, String groupId)
    {
        System.out.println("groupId = " + groupId);
        try
        {
            if (!file.isEmpty())
            {
                pictureService.uploadPicture(file,groupId);
                //上传成功后更新图组张图
                PictureGroup pictureGroup = new PictureGroup();
                pictureGroup.setGroupId(Long.valueOf(groupId));
                pictureGroup.setPictureQuantity(pictureService.selectPictureByGroupId(Long.valueOf(groupId)).size());
                pictureGroupService.updatePictureGroup(pictureGroup);
                return success();
            }
            return error();
        }
        catch (Exception e)
        {
            log.error("上传图片失败！", e);
            return error(e.getMessage());
        }
    }
}
