package com.media.picture.controller;

import com.media.common.annotation.Log;
import com.media.common.core.controller.BaseController;
import com.media.common.core.domain.AjaxResult;
import com.media.common.core.domain.entity.SysUser;
import com.media.common.core.page.TableDataInfo;
import com.media.common.enums.BusinessType;
import com.media.common.utils.DateUtils;
import com.media.common.utils.RedisUtil;
import com.media.common.utils.ShiroUtils;
import com.media.common.utils.poi.ExcelUtil;
import com.media.picture.domain.Picture;
import com.media.picture.domain.PictureGroup;
import com.media.picture.service.IMamPictureViewService;
import com.media.picture.service.IPictureCategoryService;
import com.media.picture.service.IPictureGroupService;
import com.media.picture.service.IPictureService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 图组信息Controller
 * 
 * @author liziye
 * @date 2021-04-21
 */
@Controller
@RequestMapping("/picture/group")
public class PictureGroupController extends BaseController
{
    private String prefix = "picture/group";

    @Autowired
    private IPictureGroupService pictureGroupService;
    @Autowired
    private IPictureService pictureService;
    @Autowired
    private IPictureCategoryService pictureCategoryService;

    @Autowired
    private IMamPictureViewService iMamPictureViewService;

    @Autowired
    private RedisUtil redisUtil;

    @RequiresPermissions("picture:group:view")
    @GetMapping()
    public String group()
    {
        return prefix + "/group";
    }

    /**
     * 查询图组信息列表
     */
    @RequiresPermissions("picture:group:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(PictureGroup pictureGroup)
    {
        startPage();
        List<PictureGroup> list = pictureGroupService.selectPictureGroupList(pictureGroup);
        return getDataTable(list);
    }

    /**
     * 导出图组信息列表
     */
    @RequiresPermissions("picture:group:export")
    @Log(title = "图组信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(PictureGroup pictureGroup)
    {
        List<PictureGroup> list = pictureGroupService.selectPictureGroupList(pictureGroup);
        ExcelUtil<PictureGroup> util = new ExcelUtil<PictureGroup>(PictureGroup.class);
        return util.exportExcel(list, "group");
    }

    /**
     * 新增图组信息
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存图组信息
     */
    @RequiresPermissions("picture:group:add")
    @Log(title = "图组信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(PictureGroup pictureGroup)
    {
        return toAjax(pictureGroupService.insertPictureGroup(pictureGroup));
    }

    /**
     * 修改图组信息
     */
    @GetMapping("/edit/{groupId}")
    public String edit(@PathVariable("groupId") Long groupId, ModelMap mmap)
    {
        PictureGroup pictureGroup = pictureGroupService.selectPictureGroupById(groupId);
        mmap.put("pictureGroup", pictureGroup);
        return prefix + "/edit";
    }

    /**
     * 修改保存图组信息
     */
    @RequiresPermissions("picture:group:edit")
    @Log(title = "图组信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PictureGroup pictureGroup)
    {
        return toAjax(pictureGroupService.updatePictureGroup(pictureGroup));
    }

    /**
     * 删除图组信息
     */
    @RequiresPermissions("picture:group:remove")
    @Log(title = "图组信息", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return pictureGroupService.deletePictureGroupByIds(ids) == 0 ? error("选中的图组中包含图片，无法删除！"):success();
    }

    /**
     * 浏览图组
     */
    @GetMapping("/browse/{groupId}")
    public String browse(@PathVariable("groupId") Long groupId, ModelMap mmap)
    {
        PictureGroup pictureGroup = pictureGroupService.selectPictureGroupById(groupId);
        //获取分组图片
        Picture picture = new Picture();
        picture.setGroupId(groupId);
        picture.setDelFlag("0");
        List<Picture> pictureList= pictureService.selectPictureList(picture);
        System.out.println("pictureList.size() = " + pictureList.size());
        //替换关键字中的管道符
        mmap.put("pictureGroup", pictureGroup);
        mmap.put("pictureList", pictureList);
        return prefix + "/browse";
    }

    /**
     * 设置封面图
     */
    @RequiresPermissions("picture:group:setCover")
    @PostMapping("/setCover")
    @ResponseBody
    public AjaxResult setCover(PictureGroup pictureGroup)
    {
        SysUser currentUser = ShiroUtils.getSysUser();
        pictureGroup.setUpdateBy(currentUser.getLoginName());
        pictureGroup.setUpdateTime(DateUtils.getNowDate());
        return toAjax(pictureGroupService.updatePictureCoverId(pictureGroup));
    }
}
