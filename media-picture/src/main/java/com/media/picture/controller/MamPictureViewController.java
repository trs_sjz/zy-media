package com.media.picture.controller;

import com.media.common.annotation.Log;
import com.media.common.core.controller.BaseController;
import com.media.common.core.domain.AjaxResult;
import com.media.common.core.page.TableDataInfo;
import com.media.common.enums.BusinessType;
import com.media.common.utils.poi.ExcelUtil;
import com.media.picture.domain.MamPictureView;
import com.media.picture.service.IMamPictureViewService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 图片库访问量Controller
 * 
 * @author liziye
 * @date 2021-12-15
 */
@Controller
@RequestMapping("/picture/view")
public class MamPictureViewController extends BaseController
{
    private String prefix = "picture/view";

    @Autowired
    private IMamPictureViewService mamPictureViewService;

    @RequiresPermissions("picture:view:view")
    @GetMapping()
    public String view()
    {
        return prefix + "/view";
    }

    /**
     * 查询图片库访问量列表
     */
    @RequiresPermissions("picture:view:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MamPictureView mamPictureView)
    {
        startPage();
        List<MamPictureView> list = mamPictureViewService.selectMamPictureViewList(mamPictureView);
        return getDataTable(list);
    }

    /**
     * 导出图片库访问量列表
     */
    @RequiresPermissions("picture:view:export")
    @Log(title = "图片库访问量", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MamPictureView mamPictureView)
    {
        List<MamPictureView> list = mamPictureViewService.selectMamPictureViewList(mamPictureView);
        ExcelUtil<MamPictureView> util = new ExcelUtil<MamPictureView>(MamPictureView.class);
        return util.exportExcel(list, "view");
    }

    /**
     * 新增图片库访问量
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存图片库访问量
     */
    @RequiresPermissions("picture:view:add")
    @Log(title = "图片库访问量", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MamPictureView mamPictureView)
    {
        return toAjax(mamPictureViewService.insertMamPictureView(mamPictureView));
    }

    /**
     * 修改图片库访问量
     */
    @GetMapping("/edit/{viewId}")
    public String edit(@PathVariable("viewId") Long viewId, ModelMap mmap)
    {
        MamPictureView mamPictureView = mamPictureViewService.selectMamPictureViewById(viewId);
        mmap.put("mamPictureView", mamPictureView);
        return prefix + "/edit";
    }

    /**
     * 修改保存图片库访问量
     */
    @RequiresPermissions("picture:view:edit")
    @Log(title = "图片库访问量", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MamPictureView mamPictureView)
    {
        return toAjax(mamPictureViewService.updateMamPictureView(mamPictureView));
    }

    /**
     * 删除图片库访问量
     */
    @RequiresPermissions("picture:view:remove")
    @Log(title = "图片库访问量", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(mamPictureViewService.deleteMamPictureViewByIds(ids));
    }
}
