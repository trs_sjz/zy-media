package com.media.picture.controller;

import com.media.common.annotation.Log;
import com.media.common.core.controller.BaseController;
import com.media.common.core.domain.AjaxResult;
import com.media.common.core.domain.Ztree;
import com.media.common.enums.BusinessType;
import com.media.common.utils.StringUtils;
import com.media.common.utils.poi.ExcelUtil;
import com.media.picture.domain.PictureCategory;
import com.media.picture.service.IPictureCategoryService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 图片分类信息Controller
 * 
 * @author liziye
 * @date 2021-04-08
 */
@Controller
@RequestMapping("/picture/category")
public class PictureCategoryController extends BaseController
{
    private String prefix = "picture/category";

    @Autowired
    private IPictureCategoryService pictureCategoryService;

    @RequiresPermissions("picture:category:view")
    @GetMapping()
    public String category()
    {
        return prefix + "/category";
    }

    /**
     * 查询图片分类信息树列表
     */
    @RequiresPermissions("picture:category:list")
    @PostMapping("/list")
    @ResponseBody
    public List<PictureCategory> list(PictureCategory pictureCategory)
    {
        List<PictureCategory> list = pictureCategoryService.selectPictureCategoryList(pictureCategory);
        return list;
    }

    /**
     * 导出图片分类信息列表
     */
    @RequiresPermissions("picture:category:export")
    @Log(title = "图片分类信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(PictureCategory pictureCategory)
    {
        List<PictureCategory> list = pictureCategoryService.selectPictureCategoryList(pictureCategory);
        ExcelUtil<PictureCategory> util = new ExcelUtil<PictureCategory>(PictureCategory.class);
        return util.exportExcel(list, "category");
    }

    /**
     * 新增图片分类信息
     */
    @GetMapping(value = { "/add/{categoryId}", "/add/" })
    public String add(@PathVariable(value = "categoryId", required = false) Long categoryId, ModelMap mmap)
    {
        if (StringUtils.isNotNull(categoryId))
        {
            mmap.put("pictureCategory", pictureCategoryService.selectPictureCategoryById(categoryId));
        }
        return prefix + "/add";
    }

    /**
     * 新增保存图片分类信息
     */
    @RequiresPermissions("picture:category:add")
    @Log(title = "图片分类信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(PictureCategory pictureCategory)
    {
        return toAjax(pictureCategoryService.insertPictureCategory(pictureCategory));
    }

    /**
     * 修改图片分类信息
     */
    @GetMapping("/edit/{categoryId}")
    public String edit(@PathVariable("categoryId") Long categoryId, ModelMap mmap)
    {
        PictureCategory pictureCategory = pictureCategoryService.selectPictureCategoryById(categoryId);
        mmap.put("pictureCategory", pictureCategory);
        return prefix + "/edit";
    }

    /**
     * 修改保存图片分类信息
     */
    @RequiresPermissions("picture:category:edit")
    @Log(title = "图片分类信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PictureCategory pictureCategory)
    {
        return toAjax(pictureCategoryService.updatePictureCategory(pictureCategory));
    }

    /**
     * 删除
     */
    @RequiresPermissions("picture:category:remove")
    @Log(title = "图片分类信息", businessType = BusinessType.DELETE)
    @GetMapping("/remove/{categoryId}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("categoryId") Long categoryId)
    {
        return toAjax(pictureCategoryService.deletePictureCategoryById(categoryId));
    }

    /**
     * 选择图片分类信息树
     */
    @GetMapping(value = { "/selectCategoryTree/{categoryId}", "/selectCategoryTree/" })
    public String selectCategoryTree(@PathVariable(value = "categoryId", required = false) Long categoryId, ModelMap mmap)
    {
        if (StringUtils.isNotNull(categoryId))
        {
            mmap.put("pictureCategory", pictureCategoryService.selectPictureCategoryById(categoryId));
        }
        return prefix + "/tree";
    }

    /**
     * 加载图片分类信息树列表
     */
    @GetMapping("/treeData")
    @ResponseBody
    public List<Ztree> treeData()
    {
        List<Ztree> ztrees = pictureCategoryService.selectPictureCategoryTree();
        return ztrees;
    }

}
