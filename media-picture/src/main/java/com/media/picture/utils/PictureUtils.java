package com.media.picture.utils;

/**
 * @program: ziye
 * @description: PictureUtils
 * @author: 庄霸.liziye
 * @create: 2021-04-29 15:21
 **/

import com.drew.imaging.ImageMetadataReader;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;
import com.drew.metadata.exif.ExifSubIFDDirectory;
import com.media.common.config.MediaConfig;
import com.media.common.constant.MediaConstrans;
import com.media.common.utils.file.DirFilter;
import com.media.common.utils.file.FileUtils;
import com.media.picture.domain.Picture;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;


public class PictureUtils {
    /**
     * 通过UUID生成文件名
     * @param fileName
     * @return
     */
    public static String getRandomName(String fileName){
        String uuidFileName=UUID.randomUUID().toString().replace("-","");
        return uuidFileName;
    }
    /**
     * 获取图片上传文件目录
     * @param subPath
     * @return
     */
    public static String getPictureSourcePath(String loginName,String subPath) {
        String dirPath = MediaConfig.getProfile() + "/upload/"+loginName+ "/com/media/" + MediaConstrans.MEDIA_UPLOAD_SOURCE_PACH
                + File.separator + subPath;
        File file = new File(dirPath);//获取文件路径
        if(!file.exists()) {
            file.mkdirs();
        }
        return dirPath;
    }
    /**
     * 获取图片缩略图生成目录
     * @param subPath
     * @return
     */
    public static String getPicturePrivatePath(String loginName,String subPath) {
        String dirPath =MediaConfig.getProfile() + "/upload/"+loginName+ "/com/media/" + File.separator + MediaConstrans.MEDIA_UPLOAD_PRIVATE_PACH
                + File.separator + subPath;
        File file = new File(dirPath);//获取文件路径
        if(!file.exists()) {
            file.mkdirs();
        }
        return dirPath;
    }


    /**
     * 得到块文件所在目录
     * @param fileMd5
     * @return
     */
    public static String getChunkFileFolderPath(String userName,String fileMd5) {
        return MediaConfig.getProfile() + "/upload/"+userName+ "/com/media/" + MediaConstrans.MEDIA_UPLOAD_CHUNKS_PACH + "/"+fileMd5+"/";
    }

    /**
     * 获取所有分片文件
     */
    public static List<File> getChunkFiles(String chunkFileFolderPath) {
        //将文件数组转成list，并排序
        List<File> chunkFileList = new ArrayList<File>();
        File chunkFileFolder = new File(chunkFileFolderPath);
        if(chunkFileFolder.exists()) {
            //获取路径下的所有块文件
            File[] chunkFiles = chunkFileFolder.listFiles(new DirFilter("part"));
            chunkFileList.addAll(Arrays.asList(chunkFiles));
            //排序
            Collections.sort(chunkFileList, new Comparator<File>() {
                @Override
                public int compare(File o1, File o2) {
                    if (Integer.parseInt(o1.getName().replace(".part", "")) > Integer.parseInt(o2.getName().replace(".part", ""))) {
                        return 1;
                    }
                    return -1;
                }
            });
        }

        return chunkFileList;
    }
    /**
     * 合并块文件
     */
    public static boolean mergeFile(String chunkFileFolderPath,String filePath) {
        boolean flag = false;

        RandomAccessFile raf_write = null;
        RandomAccessFile raf_read = null;
        try {
            File mergeFile = FileUtils.createFile(filePath);
            List<File> chunkFiles = PictureUtils.getChunkFiles(chunkFileFolderPath);
            //创建写文件对象
            raf_write = new RandomAccessFile(mergeFile, "rw");
            //遍历分块文件开始合并
            //读取文件缓冲区
            byte[] b = new byte[1024];
            for (File chunkFile : chunkFiles) {
                raf_read =new RandomAccessFile(chunkFile, "r");
                int len = -1;
                //读取分块文件
                while ((len = raf_read.read(b)) != -1) {
                    //向合并文件中写数据
                    raf_write.write(b, 0, len);
                }
                raf_read.close();
            }
            flag = true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }finally{
            try {
                if(raf_write!=null) {
                    raf_write.close();
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            try {
                if(raf_read!=null) {
                    raf_read.close();
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
        return flag;
    }
    /**
     * 获取图片元数据对象
     * @param filePath
     * @param picture
     */
    public static void getPictureMetadata(String filePath, Picture picture) {
        //去除图片路径中存在资源路径
        filePath = MediaConfig.getProfile() + filePath.replace("/profile","");
        File imageFile = new File(filePath);
        //图片文件不存在返回
        if(!imageFile.exists()) {
            System.out.println("不存在");
            return;
        }
        try {
            Metadata metadata = ImageMetadataReader.readMetadata(imageFile);
            for (Directory directory : metadata.getDirectories()) {
                if (directory == null) {
                    continue;
                }
                for (Tag tag : directory.getTags()) {
                    // 标签名
                    String tagName = tag.getTagName();
                    // 标签信息
                    String desc = tag.getDescription();
                    switch (tagName) {
                        case "Date/Time Original":
                            SimpleDateFormat yyyymmddhhmmss = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss");
                            Date date = yyyymmddhhmmss.parse(desc);
                            SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            //picture.setTagShootingTime(newFormat.format(date));
                            break;
                        case "Image Width":
                            //picture.setTagExifImageWidth(desc.replaceAll("[^0-9]", "").trim());
                            break;
                        case "Image Height":
                            //picture.setTagExifImageHeight(desc.replaceAll("[^0-9]", "").trim());
                            break;
                        case "X Resolution":
                            if(desc.contains("dots per inch")) {
                                picture.setTagXResolutionPrint(desc.replaceAll("[^0-9]", "").trim());

                            }else {
                                picture.setTagXResolution(desc.replaceAll("[^0-9]", "").trim());
                            }
                            break;
                        case "Y Resolution":
                            if(desc.contains("dots per inch")) {
                                picture.setTagYResolutionPrint(desc.replaceAll("[^0-9]", "").trim());
                            }else {
                                picture.setTagYResolution(desc.replaceAll("[^0-9]", "").trim());
                            }
                            break;
                        case "File Size":
                            picture.setFilesize(Long.valueOf(desc.replaceAll("[^0-9]", "").trim()));
                            break;
                        case "Expected File Name Extension":
                            picture.setFileext(desc);
                            break;
                        case "Detected MIME Type":
                            picture.setMimeType(desc);
                        default:
                    }
                }
                if (directory.containsTag(ExifSubIFDDirectory.TAG_EXIF_IMAGE_WIDTH)) {
                    // 图片宽度
                    picture.setTagExifImageWidth(directory.getDescription(ExifSubIFDDirectory.TAG_EXIF_IMAGE_WIDTH).replaceAll("[^0-9]", "").trim());
                }
                if (directory.containsTag(ExifSubIFDDirectory.TAG_EXIF_IMAGE_HEIGHT)) {
                    // 图片高度
                    picture.setTagExifImageHeight(directory.getDescription(ExifSubIFDDirectory.TAG_EXIF_IMAGE_HEIGHT).replaceAll("[^0-9]", "").trim());
                }
                if (directory.containsTag(ExifSubIFDDirectory.TAG_FNUMBER)) {
                    // 光圈F值=镜头的焦距/镜头光圈的直径
                    picture.setTagFnumber(directory.getDescription(ExifSubIFDDirectory.TAG_FNUMBER));
                }
                if (directory.containsTag(ExifSubIFDDirectory.TAG_EXPOSURE_TIME)) {
                    //曝光时间
                    picture.setTagExposureTime(directory.getString(ExifSubIFDDirectory.TAG_EXPOSURE_TIME));
                }
                if (directory.containsTag(ExifSubIFDDirectory.TAG_ISO_EQUIVALENT)) {
                    //ISO速度
                    picture.setTagIsoEquivalent(directory.getString(ExifSubIFDDirectory.TAG_ISO_EQUIVALENT));
                }
                if (directory.containsTag(ExifSubIFDDirectory.TAG_FOCAL_LENGTH)) {
                    //焦距/毫米
                    picture.setTagFocalLength(directory.getString(ExifSubIFDDirectory.TAG_FOCAL_LENGTH));
                }
                if (directory.containsTag(ExifSubIFDDirectory.TAG_MAX_APERTURE)) {
                    //最大光圈
                    picture.setTagMaxAperture(directory.getString(ExifSubIFDDirectory.TAG_MAX_APERTURE));
                }
                if (directory.containsTag(ExifSubIFDDirectory.TAG_MAKE)) {
                    //照相机制造商
                    picture.setTagMake(directory.getString(ExifSubIFDDirectory.TAG_MAKE));
                }
                if (directory.containsTag(ExifSubIFDDirectory.TAG_MODEL)) {
                    //照相机型号
                    picture.setTagModel(directory.getString(ExifSubIFDDirectory.TAG_MODEL));
                }

                // 其他参数测试开始
                if (directory.containsTag(ExifSubIFDDirectory.TAG_SOFTWARE)) {
                    // Software软件 显示固件Firmware版本
                    picture.setTagSoftware(directory.getString(ExifSubIFDDirectory.TAG_SOFTWARE));
                }
                if (directory.containsTag(ExifSubIFDDirectory.TAG_35MM_FILM_EQUIV_FOCAL_LENGTH)) {
                    //35mm焦距
                    picture.setTag35mmFilmEquivFocalLength(directory.getString(ExifSubIFDDirectory.TAG_35MM_FILM_EQUIV_FOCAL_LENGTH));

                }
                if (directory.containsTag(ExifSubIFDDirectory.TAG_APERTURE)) {
                    //孔径(图片分辨率单位)
                    picture.setTagAperture(directory.getString(ExifSubIFDDirectory.TAG_APERTURE));
                }
                if (directory.containsTag(ExifSubIFDDirectory.TAG_ARTIST)) {
                    // 作者
                    //picture.setTagArtist(directory.getString(ExifSubIFDDirectory.TAG_ARTIST));
                }
                if (directory.containsTag(ExifSubIFDDirectory.TAG_BODY_SERIAL_NUMBER)) {
                    //TAG_BODY_SERIAL_NUMBER
                    picture.setTagBodySerialNumber(directory.getString(ExifSubIFDDirectory.TAG_BODY_SERIAL_NUMBER));
                }
                if (directory.containsTag(ExifSubIFDDirectory.TAG_METERING_MODE)) {
                    // MeteringMode测光方式， 平均式测光、中央重点测光、点测光等
                    picture.setTagMeteringMode(directory.getString(ExifSubIFDDirectory.TAG_METERING_MODE));
                }
                if (directory.containsTag(ExifSubIFDDirectory.TAG_RESOLUTION_UNIT)) {
                    // XResolution/YResolution X/Y方向分辨率
                    picture.setTagResolutionUnit(directory.getString(ExifSubIFDDirectory.TAG_RESOLUTION_UNIT));
                }
                if (directory.containsTag(ExifSubIFDDirectory.TAG_EXPOSURE_BIAS)) {
                    //曝光补偿
                    picture.setTagExposureBias(directory.getString(ExifSubIFDDirectory.TAG_EXPOSURE_BIAS));
                }
                if (directory.containsTag(ExifSubIFDDirectory.TAG_COLOR_SPACE)) {
                    //色域、色彩空间
                    picture.setTagColorSpace(directory.getString(ExifSubIFDDirectory.TAG_COLOR_SPACE));
                }
                if (directory.containsTag(ExifSubIFDDirectory.TAG_YCBCR_COEFFICIENTS)) {
                    //色相系数
                    picture.setTagYcbcrCoefficients(directory.getString(ExifSubIFDDirectory.TAG_YCBCR_COEFFICIENTS));
                }
                if (directory.containsTag(ExifSubIFDDirectory.TAG_YCBCR_POSITIONING)) {
                    //色相定位
                    picture.setTagYcbcrPositioning(directory.getString(ExifSubIFDDirectory.TAG_YCBCR_POSITIONING));
                }
                if (directory.containsTag(ExifSubIFDDirectory.TAG_YCBCR_SUBSAMPLING)) {
                    //色相抽样
                    picture.setTagYcbcrSubsampling(directory.getString(ExifSubIFDDirectory.TAG_YCBCR_SUBSAMPLING));
                }
                if (directory.containsTag(ExifSubIFDDirectory.TAG_EXIF_VERSION)) {
                    //exif版本号
                    picture.setTagExifVersion(directory.getString(ExifSubIFDDirectory.TAG_EXIF_VERSION));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 下载图片
     * @Title: downImg
     * @Description:
     * @Author 管理员
     * @DateTime 2020年7月23日 上午9:54:36
     * @param response
     * @param filename
     * @param path
     */
    public static void downImg(HttpServletResponse response,String filename,String path ){
        if (filename != null) {
            FileInputStream is = null;
            BufferedInputStream bs = null;
            OutputStream os = null;
            try {
                File file = new File(path);
                if (file.exists()) {
                    //设置Headers
                    response.setHeader("Content-Type","application/octet-stream");
                    //设置下载的文件的名称-该方式已解决中文乱码问题
                    response.setHeader("Content-Disposition","attachment;filename=" +  new String(filename.getBytes("gb2312"), "ISO8859-1" ));
                    is = new FileInputStream(file);
                    bs =new BufferedInputStream(is);
                    os = response.getOutputStream();
                    byte[] buffer = new byte[1024];
                    int len = 0;
                    while((len = bs.read(buffer)) != -1){
                        os.write(buffer,0,len);
                    }
                }else{
                    System.out.println("下载的文件不存在");
                }
            }catch(IOException ex){
                ex.printStackTrace();
            }finally {
                try{
                    if(is != null){
                        is.close();
                    }
                    if( bs != null ){
                        bs.close();
                    }
                    if( os != null){
                        os.flush();
                        os.close();
                    }
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        }
    }

    //换算图片大小
    public static String getPrintSize(long size) {
        //如果字节数少于1024，则直接以B为单位，否则先除于1024，后3位因太少无意义
        if (size < 1024) {
            return String.valueOf(size) + "B";
        } else {
            size = size / 1024;
        }
        //如果原字节数除于1024之后，少于1024，则可以直接以KB作为单位
        //因为还没有到达要使用另一个单位的时候
        //接下去以此类推
        if (size < 1024) {
            return String.valueOf(size) + "KB";
        } else {
            size = size / 1024;
        }
        if (size < 1024) {
            //因为如果以MB为单位的话，要保留最后1位小数，
            //因此，把此数乘以100之后再取余
            size = size * 100;
            return String.valueOf((size / 100)) + "."
                    + String.valueOf((size % 100)) + "MB";
        } else {
            //否则如果要以GB为单位的，先除于1024再作同样的处理
            size = size * 100 / 1024;
            return String.valueOf((size / 100)) + "."
                    + String.valueOf((size % 100)) + "GB";
        }
    }


}