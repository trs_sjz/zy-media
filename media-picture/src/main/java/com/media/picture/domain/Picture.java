package com.media.picture.domain;

import com.media.common.annotation.Excel;
import com.media.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 图片信息对象 mam_picture
 * 
 * @author liziye
 * @date 2021-04-22
 */
public class Picture extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long pictureId;

    /** 分组ID */
    @Excel(name = "分组ID")
    private Long groupId;

    /** 所属图组名 */
    @Excel(name = "所属图组名称")
    private String groupName;

    /** 标题 */
    @Excel(name = "标题")
    private String title;

    /** 关键词 */
    @Excel(name = "关键词")
    private String filterwords;

    /** 分类ID */
    @Excel(name = "分类ID")
    private Long categoryids;

    /** 图片排序 */
    @Excel(name = "图片排序")
    private Integer pictureSort;

    /** 相机制造商 */
    @Excel(name = "相机制造商")
    private String tagMake;

    /** 相机型号 */
    @Excel(name = "相机型号")
    private String tagModel;

    /** 光圈值 */
    @Excel(name = "光圈值")
    private String tagFnumber;

    /** 曝光时间 */
    @Excel(name = "曝光时间")
    private String tagExposureTime;

    /** 感光度 */
    @Excel(name = "感光度")
    private String tagIsoEquivalent;

    /** 焦距 */
    @Excel(name = "焦距")
    private String tagFocalLength;

    /** 最大光圈 */
    @Excel(name = "最大光圈")
    private String tagMaxAperture;

    /** 宽度 */
    @Excel(name = "宽度")
    private String tagExifImageWidth;

    /** 高度 */
    @Excel(name = "高度")
    private String tagExifImageHeight;

    /** 水平分辨率 */
    @Excel(name = "水平分辨率")
    private String tagXResolution;

    /** 垂直分辨率 */
    @Excel(name = "垂直分辨率")
    private String tagYResolution;

    /** 水平打印分辨率 */
    @Excel(name = "水平打印分辨率")
    private String tagXResolutionPrint;

    /** 垂直打印分辨率 */
    @Excel(name = "垂直打印分辨率")
    private String tagYResolutionPrint;

    /** 显示固件Firmware版本 */
    @Excel(name = "显示固件Firmware版本")
    private String tagSoftware;

    /** 35纳米孔径 */
    @Excel(name = "35纳米孔径")
    private String tag35mmFilmEquivFocalLength;

    /** 孔径(图片分辨率单位) */
    @Excel(name = "孔径(图片分辨率单位)")
    private String tagAperture;

    /** 序列号 */
    @Excel(name = "序列号")
    private String tagBodySerialNumber;

    /** 点测光值 */
    @Excel(name = "点测光值")
    private String tagMeteringMode;

    /** 分辨率单位 */
    @Excel(name = "分辨率单位")
    private String tagResolutionUnit;

    /** 曝光补偿 */
    @Excel(name = "曝光补偿")
    private String tagExposureBias;

    /** 色域、色彩空间 */
    @Excel(name = "色域、色彩空间")
    private String tagColorSpace;

    /** 色相系数 */
    @Excel(name = "色相系数")
    private String tagYcbcrCoefficients;

    /** 色相定位 */
    @Excel(name = "色相定位")
    private String tagYcbcrPositioning;

    /** 色相抽样 */
    @Excel(name = "色相抽样")
    private String tagYcbcrSubsampling;

    /** exif版本号 */
    @Excel(name = "exif版本号")
    private String tagExifVersion;

    /** mime类型 */
    @Excel(name = "mime类型")
    private String mimeType;

    /** 目录 */
    @Excel(name = "目录")
    private String subpath;

    /** 后缀类型 */
    @Excel(name = "后缀类型")
    private String fileext;

    /** 文件名称 */
    @Excel(name = "文件名称")
    private String filename;

    /** 文件基础名称 */
    @Excel(name = "文件基础名称")
    private String filenamebase;

    /** 文件原始名称 */
    @Excel(name = "文件原始名称")
    private String fileoriginalname;

    /** 文件基础路径 */
    @Excel(name = "文件基础路径")
    private String filepathbase;

    /** 文件大小 */
    @Excel(name = "文件大小")
    private Long filesize;

    /** 中图缩略图 */
    @Excel(name = "中图缩略图")
    private String thumbMiddle;

    /** 小图缩略图 */
    @Excel(name = "小图缩略图")
    private String thumbSmall;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建用户ID */
    @Excel(name = "创建用户ID")
    private Long createByUserId;

    /** 删除者 */
    @Excel(name = "删除者")
    private String deleteBy;

    /** 删除时间 */
    @Excel(name = "删除时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date deleteTime;

    public Picture() {
    }

    public void setPictureId(Long pictureId) 
    {
        this.pictureId = pictureId;
    }

    public Long getPictureId() 
    {
        return pictureId;
    }
    public void setGroupId(Long groupId) 
    {
        this.groupId = groupId;
    }

    public Long getGroupId() 
    {
        return groupId;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setFilterwords(String filterwords) 
    {
        this.filterwords = filterwords;
    }

    public String getFilterwords() 
    {
        return filterwords;
    }
    public void setCategoryids(Long categoryids) 
    {
        this.categoryids = categoryids;
    }

    public Long getCategoryids() 
    {
        return categoryids;
    }
    public void setPictureSort(Integer pictureSort) 
    {
        this.pictureSort = pictureSort;
    }

    public Integer getPictureSort() 
    {
        return pictureSort;
    }
    public void setTagMake(String tagMake) 
    {
        this.tagMake = tagMake;
    }

    public String getTagMake() 
    {
        return tagMake;
    }
    public void setTagModel(String tagModel) 
    {
        this.tagModel = tagModel;
    }

    public String getTagModel() 
    {
        return tagModel;
    }
    public void setTagFnumber(String tagFnumber) 
    {
        this.tagFnumber = tagFnumber;
    }

    public String getTagFnumber() 
    {
        return tagFnumber;
    }
    public void setTagExposureTime(String tagExposureTime) 
    {
        this.tagExposureTime = tagExposureTime;
    }

    public String getTagExposureTime() 
    {
        return tagExposureTime;
    }
    public void setTagIsoEquivalent(String tagIsoEquivalent) 
    {
        this.tagIsoEquivalent = tagIsoEquivalent;
    }

    public String getTagIsoEquivalent() 
    {
        return tagIsoEquivalent;
    }
    public void setTagFocalLength(String tagFocalLength) 
    {
        this.tagFocalLength = tagFocalLength;
    }

    public String getTagFocalLength() 
    {
        return tagFocalLength;
    }
    public void setTagMaxAperture(String tagMaxAperture) 
    {
        this.tagMaxAperture = tagMaxAperture;
    }

    public String getTagMaxAperture() 
    {
        return tagMaxAperture;
    }
    public void setTagExifImageWidth(String tagExifImageWidth) 
    {
        this.tagExifImageWidth = tagExifImageWidth;
    }

    public String getTagExifImageWidth() 
    {
        return tagExifImageWidth;
    }
    public void setTagExifImageHeight(String tagExifImageHeight) 
    {
        this.tagExifImageHeight = tagExifImageHeight;
    }

    public String getTagExifImageHeight() 
    {
        return tagExifImageHeight;
    }
    public void setTagXResolution(String tagXResolution) 
    {
        this.tagXResolution = tagXResolution;
    }

    public String getTagXResolution() 
    {
        return tagXResolution;
    }
    public void setTagYResolution(String tagYResolution) 
    {
        this.tagYResolution = tagYResolution;
    }

    public String getTagYResolution() 
    {
        return tagYResolution;
    }
    public void setTagXResolutionPrint(String tagXResolutionPrint) 
    {
        this.tagXResolutionPrint = tagXResolutionPrint;
    }

    public String getTagXResolutionPrint() 
    {
        return tagXResolutionPrint;
    }
    public void setTagYResolutionPrint(String tagYResolutionPrint) 
    {
        this.tagYResolutionPrint = tagYResolutionPrint;
    }

    public String getTagYResolutionPrint() 
    {
        return tagYResolutionPrint;
    }
    public void setTagSoftware(String tagSoftware) 
    {
        this.tagSoftware = tagSoftware;
    }

    public String getTagSoftware() 
    {
        return tagSoftware;
    }
    public void setTag35mmFilmEquivFocalLength(String tag35mmFilmEquivFocalLength) 
    {
        this.tag35mmFilmEquivFocalLength = tag35mmFilmEquivFocalLength;
    }

    public String getTag35mmFilmEquivFocalLength() 
    {
        return tag35mmFilmEquivFocalLength;
    }
    public void setTagAperture(String tagAperture) 
    {
        this.tagAperture = tagAperture;
    }

    public String getTagAperture() 
    {
        return tagAperture;
    }
    public void setTagBodySerialNumber(String tagBodySerialNumber) 
    {
        this.tagBodySerialNumber = tagBodySerialNumber;
    }

    public String getTagBodySerialNumber() 
    {
        return tagBodySerialNumber;
    }
    public void setTagMeteringMode(String tagMeteringMode) 
    {
        this.tagMeteringMode = tagMeteringMode;
    }

    public String getTagMeteringMode() 
    {
        return tagMeteringMode;
    }
    public void setTagResolutionUnit(String tagResolutionUnit) 
    {
        this.tagResolutionUnit = tagResolutionUnit;
    }

    public String getTagResolutionUnit() 
    {
        return tagResolutionUnit;
    }
    public void setTagExposureBias(String tagExposureBias) 
    {
        this.tagExposureBias = tagExposureBias;
    }

    public String getTagExposureBias() 
    {
        return tagExposureBias;
    }
    public void setTagColorSpace(String tagColorSpace) 
    {
        this.tagColorSpace = tagColorSpace;
    }

    public String getTagColorSpace() 
    {
        return tagColorSpace;
    }
    public void setTagYcbcrCoefficients(String tagYcbcrCoefficients) 
    {
        this.tagYcbcrCoefficients = tagYcbcrCoefficients;
    }

    public String getTagYcbcrCoefficients() 
    {
        return tagYcbcrCoefficients;
    }
    public void setTagYcbcrPositioning(String tagYcbcrPositioning) 
    {
        this.tagYcbcrPositioning = tagYcbcrPositioning;
    }

    public String getTagYcbcrPositioning() 
    {
        return tagYcbcrPositioning;
    }
    public void setTagYcbcrSubsampling(String tagYcbcrSubsampling) 
    {
        this.tagYcbcrSubsampling = tagYcbcrSubsampling;
    }

    public String getTagYcbcrSubsampling() 
    {
        return tagYcbcrSubsampling;
    }
    public void setTagExifVersion(String tagExifVersion) 
    {
        this.tagExifVersion = tagExifVersion;
    }

    public String getTagExifVersion() 
    {
        return tagExifVersion;
    }
    public void setMimeType(String mimeType) 
    {
        this.mimeType = mimeType;
    }

    public String getMimeType() 
    {
        return mimeType;
    }
    public void setSubpath(String subpath) 
    {
        this.subpath = subpath;
    }

    public String getSubpath() 
    {
        return subpath;
    }
    public void setFileext(String fileext) 
    {
        this.fileext = fileext;
    }

    public String getFileext() 
    {
        return fileext;
    }
    public void setFilename(String filename) 
    {
        this.filename = filename;
    }

    public String getFilename() 
    {
        return filename;
    }
    public void setFilenamebase(String filenamebase) 
    {
        this.filenamebase = filenamebase;
    }

    public String getFilenamebase() 
    {
        return filenamebase;
    }
    public void setFileoriginalname(String fileoriginalname) 
    {
        this.fileoriginalname = fileoriginalname;
    }

    public String getFileoriginalname() 
    {
        return fileoriginalname;
    }
    public void setFilepathbase(String filepathbase) 
    {
        this.filepathbase = filepathbase;
    }

    public String getFilepathbase() 
    {
        return filepathbase;
    }
    public void setFilesize(Long filesize) 
    {
        this.filesize = filesize;
    }

    public Long getFilesize() 
    {
        return filesize;
    }
    public void setThumbMiddle(String thumbMiddle) 
    {
        this.thumbMiddle = thumbMiddle;
    }

    public String getThumbMiddle() 
    {
        return thumbMiddle;
    }
    public void setThumbSmall(String thumbSmall) 
    {
        this.thumbSmall = thumbSmall;
    }

    public String getThumbSmall() 
    {
        return thumbSmall;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setCreateByUserId(Long createByUserId) 
    {
        this.createByUserId = createByUserId;
    }

    public Long getCreateByUserId() 
    {
        return createByUserId;
    }
    public void setDeleteBy(String deleteBy) 
    {
        this.deleteBy = deleteBy;
    }

    public String getDeleteBy() 
    {
        return deleteBy;
    }
    public void setDeleteTime(Date deleteTime) 
    {
        this.deleteTime = deleteTime;
    }

    public Date getDeleteTime() 
    {
        return deleteTime;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("pictureId", getPictureId())
            .append("groupId", getGroupId())
            .append("groupName",getGroupName())
            .append("title", getTitle())
            .append("filterwords", getFilterwords())
            .append("categoryids", getCategoryids())
            .append("pictureSort", getPictureSort())
            .append("tagMake", getTagMake())
            .append("tagModel", getTagModel())
            .append("tagFnumber", getTagFnumber())
            .append("tagExposureTime", getTagExposureTime())
            .append("tagIsoEquivalent", getTagIsoEquivalent())
            .append("tagFocalLength", getTagFocalLength())
            .append("tagMaxAperture", getTagMaxAperture())
            .append("tagExifImageWidth", getTagExifImageWidth())
            .append("tagExifImageHeight", getTagExifImageHeight())
            .append("tagXResolution", getTagXResolution())
            .append("tagYResolution", getTagYResolution())
            .append("tagXResolutionPrint", getTagXResolutionPrint())
            .append("tagYResolutionPrint", getTagYResolutionPrint())
            .append("tagSoftware", getTagSoftware())
            .append("tag35mmFilmEquivFocalLength", getTag35mmFilmEquivFocalLength())
            .append("tagAperture", getTagAperture())
            .append("tagBodySerialNumber", getTagBodySerialNumber())
            .append("tagMeteringMode", getTagMeteringMode())
            .append("tagResolutionUnit", getTagResolutionUnit())
            .append("tagExposureBias", getTagExposureBias())
            .append("tagColorSpace", getTagColorSpace())
            .append("tagYcbcrCoefficients", getTagYcbcrCoefficients())
            .append("tagYcbcrPositioning", getTagYcbcrPositioning())
            .append("tagYcbcrSubsampling", getTagYcbcrSubsampling())
            .append("tagExifVersion", getTagExifVersion())
            .append("mimeType", getMimeType())
            .append("subpath", getSubpath())
            .append("fileext", getFileext())
            .append("filename", getFilename())
            .append("filenamebase", getFilenamebase())
            .append("fileoriginalname", getFileoriginalname())
            .append("filepathbase", getFilepathbase())
            .append("filesize", getFilesize())
            .append("thumbMiddle", getThumbMiddle())
            .append("thumbSmall", getThumbSmall())
            .append("delFlag", getDelFlag())
            .append("createByUserId", getCreateByUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("deleteBy", getDeleteBy())
            .append("deleteTime", getDeleteTime())
            .append("remark", getRemark())
            .toString();
    }
}
