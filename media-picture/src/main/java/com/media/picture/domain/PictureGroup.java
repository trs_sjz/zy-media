package com.media.picture.domain;

import com.media.common.annotation.Excel;
import com.media.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 图组信息对象 mam_picture_group
 * 
 * @author liziye
 * @date 2021-04-21
 */
public class PictureGroup extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 图组ID */
    private Long groupId;

    /** 部门ID */
    @Excel(name = "部门ID")
    private Integer deptId;

    /** 1是领导人 0非领导人 */
    @Excel(name = "1是领导人 0非领导人")
    private Integer leader;

    /** 分组标题 */
    @Excel(name = "分组标题")
    private String groupTitle;

    /** 分组关键字 */
    @Excel(name = "分组关键字")
    private String groupKeywords;

    /** 摄影师 */
    @Excel(name = "摄影师")
    private String photographer;

    /** 拍摄地 */
    @Excel(name = "拍摄地")
    private String filmingPlace;

    /** 拍摄日期 */
    @Excel(name = "拍摄日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date filmingDate;

    /** 图片数量 */
    @Excel(name = "图片数量")
    private Integer pictureQuantity;

    /** 组图分类 */
    @Excel(name = "组图分类")
    private String categorys;

    /** 封面ID */
    @Excel(name = "封面ID")
    private Integer coverId;

    /** 封面ID */
    @Excel(name = "封面图url")
    private String coverUrl;

    /** 部门可见 0不可见 1 可见 */
    private String deptVisible;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 拥有者id */
    @Excel(name = "拥有者id")
    private Long ownerUserid;

    /** 创建者ID */
    @Excel(name = "创建者ID")
    private Long createByUserid;

    /** 删除者 */
    @Excel(name = "删除者")
    private String deleteBy;

    /** 删除时间 */
    @Excel(name = "删除时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date deleteTime;

    public void setGroupId(Long groupId) 
    {
        this.groupId = groupId;
    }

    public Long getGroupId() 
    {
        return groupId;
    }
    public void setDeptId(Integer deptId) 
    {
        this.deptId = deptId;
    }

    public Integer getDeptId() 
    {
        return deptId;
    }
    public void setLeader(Integer leader) 
    {
        this.leader = leader;
    }

    public Integer getLeader() 
    {
        return leader;
    }
    public void setGroupTitle(String groupTitle) 
    {
        this.groupTitle = groupTitle;
    }

    public String getGroupTitle() 
    {
        return groupTitle;
    }
    public void setGroupKeywords(String groupKeywords) 
    {
        this.groupKeywords = groupKeywords;
    }

    public String getGroupKeywords() 
    {
        return groupKeywords;
    }
    public void setPhotographer(String photographer) 
    {
        this.photographer = photographer;
    }

    public String getPhotographer() 
    {
        return photographer;
    }
    public void setFilmingPlace(String filmingPlace) 
    {
        this.filmingPlace = filmingPlace;
    }

    public String getFilmingPlace() 
    {
        return filmingPlace;
    }
    public void setFilmingDate(Date filmingDate) 
    {
        this.filmingDate = filmingDate;
    }

    public Date getFilmingDate() 
    {
        return filmingDate;
    }
    public void setPictureQuantity(Integer pictureQuantity) 
    {
        this.pictureQuantity = pictureQuantity;
    }

    public Integer getPictureQuantity() 
    {
        return pictureQuantity;
    }
    public void setCategorys(String categorys) 
    {
        this.categorys = categorys;
    }

    public String getCategorys() 
    {
        return categorys;
    }
    public void setCoverId(Integer coverId) 
    {
        this.coverId = coverId;
    }

    public Integer getCoverId() 
    {
        return coverId;
    }
    public void setDeptVisible(String deptVisible) 
    {
        this.deptVisible = deptVisible;
    }

    public String getDeptVisible() 
    {
        return deptVisible;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setOwnerUserid(Long ownerUserid) 
    {
        this.ownerUserid = ownerUserid;
    }

    public Long getOwnerUserid() 
    {
        return ownerUserid;
    }
    public void setCreateByUserid(Long createByUserid) 
    {
        this.createByUserid = createByUserid;
    }

    public Long getCreateByUserid() 
    {
        return createByUserid;
    }
    public void setDeleteBy(String deleteBy) 
    {
        this.deleteBy = deleteBy;
    }

    public String getDeleteBy() 
    {
        return deleteBy;
    }
    public void setDeleteTime(Date deleteTime) 
    {
        this.deleteTime = deleteTime;
    }

    public Date getDeleteTime() 
    {
        return deleteTime;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("groupId", getGroupId())
            .append("deptId", getDeptId())
            .append("leader", getLeader())
            .append("groupTitle", getGroupTitle())
            .append("groupKeywords", getGroupKeywords())
            .append("photographer", getPhotographer())
            .append("filmingPlace", getFilmingPlace())
            .append("filmingDate", getFilmingDate())
            .append("pictureQuantity", getPictureQuantity())
            .append("categorys", getCategorys())
            .append("coverId", getCoverId())
            .append("deptVisible", getDeptVisible())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("ownerUserid", getOwnerUserid())
            .append("createByUserid", getCreateByUserid())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("deleteBy", getDeleteBy())
            .append("deleteTime", getDeleteTime())
            .append("remark", getRemark())
            .append("coverUrl", getCoverUrl())
            .toString();
    }
}
