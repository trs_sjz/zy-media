package com.media.picture.domain;

import com.media.common.annotation.Excel;
import com.media.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 图片库访问量对象 mam_picture_view
 * 
 * @author liziye
 * @date 2021-12-15
 */
public class MamPictureView extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 访问量id */
    private Long viewId;

    /** 当天访问量 */
    @Excel(name = "当天访问量")
    private Long viewNum;

    /** 日期 */
    @Excel(name = "日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date viewData;

    public void setViewId(Long viewId) 
    {
        this.viewId = viewId;
    }

    public Long getViewId() 
    {
        return viewId;
    }
    public void setViewNum(Long viewNum) 
    {
        this.viewNum = viewNum;
    }

    public Long getViewNum() 
    {
        return viewNum;
    }
    public void setViewData(Date viewData) 
    {
        this.viewData = viewData;
    }

    public Date getViewData() 
    {
        return viewData;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("viewId", getViewId())
            .append("viewNum", getViewNum())
            .append("viewData", getViewData())
            .toString();
    }
}
