package com.media.video.mapper;

import com.media.video.domain.MamVideoStream;

import java.util.List;

/**
 * 视频转码流对象Mapper接口
 * 
 * @author liziye
 * @date 2022-03-15
 */
public interface MamVideoStreamMapper 
{
    /**
     * 查询视频转码流对象
     * 
     * @param streamId 视频转码流对象ID
     * @return 视频转码流对象
     */
    public MamVideoStream selectMamVideoStreamById(Long streamId);

    /**
     * 查询视频转码流对象列表
     * 
     * @param mamVideoStream 视频转码流对象
     * @return 视频转码流对象集合
     */
    public List<MamVideoStream> selectMamVideoStreamList(MamVideoStream mamVideoStream);

    /**
     * 新增视频转码流对象
     * 
     * @param mamVideoStream 视频转码流对象
     * @return 结果
     */
    public int insertMamVideoStream(MamVideoStream mamVideoStream);

    /**
     * 修改视频转码流对象
     * 
     * @param mamVideoStream 视频转码流对象
     * @return 结果
     */
    public int updateMamVideoStream(MamVideoStream mamVideoStream);

    /**
     * 删除视频转码流对象
     * 
     * @param streamId 视频转码流对象ID
     * @return 结果
     */
    public int deleteMamVideoStreamById(Long streamId);

    /**
     * 批量删除视频转码流对象
     * 
     * @param streamIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteMamVideoStreamByIds(String[] streamIds);
}
