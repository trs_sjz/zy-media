package com.media.video.mapper;

import com.media.video.domain.MamVideoFrament;

import java.util.List;

/**
 * 视频片段Mapper接口
 * 
 * @author liziye
 * @date 2022-03-18
 */
public interface MamVideoFramentMapper 
{
    /**
     * 查询视频片段
     * 
     * @param framentId 视频片段ID
     * @return 视频片段
     */
    public MamVideoFrament selectMamVideoFramentById(Long framentId);

    /**
     * 查询视频片段列表
     * 
     * @param mamVideoFrament 视频片段
     * @return 视频片段集合
     */
    public List<MamVideoFrament> selectMamVideoFramentList(MamVideoFrament mamVideoFrament);

    /**
     * 新增视频片段
     * 
     * @param mamVideoFrament 视频片段
     * @return 结果
     */
    public int insertMamVideoFrament(MamVideoFrament mamVideoFrament);

    /**
     * 修改视频片段
     * 
     * @param mamVideoFrament 视频片段
     * @return 结果
     */
    public int updateMamVideoFrament(MamVideoFrament mamVideoFrament);

    /**
     * 删除视频片段
     * 
     * @param framentId 视频片段ID
     * @return 结果
     */
    public int deleteMamVideoFramentById(Long framentId);

    /**
     * 批量删除视频片段
     * 
     * @param framentIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteMamVideoFramentByIds(String[] framentIds);
}
