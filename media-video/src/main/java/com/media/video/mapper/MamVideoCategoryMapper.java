package com.media.video.mapper;

import com.media.video.domain.MamVideoCategory;

import java.util.List;

/**
 * 视频分类信息Mapper接口
 *
 * @author liziye
 * @date 2021-12-17
 */
public interface MamVideoCategoryMapper
{
    /**
     * 查询视频分类信息
     *
     * @param categoryId 视频分类信息ID
     * @return 视频分类信息
     */
    public MamVideoCategory selectMamVideoCategoryById(Long categoryId);

    /**
     * 查询视频分类信息列表
     *
     * @param mamVideoCategory 视频分类信息
     * @return 视频分类信息集合
     */
    public List<MamVideoCategory> selectMamVideoCategoryList(MamVideoCategory mamVideoCategory);

    /**
     * 新增视频分类信息
     *
     * @param mamVideoCategory 视频分类信息
     * @return 结果
     */
    public int insertMamVideoCategory(MamVideoCategory mamVideoCategory);

    /**
     * 修改视频分类信息
     *
     * @param mamVideoCategory 视频分类信息
     * @return 结果
     */
    public int updateMamVideoCategory(MamVideoCategory mamVideoCategory);

    /**
     * 删除视频分类信息
     *
     * @param categoryId 视频分类信息ID
     * @return 结果
     */
    public int deleteMamVideoCategoryById(Long categoryId);

    /**
     * 批量删除视频分类信息
     *
     * @param categoryIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteMamVideoCategoryByIds(String[] categoryIds);
}
