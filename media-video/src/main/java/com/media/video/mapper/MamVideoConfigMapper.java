package com.media.video.mapper;

import com.media.video.domain.MamVideoConfig;

import java.util.List;

/**
 * 视频转码配置Mapper接口
 * 
 * @author liziye
 * @date 2022-03-11
 */
public interface MamVideoConfigMapper 
{
    /**
     * 查询视频转码配置
     * 
     * @param configId 视频转码配置ID
     * @return 视频转码配置
     */
    public MamVideoConfig selectMamVideoConfigById(Integer configId);

    /**
     * 查询视频转码配置列表
     * 
     * @param mamVideoConfig 视频转码配置
     * @return 视频转码配置集合
     */
    public List<MamVideoConfig> selectMamVideoConfigList(MamVideoConfig mamVideoConfig);

    /**
     * 新增视频转码配置
     * 
     * @param mamVideoConfig 视频转码配置
     * @return 结果
     */
    public int insertMamVideoConfig(MamVideoConfig mamVideoConfig);

    /**
     * 修改视频转码配置
     * 
     * @param mamVideoConfig 视频转码配置
     * @return 结果
     */
    public int updateMamVideoConfig(MamVideoConfig mamVideoConfig);

    /**
     * 删除视频转码配置
     * 
     * @param configId 视频转码配置ID
     * @return 结果
     */
    public int deleteMamVideoConfigById(Integer configId);

    /**
     * 批量删除视频转码配置
     * 
     * @param configIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteMamVideoConfigByIds(String[] configIds);
}
