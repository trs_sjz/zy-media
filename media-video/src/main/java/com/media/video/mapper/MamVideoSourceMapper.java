package com.media.video.mapper;

import com.media.video.domain.MamVideoSource;

import java.util.List;

/**
 * 源视频Mapper接口
 * 
 * @author liziye
 * @date 2022-03-15
 */
public interface MamVideoSourceMapper 
{
    /**
     * 查询源视频
     * 
     * @param id 源视频ID
     * @return 源视频
     */
    public MamVideoSource selectMamVideoSourceById(Long id);

    /**
     * 查询源视频列表
     * 
     * @param mamVideoSource 源视频
     * @return 源视频集合
     */
    public List<MamVideoSource> selectMamVideoSourceList(MamVideoSource mamVideoSource);

    /**
     * 新增源视频
     * 
     * @param mamVideoSource 源视频
     * @return 结果
     */
    public int insertMamVideoSource(MamVideoSource mamVideoSource);

    /**
     * 修改源视频
     * 
     * @param mamVideoSource 源视频
     * @return 结果
     */
    public int updateMamVideoSource(MamVideoSource mamVideoSource);

    /**
     * 删除源视频
     * 
     * @param id 源视频ID
     * @return 结果
     */
    public int deleteMamVideoSourceById(Long id);

    /**
     * 批量删除源视频
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMamVideoSourceByIds(String[] ids);
}
