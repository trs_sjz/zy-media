package com.media.video.mapper;

import com.media.video.domain.MamVideoLog;

import java.util.HashMap;
import java.util.List;

/**
 * 视频日志Mapper接口
 * 
 * @author liziye
 * @date 2022-03-18
 */
public interface MamVideoLogMapper 
{
    /**
     * 查询视频日志
     * 
     * @param logId 视频日志ID
     * @return 视频日志
     */
    public MamVideoLog selectMamVideoLogById(Long logId);

    /**
     * 查询视频日志列表
     * 
     * @param mamVideoLog 视频日志
     * @return 视频日志集合
     */
    public List<MamVideoLog> selectMamVideoLogList(MamVideoLog mamVideoLog);

    /**
     * 新增视频日志
     * 
     * @param mamVideoLog 视频日志
     * @return 结果
     */
    public int insertMamVideoLog(MamVideoLog mamVideoLog);

    /**
     * 修改视频日志
     * 
     * @param mamVideoLog 视频日志
     * @return 结果
     */
    public int updateMamVideoLog(MamVideoLog mamVideoLog);

    /**
     * 删除视频日志
     * 
     * @param logId 视频日志ID
     * @return 结果
     */
    public int deleteMamVideoLogById(Long logId);

    /**
     * 批量删除视频日志
     * 
     * @param logIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteMamVideoLogByIds(String[] logIds);

    /**
     * 获取视频下载量（播放量）图表数据
     *
     * @param mamVideoLog
     * @return 结果
     */
    List<HashMap<String, Object>> getVideoChartData(MamVideoLog mamVideoLog);
}
