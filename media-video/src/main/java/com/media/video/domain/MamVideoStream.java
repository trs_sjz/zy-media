package com.media.video.domain;

import com.media.common.annotation.Excel;
import com.media.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 视频转码流对象对象 mam_video_stream
 * 
 * @author liziye
 * @date 2022-03-15
 */
public class MamVideoStream extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 转码流id */
    private Long streamId;

    /** 视频ID */
    @Excel(name = "视频ID")
    private Long videoid;

    /**  */
    @Excel(name = "")
    private Long attachedpic;

    /** 音频比特率 */
    @Excel(name = "音频比特率")
    private Long audiobitrate;

    /** 音频声道 */
    @Excel(name = "音频声道")
    private Long audiochannels;

    /** 音频编解码器 */
    @Excel(name = "音频编解码器")
    private String audiocodec;

    /** 音频格式 */
    @Excel(name = "音频格式")
    private String audioformat;

    /** 音频采样速率 */
    @Excel(name = "音频采样速率")
    private Long audiosamplerate;

    /** 码率 */
    @Excel(name = "码率")
    private Long bitrate;

    /** 视音频分离器 */
    @Excel(name = "视音频分离器")
    private String demuxer;

    /** 时长 */
    @Excel(name = "时长")
    private Long duration;

    /** 平均帧率 */
    @Excel(name = "平均帧率")
    private Long fps;

    /** 帧速率 */
    @Excel(name = "帧速率")
    private Long framerate;

    /** 高度 */
    @Excel(name = "高度")
    private Long height;

    /** 媒体类型 */
    @Excel(name = "媒体类型")
    private String mediatype;

    /** 帧数 */
    @Excel(name = "帧数")
    private Long nbframes;

    /** 分辨率 */
    @Excel(name = "分辨率")
    private String pixelformat;

    /** 视频编码 */
    @Excel(name = "视频编码")
    private String videocodec;

    /** 视频格式 */
    @Excel(name = "视频格式")
    private String videoformat;

    /** 音视频编码器的级 */
    @Excel(name = "音视频编码器的级")
    private String videolevel;

    /** 基本画质 */
    @Excel(name = "基本画质")
    private String videoprofile;

    /** 视频宽度 */
    @Excel(name = "视频宽度")
    private Long width;

    /**  */
    @Excel(name = "")
    private Long consequent;

    /**  */
    @Excel(name = "")
    private Integer consoleonly;

    /**  */
    @Excel(name = "")
    private Long consumerappid;

    /**  */
    @Excel(name = "")
    private Long cputime;

    /** 转码时长 */
    @Excel(name = "转码时长")
    private Long elapsedseconds;

    /** 后缀 */
    @Excel(name = "后缀")
    private String fileext;

    /** 文件名 */
    @Excel(name = "文件名")
    private String filename;

    /** 文件路径 */
    @Excel(name = "文件路径")
    private String filepath;

    /** 格式 */
    @Excel(name = "格式")
    private String format;

    /** root权限 */
    @Excel(name = "root权限")
    private String fsroot;

    /** 是否重新转码 */
    @Excel(name = "是否重新转码")
    private Integer isretranscode;

    /**  */
    @Excel(name = "")
    private String maxmenkb;

    /** 操作者 */
    @Excel(name = "操作者")
    private String operator;

    /** 任务状态 */
    @Excel(name = "任务状态")
    private String progressivestatus;

    /** 仓库ID */
    @Excel(name = "仓库ID")
    private Long repositoryid;

    /** 大小 */
    @Excel(name = "大小")
    private Long size;

    /** 目录 */
    @Excel(name = "目录")
    private String subpath;

    /** 转码命令 */
    @Excel(name = "转码命令")
    private String transcodecmd;

    /** 类型 */
    @Excel(name = "类型")
    private Long type;

    private MamVideo video;

    public void setStreamId(Long streamId) 
    {
        this.streamId = streamId;
    }

    public Long getStreamId() 
    {
        return streamId;
    }
    public void setVideoid(Long videoid) 
    {
        this.videoid = videoid;
    }

    public Long getVideoid() 
    {
        return videoid;
    }
    public void setAttachedpic(Long attachedpic) 
    {
        this.attachedpic = attachedpic;
    }

    public Long getAttachedpic() 
    {
        return attachedpic;
    }
    public void setAudiobitrate(Long audiobitrate) 
    {
        this.audiobitrate = audiobitrate;
    }

    public Long getAudiobitrate() 
    {
        return audiobitrate;
    }
    public void setAudiochannels(Long audiochannels) 
    {
        this.audiochannels = audiochannels;
    }

    public Long getAudiochannels() 
    {
        return audiochannels;
    }
    public void setAudiocodec(String audiocodec) 
    {
        this.audiocodec = audiocodec;
    }

    public String getAudiocodec() 
    {
        return audiocodec;
    }
    public void setAudioformat(String audioformat) 
    {
        this.audioformat = audioformat;
    }

    public String getAudioformat() 
    {
        return audioformat;
    }
    public void setAudiosamplerate(Long audiosamplerate) 
    {
        this.audiosamplerate = audiosamplerate;
    }

    public Long getAudiosamplerate() 
    {
        return audiosamplerate;
    }
    public void setBitrate(Long bitrate) 
    {
        this.bitrate = bitrate;
    }

    public Long getBitrate() 
    {
        return bitrate;
    }
    public void setDemuxer(String demuxer) 
    {
        this.demuxer = demuxer;
    }

    public String getDemuxer() 
    {
        return demuxer;
    }
    public void setDuration(Long duration) 
    {
        this.duration = duration;
    }

    public Long getDuration() 
    {
        return duration;
    }
    public void setFps(Long fps) 
    {
        this.fps = fps;
    }

    public Long getFps() 
    {
        return fps;
    }
    public void setFramerate(Long framerate) 
    {
        this.framerate = framerate;
    }

    public Long getFramerate() 
    {
        return framerate;
    }
    public void setHeight(Long height) 
    {
        this.height = height;
    }

    public Long getHeight() 
    {
        return height;
    }
    public void setMediatype(String mediatype) 
    {
        this.mediatype = mediatype;
    }

    public String getMediatype() 
    {
        return mediatype;
    }
    public void setNbframes(Long nbframes) 
    {
        this.nbframes = nbframes;
    }

    public Long getNbframes() 
    {
        return nbframes;
    }
    public void setPixelformat(String pixelformat) 
    {
        this.pixelformat = pixelformat;
    }

    public String getPixelformat() 
    {
        return pixelformat;
    }
    public void setVideocodec(String videocodec) 
    {
        this.videocodec = videocodec;
    }

    public String getVideocodec() 
    {
        return videocodec;
    }
    public void setVideoformat(String videoformat) 
    {
        this.videoformat = videoformat;
    }

    public String getVideoformat() 
    {
        return videoformat;
    }
    public void setVideolevel(String videolevel) 
    {
        this.videolevel = videolevel;
    }

    public String getVideolevel() 
    {
        return videolevel;
    }
    public void setVideoprofile(String videoprofile) 
    {
        this.videoprofile = videoprofile;
    }

    public String getVideoprofile() 
    {
        return videoprofile;
    }
    public void setWidth(Long width) 
    {
        this.width = width;
    }

    public Long getWidth() 
    {
        return width;
    }
    public void setConsequent(Long consequent) 
    {
        this.consequent = consequent;
    }

    public Long getConsequent() 
    {
        return consequent;
    }
    public void setConsoleonly(Integer consoleonly) 
    {
        this.consoleonly = consoleonly;
    }

    public Integer getConsoleonly() 
    {
        return consoleonly;
    }
    public void setConsumerappid(Long consumerappid) 
    {
        this.consumerappid = consumerappid;
    }

    public Long getConsumerappid() 
    {
        return consumerappid;
    }
    public void setCputime(Long cputime) 
    {
        this.cputime = cputime;
    }

    public Long getCputime() 
    {
        return cputime;
    }
    public void setElapsedseconds(Long elapsedseconds) 
    {
        this.elapsedseconds = elapsedseconds;
    }

    public Long getElapsedseconds() 
    {
        return elapsedseconds;
    }
    public void setFileext(String fileext) 
    {
        this.fileext = fileext;
    }

    public String getFileext() 
    {
        return fileext;
    }
    public void setFilename(String filename) 
    {
        this.filename = filename;
    }

    public String getFilename() 
    {
        return filename;
    }
    public void setFilepath(String filepath) 
    {
        this.filepath = filepath;
    }

    public String getFilepath() 
    {
        return filepath;
    }
    public void setFormat(String format) 
    {
        this.format = format;
    }

    public String getFormat() 
    {
        return format;
    }
    public void setFsroot(String fsroot) 
    {
        this.fsroot = fsroot;
    }

    public String getFsroot() 
    {
        return fsroot;
    }
    public void setIsretranscode(Integer isretranscode) 
    {
        this.isretranscode = isretranscode;
    }

    public Integer getIsretranscode() 
    {
        return isretranscode;
    }
    public void setMaxmenkb(String maxmenkb) 
    {
        this.maxmenkb = maxmenkb;
    }

    public String getMaxmenkb() 
    {
        return maxmenkb;
    }
    public void setOperator(String operator) 
    {
        this.operator = operator;
    }

    public String getOperator() 
    {
        return operator;
    }
    public void setProgressivestatus(String progressivestatus) 
    {
        this.progressivestatus = progressivestatus;
    }

    public String getProgressivestatus() 
    {
        return progressivestatus;
    }
    public void setRepositoryid(Long repositoryid) 
    {
        this.repositoryid = repositoryid;
    }

    public Long getRepositoryid() 
    {
        return repositoryid;
    }
    public void setSize(Long size) 
    {
        this.size = size;
    }

    public Long getSize() 
    {
        return size;
    }
    public void setSubpath(String subpath) 
    {
        this.subpath = subpath;
    }

    public String getSubpath() 
    {
        return subpath;
    }
    public void setTranscodecmd(String transcodecmd) 
    {
        this.transcodecmd = transcodecmd;
    }

    public String getTranscodecmd() 
    {
        return transcodecmd;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }

    public MamVideo getVideo() {
        return video;
    }

    public void setVideo(MamVideo video) {
        this.video = video;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("streamId", getStreamId())
            .append("videoid", getVideoid())
            .append("attachedpic", getAttachedpic())
            .append("audiobitrate", getAudiobitrate())
            .append("audiochannels", getAudiochannels())
            .append("audiocodec", getAudiocodec())
            .append("audioformat", getAudioformat())
            .append("audiosamplerate", getAudiosamplerate())
            .append("bitrate", getBitrate())
            .append("demuxer", getDemuxer())
            .append("duration", getDuration())
            .append("fps", getFps())
            .append("framerate", getFramerate())
            .append("height", getHeight())
            .append("mediatype", getMediatype())
            .append("nbframes", getNbframes())
            .append("pixelformat", getPixelformat())
            .append("videocodec", getVideocodec())
            .append("videoformat", getVideoformat())
            .append("videolevel", getVideolevel())
            .append("videoprofile", getVideoprofile())
            .append("width", getWidth())
            .append("consequent", getConsequent())
            .append("consoleonly", getConsoleonly())
            .append("consumerappid", getConsumerappid())
            .append("cputime", getCputime())
            .append("elapsedseconds", getElapsedseconds())
            .append("fileext", getFileext())
            .append("filename", getFilename())
            .append("filepath", getFilepath())
            .append("format", getFormat())
            .append("fsroot", getFsroot())
            .append("isretranscode", getIsretranscode())
            .append("maxmenkb", getMaxmenkb())
            .append("operator", getOperator())
            .append("progressivestatus", getProgressivestatus())
            .append("repositoryid", getRepositoryid())
            .append("size", getSize())
            .append("subpath", getSubpath())
            .append("transcodecmd", getTranscodecmd())
            .append("type", getType())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
