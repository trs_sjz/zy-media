package com.media.video.domain;

import com.media.common.annotation.Excel;
import com.media.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 视频缩略图对象 mam_video_thumb
 * 
 * @author liziye
 * @date 2022-03-15
 */
public class MamVideoThumb extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long thumbId;

    /** 视频ID */
    @Excel(name = "视频ID")
    private Long videoId;

    /** 图片目录 */
    @Excel(name = "图片目录")
    private String subpath;

    /** 当前截取时间 */
    @Excel(name = "当前截取时间")
    private String interceptTime;

    /** 文件名称 */
    @Excel(name = "文件名称")
    private String filename;

    /** 文件路径 */
    @Excel(name = "文件路径")
    private String filepath;

    /** 创建者ID */
    @Excel(name = "创建者ID")
    private Long createUserId;

    public void setThumbId(Long thumbId) 
    {
        this.thumbId = thumbId;
    }

    public Long getThumbId() 
    {
        return thumbId;
    }
    public void setVideoId(Long videoId) 
    {
        this.videoId = videoId;
    }

    public Long getVideoId() 
    {
        return videoId;
    }
    public void setSubpath(String subpath) 
    {
        this.subpath = subpath;
    }

    public String getSubpath() 
    {
        return subpath;
    }
    public void setInterceptTime(String interceptTime) 
    {
        this.interceptTime = interceptTime;
    }

    public String getInterceptTime() 
    {
        return interceptTime;
    }
    public void setFilename(String filename) 
    {
        this.filename = filename;
    }

    public String getFilename() 
    {
        return filename;
    }
    public void setFilepath(String filepath) 
    {
        this.filepath = filepath;
    }

    public String getFilepath() 
    {
        return filepath;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("thumbId", getThumbId())
            .append("videoId", getVideoId())
            .append("subpath", getSubpath())
            .append("interceptTime", getInterceptTime())
            .append("filename", getFilename())
            .append("filepath", getFilepath())
            .append("createBy", getCreateBy())
            .append("createUserId", getCreateUserId())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
