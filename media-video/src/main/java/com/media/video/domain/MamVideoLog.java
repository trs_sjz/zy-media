package com.media.video.domain;

import com.media.common.annotation.Excel;
import com.media.common.core.domain.BaseEntity;
import com.media.common.core.domain.entity.SysUser;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 视频日志对象 mam_video_log
 * 
 * @author liziye
 * @date 2022-03-18
 */
public class MamVideoLog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 日志ID */
    private Long logId;

    /** 对象ID */
    @Excel(name = "对象ID")
    private Long objectId;

    /** 操作用户 */
    @Excel(name = "操作用户")
    private Long userId;

    /** 创建该视频的用户id */
    @Excel(name = "创建该视频的用户id")
    private Long createUserId;

    /** 视频分类id */
    @Excel(name = "视频分类id")
    private Long categoryId;

    /** 操作媒资类型 */
    @Excel(name = "操作媒资类型")
    private String actionMediaType;

    /** 操作action */
    @Excel(name = "操作action")
    private String action;

    /** 操作名词 */
    @Excel(name = "操作名词")
    private String actionName;

    /** 操作详情 */
    @Excel(name = "操作详情")
    private String actionDes;

    /** 操作APP编码 */
    @Excel(name = "操作APP编码")
    private String appCode;

    /** 来源 */
    @Excel(name = "来源")
    private String source;

    /** 操作类型 */
    @Excel(name = "操作类型")
    private String type;

    /** 用户*/
    private SysUser user;

    public void setLogId(Long logId) 
    {
        this.logId = logId;
    }

    public Long getLogId() 
    {
        return logId;
    }
    public void setObjectId(Long objectId) 
    {
        this.objectId = objectId;
    }

    public Long getObjectId() 
    {
        return objectId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }
    public void setCategoryId(Long categoryId) 
    {
        this.categoryId = categoryId;
    }

    public Long getCategoryId() 
    {
        return categoryId;
    }
    public void setActionMediaType(String actionMediaType) 
    {
        this.actionMediaType = actionMediaType;
    }

    public String getActionMediaType() 
    {
        return actionMediaType;
    }
    public void setAction(String action) 
    {
        this.action = action;
    }

    public String getAction() 
    {
        return action;
    }
    public void setActionName(String actionName) 
    {
        this.actionName = actionName;
    }

    public String getActionName() 
    {
        return actionName;
    }
    public void setActionDes(String actionDes) 
    {
        this.actionDes = actionDes;
    }

    public String getActionDes() 
    {
        return actionDes;
    }
    public void setAppCode(String appCode) 
    {
        this.appCode = appCode;
    }

    public String getAppCode() 
    {
        return appCode;
    }
    public void setSource(String source) 
    {
        this.source = source;
    }

    public String getSource() 
    {
        return source;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }

    public SysUser getUser() {
        return user;
    }

    public void setUser(SysUser user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("logId", getLogId())
            .append("objectId", getObjectId())
            .append("userId", getUserId())
            .append("createUserId", getCreateUserId())
            .append("categoryId", getCategoryId())
            .append("actionMediaType", getActionMediaType())
            .append("action", getAction())
            .append("actionName", getActionName())
            .append("actionDes", getActionDes())
            .append("appCode", getAppCode())
            .append("source", getSource())
            .append("type", getType())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
