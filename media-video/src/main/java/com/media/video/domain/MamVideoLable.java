package com.media.video.domain;

import com.media.common.annotation.Excel;
import com.media.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 视频标签对象 mam_video_lable
 * 
 * @author liziye
 * @date 2022-03-16
 */
public class MamVideoLable extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long lableId;

    /** 视频ID */
    @Excel(name = "视频ID")
    private Long videoId;

    /** 人物 */
    @Excel(name = "人物")
    private String person;

    /** 物体 */
    @Excel(name = "物体")
    private String object;

    /** 地点 */
    @Excel(name = "地点")
    private String address;

    /** 动作事件 */
    @Excel(name = "动作事件")
    private String action;

    /** 标识 */
    @Excel(name = "标识")
    private String mark;

    /** 场景 */
    @Excel(name = "场景")
    private String scene;

    /** 组织机构 */
    @Excel(name = "组织机构")
    private String organization;

    /** 关键词 */
    @Excel(name = "关键词")
    private String keyword;

    public void setLableId(Long lableId) 
    {
        this.lableId = lableId;
    }

    public Long getLableId() 
    {
        return lableId;
    }
    public void setVideoId(Long videoId) 
    {
        this.videoId = videoId;
    }

    public Long getVideoId() 
    {
        return videoId;
    }
    public void setPerson(String person) 
    {
        this.person = person;
    }

    public String getPerson() 
    {
        return person;
    }
    public void setObject(String object) 
    {
        this.object = object;
    }

    public String getObject() 
    {
        return object;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setAction(String action) 
    {
        this.action = action;
    }

    public String getAction() 
    {
        return action;
    }
    public void setMark(String mark) 
    {
        this.mark = mark;
    }

    public String getMark() 
    {
        return mark;
    }
    public void setScene(String scene) 
    {
        this.scene = scene;
    }

    public String getScene() 
    {
        return scene;
    }
    public void setOrganization(String organization) 
    {
        this.organization = organization;
    }

    public String getOrganization() 
    {
        return organization;
    }
    public void setKeyword(String keyword) 
    {
        this.keyword = keyword;
    }

    public String getKeyword() 
    {
        return keyword;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("lableId", getLableId())
            .append("videoId", getVideoId())
            .append("person", getPerson())
            .append("object", getObject())
            .append("address", getAddress())
            .append("action", getAction())
            .append("mark", getMark())
            .append("scene", getScene())
            .append("organization", getOrganization())
            .append("keyword", getKeyword())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
