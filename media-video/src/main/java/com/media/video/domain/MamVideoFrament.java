package com.media.video.domain;

import com.media.common.annotation.Excel;
import com.media.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 视频片段对象 mam_video_frament
 * 
 * @author liziye
 * @date 2022-03-18
 */
public class MamVideoFrament extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 片段ID */
    private Long framentId;

    /** 视频ID */
    @Excel(name = "视频ID")
    private Long videoId;

    /** 片段名称 */
    @Excel(name = "片段名称")
    private String name;

    /** 片段截图 */
    @Excel(name = "片段截图")
    private String thumbUrl;

    /** 起始时间点 */
    @Excel(name = "起始时间点")
    private Long start;

    /** 结束时间点 */
    @Excel(name = "结束时间点")
    private Long end;

    /** 创建者ID */
    @Excel(name = "创建者ID")
    private Long createUserId;

    public void setFramentId(Long framentId) 
    {
        this.framentId = framentId;
    }

    public Long getFramentId() 
    {
        return framentId;
    }
    public void setVideoId(Long videoId) 
    {
        this.videoId = videoId;
    }

    public Long getVideoId() 
    {
        return videoId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setThumbUrl(String thumbUrl) 
    {
        this.thumbUrl = thumbUrl;
    }

    public String getThumbUrl() 
    {
        return thumbUrl;
    }
    public void setStart(Long start) 
    {
        this.start = start;
    }

    public Long getStart() 
    {
        return start;
    }
    public void setEnd(Long end) 
    {
        this.end = end;
    }

    public Long getEnd() 
    {
        return end;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("framentId", getFramentId())
            .append("videoId", getVideoId())
            .append("name", getName())
            .append("thumbUrl", getThumbUrl())
            .append("start", getStart())
            .append("end", getEnd())
            .append("createUserId", getCreateUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
