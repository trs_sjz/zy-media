package com.media.video.domain;

import com.media.common.annotation.Excel;
import com.media.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 视频加工任务对象 mam_video_processjob
 * 
 * @author liziye
 * @date 2022-03-15
 */
public class MamVideoProcessjob extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 任务id */
    private Long id;

    /** 创建服务器名称 */
    @Excel(name = "创建服务器名称")
    private String creatornodekey;

    /** 执行服务器名称 */
    @Excel(name = "执行服务器名称")
    private String markernodekey;

    /** 详情 */
    @Excel(name = "详情")
    private String detail;

    /** 对象ID */
    @Excel(name = "对象ID")
    private Long domainobjid;

    /** 开始时间 */
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date marktime;

    /** 排序 */
    @Excel(name = "排序")
    private Long processorder;

    /** 执行的状态 */
    @Excel(name = "执行的状态")
    private String state;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 类型 */
    @Excel(name = "类型")
    private String type;

    /** 用户名称 */
    @Excel(name = "用户名称")
    private String userName;

    /** 创建用户ID */
    @Excel(name = "创建用户ID")
    private Long createByUserid;

    /** 更新者ID */
    @Excel(name = "更新者ID")
    private Long updateByUserid;

    /**视频对象*/
    private MamVideo mamVideo;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCreatornodekey(String creatornodekey) 
    {
        this.creatornodekey = creatornodekey;
    }

    public String getCreatornodekey() 
    {
        return creatornodekey;
    }
    public void setMarkernodekey(String markernodekey) 
    {
        this.markernodekey = markernodekey;
    }

    public String getMarkernodekey() 
    {
        return markernodekey;
    }
    public void setDetail(String detail) 
    {
        this.detail = detail;
    }

    public String getDetail() 
    {
        return detail;
    }
    public void setDomainobjid(Long domainobjid) 
    {
        this.domainobjid = domainobjid;
    }

    public Long getDomainobjid() 
    {
        return domainobjid;
    }
    public void setMarktime(Date marktime) 
    {
        this.marktime = marktime;
    }

    public Date getMarktime() 
    {
        return marktime;
    }
    public void setProcessorder(Long processorder) 
    {
        this.processorder = processorder;
    }

    public Long getProcessorder() 
    {
        return processorder;
    }
    public void setState(String state) 
    {
        this.state = state;
    }

    public String getState() 
    {
        return state;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }
    public void setCreateByUserid(Long createByUserid) 
    {
        this.createByUserid = createByUserid;
    }

    public Long getCreateByUserid() 
    {
        return createByUserid;
    }
    public void setUpdateByUserid(Long updateByUserid) 
    {
        this.updateByUserid = updateByUserid;
    }

    public Long getUpdateByUserid() 
    {
        return updateByUserid;
    }

    public MamVideo getMamVideo() {
        return mamVideo;
    }

    public void setMamVideo(MamVideo mamVideo) {
        this.mamVideo = mamVideo;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("creatornodekey", getCreatornodekey())
            .append("markernodekey", getMarkernodekey())
            .append("detail", getDetail())
            .append("domainobjid", getDomainobjid())
            .append("marktime", getMarktime())
            .append("processorder", getProcessorder())
            .append("state", getState())
            .append("status", getStatus())
            .append("type", getType())
            .append("createBy", getCreateBy())
            .append("createByUserid", getCreateByUserid())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateByUserid", getUpdateByUserid())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
