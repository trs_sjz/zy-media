package com.media.video.domain;

import com.media.common.annotation.Excel;
import com.media.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 源视频对象 mam_video_source
 * 
 * @author liziye
 * @date 2022-03-15
 */
public class MamVideoSource extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long id;

    /** 附件ID */
    private Long attachedpic;

    /** 音频比特率 */
    private Long audiobitrate;

    /** 声道 */
    private Long audiochannels;

    /** 音频编码 */
    private String audiocodec;

    /** 音频格式 */
    private String audioformat;

    /** 音频采样率 */
    private Long audiosamplerate;

    /** 比特率 */
    private Long bitrate;

    /** 格式 */
    private String demuxer;

    /** 时长 */
    private Long duration;

    /** fps */
    private Long fps;

    /** 帧 */
    private Long framerate;

    /** 高度 */
    private Long height;

    /** 媒资类型 */
    private String mediatype;

    /** 丢帧数 */
    private Long nbframes;

    /** 像素 */
    private String pixelformat;

    /** 视频编码 */
    private String videocodec;

    /** 视频格式 */
    private String videoformat;

    /** 视频级别 */
    private String videolevel;

    /** 视频属性 */
    private String videoprofile;

    /** 宽度 */
    private Long width;

    /**  */
    private Integer autosegment;

    /**  */
    private Long categoryid;

    /** 创建IP */
    private String createdip;

    /** 创建方式 */
    private String createdmanner;

    /** 文件后缀 */
    private String fileext;

    /** 文件名称 */
    private String filename;

    /** 文件路径 */
    private String filepath;

    /**  */
    private String fsroot;

    /** 语言 */
    private String lang;

    /** md5值 */
    private String md5sum;

    /** 类型 */
    private String mtype;

    /** 原始名称 */
    private String originname;

    /** 样本ID */
    private Long sampleid;

    /** 文件大小 */
    private Long size;

    /** 来源 */
    private String source;

    /** 状态 */
    @Excel(name = "状态")
    private String status;

    /** 目录 */
    private String subpath;

    /** 标题 */
    @Excel(name = "标题")
    private String title;

    /**  */
    private String typeids;

    /**  */
    private String year;

    /** 创建用户ID */
    private Long createUserId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setAttachedpic(Long attachedpic) 
    {
        this.attachedpic = attachedpic;
    }

    public Long getAttachedpic() 
    {
        return attachedpic;
    }
    public void setAudiobitrate(Long audiobitrate) 
    {
        this.audiobitrate = audiobitrate;
    }

    public Long getAudiobitrate() 
    {
        return audiobitrate;
    }
    public void setAudiochannels(Long audiochannels) 
    {
        this.audiochannels = audiochannels;
    }

    public Long getAudiochannels() 
    {
        return audiochannels;
    }
    public void setAudiocodec(String audiocodec) 
    {
        this.audiocodec = audiocodec;
    }

    public String getAudiocodec() 
    {
        return audiocodec;
    }
    public void setAudioformat(String audioformat) 
    {
        this.audioformat = audioformat;
    }

    public String getAudioformat() 
    {
        return audioformat;
    }
    public void setAudiosamplerate(Long audiosamplerate) 
    {
        this.audiosamplerate = audiosamplerate;
    }

    public Long getAudiosamplerate() 
    {
        return audiosamplerate;
    }
    public void setBitrate(Long bitrate) 
    {
        this.bitrate = bitrate;
    }

    public Long getBitrate() 
    {
        return bitrate;
    }
    public void setDemuxer(String demuxer) 
    {
        this.demuxer = demuxer;
    }

    public String getDemuxer() 
    {
        return demuxer;
    }
    public void setDuration(Long duration) 
    {
        this.duration = duration;
    }

    public Long getDuration() 
    {
        return duration;
    }
    public void setFps(Long fps) 
    {
        this.fps = fps;
    }

    public Long getFps() 
    {
        return fps;
    }
    public void setFramerate(Long framerate) 
    {
        this.framerate = framerate;
    }

    public Long getFramerate() 
    {
        return framerate;
    }
    public void setHeight(Long height) 
    {
        this.height = height;
    }

    public Long getHeight() 
    {
        return height;
    }
    public void setMediatype(String mediatype) 
    {
        this.mediatype = mediatype;
    }

    public String getMediatype() 
    {
        return mediatype;
    }
    public void setNbframes(Long nbframes) 
    {
        this.nbframes = nbframes;
    }

    public Long getNbframes() 
    {
        return nbframes;
    }
    public void setPixelformat(String pixelformat) 
    {
        this.pixelformat = pixelformat;
    }

    public String getPixelformat() 
    {
        return pixelformat;
    }
    public void setVideocodec(String videocodec) 
    {
        this.videocodec = videocodec;
    }

    public String getVideocodec() 
    {
        return videocodec;
    }
    public void setVideoformat(String videoformat) 
    {
        this.videoformat = videoformat;
    }

    public String getVideoformat() 
    {
        return videoformat;
    }
    public void setVideolevel(String videolevel) 
    {
        this.videolevel = videolevel;
    }

    public String getVideolevel() 
    {
        return videolevel;
    }
    public void setVideoprofile(String videoprofile) 
    {
        this.videoprofile = videoprofile;
    }

    public String getVideoprofile() 
    {
        return videoprofile;
    }
    public void setWidth(Long width) 
    {
        this.width = width;
    }

    public Long getWidth() 
    {
        return width;
    }
    public void setAutosegment(Integer autosegment) 
    {
        this.autosegment = autosegment;
    }

    public Integer getAutosegment() 
    {
        return autosegment;
    }
    public void setCategoryid(Long categoryid) 
    {
        this.categoryid = categoryid;
    }

    public Long getCategoryid() 
    {
        return categoryid;
    }
    public void setCreatedip(String createdip) 
    {
        this.createdip = createdip;
    }

    public String getCreatedip() 
    {
        return createdip;
    }
    public void setCreatedmanner(String createdmanner) 
    {
        this.createdmanner = createdmanner;
    }

    public String getCreatedmanner() 
    {
        return createdmanner;
    }
    public void setFileext(String fileext) 
    {
        this.fileext = fileext;
    }

    public String getFileext() 
    {
        return fileext;
    }
    public void setFilename(String filename) 
    {
        this.filename = filename;
    }

    public String getFilename() 
    {
        return filename;
    }
    public void setFilepath(String filepath) 
    {
        this.filepath = filepath;
    }

    public String getFilepath() 
    {
        return filepath;
    }
    public void setFsroot(String fsroot) 
    {
        this.fsroot = fsroot;
    }

    public String getFsroot() 
    {
        return fsroot;
    }
    public void setLang(String lang) 
    {
        this.lang = lang;
    }

    public String getLang() 
    {
        return lang;
    }
    public void setMd5sum(String md5sum) 
    {
        this.md5sum = md5sum;
    }

    public String getMd5sum() 
    {
        return md5sum;
    }
    public void setMtype(String mtype) 
    {
        this.mtype = mtype;
    }

    public String getMtype() 
    {
        return mtype;
    }
    public void setOriginname(String originname) 
    {
        this.originname = originname;
    }

    public String getOriginname() 
    {
        return originname;
    }
    public void setSampleid(Long sampleid) 
    {
        this.sampleid = sampleid;
    }

    public Long getSampleid() 
    {
        return sampleid;
    }
    public void setSize(Long size) 
    {
        this.size = size;
    }

    public Long getSize() 
    {
        return size;
    }
    public void setSource(String source) 
    {
        this.source = source;
    }

    public String getSource() 
    {
        return source;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setSubpath(String subpath) 
    {
        this.subpath = subpath;
    }

    public String getSubpath() 
    {
        return subpath;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setTypeids(String typeids) 
    {
        this.typeids = typeids;
    }

    public String getTypeids() 
    {
        return typeids;
    }
    public void setYear(String year) 
    {
        this.year = year;
    }

    public String getYear() 
    {
        return year;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("attachedpic", getAttachedpic())
            .append("audiobitrate", getAudiobitrate())
            .append("audiochannels", getAudiochannels())
            .append("audiocodec", getAudiocodec())
            .append("audioformat", getAudioformat())
            .append("audiosamplerate", getAudiosamplerate())
            .append("bitrate", getBitrate())
            .append("demuxer", getDemuxer())
            .append("duration", getDuration())
            .append("fps", getFps())
            .append("framerate", getFramerate())
            .append("height", getHeight())
            .append("mediatype", getMediatype())
            .append("nbframes", getNbframes())
            .append("pixelformat", getPixelformat())
            .append("videocodec", getVideocodec())
            .append("videoformat", getVideoformat())
            .append("videolevel", getVideolevel())
            .append("videoprofile", getVideoprofile())
            .append("width", getWidth())
            .append("autosegment", getAutosegment())
            .append("categoryid", getCategoryid())
            .append("createdip", getCreatedip())
            .append("createdmanner", getCreatedmanner())
            .append("fileext", getFileext())
            .append("filename", getFilename())
            .append("filepath", getFilepath())
            .append("fsroot", getFsroot())
            .append("lang", getLang())
            .append("md5sum", getMd5sum())
            .append("mtype", getMtype())
            .append("originname", getOriginname())
            .append("sampleid", getSampleid())
            .append("size", getSize())
            .append("source", getSource())
            .append("status", getStatus())
            .append("subpath", getSubpath())
            .append("title", getTitle())
            .append("typeids", getTypeids())
            .append("year", getYear())
            .append("createUserId", getCreateUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
