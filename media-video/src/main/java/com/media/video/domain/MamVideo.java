package com.media.video.domain;

import com.media.common.annotation.Excel;
import com.media.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 视频信息对象 mam_video
 * 
 * @author liziye
 * @date 2022-03-11
 */
public class MamVideo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long videoId;

    /** 部门ID */
    @Excel(name = "部门ID")
    private Long deptId;

    /** 分类ID */
    @Excel(name = "分类ID")
    private Long categoryId;

    /** 分类名称 */
    @Excel(name = "分类名称")
    private String categoryName;


    /** 视频分组 */
    private String groups;

    /** 源视频ID */
    private Long sourceVideoId;

    /** 标题 */
    @Excel(name = "标题")
    private String title;

    /** 副标题 */
    @Excel(name = "副标题")
    private String subtitle;

    /** 关键字 */
    @Excel(name = "关键字")
    private String keywords;

    /** 路径 */
    private String subpath;

    /** 视频名称 */
    private String name;

    /** 视频原名称 */
    private String originfilename;

    /** 播放次数 */
    @Excel(name = "播放次数")
    private Long playcount;

    /** 处理状态 */
    private String processstatus;

    /** 视频状态 */
    private Integer status;

    /** 大小 */
    private Long size;

    /** 来源 */
    private String source;

    /** 码流 */
    private String streams;

    /** 视频截图 */
    private String thumbname;

    /** 视频截图缩略图 */
    private String thumbs;

    /** 音频比特率 */
    private Long audiobitrate;

    /** 声道 */
    private Long audiochannels;

    /** 音频编码器 */
    private String audiocodec;

    /** 音频格式 */
    private String audioformat;

    /** 音频采样率 */
    private Long audiosamplerate;

    /** 比特率 */
    private Long bitrate;

    /** 视频格式 */
    private String demuxer;

    /** 播放时长 */
    private Long duration;

    /** 每秒传输帧数 */
    private Long fps;

    /** 帧速率 */
    private Long framerate;

    /** 视频宽度 */
    private Long width;

    /** 视频高度 */
    private Long height;

    /** 视频类型 */
    private String mediatype;

    /** 丢帧数 */
    private Long nbframes;

    /** 像素 */
    private String pixelformat;

    /** 视频编码 */
    private String videocodec;

    /** 视频格式 */
    private String videoformat;

    /** 视频级别 */
    private String videolevel;

    /** 视频属性 */
    private String videoprofile;

    /** 响应时间 */
    private Long elapsedseconds;

    /** 后缀 */
    private String fileext;

    /** 文件名称 */
    private String filename;

    /** 文件名称基础 */
    private String filenamebase;

    /** 文件基础路径 */
    private String filepathbase;

    /** 过滤关键字 */
    private String filterwords;

    /** 视频作者 */
    @Excel(name = "视频作者")
    private String author;

    /** 1是领导人 0非领导人 */
    @Excel(name = "1是领导人 0非领导人")
    private Integer leader;

    /**预览地址*/
    private String previewUrl;

    /** 删除状态 */
    private String delFlag;

    /** 创建方式 */
    @Excel(name = "创建方式")
    private String createdmanner;

    /** 创建用户ID */
    @Excel(name = "创建用户ID")
    private Long createUserId;

    /**视频标签*/
    private MamVideoLable mamVideoLable;

    /** 删除人 */
    private String deleteBy;

    /** 删除时间 */
    private Date deleteTime;

    /** 视频拍摄时间 */
    @Excel(name = "视频拍摄时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date shootingTime;

    /**上传者*/
    private String uploader;

    public void setVideoId(Long videoId) 
    {
        this.videoId = videoId;
    }

    public Long getVideoId() 
    {
        return videoId;
    }
    public void setDeptId(Long deptId) 
    {
        this.deptId = deptId;
    }

    public Long getDeptId() 
    {
        return deptId;
    }
    public void setCategoryId(Long categoryId) 
    {
        this.categoryId = categoryId;
    }

    public Long getCategoryId() 
    {
        return categoryId;
    }
    public void setGroups(String groups) 
    {
        this.groups = groups;
    }

    public String getGroups() 
    {
        return groups;
    }
    public void setSourceVideoId(Long sourceVideoId) 
    {
        this.sourceVideoId = sourceVideoId;
    }

    public Long getSourceVideoId() 
    {
        return sourceVideoId;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setSubtitle(String subtitle) 
    {
        this.subtitle = subtitle;
    }

    public String getSubtitle() 
    {
        return subtitle;
    }
    public void setKeywords(String keywords) 
    {
        this.keywords = keywords;
    }

    public String getKeywords() 
    {
        return keywords;
    }
    public void setSubpath(String subpath) 
    {
        this.subpath = subpath;
    }

    public String getSubpath() 
    {
        return subpath;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setOriginfilename(String originfilename) 
    {
        this.originfilename = originfilename;
    }

    public String getOriginfilename() 
    {
        return originfilename;
    }
    public void setPlaycount(Long playcount) 
    {
        this.playcount = playcount;
    }

    public Long getPlaycount() 
    {
        return playcount;
    }
    public void setProcessstatus(String processstatus) 
    {
        this.processstatus = processstatus;
    }

    public String getProcessstatus() 
    {
        return processstatus;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setSize(Long size) 
    {
        this.size = size;
    }

    public Long getSize() 
    {
        return size;
    }
    public void setSource(String source) 
    {
        this.source = source;
    }

    public String getSource() 
    {
        return source;
    }
    public void setStreams(String streams) 
    {
        this.streams = streams;
    }

    public String getStreams() 
    {
        return streams;
    }
    public void setThumbname(String thumbname) 
    {
        this.thumbname = thumbname;
    }

    public String getThumbname() 
    {
        return thumbname;
    }
    public void setThumbs(String thumbs) 
    {
        this.thumbs = thumbs;
    }

    public String getThumbs() 
    {
        return thumbs;
    }
    public void setAudiobitrate(Long audiobitrate) 
    {
        this.audiobitrate = audiobitrate;
    }

    public Long getAudiobitrate() 
    {
        return audiobitrate;
    }
    public void setAudiochannels(Long audiochannels) 
    {
        this.audiochannels = audiochannels;
    }

    public Long getAudiochannels() 
    {
        return audiochannels;
    }
    public void setAudiocodec(String audiocodec) 
    {
        this.audiocodec = audiocodec;
    }

    public String getAudiocodec() 
    {
        return audiocodec;
    }
    public void setAudioformat(String audioformat) 
    {
        this.audioformat = audioformat;
    }

    public String getAudioformat() 
    {
        return audioformat;
    }
    public void setAudiosamplerate(Long audiosamplerate) 
    {
        this.audiosamplerate = audiosamplerate;
    }

    public Long getAudiosamplerate() 
    {
        return audiosamplerate;
    }
    public void setBitrate(Long bitrate) 
    {
        this.bitrate = bitrate;
    }

    public Long getBitrate() 
    {
        return bitrate;
    }
    public void setDemuxer(String demuxer) 
    {
        this.demuxer = demuxer;
    }

    public String getDemuxer() 
    {
        return demuxer;
    }
    public void setDuration(Long duration) 
    {
        this.duration = duration;
    }

    public Long getDuration() 
    {
        return duration;
    }
    public void setFps(Long fps) 
    {
        this.fps = fps;
    }

    public Long getFps() 
    {
        return fps;
    }
    public void setFramerate(Long framerate) 
    {
        this.framerate = framerate;
    }

    public Long getFramerate() 
    {
        return framerate;
    }
    public void setWidth(Long width) 
    {
        this.width = width;
    }

    public Long getWidth() 
    {
        return width;
    }
    public void setHeight(Long height) 
    {
        this.height = height;
    }

    public Long getHeight() 
    {
        return height;
    }
    public void setMediatype(String mediatype) 
    {
        this.mediatype = mediatype;
    }

    public String getMediatype() 
    {
        return mediatype;
    }
    public void setNbframes(Long nbframes) 
    {
        this.nbframes = nbframes;
    }

    public Long getNbframes() 
    {
        return nbframes;
    }
    public void setPixelformat(String pixelformat) 
    {
        this.pixelformat = pixelformat;
    }

    public String getPixelformat() 
    {
        return pixelformat;
    }
    public void setVideocodec(String videocodec) 
    {
        this.videocodec = videocodec;
    }

    public String getVideocodec() 
    {
        return videocodec;
    }
    public void setVideoformat(String videoformat) 
    {
        this.videoformat = videoformat;
    }

    public String getVideoformat() 
    {
        return videoformat;
    }
    public void setVideolevel(String videolevel) 
    {
        this.videolevel = videolevel;
    }

    public String getVideolevel() 
    {
        return videolevel;
    }
    public void setVideoprofile(String videoprofile) 
    {
        this.videoprofile = videoprofile;
    }

    public String getVideoprofile() 
    {
        return videoprofile;
    }
    public void setElapsedseconds(Long elapsedseconds) 
    {
        this.elapsedseconds = elapsedseconds;
    }

    public Long getElapsedseconds() 
    {
        return elapsedseconds;
    }
    public void setFileext(String fileext) 
    {
        this.fileext = fileext;
    }

    public String getFileext() 
    {
        return fileext;
    }
    public void setFilename(String filename) 
    {
        this.filename = filename;
    }

    public String getFilename() 
    {
        return filename;
    }
    public void setFilenamebase(String filenamebase) 
    {
        this.filenamebase = filenamebase;
    }

    public String getFilenamebase() 
    {
        return filenamebase;
    }
    public void setFilepathbase(String filepathbase) 
    {
        this.filepathbase = filepathbase;
    }

    public String getFilepathbase() 
    {
        return filepathbase;
    }
    public void setFilterwords(String filterwords) 
    {
        this.filterwords = filterwords;
    }

    public String getFilterwords() 
    {
        return filterwords;
    }
    public void setAuthor(String author) 
    {
        this.author = author;
    }

    public String getAuthor() 
    {
        return author;
    }
    public void setLeader(Integer leader) 
    {
        this.leader = leader;
    }

    public Integer getLeader() 
    {
        return leader;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setCreatedmanner(String createdmanner) 
    {
        this.createdmanner = createdmanner;
    }

    public String getCreatedmanner() 
    {
        return createdmanner;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }
    public void setDeleteBy(String deleteBy) 
    {
        this.deleteBy = deleteBy;
    }

    public String getDeleteBy() 
    {
        return deleteBy;
    }
    public void setDeleteTime(Date deleteTime) 
    {
        this.deleteTime = deleteTime;
    }

    public Date getDeleteTime() 
    {
        return deleteTime;
    }
    public void setShootingTime(Date shootingTime) 
    {
        this.shootingTime = shootingTime;
    }

    public Date getShootingTime() 
    {
        return shootingTime;
    }

    public void setPreviewUrl() {
        this.previewUrl = "/resource"+this.filepathbase+"/"+this.filenamebase+this.streams;

    }
    public String getPreviewUrl() {
        this.previewUrl = "/profile"+this.filepathbase+"/"+this.filenamebase+this.streams;
        return previewUrl;
    }

    public MamVideoLable getMamVideoLable() {
        return mamVideoLable;
    }

    public void setMamVideoLable(MamVideoLable mamVideoLable) {
        this.mamVideoLable = mamVideoLable;
    }

    public String getUploader() {
        return uploader;
    }

    public void setUploader(String uploader) {
        this.uploader = uploader;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("videoId", getVideoId())
            .append("deptId", getDeptId())
            .append("categoryId", getCategoryId())
            .append("groups", getGroups())
            .append("sourceVideoId", getSourceVideoId())
            .append("title", getTitle())
            .append("subtitle", getSubtitle())
            .append("keywords", getKeywords())
            .append("subpath", getSubpath())
            .append("name", getName())
            .append("originfilename", getOriginfilename())
            .append("playcount", getPlaycount())
            .append("processstatus", getProcessstatus())
            .append("status", getStatus())
            .append("size", getSize())
            .append("source", getSource())
            .append("streams", getStreams())
            .append("thumbname", getThumbname())
            .append("thumbs", getThumbs())
            .append("audiobitrate", getAudiobitrate())
            .append("audiochannels", getAudiochannels())
            .append("audiocodec", getAudiocodec())
            .append("audioformat", getAudioformat())
            .append("audiosamplerate", getAudiosamplerate())
            .append("bitrate", getBitrate())
            .append("demuxer", getDemuxer())
            .append("duration", getDuration())
            .append("fps", getFps())
            .append("framerate", getFramerate())
            .append("width", getWidth())
            .append("height", getHeight())
            .append("mediatype", getMediatype())
            .append("nbframes", getNbframes())
            .append("pixelformat", getPixelformat())
            .append("videocodec", getVideocodec())
            .append("videoformat", getVideoformat())
            .append("videolevel", getVideolevel())
            .append("videoprofile", getVideoprofile())
            .append("elapsedseconds", getElapsedseconds())
            .append("fileext", getFileext())
            .append("filename", getFilename())
            .append("filenamebase", getFilenamebase())
            .append("filepathbase", getFilepathbase())
            .append("filterwords", getFilterwords())
            .append("author", getAuthor())
            .append("leader", getLeader())
            .append("delFlag", getDelFlag())
            .append("createdmanner", getCreatedmanner())
            .append("createUserId", getCreateUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("deleteBy", getDeleteBy())
            .append("deleteTime", getDeleteTime())
            .append("remark", getRemark())
            .append("shootingTime", getShootingTime())
            .toString();
    }
}
