package com.media.video.task;

import com.media.common.utils.file.FileUtils;
import com.media.common.utils.spring.SpringUtils;
import com.media.system.service.ISysConfigService;
import com.media.video.domain.MamVideo;
import com.media.video.domain.MamVideoProcessjob;
import com.media.video.domain.MamVideoSource;
import com.media.video.enums.VideoProcessjobType;
import com.media.video.ffmepg.operation.video.FFMpegVideoFormat;
import com.media.video.ffmepg.operation.video.FFMpegVideoScreenShot;
import com.media.video.service.IMamVideoProcessjobService;
import com.media.video.service.IMamVideoService;
import com.media.video.service.IMamVideoSourceService;
import com.media.video.task.context.FFTaskContext;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 定时任务加工视频，生成缩略图
 * 
 * @author jason
 */
@Service
@Component("videoTask")
public class VideoTask {
	/**
	 * 视频转码任务
	 */
	IMamVideoProcessjobService videoProcessjobService = SpringUtils.getBean(IMamVideoProcessjobService.class);
	/**
	 * 视频转码结果流
	 */
	ISysConfigService configService = SpringUtils.getBean(ISysConfigService.class);
	/**
	 * 视频信息服务类
	 */
	IMamVideoService videoService = SpringUtils.getBean(IMamVideoService.class);
	/**
	 * 源视频服务类
	 */
	IMamVideoSourceService videoSourceService = SpringUtils.getBean(IMamVideoSourceService.class);

	public void process() {
		// ffmpeg路径
		String ffmpegPath = configService.selectConfigByKey("sys.video.ffMpegPath");
		List<MamVideoProcessjob> list = videoProcessjobService.selectMamProcessjobList();
		FFMpegVideoFormat mp4Operation = null;
		FFMepgVideoFormatTask mp4Task = null;
		for (int i = 0; i < list.size(); i++) {
			MamVideoProcessjob processjob = list.get(i);
			//判断是否包含处理规则
			if(StringUtils.isNotBlank(processjob.getDetail())) {
				
				MamVideoSource videoSource = videoSourceService.selectMamVideoSourceById(processjob.getMamVideo().getSourceVideoId());
				if(processjob.getType().equals(VideoProcessjobType.STD.toString())&&StringUtils.isBlank(processjob.getMamVideo().getThumbname())) {
					// 并生成截图
					processVideoInfo(ffmpegPath, videoSource, processjob);
				}
				
				mp4Operation = new FFMpegVideoFormat();
				mp4Operation.setFfmpegPath(ffmpegPath);
				mp4Operation.setProcessjob(processjob);
				mp4Operation.setMamVideoSource(videoSource);
				
				mp4Task = new FFMepgVideoFormatTask(mp4Operation);
				//System.out.println(mp4Operation.toString());
				FFTaskContext.getContext().addTask(mp4Task);
			}
			
		}
	}

	/**
	 * 解析视频详情，同时生成缩略图
	 *
	 */
	private void processVideoInfo(String ffmpegPath, MamVideoSource videoSource, MamVideoProcessjob processjob) {
		MamVideo video = processjob.getMamVideo();
		//缩略图名称
		String thumbName = "";
		if(StringUtils.isNotBlank(video.getThumbname())) {
			thumbName = FileUtils.getFileNameWithoutSuffix(video.getThumbname());
		}else {
			thumbName = "v" + video.getVideoId() + "_" + System.currentTimeMillis();     
		}
    	
        double timePointArray[] = {0,0.25,0.5,0.75};
		// 截图bean
        FFMpegVideoScreenShot videoScreenShot = null;
		// 截图任务
		FFMpegVideoScreenShotTask videoScreenShotTask = null;
		
		boolean isCover = false;
    	for(int i=0;i<timePointArray.length;i++) {
    		double timePoint = timePointArray[i];
    		if(timePoint==0.25) {
    			isCover = true;
    		}else {
    			isCover = false;
    		}
    		double startTimePoint=videoSource.getDuration()*timePoint;
    		String startTime = String.valueOf(startTimePoint);
    		String screenshotName = video.getVideoId() + "_" + String.valueOf(Math.round(startTimePoint));
    		videoScreenShot = new FFMpegVideoScreenShot(ffmpegPath,processjob,videoSource,screenshotName,startTime,isCover,thumbName);
    		videoScreenShotTask = new FFMpegVideoScreenShotTask(videoScreenShot);
			// 截取截图
    		FFTaskContext.getContext().addTask(videoScreenShotTask);
    	}
    	
    	video.setThumbname(thumbName+".jpg");
        video.setThumbs("_middle.jpg;_small.jpg");
		// 更新缩略图信息
		videoService.updateMamVideoProcessInfo(video);

	}
}
