package com.media.video.task;

import com.alibaba.fastjson.JSONObject;
import com.media.common.config.MediaConfig;
import com.media.common.enums.MediaType;
import com.media.common.utils.DateUtils;
import com.media.common.utils.IpUtils;
import com.media.common.utils.spring.SpringUtils;
import com.media.video.domain.MamVideo;
import com.media.video.domain.MamVideoProcessjob;
import com.media.video.domain.MamVideoStream;
import com.media.video.enums.VideoProcessStatus;
import com.media.video.enums.VideoProcessjobType;
import com.media.video.ffmepg.operation.video.FFMpegVideoFormat;
import com.media.video.ffmepg.utils.FFBigDecimalUtil;
import com.media.video.ffmepg.utils.FFVideoUtil;
import com.media.video.ffmepg.utils.FFprobeUtils;
import com.media.video.service.IMamVideoProcessjobService;
import com.media.video.service.IMamVideoService;
import com.media.video.service.IMamVideoStreamService;
import com.media.video.task.bean.FFTaskStateEnum;
import com.media.video.task.bean.FFVideoTask;
import com.media.video.utils.VideoUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 视频转mp4任务 com.ugirls.ffmepg.task.bean.tasks 2018/6/11.
 *
 * @author jason
 * @version V1.0
 */

public class FFMepgVideoFormatTask extends FFVideoTask<FFMpegVideoFormat> {
	/**
	 * 视频转码任务
	 */
	IMamVideoProcessjobService mamVideoProcessjobService = SpringUtils.getBean(IMamVideoProcessjobService.class);
	/**
	 * 视频信息服务类
	 */
	IMamVideoService mamVideoService = SpringUtils.getBean(IMamVideoService.class);
	/**
	 * 视频转码结果流
	 */
	IMamVideoStreamService mamVideoStreamService = SpringUtils.getBean(IMamVideoStreamService.class);
	FFMpegVideoFormat videoFormatMp4;
	/**
	 * 进度正则查询
	 */
	private String frameRegexDuration = "frame=([\\s,\\d]*) fps=(.*?) q=(.*?) size=([\\s\\S]*) time=(.*?) bitrate=([\\s\\S]*) speed=(.*?)x";

	/**
	 * 正则模式
	 */
	private Pattern framePattern = Pattern.compile(frameRegexDuration);

	/**
	 * 线程开启前执行
	 */
	@Override
	public void callExecStart() {
		/* 更新任务状态 */
		System.out.println("this.videoFormatMp4 = " + this.videoFormatMp4);
		MamVideoProcessjob mamVideoProcessjob = this.videoFormatMp4.getProcessjob();
		mamVideoProcessjob.setMarkernodekey(IpUtils.getHostName());
		mamVideoProcessjob.setMarktime(DateUtils.getNowDate());
		mamVideoProcessjob.setUpdateTime(DateUtils.getNowDate());
		mamVideoProcessjob.setState(VideoProcessStatus.ST_PROCESSING.toString());
		mamVideoProcessjobService.updateMamVideoProcessjob(mamVideoProcessjob);
		/* 更新视频状态 */
		if (mamVideoProcessjob.getType().equals(VideoProcessjobType.STD.toString())) {
			Map<String, Object> configMap = JSONObject.parseObject(mamVideoProcessjob.getDetail());
			// 视频比特率
			String streamBitrate = configMap.get("streambitrate").toString();
			if(StringUtils.isBlank(streamBitrate)) {
				streamBitrate="original";
			}
			String streams = "_" + streamBitrate + this.videoFormatMp4.getTargetVideoExtension();
			System.out.println("this.videoFormatMp4.getTargetVideoExtension() = " + this.videoFormatMp4.getTargetVideoExtension());
			System.out.println("streams = " + streams);
			MamVideo mamVideo = this.videoFormatMp4.getProcessjob().getMamVideo();
			mamVideo.setStreams(streams);
			mamVideo.setProcessstatus(VideoProcessStatus.ST_PROCESSING.toString());
			mamVideo.setUpdateTime(DateUtils.getNowDate());
			mamVideoService.updateMamVideoProcessState(mamVideo);
		}
		System.out.println("=======转码线程" + this.getTaskId() + "开始============================");
	}

	/**
	 * 线程结束后的回调
	 */
	@Override
	public void callExecEnd() {
		long elapsedseconds = this.getEndRunTime() - this.getStartRunTime();
		System.out.println("=======转码线程结束" + this.getTaskId() + "开始处理============================"+this.getProgress().getState());
		String videoProcessStatus = "";
		if (this.getProgress().getState().equals(FFTaskStateEnum.COMPLETE)) {
			videoProcessStatus = VideoProcessStatus.SUC_DONE.toString();
			/* 更新视频状态 */
			MamVideo mamVideo = this.videoFormatMp4.getProcessjob().getMamVideo();
			Map<String, Map<String, Object>> mediaInfoMap = FFprobeUtils.getMediaInfo(videoFormatMp4.getFfmpegPath(),
					MediaConfig.getProfile() + videoFormatMp4.getMamVideoSource().getFilepath());
			if (mediaInfoMap != null) {
				Map<String, Object> audioMap = (Map<String, Object>) mediaInfoMap.get("audio");
				Map<String, Object> videoMap = (Map<String, Object>) mediaInfoMap.get("video");
				Map<String, Object> formatMap = (Map<String, Object>) mediaInfoMap.get("format");
                
				if(audioMap!=null) {
					mamVideo.setAudiobitrate(Long.valueOf(String.valueOf(audioMap.get("bit_rate"))));
					mamVideo.setAudiochannels(Long.valueOf(String.valueOf(audioMap.get("channels"))));
					mamVideo.setAudiocodec(String.valueOf(audioMap.get("codec_tag_string")));
					mamVideo.setAudioformat(String.valueOf(audioMap.get("codec_name")));
					mamVideo.setAudiosamplerate(Long.valueOf(String.valueOf(audioMap.get("sample_rate"))));
				}
				if(videoMap!=null) {
					mamVideo.setFramerate(Long.valueOf(String.valueOf(videoMap.get("r_frame_rate"))));
					mamVideo.setHeight(Long.valueOf(String.valueOf(videoMap.get("height"))));
					mamVideo.setMediatype(MediaType.VIDEO.toString());
					mamVideo.setNbframes(Long.valueOf(String.valueOf(videoMap.get("nb_frames"))));
					mamVideo.setPixelformat(String.valueOf(videoMap.get("pix_fmt")));
					mamVideo.setVideocodec(String.valueOf(videoMap.get("codec_tag_string")));
					mamVideo.setVideoformat(String.valueOf(videoMap.get("codec_name")));
					mamVideo.setVideolevel(String.valueOf(videoMap.get("level")));
					mamVideo.setVideoprofile(String.valueOf(videoMap.get("profile")));
					mamVideo.setWidth(Long.valueOf(String.valueOf(videoMap.get("width"))));
				}
				if(formatMap != null && formatMap.get("bit_rate") != null) {
					mamVideo.setBitrate(Long.valueOf(String.valueOf(formatMap.get("bit_rate"))));
					mamVideo.setDemuxer(String.valueOf(formatMap.get("format_name")));
					mamVideo.setDuration(Long.valueOf(String.valueOf(formatMap.get("duration"))));
				
				}
				//video.setFps(0L);
			}
			mamVideo.setFilename(videoFormatMp4.getTargetVideoName() + videoFormatMp4.getTargetVideoExtension());
			mamVideo.setFileext(videoFormatMp4.getTargetVideoExtension());
			mamVideo.setElapsedseconds(elapsedseconds);
			mamVideo.setProcessstatus(videoProcessStatus);
			mamVideo.setUpdateTime(DateUtils.getNowDate());
			mamVideoService.updateMamVideoProcessInfo(mamVideo);
			/** 添加视频流 */
			MamVideoStream mamVideoStream = VideoUtils.getVideoStream(videoFormatMp4.getFfmpegPath(),
					videoFormatMp4.getVideoPrivatePath(),
					videoFormatMp4.getTargetVideoName() + videoFormatMp4.getTargetVideoExtension(), mamVideo,
					elapsedseconds, videoFormatMp4.getCommandCmd());
			//设置流路径
			mamVideoStream.setFilepath(mamVideo.getFilepathbase());
			mamVideoStreamService.insertMamVideoStream(mamVideoStream);
		} else {
			videoProcessStatus = VideoProcessStatus.FAIL_CONVERT.toString();
		}
		// 更新任务状态
		MamVideoProcessjob mamVideoProcessjob = this.videoFormatMp4.getProcessjob();
		mamVideoProcessjob.setUpdateTime(DateUtils.getNowDate());
		mamVideoProcessjob.setState(videoProcessStatus);
		mamVideoProcessjobService.updateMamVideoProcessjobState(mamVideoProcessjob);
		System.out.println("=======转码线程" + this.getTaskId() + "结束" + videoFormatMp4.getProcessjob().getId());
	}

	/**
	 * 任务构造
	 * 
	 * @param format 操作
	 */
	public FFMepgVideoFormatTask(FFMpegVideoFormat format) {
		super(format);
		this.videoFormatMp4 = format;
	}
	

	/**
	 * 回调
	 * 
	 * @param line 一行结果
	 */
	@Override
	public void callBackResultLine(String line) {
		System.out.println("线程执行信息:"+line); 
		if (super.getTimeLengthSec() != null) {
			// 获取视频信息
			Matcher m = framePattern.matcher(line);
			if (m.find()) {
				try {
					String execTimeStr = m.group(5);
					int execTimeInt = FFVideoUtil.getTimelen(execTimeStr);
					double devnum = FFBigDecimalUtil.div(execTimeInt, super.getTimeLengthSec(), 5);
					double progressDouble = FFBigDecimalUtil.mul(devnum, 100);
					super.getProgress().setProgress((int) progressDouble);
					System.out.println(progressDouble+"===============================================");
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
