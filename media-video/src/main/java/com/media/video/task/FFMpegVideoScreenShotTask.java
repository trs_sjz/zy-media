package com.media.video.task;

import com.alibaba.fastjson.JSONObject;
import com.media.common.utils.DateUtils;
import com.media.common.utils.file.FileUtils;
import com.media.common.utils.spring.SpringUtils;
import com.media.video.domain.MamVideoThumb;
import com.media.video.ffmepg.operation.video.FFMpegVideoScreenShot;
import com.media.video.service.IMamVideoThumbService;
import com.media.video.task.bean.FFVideoTask;
import com.media.video.utils.VideoUtils;
import net.coobird.thumbnailator.Thumbnails;

import java.io.File;
import java.io.IOException;
import java.util.Map;

/**
 * 视频截图任务
 * com.ugirls.ffmepg.task.bean.tasks
 * 2018/6/12.
 *
 * @author zhanghaishan
 * @version V1.0
 */
public class FFMpegVideoScreenShotTask extends FFVideoTask<FFMpegVideoScreenShot> {

	/**
	 * 视频信息服务类
	 */
	IMamVideoThumbService iMamVideoThumbService = SpringUtils.getBean(IMamVideoThumbService.class);
	
	private FFMpegVideoScreenShot videoScreenShot;
    public FFMpegVideoScreenShotTask(FFMpegVideoScreenShot operation) {
        super(operation);
        this.videoScreenShot = operation;
    }

    @Override
    public void callBackResultLine(String line) {

    }

    @Override
    public void callExecStart() {
        
    }

    @Override
    public void callExecEnd() {
    	
    	//添加截图
    	String thumbSubpath = videoScreenShot.getProcessjob().getMamVideo().getSubpath()+"/"+videoScreenShot.getProcessjob().getMamVideo().getFilenamebase();
    	MamVideoThumb videoThumb = new MamVideoThumb();
    	videoThumb.setVideoId(videoScreenShot.getProcessjob().getMamVideo().getVideoId());
    	videoThumb.setFilename(videoScreenShot.getScreenshotName()+".jpg");
    	videoThumb.setSubpath(thumbSubpath);
    	videoThumb.setFilepath(videoScreenShot.getProcessjob().getMamVideo().getFilepathbase()+"/"+videoScreenShot.getProcessjob().getMamVideo().getFilenamebase());
    	videoThumb.setInterceptTime(videoScreenShot.getStartTime());
    	videoThumb.setCreateUserId(0L);
    	videoThumb.setCreateBy("MAM.SYSTEM");
    	videoThumb.setCreateTime(DateUtils.getNowDate());
    	videoThumb.setUpdateBy("MAM.SYSTEM");
    	videoThumb.setUpdateTime(DateUtils.getNowDate());
		iMamVideoThumbService.insertMamVideoThumb(videoThumb);
        if(videoScreenShot.isCover()) {
        	 //获取转码设置
            Map<String,Object> configMap =  JSONObject.parseObject(videoScreenShot.getProcessjob().getDetail()); 
            
            int thumbMiddleWidth = Integer.parseInt(configMap.get("thumbMiddleWidth").toString());
            int thumbMiddleHeight = Integer.parseInt(configMap.get("thumbMiddleHeight").toString());
            int thumbSmallWidth = Integer.parseInt(configMap.get("thumbSmallWidth").toString());
            int thumbSmallHeight = Integer.parseInt(configMap.get("thumbSmallHeight").toString());
            
        	String screenshotPath =  VideoUtils.getVideoThumbPath(videoScreenShot.getVideoSource().getSubpath(),videoScreenShot.getProcessjob().getMamVideo().getFilenamebase(),videoScreenShot.getVideoSource().getCreateBy());
    		
        	String fileIn = screenshotPath+File.separator+videoScreenShot.getScreenshotName()+".jpg";
        	String fileOutPath = screenshotPath+File.separator;
			//复制图片到
        	FileUtils.copy(fileIn, fileOutPath+videoScreenShot.getThumbImage()+".jpg");
        	try {
				//app
				Thumbnails.of(fileIn).size(thumbMiddleWidth,thumbMiddleHeight).toFile(fileOutPath+videoScreenShot.getThumbImage()+"_middle.jpg");
				//list
				Thumbnails.of(fileIn).size(thumbSmallWidth,thumbSmallHeight).toFile(fileOutPath+videoScreenShot.getThumbImage()+"_small.jpg");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}  
        }
    }
}
