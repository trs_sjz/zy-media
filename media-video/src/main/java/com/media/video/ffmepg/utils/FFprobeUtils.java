package com.media.video.ffmepg.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.util.IOUtils;
import com.media.common.utils.Arith;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class FFprobeUtils {

	/**
	 * 抽取视频元数据
	 * 
	 * @param ffmpegPath
	 * @param filePath
	 * @return
	 */
	public static String getAnalysisInfo(String ffmpegPath, String filePath) {
		String cmd = ffmpegPath + "ffprobe -v quiet  -show_format -show_streams -print_format json -show_format -i "
				+ filePath;
		StringBuffer sb = new StringBuffer();
		BufferedInputStream in = null;
		BufferedReader inBr = null;
		try {
			Runtime run = Runtime.getRuntime();
			Process p;
			if (SystemUtils.IS_OS_LINUX) {
				p = run.exec(new String[] { "/bin/sh", "-c", cmd });
			} else {
				p = run.exec(cmd);
			}
          
			in = new BufferedInputStream(p.getInputStream());
			inBr = new BufferedReader(new InputStreamReader(in));
			String lineStr;
			while ((lineStr = inBr.readLine()) != null){
				sb.append(lineStr);
			}
			if (p.waitFor() != 0) {
				if (p.exitValue() == 1){
					System.err.println("命令执行失败!");
				}
			}

		} catch (Exception e) {
			System.out.println(e.getMessage()+"获取视频信息失败");
			e.printStackTrace();
		} finally {
			IOUtils.close(inBr);
			IOUtils.close(in);
		}
		return sb.toString();
	}

	/**
	 * 获取视频元数据,并转换格式
	 * 
	 * @param ffmpegPath
	 * @param filePath
	 * @return
	 */
	public static Map<String, Map<String, Object>> getMediaInfo(String ffmpegPath, String filePath) {
		Map<String, Map<String, Object>> map = null;
		String jsonStrBody = getAnalysisInfo(ffmpegPath, filePath); // 获取视频元数据
		System.out.println(jsonStrBody);
		System.out.println("====解析开始======================");
		System.out.println(jsonStrBody);
		System.out.println("=========解析结束=================");
		if (StringUtils.isNotBlank(jsonStrBody)) {
			map = new HashMap<String, Map<String, Object>>();
			JSONObject jsonobj = JSON.parseObject(jsonStrBody);
			if (jsonobj.getJSONObject("format") != null) {
				Map<String, Object> format = new HashMap<String, Object>();
				String duration = jsonobj.getJSONObject("format").getString("duration");
				format.put("duration", duration.substring(0, duration.indexOf(".")));
				format.put("start_time", jsonobj.getJSONObject("format").getString("start_time"));
				format.put("bit_rate", jsonobj.getJSONObject("format").getString("bit_rate"));
				format.put("size", jsonobj.getJSONObject("format").getString("size"));
				format.put("probe_score", jsonobj.getJSONObject("format").getString("probe_score"));
				format.put("nb_programs", jsonobj.getJSONObject("format").getString("nb_programs"));
				format.put("format_long_name", jsonobj.getJSONObject("format").getString("format_long_name"));
				format.put("nb_streams", jsonobj.getJSONObject("format").getString("nb_streams"));
				format.put("format_name", jsonobj.getJSONObject("format").getString("format_name"));
				map.put("format", format);
			}

			if (jsonobj.getJSONArray("streams") != null) {
				// 音频数据
				Map<String, Object> audio = null;
				// 视频数据
				Map<String, Object> video = null;

				JSONArray streams = jsonobj.getJSONArray("streams");
				for (int i = 0; i < streams.size(); i++) {
					String codecType = streams.getJSONObject(i).getString("codec_type");
					if (StringUtils.isNotBlank(codecType) && codecType.equals("audio")) {
						audio = new HashMap<String, Object>();
						audio.put("time_base", jsonobj.getJSONArray("streams").getJSONObject(i).getString("time_base"));
						String r_frame_rate = jsonobj.getJSONArray("streams").getJSONObject(i)
								.getString("r_frame_rate");

						audio.put("r_frame_rate", Arith.divisible(r_frame_rate));
						audio.put("start_pts", jsonobj.getJSONArray("streams").getJSONObject(i).getString("start_pts"));
						audio.put("index", jsonobj.getJSONArray("streams").getJSONObject(i).getString("index"));
						audio.put("duration_ts",
								jsonobj.getJSONArray("streams").getJSONObject(i).getString("duration_ts"));
						audio.put("codec_name",
								jsonobj.getJSONArray("streams").getJSONObject(i).getString("codec_name"));
						String audioDuration = jsonobj.getJSONArray("streams").getJSONObject(i).getString("duration");
						if (StringUtils.isNotBlank(audioDuration)) {
							audio.put("duration", audioDuration.substring(0, audioDuration.indexOf(".")));
						}

						audio.put("start_time",jsonobj.getJSONArray("streams").getJSONObject(i).getString("start_time"));
						
						audio.put("bit_rate", jsonobj.getJSONArray("streams").getJSONObject(i).getString("bit_rate"));
				        
						audio.put("codec_tag", jsonobj.getJSONArray("streams").getJSONObject(i).getString("codec_tag"));
						audio.put("sample_rate",
								jsonobj.getJSONArray("streams").getJSONObject(i).getString("sample_rate"));
						audio.put("channels", jsonobj.getJSONArray("streams").getJSONObject(i).getString("channels"));
						audio.put("sample_fmt",
								jsonobj.getJSONArray("streams").getJSONObject(i).getString("sample_fmt"));
						audio.put("codec_time_base",
								jsonobj.getJSONArray("streams").getJSONObject(i).getString("codec_time_base"));
						audio.put("codec_tag_string",
								jsonobj.getJSONArray("streams").getJSONObject(i).getString("codec_tag_string"));
						audio.put("bits_per_sample",
								jsonobj.getJSONArray("streams").getJSONObject(i).getString("bits_per_sample"));
						audio.put("avg_frame_rate",
								jsonobj.getJSONArray("streams").getJSONObject(i).getString("avg_frame_rate"));
						audio.put("codec_type",
								jsonobj.getJSONArray("streams").getJSONObject(i).getString("codec_type"));
						audio.put("codec_long_name",
								jsonobj.getJSONArray("streams").getJSONObject(i).getString("codec_long_name"));
						map.put("audio", audio);
					} else if (StringUtils.isNotBlank(codecType) && codecType.equals("video")) {
						video = new HashMap<String, Object>(); 
						video.put("pix_fmt", jsonobj.getJSONArray("streams").getJSONObject(i).getString("pix_fmt"));
						video.put("r_frame_rate", Arith
								.divisible(jsonobj.getJSONArray("streams").getJSONObject(i).getString("r_frame_rate")));
						video.put("start_pts", jsonobj.getJSONArray("streams").getJSONObject(i).getString("start_pts"));
						video.put("duration_ts",
								jsonobj.getJSONArray("streams").getJSONObject(i).getString("duration_ts"));
						video.put("duration", jsonobj.getJSONArray("streams").getJSONObject(i).getString("duration"));
						video.put("bit_rate", jsonobj.getJSONArray("streams").getJSONObject(i).getString("bit_rate"));
						video.put("sample_aspect_ratio",
								jsonobj.getJSONArray("streams").getJSONObject(i).getString("sample_aspect_ratio"));
						video.put("codec_tag_string",
								jsonobj.getJSONArray("streams").getJSONObject(i).getString("codec_tag_string"));
						video.put("avg_frame_rate",
								jsonobj.getJSONArray("streams").getJSONObject(i).getString("avg_frame_rate"));
						video.put("codec_long_name",
								jsonobj.getJSONArray("streams").getJSONObject(i).getString("codec_long_name"));
						video.put("height", jsonobj.getJSONArray("streams").getJSONObject(i).getString("height"));
						video.put("time_base", jsonobj.getJSONArray("streams").getJSONObject(i).getString("time_base"));
						video.put("coded_height",
								jsonobj.getJSONArray("streams").getJSONObject(i).getString("coded_height"));
						video.put("level", jsonobj.getJSONArray("streams").getJSONObject(i).getString("level"));
						video.put("profile", jsonobj.getJSONArray("streams").getJSONObject(i).getString("profile"));
						video.put("index", jsonobj.getJSONArray("streams").getJSONObject(i).getString("index"));
						video.put("codec_name",
								jsonobj.getJSONArray("streams").getJSONObject(i).getString("codec_name"));
						video.put("start_time",
								jsonobj.getJSONArray("streams").getJSONObject(i).getString("start_time"));
						video.put("codec_tag", jsonobj.getJSONArray("streams").getJSONObject(i).getString("codec_tag"));
						video.put("has_b_frames",
								jsonobj.getJSONArray("streams").getJSONObject(i).getString("has_b_frames"));
						video.put("refs", jsonobj.getJSONArray("streams").getJSONObject(i).getString("refs"));
						video.put("codec_time_base",
								jsonobj.getJSONArray("streams").getJSONObject(i).getString("codec_time_base"));
						video.put("width", jsonobj.getJSONArray("streams").getJSONObject(i).getString("width"));
						video.put("display_aspect_ratio",
								jsonobj.getJSONArray("streams").getJSONObject(i).getString("display_aspect_ratio"));
						video.put("coded_width",
								jsonobj.getJSONArray("streams").getJSONObject(i).getString("coded_width"));
						video.put("codec_type",
								jsonobj.getJSONArray("streams").getJSONObject(i).getString("codec_type"));
						String nb_frames = "0";
						if (StringUtils
								.isNotBlank(jsonobj.getJSONArray("streams").getJSONObject(i).getString("nb_frames"))) {
							nb_frames = jsonobj.getJSONArray("streams").getJSONObject(i).getString("nb_frames");
						}
						video.put("nb_frames", nb_frames);
						map.put("video", video);
					}

				}

			}
		}
		return map;
	}

}
