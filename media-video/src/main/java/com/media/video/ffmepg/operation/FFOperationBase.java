package com.media.video.ffmepg.operation;


import java.util.ArrayList;
import java.util.List;

/**
 * TODO:
 * com.ugirls.ffmepg.operation
 * 2018/6/6.
 *
 * @author zhanghaishan
 * @version V1.0
 */
public abstract class FFOperationBase {

    private String ffmpegPath = null;
    private String command = null;
    private List<String> commandParams = new ArrayList<>();

    
    
    public String getFfmpegPath() {
		return ffmpegPath;
	}

	public void setFfmpegPath(String ffmpegPath) {
		this.ffmpegPath = ffmpegPath;
	}

	/**
     * 返回命令
     * @return 命令
     */
    public String getCommand(){
        if(command == null){
            command = toString();
        }
        return command;
    }

    public void setCommand(String command) {
		this.command = command;
	}

	/**
     * 返回命令参数
     * @return 参数
     */
    public List<String> getCommandParams(){
        if(commandParams.size()==0){
            toString();
        }
        return commandParams;
    }


    /**
     * 直接转成命令
     * @return 命令
     */
    @Override
    public String toString() {
        return command;
    }
}
