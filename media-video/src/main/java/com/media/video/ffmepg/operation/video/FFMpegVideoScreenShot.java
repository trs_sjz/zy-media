package com.media.video.ffmepg.operation.video;

import com.media.common.config.MediaConfig;
import com.media.common.utils.StringUtils;
import com.media.video.domain.MamVideoProcessjob;
import com.media.video.domain.MamVideoSource;
import com.media.video.ffmepg.operation.FFMpegOperationBase;
import com.media.video.utils.VideoUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * 视频截图操作 com.ugirls.ffmepg.operation.ffmpeg.vidoe 2018/6/6.
 *
 * @author zhanghaishan
 * @version V1.0
 */
public class FFMpegVideoScreenShot extends FFMpegOperationBase {

	private String commandCmd;
	/**
	 * ffmpeg路径
	 */
	private String ffmpegPath;
	private MamVideoProcessjob mamVideoProcessjob;
	private MamVideoSource mamVideoSource;
	/**
	 * 截图名称
	 */
	private String screenshotName;
	/**
	 * 截图时间
	 */
	private String startTime;
	/**
	 * 是否封面
	 */
	private boolean isCover;
	/**
	 * 视频缩略图
	 */
	private String thumbImage;

	/**
	 * 视频截图操作
	 *
	 */

	public FFMpegVideoScreenShot(String ffmpegPath, MamVideoProcessjob mamVideoProcessjob, MamVideoSource mamVideoSource,
			String screenshotName, String startTime, boolean isCover, String thumbImage) {
		super();
		this.ffmpegPath = ffmpegPath;
		this.mamVideoProcessjob = mamVideoProcessjob;
		this.mamVideoSource = mamVideoSource;
		this.screenshotName = screenshotName;
		this.startTime = startTime;
		this.isCover = isCover;
		this.thumbImage = thumbImage;
	}
	public String getCommandCmd() {
		return commandCmd;
	}

	

	public void setCommandCmd(String commandCmd) {
		this.commandCmd = commandCmd;
	}
	@Override
	public String getFfmpegPath() {
		return ffmpegPath;
	}

	@Override
	public void setFfmpegPath(String ffmpegPath) {
		this.ffmpegPath = ffmpegPath;
	}

	public String getScreenshotName() {
		return screenshotName;
	}

	public void setScreenshotName(String screenshotName) {
		this.screenshotName = screenshotName;
	}

	public MamVideoSource getVideoSource() {
		return mamVideoSource;
	}
	public void setVideoSource(MamVideoSource videoSource) {
		this.mamVideoSource = videoSource;
	}
	public MamVideoProcessjob getProcessjob() {
		return mamVideoProcessjob;
	}
	public void setProcessjob(MamVideoProcessjob processjob) {
		this.mamVideoProcessjob = processjob;
	}
	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public boolean isCover() {
		return isCover;
	}

	public void setCover(boolean isCover) {
		this.isCover = isCover;
	}

	public String getThumbImage() {
		return thumbImage;
	}

	public void setThumbImage(String thumbImage) {
		this.thumbImage = thumbImage;
	}

	/**
	 * 直接转成命令
	 * 
	 * @return 命令
	 */
	@Override
	public String toString() {
		// 获取源视频路径
		String videoSourceSrc = MediaConfig.getProfile() + mamVideoSource.getFilepath() ;
		String screenshotPath =  VideoUtils.getVideoThumbPath(mamVideoSource.getSubpath(), mamVideoProcessjob.getMamVideo().getFilenamebase(), mamVideoSource.getCreateBy());
		//源视频分辨率
		String resolution = mamVideoSource.getWidth() + "x" + mamVideoSource.getHeight();
		
        String imageSrc = screenshotPath+File.separator+screenshotName+".jpg";
		List<String> cutpic = new ArrayList<String>();
		cutpic.add(ffmpegPath + "ffmpeg");
		// 添加参数＂-ss＂，该参数指定截取的起始时间
		cutpic.add("-ss");
		// 添加起始时间为第1秒
		cutpic.add(startTime);
		cutpic.add("-i");
		cutpic.add(videoSourceSrc);
		cutpic.add("-y");
		cutpic.add("-f");
		cutpic.add("image2");
		// 添加参数＂-t＂，该参数指定持续时间
		cutpic.add("-t");
		// 添加持续时间为1毫秒
		cutpic.add("0.001");
		// 添加参数＂-s＂，该参数指定截取的图片大小
		cutpic.add("-s");
		// 添加截取的图片大小为350*240
		cutpic.add(resolution);
		// 添加截取的图片的保存路径
		cutpic.add(imageSrc);
		this.commandCmd = StringUtils.join(cutpic.toArray(), " ");
		return commandCmd;
	}
}
