package com.media.video.ffmepg.excption;

/**
 * 未找到异常
 * com.trs.ffmepg.excption
 * 2018/11/29.
 *
 * @author zhanghaishan
 * @version V1.0
 */
public class FFMpegOperationNotFoundExcption extends RuntimeException {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public FFMpegOperationNotFoundExcption(String ex){
        super(ex);
    }
}
