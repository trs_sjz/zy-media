package com.media.video.ffmepg.operation;


import com.media.video.ffmepg.annotation.FFCmd;

/**
 * TODO: ffmepg命令处理抽象父类
 * com.ugirls.ffmepg.operation.ffmpeg
 * 2018/6/6.
 *
 * @author zhanghaishan
 * @version V1.0
 */
@FFCmd(key = "ffmpeg")
public abstract class FFMpegOperationBase extends FFOperationBase {

}
