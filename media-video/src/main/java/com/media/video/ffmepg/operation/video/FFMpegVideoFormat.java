package com.media.video.ffmepg.operation.video;

import com.alibaba.fastjson.JSONObject;
import com.media.common.config.MediaConfig;
import com.media.common.core.text.Convert;
import com.media.common.utils.spring.SpringUtils;
import com.media.system.domain.SysSetting;
import com.media.system.service.ISysSettingService;
import com.media.video.domain.MamVideoProcessjob;
import com.media.video.domain.MamVideoSource;
import com.media.video.ffmepg.operation.FFMpegOperationBase;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.Map;

/**
 * 视频转码
 *
 * @author liziye
 * @date 2022-03-15
 */
public class FFMpegVideoFormat extends FFMpegOperationBase {

	/**
	 * 转码任务
	 */
	private MamVideoProcessjob mamVideoProcessjob;
	//视频源数据
	private MamVideoSource mamVideoSource;
	/**
	 * 生成的转码
	 */
	private String commandCmd;
	/**
	 * ffmpeg路径
	 */
	private String ffmpegPath;
	/**
	 * 转码的视频文件名
	 */
	private String targetVideoName;
	/**
	 * 目标视频后缀
	 */
	private String targetVideoExtension;
	/**
	 * 转码的视频地址
	 */
	private String videoPrivatePath;
	
	public String getCommandCmd() {
		return commandCmd;
	}

	public void setCommandCmd(String commandCmd) {
		this.commandCmd = commandCmd;
	}

	@Override
	public String getFfmpegPath() {
		return ffmpegPath;
	}

	@Override
	public void setFfmpegPath(String ffmpegPath) {
		this.ffmpegPath = ffmpegPath;
	}

	
	public MamVideoProcessjob getProcessjob() {
		return mamVideoProcessjob;
	}

	public void setProcessjob(MamVideoProcessjob mamVideoProcessjob) {
		this.mamVideoProcessjob = mamVideoProcessjob;
	}
	
	
	 public String getTargetVideoName() {
		return targetVideoName;
	}

	public void setTargetVideoName(String targetVideoName) {
		this.targetVideoName = targetVideoName;
	}

	public String getTargetVideoExtension() {
		return targetVideoExtension;
	}

	public void setTargetVideoExtension(String targetVideoExtension) {
		this.targetVideoExtension = targetVideoExtension;
	}

	public String getVideoPrivatePath() {
		return videoPrivatePath;
	}

	public void setVideoPrivatePath(String videoPrivatePath) {
		
		this.videoPrivatePath = videoPrivatePath;
		
	}

	public MamVideoSource getMamVideoSource() {
		return mamVideoSource;
	}

	public void setMamVideoSource(MamVideoSource mamVideoSource) {
		this.mamVideoSource = mamVideoSource;
	}

	/**
	 * 创建视频转换路径
	 */
	private String createVideoPrivatePath(String videoPrivatePath) {
		this.videoPrivatePath = videoPrivatePath;
		File videoPrivateFolder = new File(videoPrivatePath);
        //创建父目录
        if (videoPrivateFolder.getParentFile() != null && !videoPrivateFolder.getParentFile().exists()) {
        	videoPrivateFolder.getParentFile().mkdirs();
        }
        return videoPrivatePath;
	}
  
	/**
     * 直接转成命令
     * @return 命令
     */
    @Override
    public String toString() {

		ISysSettingService sysSettingService = SpringUtils.getBean(ISysSettingService.class);

		String videoSrc = MediaConfig.getProfile() + mamVideoSource.getFilepath();
    	//ISysSettingService sysSettingService = SpringUtils.getBean(ISysSettingService.class);
    	 //获取转码设置
        Map<String,Object> configMap =  JSONObject.parseObject(mamVideoProcessjob.getDetail());
		//视频质量
        String streamquality = configMap.get("streamquality").toString();
		//视频比特率
        String streamformat = configMap.get("streamformat").toString();
		//视频比特率
    	String streamBitrate = configMap.get("streambitrate").toString();
    	Long streamResolution = Convert.toLong(configMap.get("streamresolution"));
		//音频比特率
        String streamaudioBitrate = configMap.get("streamaudioBitrate").toString();

		//获取系统配置信息
        SysSetting settingInfo = sysSettingService.selectSysSettingById(1);
        
        if(streamformat.equals("FLV")) {
        	this.targetVideoExtension = ".flv";
        }else {
        	this.targetVideoExtension = ".mp4";
        }
        
        if(StringUtils.isBlank(streamBitrate)) {
			streamBitrate="original";
		}
        
        this.targetVideoName = mamVideoProcessjob.getMamVideo().getFilenamebase()+"_"+streamBitrate;
        String videoPrivatePath = createVideoPrivatePath(MediaConfig.getProfile() + mamVideoProcessjob.getMamVideo().getFilepathbase()
    			+File.separator+this.targetVideoName+this.targetVideoExtension);      //转码生成的路径
    
        //String resolution=videoSource.getWidth()+"x"+videoSource.getHeight();    //分辨率
        
        String resolution = "";
    	if(streamResolution > mamVideoSource.getHeight()) {
    		resolution = mamVideoSource.getWidth()+"x"+mamVideoSource.getHeight();
    	}else {
    		//Long width = videoSource.getWidth()*streamResolution/videoSource.getHeight()+1;
    		Long width = mamVideoSource.getWidth()/(mamVideoSource.getHeight()/streamResolution);
    		resolution = width+"x"+streamResolution;
    	}
    	
    	StringBuffer command = new StringBuffer();
    	command.append(ffmpegPath);
    	command.append("ffmpeg");
    	command.append(" -benchmark ");
    	command.append(" -i ");
    	command.append(videoSrc);
    	command.append(" -y ");
    	command.append(" -c:a ");
    	if(streamformat.equals("FLV")) {  
    		command.append(" libmp3lame ");
    		command.append(" -ar ");
    		command.append(" 22050 ");
    		if(!streamquality.equals("0")) {
	    		command.append(" -b ");
	    		command.append(streamBitrate);
    		}
    		command.append(" -copyts ");
    		command.append(" -g ");
    		command.append(" 3 ");
    	}else {  
    		//转码MP4
    		if(mamVideoSource.getFileext().equals("wmv")) {
    			if(!streamquality.equals("0")) {
	        		command.append(" libfaac -b:a ");
	        		command.append(streamaudioBitrate);
    			}
        	}else {
        		command.append(" copy ");
        	}
        	command.append(" -c:v ");
        	command.append(" libx264  ");
        	command.append(" -fpre ");
        	command.append(this.ffmpegPath);
        	command.append("libx264-hq.ffpreset");
        	if(!streamquality.equals("0")) {
	        	command.append(" -b:v ");
	        	command.append(streamBitrate);
	        	command.append(" -bt ");
	        	command.append(streamBitrate);
        	}
    	}
    	command.append(" -filter:v ");
    	command.append(" yadif ");
    	if(!streamquality.equals("0")) {
    		command.append(" -s ");
        	command.append(resolution);
    	}
    	//视频水印存在bug，待解决
    	/*if(settingInfo.getVideologoMode()!=null && settingInfo.getVideologoMode()==1) {
    		System.out.println("台标模式");
    		command.append(" ");
    		command.append(" -vf ");
    		command.append("'movie=");
    		command.append(MediaConfig.getProfile() + settingInfo.getVideologoImage());
    		command.append(" [watermark]; [in][watermark] overlay=main_w-overlay_w-10:10 [out]'");
    		//command.append(" \"movie=D\\\\:/TRS/TRSMAM/resource/videologo/videologo.png [watermark]; [in][watermark] overlay=main_w-overlay_w-10:10 [out]\" ");
    		System.out.println("台标模式"+command.toString());
    	}*/
    	command.append(" ");
    	command.append(videoPrivatePath);
    	
    	this.commandCmd = command.toString();
        return commandCmd;
    }
    
}
