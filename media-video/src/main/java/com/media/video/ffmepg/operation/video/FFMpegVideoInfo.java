package com.media.video.ffmepg.operation.video;

import com.media.video.ffmepg.annotation.FFCmd;
import com.media.video.ffmepg.operation.FFMpegOperationBase;

/**
 * FFmpeg操作父类
 * com.ugirls.ffmepg.operation.ffmpeg.vidoe
 * 2018/6/11.
 *
 * @author zhanghaishan
 * @version V1.0
 */
public class FFMpegVideoInfo extends FFMpegOperationBase {

    @FFCmd(key = "-i")
    private String videoUrl;

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }
}
