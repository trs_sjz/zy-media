package com.media.video.ffmepg.excption;

/**
 * 操作转换异常
 * com.trs.ffmepg.excption
 * 2018/11/29.
 *
 * @author zhanghaishan
 * @version V1.0
 */
public class FFMpegOperationConvertExcption extends RuntimeException{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public FFMpegOperationConvertExcption(String ex){
        super(ex);
    }

}
