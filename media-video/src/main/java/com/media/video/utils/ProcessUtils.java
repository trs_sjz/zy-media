package com.media.video.utils;

import com.media.common.enums.WorkFlowState;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;


public class ProcessUtils {
	/**
     * 获取上一个序列ID
     * @param procinst
     * @return
     */
	public static String getPrevActId(String procinst,String key) {
		String procId = "0";
		List<String> list = Arrays.asList(procinst.split(","));
		if(list!=null&&list.size()>0) {
			int index = list.indexOf(key);
			if(index==-1) {
				procId = WorkFlowState.NONE.toString();
			}else if(index==0) {
				procId = "0";
			}else if(index>0) {
				procId = list.get(index-1);
			}
		}
		return procId;
	}
	/**
     * 获取下一个序列ID
     * @param procinst
     * @return
     */
	public static String getNextActId(String procinst,String key) {
		String procId = "0";
		List<String> list = Arrays.asList(procinst.split(","));
		if(list!=null&&list.size()>0) {
			int index = list.indexOf(key);
			if(index==-1) {
				procId = WorkFlowState.NONE.toString();
			}else if(index<list.size()-1) {
				procId = list.get(index+1);
			}else {
				procId = WorkFlowState.SUC_DONE.toString();
			}
		}

	    return procId;
	}
    /**
     * 获取序列最后一个ID
     * @param procinst
     * @return
     */
	public static String getLastActId(String procinst) {
		String lastId = "0";
		if(StringUtils.isNotBlank(procinst)) {
			String[] arr = procinst.split(",");
			lastId = arr[arr.length-1];
		}
	    return lastId;
	}
}
