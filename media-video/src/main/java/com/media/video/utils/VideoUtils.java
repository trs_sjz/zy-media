package com.media.video.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.media.common.config.MediaConfig;
import com.media.common.constant.MediaConstrans;
import com.media.common.enums.MediaType;
import com.media.common.utils.Arith;
import com.media.common.utils.DateUtils;
import com.media.common.utils.file.FileUtils;
import com.media.video.domain.MamVideo;
import com.media.video.domain.MamVideoStream;
import com.media.video.ffmepg.utils.FFprobeUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;

public class VideoUtils {
    
	/**
	 * 读取视频信息
	 * @param ffprobePath
	 * @param filePath
	 * @param fileName
	 * @param video
	 * @param elapsedseconds
	 * @param transcodecmd
	 * @return
	 */
	public static MamVideoStream getVideoStream(String ffprobePath, String filePath, String fileName, MamVideo video, Long elapsedseconds, String transcodecmd) {
		String jsonStrBody = FFprobeUtils.getAnalysisInfo(ffprobePath, filePath);
		 
		JSONObject jsonobj = JSON.parseObject(jsonStrBody);
		MamVideoStream videoStream = new MamVideoStream();
		videoStream.setVideoid(video.getVideoId());
		
		//解析音视频流
		if(jsonobj.getJSONArray("streams")!=null) {
			JSONArray streams = jsonobj.getJSONArray("streams");
			for (int i = 0; i < streams.size(); i++) {
				String codecType = streams.getJSONObject(i).getString("codec_type");
				if (StringUtils.isNotBlank(codecType) && codecType.equals("audio")) {
					videoStream.setAudiobitrate(streams.getJSONObject(i).getLong("bit_rate"));
		        	videoStream.setAudiochannels(streams.getJSONObject(i).getLong("channels"));
		    		videoStream.setAudiocodec(streams.getJSONObject(i).getString("codec_tag_string"));
		    		videoStream.setAudioformat(streams.getJSONObject(i).getString("codec_name"));
		    		videoStream.setAudiosamplerate(streams.getJSONObject(i).getLong("sample_rate"));
				}else if (StringUtils.isNotBlank(codecType) && codecType.equals("video")) {
					 videoStream.setFramerate(Arith
								.divisible(jsonobj.getJSONArray("streams").getJSONObject(i).getString("r_frame_rate")));
					 videoStream.setHeight(jsonobj.getJSONArray("streams").getJSONObject(i).getLong("height"));
					 videoStream.setNbframes(jsonobj.getJSONArray("streams").getJSONObject(i).getLong("nb_frames"));
					 videoStream.setPixelformat(jsonobj.getJSONArray("streams").getJSONObject(i).getString("pix_fmt"));
					 videoStream.setVideocodec(jsonobj.getJSONArray("streams").getJSONObject(i).getString("codec_tag_string"));
					 videoStream.setVideoformat(jsonobj.getJSONArray("streams").getJSONObject(i).getString("codec_name"));
					 videoStream.setVideolevel(jsonobj.getJSONArray("streams").getJSONObject(i).getString("level"));
					 videoStream.setVideoprofile(jsonobj.getJSONArray("streams").getJSONObject(i).getString("profile"));
					 videoStream.setWidth(jsonobj.getJSONArray("streams").getJSONObject(i).getLong("width"));
				}
			}
		}
		
		if(jsonobj.getJSONObject("format")!=null) {
			videoStream.setBitrate(jsonobj.getJSONObject("format").getLong("bit_rate"));
			videoStream.setDemuxer(jsonobj.getJSONObject("format").getString("format_name"));
			double duration =  jsonobj.getJSONObject("format").getDouble("duration");
			videoStream.setDuration(new Double(duration).longValue());
			
			videoStream.setSize(jsonobj.getJSONObject("format").getLong("size"));
		}
		videoStream.setFps(0L);
		videoStream.setMediatype(MediaType.VIDEO.toString());
		videoStream.setConsequent(0L);
		videoStream.setConsoleonly(0);
		videoStream.setConsumerappid(0L);
		videoStream.setCputime(0L);
		videoStream.setElapsedseconds(elapsedseconds/1000);
		videoStream.setFileext(FileUtils.getFileSuffix(fileName));
		videoStream.setFilename(fileName);
		videoStream.setFormat("");
		videoStream.setFsroot("");
		videoStream.setIsretranscode(0);
		videoStream.setMaxmenkb("");
		videoStream.setOperator("");
		videoStream.setProgressivestatus("YES");
		videoStream.setRepositoryid(1L);
		
		videoStream.setSubpath(video.getSubpath());
		videoStream.setTranscodecmd(transcodecmd);
		videoStream.setType(0L);
		videoStream.setCreateTime(DateUtils.getNowDate());
		videoStream.setUpdateTime(DateUtils.getNowDate());
		return videoStream;
	}
	/**
	 * 获取视频截图的路径
	 */
	public static String  getVideoThumbPath(String subPath,String fileNameBase,String loginName) {
		String path = MediaConfig.getProfile()+"/upload/"+loginName+"/video/"+ MediaConstrans.MEDIA_UPLOAD_PRIVATE_PACH
    			+File.separator+subPath+File.separator+fileNameBase;
		File file = new File(path);
		if(!file.isDirectory()) {
			file.mkdirs();        //目录不存在创建
		}
		
		return path;
	}
	/**
	 * 获取视频私有目录
	 */
	public static String getVideoPrivatePath(String loginName) {
		return MediaConfig.getProfile()+"/upload/"+loginName+"/video/"+MediaConstrans.MEDIA_UPLOAD_PRIVATE_PACH+"/";
	}
	
	/**
	 * 获取视频私有目录
	 * @return
	 */
	public static String getVideoBasePath(String loginName) {
		return  "/upload/"+loginName+"/video/"+MediaConstrans.MEDIA_UPLOAD_PRIVATE_PACH+"/";
	}
	/**
	 * 获取视频上传源路径
	 * @return
	 */
	public static String getVideoSourcePath(String subPath,String loginName) {
		String videoSourcePath = MediaConfig.getProfile()+"/upload/"+loginName+"/video/"+MediaConstrans.MEDIA_UPLOAD_SOURCE_PACH+"/"+subPath;
    	File file = new File(videoSourcePath);
        if (!file.exists()) {
              file.mkdirs();
        }
		return videoSourcePath;
	}
	

}
