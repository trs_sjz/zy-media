package com.media.video.controller;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.media.common.annotation.Log;
import com.media.common.core.controller.BaseController;
import com.media.common.core.domain.AjaxResult;
import com.media.common.core.domain.entity.SysUser;
import com.media.common.core.page.TableDataInfo;
import com.media.common.core.text.Convert;
import com.media.common.enums.BusinessType;
import com.media.common.utils.*;
import com.media.common.utils.file.FileUtils;
import com.media.common.utils.poi.ExcelUtil;
import com.media.video.domain.MamVideo;
import com.media.video.domain.MamVideoProcessjob;
import com.media.video.domain.MamVideoSource;
import com.media.video.enums.VideoProcessStatus;
import com.media.video.enums.VideoProcessjobType;
import com.media.video.service.IMamVideoConfigService;
import com.media.video.service.IMamVideoProcessjobService;
import com.media.video.service.IMamVideoService;
import com.media.video.service.IMamVideoSourceService;
import com.media.video.utils.VideoUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.wltea.analyzer.core.IKSegmenter;
import org.wltea.analyzer.core.Lexeme;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.List;

/**
 * 视频信息Controller
 * 
 * @author liziye
 * @date 2022-03-11
 */
@Controller
@RequestMapping("/video/video")
public class MamVideoController extends BaseController
{
    private String prefix = "video/video";

    @Autowired
    private IMamVideoService mamVideoService;

    @Autowired
    private IMamVideoConfigService mamVideoConfigService;

    @Autowired
    private IMamVideoSourceService mamVideoSourceService;

    @Autowired
    private IMamVideoProcessjobService mamVideoProcessjobService;

    @RequiresPermissions("video:video:view")
    @GetMapping()
    public String video()
    {
        return prefix + "/video";
    }

    /**
     * 查询视频信息列表
     */
    @RequiresPermissions("video:video:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MamVideo mamVideo)
    {
        startPage();
        List<MamVideo> list = mamVideoService.selectMamVideoList(mamVideo);
        return getDataTable(list);
    }


    @RequiresPermissions("video:video:deptView")
    @GetMapping("/deptView")
    public String DeptVideo()
    {
        return prefix + "/deptVideo";
    }

    /**
     * 查询视频管理列表
     */
    @RequiresPermissions("video:video:deptList")
    @PostMapping("/deptList")
    @ResponseBody
    public TableDataInfo DeptList(MamVideo mamVideo)
    {
        SysUser currentUser = ShiroUtils.getSysUser();
        mamVideo.setDeptId(currentUser.getDeptId());
        startPage();
        List<MamVideo> list = mamVideoService.selectMamVideoList(mamVideo);
        return getDataTable(list);
    }

    /**
     * 导出视频信息列表
     */
    @RequiresPermissions("video:video:export")
    @Log(title = "视频信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MamVideo mamVideo)
    {
        List<MamVideo> list = mamVideoService.selectMamVideoList(mamVideo);
        ExcelUtil<MamVideo> util = new ExcelUtil<MamVideo>(MamVideo.class);
        return util.exportExcel(list, "video");
    }

    /**
     * 新增视频信息
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }


    /**
     * 新增保存视频管理
     */
    @RequiresPermissions("video:video:add")
    @Log(title = "视频信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MamVideo mamVideo, String sourceVideoIds)
    {
        if(StringUtils.isNotBlank(sourceVideoIds)) {
            SysUser currentUser = ShiroUtils.getSysUser();
            Long[] sourceVideos = Convert.toLongArray(sourceVideoIds);
            MamVideoSource mamVideoSource;
            for(Long sourceVideoId:sourceVideos) {
                mamVideoSource = mamVideoSourceService.selectMamVideoSourceById(sourceVideoId);
                System.out.println("mamVideoSource = " + mamVideoSource);
                //视频保存路径
                String filepathbase = VideoUtils.getVideoBasePath(mamVideoSource.getCreateBy()) + mamVideoSource.getSubpath();

                mamVideo.setDeptId(currentUser.getDeptId());
                mamVideo.setName(mamVideoSource.getFilename());
                if(StringUtils.isBlank(mamVideo.getTitle())) {
                    mamVideo.setTitle(mamVideoSource.getTitle());
                }
                mamVideo.setSourceVideoId(sourceVideoId);
                mamVideo.setOriginfilename(mamVideoSource.getOriginname());
                mamVideo.setProcessstatus(VideoProcessStatus.NEW.toString());
                mamVideo.setSubpath(mamVideoSource.getSubpath());
                mamVideo.setFilenamebase(FileUtils.getFileNameWithoutSuffix(mamVideoSource.getFilename()));
                mamVideo.setFilepathbase(filepathbase);
                mamVideo.setSize(mamVideoSource.getSize());
                mamVideo.setMediatype(mamVideoSource.getMediatype());
                mamVideo.setCreatedmanner(mamVideoSource.getCreatedmanner());
                mamVideo.setStatus(0);
                mamVideo.setLeader(Integer.valueOf(ServletUtils.getParameter("leader")));
                mamVideo.setCreateUserId(currentUser.getUserId());
                mamVideo.setCreateTime(DateUtils.getNowDate());
                mamVideo.setCreateBy(currentUser.getLoginName());
                mamVideo.setUpdateBy(currentUser.getLoginName());
                mamVideo.setUpdateTime(DateUtils.getNowDate());
                //添加待加工任务
                int result = mamVideoService.insertMamVideo(mamVideo);
                if(result>0) {
                    MamVideoProcessjob videoProcessjob = new MamVideoProcessjob();
                    videoProcessjob.setDomainobjid(mamVideo.getVideoId());
                    videoProcessjob.setCreatornodekey(IpUtils.getHostName());
                    videoProcessjob.setState(VideoProcessStatus.NEW.toString());
                    videoProcessjob.setStatus(0L);
                    //设置转码参数
                    videoProcessjob.setDetail(mamVideoConfigService.selectVideoConfig());
                    videoProcessjob.setCreateBy(currentUser.getLoginName());
                    videoProcessjob.setCreateByUserid(currentUser.getUserId());
                    videoProcessjob.setType(VideoProcessjobType.STD.toString());
                    //创建视频加工任务
                    mamVideoProcessjobService.insertMamVideoProcessjob(videoProcessjob);
                    mamVideoSource.setStatus(VideoProcessStatus.ACCEPT.toString());
                    //更新源视频状态
                    mamVideoSourceService.updateMamVideoSource(mamVideoSource);
                }
            }
        }else {
            AjaxResult.error("请上传视频文件!");
        }
        return toAjax(1);
    }

    /**
     * 修改视频信息
     */
    @GetMapping("/edit/{videoId}")
    public String edit(@PathVariable("videoId") Long videoId, ModelMap mmap)
    {
        MamVideo mamVideo = mamVideoService.selectMamVideoById(videoId);
        mmap.put("video", mamVideo);
        return prefix + "/edit";
    }

    /**
     * 修改保存视频信息
     */
    @RequiresPermissions("video:video:edit")
    @Log(title = "视频信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MamVideo mamVideo)
    {
        return toAjax(mamVideoService.updateMamVideo(mamVideo));
    }

    /**
     * 删除视频信息
     */
    @RequiresPermissions("video:video:remove")
    @Log(title = "视频信息", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(mamVideoService.deleteMamVideoByIds(ids));
    }


    /**
     * 视频播放预览
     * @param videoId
     * @param mmap
     * @return
     */
    @RequiresPermissions("video:video:preview")
    @GetMapping("/preview/{videoId}")
    public String preview(@PathVariable("videoId") Long videoId,ModelMap mmap)
    {
        MamVideo mamVideo = mamVideoService.selectMamVideoById(videoId);
        mmap.put("video", mamVideo);
        mmap.put("videoId", videoId);
        mmap.put("previewUrl", mamVideo.getPreviewUrl());
        System.out.println("mamVideo.getPreviewUrl() = " + mamVideo.getPreviewUrl());
        return prefix + "/preview";
    }

    /**
     * 组图标题分词
     */
    @RequestMapping(value="/splitWords",method = RequestMethod.GET ,produces="application/json;charset=UTF-8,text/html;charset=UTF-8")
    @ResponseBody
    public JSONPObject splitWords(HttpSession session, HttpServletRequest request, @RequestParam String callback, @RequestParam String title) throws Exception{
        StringReader reader = new StringReader(title);
        IKSegmenter iks = new IKSegmenter(reader, true);
        StringBuilder buffer = new StringBuilder();
        try {
            Lexeme lexeme;
            while ((lexeme = iks.next()) != null) {
                buffer.append(lexeme.getLexemeText()).append(' ');
            }
        } catch (IOException e) {
        }
        //去除最后一个,
        if (buffer.length() > 0) {
            buffer.setLength(buffer.length() - 1);
        }
        HashMap<String, String> msgMap = new HashMap<String, String>();
        msgMap.put("msg", buffer.toString());
        return new JSONPObject(callback, msgMap);
    }


}
