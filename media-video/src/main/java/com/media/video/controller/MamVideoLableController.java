package com.media.video.controller;

import com.media.common.annotation.Log;
import com.media.common.core.controller.BaseController;
import com.media.common.core.domain.AjaxResult;
import com.media.common.core.page.TableDataInfo;
import com.media.common.enums.BusinessType;
import com.media.common.utils.poi.ExcelUtil;
import com.media.video.domain.MamVideoLable;
import com.media.video.service.IMamVideoLableService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 视频标签Controller
 * 
 * @author liziye
 * @date 2022-03-16
 */
@Controller
@RequestMapping("/video/lable")
public class MamVideoLableController extends BaseController
{
    private String prefix = "video/lable";

    @Autowired
    private IMamVideoLableService mamVideoLableService;

    @RequiresPermissions("video:lable:view")
    @GetMapping()
    public String lable()
    {
        return prefix + "/lable";
    }

    /**
     * 查询视频标签列表
     */
    @RequiresPermissions("video:lable:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MamVideoLable mamVideoLable)
    {
        startPage();
        List<MamVideoLable> list = mamVideoLableService.selectMamVideoLableList(mamVideoLable);
        return getDataTable(list);
    }

    /**
     * 导出视频标签列表
     */
    @RequiresPermissions("video:lable:export")
    @Log(title = "视频标签", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MamVideoLable mamVideoLable)
    {
        List<MamVideoLable> list = mamVideoLableService.selectMamVideoLableList(mamVideoLable);
        ExcelUtil<MamVideoLable> util = new ExcelUtil<MamVideoLable>(MamVideoLable.class);
        return util.exportExcel(list, "lable");
    }

    /**
     * 新增视频标签
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存视频标签
     */
    @RequiresPermissions("video:lable:add")
    @Log(title = "视频标签", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MamVideoLable mamVideoLable)
    {
        return toAjax(mamVideoLableService.insertMamVideoLable(mamVideoLable));
    }

    /**
     * 修改视频标签
     */
    @GetMapping("/edit/{lableId}")
    public String edit(@PathVariable("lableId") Long lableId, ModelMap mmap)
    {
        MamVideoLable mamVideoLable = mamVideoLableService.selectMamVideoLableById(lableId);
        mmap.put("mamVideoLable", mamVideoLable);
        return prefix + "/edit";
    }

    /**
     * 修改保存视频标签
     */
    @RequiresPermissions("video:lable:edit")
    @Log(title = "视频标签", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MamVideoLable mamVideoLable)
    {
        return toAjax(mamVideoLableService.updateMamVideoLable(mamVideoLable));
    }

    /**
     * 删除视频标签
     */
    @RequiresPermissions("video:lable:remove")
    @Log(title = "视频标签", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(mamVideoLableService.deleteMamVideoLableByIds(ids));
    }

    /**
     * 修改保存视频标签
     */
    @RequiresPermissions("video:lable:edit")
    @Log(title = "视频标签", businessType = BusinessType.UPDATE)
    @RequestMapping(value ="/update",method = RequestMethod.POST,produces = "application/json;charset=UTF-8")
    @ResponseBody
    public AjaxResult update(@RequestBody MamVideoLable mamVideoLable)
    {
        return toAjax(mamVideoLableService.updateMamVideoLable(mamVideoLable));
    }
}
