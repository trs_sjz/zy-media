package com.media.video.controller;

import com.media.common.annotation.Log;
import com.media.common.core.controller.BaseController;
import com.media.common.core.domain.AjaxResult;
import com.media.common.core.domain.Ztree;
import com.media.common.enums.BusinessType;
import com.media.common.utils.StringUtils;
import com.media.common.utils.poi.ExcelUtil;
import com.media.video.domain.MamVideoCategory;
import com.media.video.service.IMamVideoCategoryService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 视频分类信息Controller
 *
 * @author liziye
 * @date 2021-12-17
 */
@Controller
@RequestMapping("/video/category")
public class MamVideoCategoryController extends BaseController
{
    private String prefix = "video/category";

    @Autowired
    private IMamVideoCategoryService mamVideoCategoryService;

    @RequiresPermissions("video:category:view")
    @GetMapping()
    public String category()
    {
        return prefix + "/category";
    }

    /**
     * 查询视频分类信息树列表
     */
    @RequiresPermissions("video:category:list")
    @PostMapping("/list")
    @ResponseBody
    public List<MamVideoCategory> list(MamVideoCategory mamVideoCategory)
    {
        List<MamVideoCategory> list = mamVideoCategoryService.selectMamVideoCategoryList(mamVideoCategory);
        return list;
    }

    /**
     * 导出视频分类信息列表
     */
    @RequiresPermissions("video:category:export")
    @Log(title = "视频分类信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MamVideoCategory mamVideoCategory)
    {
        List<MamVideoCategory> list = mamVideoCategoryService.selectMamVideoCategoryList(mamVideoCategory);
        ExcelUtil<MamVideoCategory> util = new ExcelUtil<MamVideoCategory>(MamVideoCategory.class);
        return util.exportExcel(list, "category");
    }

    /**
     * 新增视频分类信息
     */
    @GetMapping(value = { "/add/{categoryId}", "/add/" })
    public String add(@PathVariable(value = "categoryId", required = false) Long categoryId, ModelMap mmap)
    {
        if (StringUtils.isNotNull(categoryId))
        {
            mmap.put("mamVideoCategory", mamVideoCategoryService.selectMamVideoCategoryById(categoryId));
        }
        return prefix + "/add";
    }

    /**
     * 新增保存视频分类信息
     */
    @RequiresPermissions("video:category:add")
    @Log(title = "视频分类信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MamVideoCategory mamVideoCategory)
    {
        return toAjax(mamVideoCategoryService.insertMamVideoCategory(mamVideoCategory));
    }

    /**
     * 修改视频分类信息
     */
    @GetMapping("/edit/{categoryId}")
    public String edit(@PathVariable("categoryId") Long categoryId, ModelMap mmap)
    {
        MamVideoCategory mamVideoCategory = mamVideoCategoryService.selectMamVideoCategoryById(categoryId);
        mmap.put("mamVideoCategory", mamVideoCategory);
        return prefix + "/edit";
    }

    /**
     * 修改保存视频分类信息
     */
    @RequiresPermissions("video:category:edit")
    @Log(title = "视频分类信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MamVideoCategory mamVideoCategory)
    {
        return toAjax(mamVideoCategoryService.updateMamVideoCategory(mamVideoCategory));
    }

    /**
     * 删除
     */
    @RequiresPermissions("video:category:remove")
    @Log(title = "视频分类信息", businessType = BusinessType.DELETE)
    @GetMapping("/remove/{categoryId}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("categoryId") Long categoryId)
    {
        return toAjax(mamVideoCategoryService.deleteMamVideoCategoryById(categoryId));
    }

    /**
     * 选择视频分类信息树
     */
    @GetMapping(value = { "/selectCategoryTree/{categoryId}", "/selectCategoryTree/" })
    public String selectCategoryTree(@PathVariable(value = "categoryId", required = false) Long categoryId, ModelMap mmap)
    {
        if (StringUtils.isNotNull(categoryId))
        {
            mmap.put("mamVideoCategory", mamVideoCategoryService.selectMamVideoCategoryById(categoryId));
        }
        return prefix + "/tree";
    }

    /**
     * 加载视频分类信息树列表
     */
    @GetMapping("/treeData")
    @ResponseBody
    public List<Ztree> treeData()
    {
        List<Ztree> ztrees = mamVideoCategoryService.selectMamVideoCategoryTree();
        return ztrees;
    }
}
