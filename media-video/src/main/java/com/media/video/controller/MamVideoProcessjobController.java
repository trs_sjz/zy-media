package com.media.video.controller;

import com.alibaba.druid.support.json.JSONUtils;
import com.media.common.annotation.Log;
import com.media.common.core.controller.BaseController;
import com.media.common.core.domain.AjaxResult;
import com.media.common.core.domain.entity.SysUser;
import com.media.common.core.page.TableDataInfo;
import com.media.common.enums.BusinessType;
import com.media.common.utils.IpUtils;
import com.media.common.utils.ShiroUtils;
import com.media.common.utils.poi.ExcelUtil;
import com.media.video.domain.MamVideoConfig;
import com.media.video.domain.MamVideoProcessjob;
import com.media.video.enums.VideoProcessStatus;
import com.media.video.enums.VideoProcessjobType;
import com.media.video.service.IMamVideoConfigService;
import com.media.video.service.IMamVideoProcessjobService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 视频加工任务Controller
 * 
 * @author liziye
 * @date 2022-03-15
 */
@Controller
@RequestMapping("/video/processjob")
public class MamVideoProcessjobController extends BaseController
{
    private String prefix = "video/processjob";

    @Autowired
    private IMamVideoProcessjobService mamVideoProcessjobService;

    @Autowired
    private IMamVideoConfigService mamVideoConfigService;

    @RequiresPermissions("video:processjob:view")
    @GetMapping()
    public String processjob()
    {
        return prefix + "/processjob";
    }

    /**
     * 查询视频加工任务列表
     */
    @RequiresPermissions("video:processjob:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MamVideoProcessjob mamVideoProcessjob)
    {
        startPage();
        List<MamVideoProcessjob> list = mamVideoProcessjobService.selectMamVideoProcessjobList(mamVideoProcessjob);
        return getDataTable(list);
    }

    /**
     * 导出视频加工任务列表
     */
    @RequiresPermissions("video:processjob:export")
    @Log(title = "视频加工任务", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MamVideoProcessjob mamVideoProcessjob)
    {
        List<MamVideoProcessjob> list = mamVideoProcessjobService.selectMamVideoProcessjobList(mamVideoProcessjob);
        ExcelUtil<MamVideoProcessjob> util = new ExcelUtil<MamVideoProcessjob>(MamVideoProcessjob.class);
        return util.exportExcel(list, "processjob");
    }

    /**
     * 新增视频加工任务表
     */
    @GetMapping("/add/{videoId}")
    public String add(@PathVariable("videoId") Long videoId, ModelMap mmap)
    {
        MamVideoConfig videoconfig = new MamVideoConfig();
        List<MamVideoConfig> list = mamVideoConfigService.selectMamVideoConfigList(videoconfig);
        Map<String,String> config = new HashMap<String,String>();
        for(MamVideoConfig vConfig:list) {
            config.put(vConfig.getConfigKey(), vConfig.getConfigValue());

        }
        mmap.put("config", config);
        mmap.put("videoId", videoId);
        return prefix + "/add";
    }

    /**
     * 新增保存视频加工任务表
     */
    @RequiresPermissions("video:processjob:add")
    @Log(title = "视频加工任务表", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(@RequestParam Map<String, String> params)
    {
        //获取登录用户信息
        SysUser currentUser = ShiroUtils.getSysUser();
        Map<String,String> map = new HashMap<String,String>();

        map.put("streamformat", params.get("streamformat"));
        map.put("streamquality", params.get("streamquality"));
        if(params.get("streamquality")!=null&&!params.get("streamquality").equals("0")) {
            String streamquality = params.get("streamquality");
            map.put("streambitrate", params.get("stream"+streamquality+"bitrate"));
            map.put("streamresolution", params.get("stream"+streamquality+"resolution"));
            map.put("streamaudioBitrate", params.get("stream"+streamquality+"audioBitrate"));
            map.put("streamaudioChannels", params.get("stream"+streamquality+"audioChannels"));
        }else {
            map.put("streambitrate", "");
            map.put("streamresolution", "");
            map.put("streamaudioBitrate", "");
            map.put("streamaudioChannels", "");
        }
        map.put("thumbMiddleWidth", params.get("thumbMiddleWidth"));
        map.put("thumbMiddleHeight", params.get("thumbMiddleHeight"));
        map.put("thumbSmallWidth", params.get("thumbSmallWidth"));
        map.put("thumbSmallHeight", params.get("thumbSmallHeight"));
        for (Map.Entry<String, String> entry : map.entrySet()) {
            System.out.print(entry.getKey()+"===:"+entry.getValue());
        }
        String detail = JSONUtils.toJSONString(map);
        Long videoId = Long.parseLong(params.get("videoId"));
        MamVideoProcessjob videoProcessjob = new MamVideoProcessjob();
        videoProcessjob.setDomainobjid(videoId);
        videoProcessjob.setCreatornodekey(IpUtils.getHostName());
        videoProcessjob.setState(VideoProcessStatus.NEW.toString());
        videoProcessjob.setStatus(0L);
        //设置转码参数
        videoProcessjob.setDetail(detail);
        videoProcessjob.setCreateBy(currentUser.getLoginName());
        videoProcessjob.setCreateByUserid(currentUser.getUserId());
        videoProcessjob.setType(VideoProcessjobType.RE.toString());
        ///创建视频加工任务
        int result = mamVideoProcessjobService.insertMamVideoProcessjob(videoProcessjob);
        return toAjax(result);
    }

    /**
     * 修改视频加工任务
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        MamVideoProcessjob videoProcessjob = mamVideoProcessjobService.selectMamVideoProcessjobById(id);
        String status = videoProcessjob.getState();
        String type = videoProcessjob.getType();
        if(status=="NEW"){
            videoProcessjob.setState("待处理");
        }else if(status.equals("ACCEPT")){
            videoProcessjob.setState("已受理");
        }else if(status.equals("ST_PROCESSING")){
            videoProcessjob.setState("处理中");
        }else if(status.equals("SUC_DONE")){
            videoProcessjob.setState("处理成功");
        }else if(status.equals("FAIL_CONVERT")){
            videoProcessjob.setState("处理失败");
        }

        if(type.equals("STD")){
            videoProcessjob.setType("全新任务");
        }else if(type.equals("RE")){
            videoProcessjob.setType("重新编码");
        }
        mmap.put("videoProcessjob", videoProcessjob);
        return prefix + "/view";
    }

    /**
     * 修改保存视频加工任务
     */
    @RequiresPermissions("video:processjob:edit")
    @Log(title = "视频加工任务", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MamVideoProcessjob mamVideoProcessjob)
    {
        return toAjax(mamVideoProcessjobService.updateMamVideoProcessjob(mamVideoProcessjob));
    }

    /**
     * 删除视频加工任务
     */
    @RequiresPermissions("video:processjob:remove")
    @Log(title = "视频加工任务", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(mamVideoProcessjobService.deleteMamVideoProcessjobByIds(ids));
    }
}
