package com.media.video.controller;

import com.media.common.annotation.Log;
import com.media.common.core.controller.BaseController;
import com.media.common.core.domain.AjaxResult;
import com.media.common.core.page.TableDataInfo;
import com.media.common.enums.BusinessType;
import com.media.common.utils.poi.ExcelUtil;
import com.media.video.domain.MamVideoFrament;
import com.media.video.service.IMamVideoFramentService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 视频片段Controller
 * 
 * @author liziye
 * @date 2022-03-18
 */
@Controller
@RequestMapping("/video/frament")
public class MamVideoFramentController extends BaseController
{
    private String prefix = "video/frament";

    @Autowired
    private IMamVideoFramentService mamVideoFramentService;

    @RequiresPermissions("video:frament:view")
    @GetMapping()
    public String frament()
    {
        return prefix + "/frament";
    }

    /**
     * 查询视频片段列表
     */
    @RequiresPermissions("video:frament:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MamVideoFrament mamVideoFrament)
    {
        startPage();
        List<MamVideoFrament> list = mamVideoFramentService.selectMamVideoFramentList(mamVideoFrament);
        return getDataTable(list);
    }

    /**
     * 导出视频片段列表
     */
    @RequiresPermissions("video:frament:export")
    @Log(title = "视频片段", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MamVideoFrament mamVideoFrament)
    {
        List<MamVideoFrament> list = mamVideoFramentService.selectMamVideoFramentList(mamVideoFrament);
        ExcelUtil<MamVideoFrament> util = new ExcelUtil<MamVideoFrament>(MamVideoFrament.class);
        return util.exportExcel(list, "frament");
    }

    /**
     * 新增视频片段
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存视频片段
     */
    @RequiresPermissions("video:frament:add")
    @Log(title = "视频片段", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MamVideoFrament mamVideoFrament)
    {
        int result = mamVideoFramentService.insertMamVideoFrament(mamVideoFrament);
        if(result>0) {
            return AjaxResult.success(mamVideoFrament);
        }else {
            return AjaxResult.error("添加片段失败");
        }
    }

    /**
     * 修改视频片段
     */
    @GetMapping("/edit/{framentId}")
    public String edit(@PathVariable("framentId") Long framentId, ModelMap mmap)
    {
        MamVideoFrament mamVideoFrament = mamVideoFramentService.selectMamVideoFramentById(framentId);
        mmap.put("mamVideoFrament", mamVideoFrament);
        return prefix + "/edit";
    }

    /**
     * 修改保存视频片段
     */
    @RequiresPermissions("video:frament:edit")
    @Log(title = "视频片段", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MamVideoFrament mamVideoFrament)
    {
        return toAjax(mamVideoFramentService.updateMamVideoFrament(mamVideoFrament));
    }

    /**
     * 删除视频片段
     */
    @RequiresPermissions("video:frament:remove")
    @Log(title = "视频片段", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(mamVideoFramentService.deleteMamVideoFramentByIds(ids));
    }
}
