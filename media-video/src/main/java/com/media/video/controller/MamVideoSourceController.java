package com.media.video.controller;

import com.media.common.annotation.Log;
import com.media.common.config.MediaConfig;
import com.media.common.core.controller.BaseController;
import com.media.common.core.domain.AjaxResult;
import com.media.common.core.domain.entity.SysUser;
import com.media.common.core.page.TableDataInfo;
import com.media.common.enums.BusinessType;
import com.media.common.exception.file.FileException;
import com.media.common.utils.ShiroUtils;
import com.media.common.utils.poi.ExcelUtil;
import com.media.framework.web.domain.response.CheckChunkResult;
import com.media.video.domain.MamVideo;
import com.media.video.domain.MamVideoSource;
import com.media.video.service.IMamVideoLogService;
import com.media.video.service.IMamVideoService;
import com.media.video.service.IMamVideoSourceService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

/**
 * 源视频Controller
 * 
 * @author liziye
 * @date 2022-03-15
 */
@Controller
@RequestMapping("/video/source")
public class MamVideoSourceController extends BaseController
{
    private String prefix = "video/source";

    @Autowired
    private IMamVideoSourceService mamVideoSourceService;

    @Autowired
    private IMamVideoLogService mamVideoLogService;

    @Autowired
    private IMamVideoService mamVideoService;

    @RequiresPermissions("video:source:view")
    @GetMapping()
    public String source()
    {
        return prefix + "/source";
    }

    /**
     * 查询源视频列表
     */
    @RequiresPermissions("video:source:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MamVideoSource mamVideoSource)
    {
        startPage();
        List<MamVideoSource> list = mamVideoSourceService.selectMamVideoSourceList(mamVideoSource);
        return getDataTable(list);
    }

    /**
     * 导出源视频列表
     */
    @RequiresPermissions("video:source:export")
    @Log(title = "源视频", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MamVideoSource mamVideoSource)
    {
        List<MamVideoSource> list = mamVideoSourceService.selectMamVideoSourceList(mamVideoSource);
        ExcelUtil<MamVideoSource> util = new ExcelUtil<MamVideoSource>(MamVideoSource.class);
        return util.exportExcel(list, "source");
    }

    /**
     * 新增源视频
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存源视频
     */
    @RequiresPermissions("video:source:add")
    @Log(title = "源视频", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MamVideoSource mamVideoSource)
    {
        return toAjax(mamVideoSourceService.insertMamVideoSource(mamVideoSource));
    }

    /**
     * 修改源视频
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        MamVideoSource mamVideoSource = mamVideoSourceService.selectMamVideoSourceById(id);
        mmap.put("mamVideoSource", mamVideoSource);
        return prefix + "/edit";
    }

    /**
     * 修改保存源视频
     */
    @RequiresPermissions("video:source:edit")
    @Log(title = "源视频", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MamVideoSource mamVideoSource)
    {
        return toAjax(mamVideoSourceService.updateMamVideoSource(mamVideoSource));
    }

    /**
     * 删除源视频
     */
    @RequiresPermissions("video:source:remove")
    @Log(title = "源视频", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(mamVideoSourceService.deleteMamVideoSourceByIds(ids));
    }

    /**
     * 查看源视频信息
     */
    @GetMapping("/view/{id}")
    public String view(@PathVariable("id") Long id, ModelMap mmap)
    {
        MamVideoSource mamVideoSource = mamVideoSourceService.selectMamVideoSourceById(id);
        mmap.put("mamVideoSource", mamVideoSource);
        return prefix + "/view";
    }

    /**
     * 上传视频
     */
    @Log(title = "上传视频", businessType = BusinessType.INSERT)
    @PostMapping("fileUpload")
    @ResponseBody
    public Map<String,Object> fileUpload(@RequestParam MultipartFile file, @RequestParam("name") String name, @RequestParam("type") String type, @RequestParam(value = "chunks",required = false, defaultValue = "1") Integer chunks, @RequestParam(value = "chunk",required = false, defaultValue = "1") Integer chunk, @RequestParam("size") Integer size) throws IOException {
        return mamVideoSourceService.fileUploadChunk(file, name,type, chunks, chunk, size);
    }
    /**
     * 检查块文件
     */
    @PostMapping("fileUploadCheck")
    @ResponseBody
    public CheckChunkResult fileUploadCheck(@RequestParam("name") String name, @RequestParam("blockmd5") String blockmd5, @RequestParam("chunkIndex") Integer chunkIndex, @RequestParam("size") Integer chunkSize) {
        return mamVideoSourceService.checkChunk(name, blockmd5, chunkIndex, chunkSize);
    }

    /**
     * 合并块文件
     */
    @PostMapping("fileMergeChunks")
    @ResponseBody
    public Map<String,Object> fileMergeChunks(@RequestParam("name") String name,@RequestParam("filename") String filename,@RequestParam("chunks") Integer chunks,@RequestParam("ext") String ext,@RequestParam("type") String type,@RequestParam("size") Integer size) {
        return mamVideoSourceService.mergeChunks(name,filename, chunks, ext, type, size);
    }

    /**
     * 下载源视频
     */
    @RequiresPermissions("video:source:download")
    @Log(title = "下载源视频", businessType = BusinessType.EXPORT)
    @GetMapping("/download/{sourceId}")
    public String download(@PathVariable("sourceId") Long sourceId, HttpServletRequest request, HttpServletResponse response) {
        MamVideoSource videoSource = mamVideoSourceService.selectMamVideoSourceById(sourceId);
        if(videoSource==null) {
            throw new FileException("源视频文件不存在!", null);
        }
        String filePath = MediaConfig.getProfile() +videoSource.getFilepath();
        File file = new File(filePath);
        if (!file.exists()) {
            throw new FileException("视频不存在!", null);
        }
        SysUser currentUser = ShiroUtils.getSysUser();
        //开始下载
        //response.setContentType("application/force-download");// 设置强制下载不打开
        // 告诉浏览器输出内容为流
        response.setHeader("Content-Type", "application/octet-stream;charset=utf-8");

        byte[] buffer = new byte[1024];
        FileInputStream fis = null;
        BufferedInputStream bis = null;
        try {
            // 设置文件名
            response.addHeader("Content-Disposition", "attachment;fileName=" + URLEncoder.encode(videoSource.getRemark(),"UTF-8"));
            fis = new FileInputStream(file);
            bis = new BufferedInputStream(fis);
            OutputStream os = response.getOutputStream();
            int i = bis.read(buffer);
            while (i != -1) {
                os.write(buffer, 0, i);
                i = bis.read(buffer);
            }
            String actionDes = "下载视频:"+currentUser.getLoginName()+"下载视频";
            MamVideo video = mamVideoService.selectMamVideoBySourceVideoId(sourceId);
            mamVideoLogService.insertVideoLog(video.getVideoId(), com.media.common.enums.MediaType.VIDEO.toString(), "download_video", "下载视频", actionDes, "MEDIA_CODE", "视频库-我的视频", "operation", "",currentUser.getUserId(),video.getCategoryId());
            return "下载成功";
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (bis != null) {
                try {
                    bis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return  "下载失败";
    }
}
