package com.media.video.controller;

import com.media.common.annotation.Log;
import com.media.common.core.controller.BaseController;
import com.media.common.core.domain.AjaxResult;
import com.media.common.core.domain.entity.SysUser;
import com.media.common.enums.BusinessType;
import com.media.common.utils.DateUtils;
import com.media.common.utils.ShiroUtils;
import com.media.video.domain.MamVideoConfig;
import com.media.video.service.IMamVideoConfigService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 视频转码配置Controller
 * 
 * @author liziye
 * @date 2022-03-11
 */
@Controller
@RequestMapping("/video/config")
public class MamVideoConfigController extends BaseController
{
    private String prefix = "video/config";

    @Autowired
    private IMamVideoConfigService mamVideoConfigService;

    @RequiresPermissions("video:config:view")
    @GetMapping()
    public String config(ModelMap mmap)
    {
        MamVideoConfig mamVideoConfig = new MamVideoConfig();
        List<MamVideoConfig> list = mamVideoConfigService.selectMamVideoConfigList(mamVideoConfig);
        Map<String,String> config = new HashMap<String,String>();

        for(MamVideoConfig vConfig:list) {
            config.put(vConfig.getConfigKey(), vConfig.getConfigValue());

        }
        mmap.put("config", config);
        return prefix + "/config";
    }

    /**
     * 修改保存视频转码配置
     */
    @RequiresPermissions("video:config:edit")
    @Log(title = "视频转码配置", businessType = BusinessType.UPDATE)
    @PostMapping("/save")
    @ResponseBody
    public AjaxResult editSave(@RequestParam Map<String, String> params)
    {
        SysUser currentUser = ShiroUtils.getSysUser();
        MamVideoConfig videoConfig;
        for (Map.Entry<String, String> entry : params.entrySet()) {
            videoConfig = new MamVideoConfig();
            videoConfig.setConfigKey(entry.getKey());
            videoConfig.setConfigValue(entry.getValue());
            videoConfig.setUpdateBy(currentUser.getLoginName());
            videoConfig.setUpdateTime(DateUtils.getNowDate());
            mamVideoConfigService.updateMamVideoConfig(videoConfig);
        }
        return success();
    }
}
