package com.media.video.controller;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.media.common.annotation.Log;
import com.media.common.core.controller.BaseController;
import com.media.common.core.domain.AjaxResult;
import com.media.common.core.domain.entity.SysUser;
import com.media.common.core.page.TableDataInfo;
import com.media.common.enums.BusinessType;
import com.media.common.utils.ShiroUtils;
import com.media.common.utils.poi.ExcelUtil;
import com.media.video.domain.MamVideo;
import com.media.video.domain.MamVideoLog;
import com.media.video.service.IMamVideoLogService;
import com.media.video.service.IMamVideoService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;

/**
 * 视频日志Controller
 * 
 * @author liziye
 * @date 2022-03-18
 */
@Controller
@RequestMapping("/video/log")
public class MamVideoLogController extends BaseController
{
    private String prefix = "video/log";

    @Autowired
    private IMamVideoLogService mamVideoLogService;

    @Autowired
    private IMamVideoService mamVideoService;

    /**
     * 查询操作日志表列表
     */
    @RequiresPermissions("system:MamActionLog:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(@RequestBody MamVideoLog mamVideoLog)
    {
        startPage();
        List<MamVideoLog> list = mamVideoLogService.selectMamVideoLogList(mamVideoLog);
        return getDataTable(list);
    }

    /**
     * 视频下载日志页面
     *
     */
    @RequiresPermissions("video:downloadLog:view")
    @GetMapping("/download")
    public String download()
    {
        return prefix + "/downloadLog";
    }


    /**
     * 查询视频下载日志
     */
    @RequiresPermissions("video:downloadLog:list")
    @PostMapping("/downloadList")
    @ResponseBody
    public TableDataInfo downloadList(MamVideoLog mamVideoLog)
    {
        startPage();
        mamVideoLog.setAction("download_video");
        List<MamVideoLog> list = mamVideoLogService.selectMamVideoLogList(mamVideoLog);
        return getDataTable(list);
    }

    /**
     * 查询某个视频的下载日志
     */
    @RequiresPermissions("video:downloadLog:list")
    @GetMapping("/downloadList/{logId}")
    public String downloadGroupPictureList(@PathVariable("logId") String logId, ModelMap mmap)
    {
        MamVideoLog mamActionLog = mamVideoLogService.selectMamVideoLogById(Long.valueOf(logId));
        mmap.put("videoId", mamActionLog.getObjectId());
        return prefix + "/downloadList";
    }

    /**
     * 视频播放日志页面
     *
     */
    @RequiresPermissions("video:playLog:view")
    @GetMapping("/play")
    public String play()
    {
        return prefix + "/playLog";
    }

    /**
     * 查询视频播放日志
     */
    @RequiresPermissions("video:playLog:list")
    @PostMapping("/playList")
    @ResponseBody
    public TableDataInfo playList(MamVideoLog actionLog)
    {
        startPage();
        actionLog.setAction("play_video");
        List<MamVideoLog> list = mamVideoLogService.selectMamVideoLogList(actionLog);
        return getDataTable(list);
    }

    /**
     * 查询某个视频的播放日志
     */
    @RequiresPermissions("video:playLog:list")
    @GetMapping("/playList/{logId}")
    public String playList(@PathVariable("logId") String logId, ModelMap mmap)
    {
        MamVideoLog mamVideoLog = mamVideoLogService.selectMamVideoLogById(Long.valueOf(logId));
        mmap.put("videoId", mamVideoLog.getObjectId());
        return prefix + "/playList";
    }

    /**
     * 视频播放插入日志
     */
    @PostMapping("/insertPlayLog/{videoId}")
    @ResponseBody
    public AjaxResult insertPlayLog(@PathVariable("videoId") Long videoId)
    {
        SysUser currentUser = ShiroUtils.getSysUser();
        MamVideo mamVideo = mamVideoService.selectMamVideoById(videoId);
        String actionDes = "播放视频:"+currentUser.getLoginName()+"播放视频《"+mamVideo.getTitle()+"》";
        mamVideoLogService.insertVideoLog(videoId, com.media.common.enums.MediaType.VIDEO.toString(), "play_video", "播放视频", actionDes, "MEDIA_CODE", "视频库", "operation", "",currentUser.getUserId(),mamVideo.getCategoryId());
        return AjaxResult.success();
    }

    @Log(title = "导出视频下载日志", businessType = BusinessType.EXPORT)
    @RequiresPermissions("video:downloadLog:export")
    @PostMapping("/exportDownloadLog")
    @ResponseBody
    public AjaxResult exportDownloadLog(MamVideoLog mamVideoLog)
    {
        mamVideoLog.setAction("download_video");
        List<MamVideoLog> list = mamVideoLogService.selectMamVideoLogList(mamVideoLog);
        ExcelUtil<MamVideoLog> util = new ExcelUtil<MamVideoLog>(MamVideoLog.class);
        return util.exportExcel(list, "视频下载日志");
    }

    @Log(title = "导出视频播放日志", businessType = BusinessType.EXPORT)
    @RequiresPermissions("video:playLog:export")
    @PostMapping("/exportPlayLog")
    @ResponseBody
    public AjaxResult exportPlayLog(MamVideoLog mamVideoLog)
    {
        mamVideoLog.setAction("play_video");
        List<MamVideoLog> list = mamVideoLogService.selectMamVideoLogList(mamVideoLog);
        ExcelUtil<MamVideoLog> util = new ExcelUtil<MamVideoLog>(MamVideoLog.class);
        return util.exportExcel(list, "视频播放日志");
    }

    /**
     * 获取当前视频（下载量/播放量）图表信息
     */
    @RequestMapping(value="/getVideoChart",method = RequestMethod.GET ,produces="application/json;charset=UTF-8,text/html;charset=UTF-8")
    @ResponseBody
    public JSONPObject getVideoChart(HttpSession session, HttpServletRequest request, @RequestParam String callback, @RequestParam String videoId, @RequestParam String action) throws Exception{
        MamVideoLog mamVideoLog = new MamVideoLog();
        mamVideoLog.setAction(action);
        mamVideoLog.setObjectId(Long.valueOf(videoId));
        List<HashMap<String, Object>> chartData = mamVideoLogService.getVideoChartData(mamVideoLog);
        HashMap<String, List> chartMap = new HashMap<String, List>();
        chartMap.put("chartData", chartData);
        return new JSONPObject(callback, chartMap);
    }
}
