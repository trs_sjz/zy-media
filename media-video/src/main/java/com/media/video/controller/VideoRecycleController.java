package com.media.video.controller;

import com.media.common.annotation.Log;
import com.media.common.core.controller.BaseController;
import com.media.common.core.domain.AjaxResult;
import com.media.common.core.domain.entity.SysUser;
import com.media.common.core.page.TableDataInfo;
import com.media.common.core.text.Convert;
import com.media.common.enums.BusinessType;
import com.media.common.utils.ShiroUtils;
import com.media.video.domain.MamVideo;
import com.media.video.service.IMamVideoService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


@Controller
@RequestMapping("/video/recycle")
public class VideoRecycleController  extends BaseController {
	private String prefix = "video/recycle";

    @Autowired
    private IMamVideoService mamVideoService;

    
    @RequiresPermissions("video:recycle:view")
    @GetMapping()
    public String recycle()
    {
        return prefix + "/recycle";
    }
    
    /**
     * 视频回收站列表数据
     */
    @RequiresPermissions("video:recycle:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MamVideo mamVideo)
    {
    	SysUser currentUser = ShiroUtils.getSysUser();
    	mamVideo.setCreateUserId(currentUser.getUserId());
        startPage();
        List<MamVideo> list = mamVideoService.selectRecycleMamVideoList(mamVideo);
        return getDataTable(list);
    }
    
    /**
     * 还原视频管理
     */
    @RequiresPermissions("video:recycle:restore")
    @Log(title = "还原视频管理", businessType = BusinessType.DELETE)
    @PostMapping( "/restore")
    @ResponseBody
    public AjaxResult restore(String ids)
    {
         return toAjax(mamVideoService.restoreMamVideoByIds(ids));
    }
    
    /**
     * 永久删除视频
     */
    @RequiresPermissions("video:recycle:deleteForever")
    @Log(title = "永久删除视频", businessType = BusinessType.DELETE)
    @PostMapping( "/deleteForever")
    @ResponseBody
    public AjaxResult deleteForever(String ids)
    {
    	if(StringUtils.isNotBlank(ids)) {
    		Long[] idsArray = Convert.toLongArray(ids);
    		for(int i=0;i<idsArray.length;i++) {
                MamVideo mamVideo = mamVideoService.selectMamVideoById(idsArray[i]);
                mamVideoService.deleteMamVideo(mamVideo);
    		}
    	}else {
    		error("请选择要删除的视频");
    	}
        return success("删除成功！");
    }
    
    /**
     * 清空回收站
     */
    @RequiresPermissions("video:recycle:clearRecycle")
    @Log(title = "清空回收站", businessType = BusinessType.DELETE)
    @PostMapping( "/clearRecycle")
    @ResponseBody
    public AjaxResult clearRecycle()
    {
    	SysUser  currentUser = ShiroUtils.getSysUser();
    	MamVideo videoSearch = new MamVideo();
    	videoSearch.setCreateUserId(currentUser.getUserId());
		List<MamVideo> list =  mamVideoService.selectRecycleMamVideoList(videoSearch);
		for(MamVideo video:list) {
			mamVideoService.deleteMamVideo(video);
		}
        return success("清空回收站成功！");
    }
}
