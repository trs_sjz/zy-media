package com.media.video.controller;

import com.media.common.annotation.Log;
import com.media.common.core.controller.BaseController;
import com.media.common.core.domain.AjaxResult;
import com.media.common.core.page.TableDataInfo;
import com.media.common.core.text.Convert;
import com.media.common.enums.BusinessType;
import com.media.common.utils.StringUtils;
import com.media.common.utils.poi.ExcelUtil;
import com.media.video.domain.MamVideo;
import com.media.video.domain.MamVideoConfig;
import com.media.video.domain.MamVideoThumb;
import com.media.video.service.IMamVideoConfigService;
import com.media.video.service.IMamVideoLogService;
import com.media.video.service.IMamVideoService;
import com.media.video.service.IMamVideoThumbService;
import com.media.video.utils.VideoUtils;
import net.coobird.thumbnailator.Thumbnails;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 视频缩略图Controller
 * 
 * @author liziye
 * @date 2022-03-15
 */
@Controller
@RequestMapping("/video/thumb")
public class MamVideoThumbController extends BaseController
{
    private String prefix = "system/thumb";

    @Autowired
    private IMamVideoThumbService mamVideoThumbService;

    @Autowired
    private IMamVideoService mamVideoService;

    @Autowired
    private IMamVideoLogService mamVideoLogService;

    @Autowired
    private IMamVideoConfigService mamVideoConfigService;

    @RequiresPermissions("system:thumb:view")
    @GetMapping()
    public String thumb()
    {
        return prefix + "/thumb";
    }

    /**
     * 查询视频缩略图列表
     */
    @RequiresPermissions("system:thumb:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MamVideoThumb mamVideoThumb)
    {
        startPage();
        List<MamVideoThumb> list = mamVideoThumbService.selectMamVideoThumbList(mamVideoThumb);
        return getDataTable(list);
    }

    /**
     * 导出视频缩略图列表
     */
    @RequiresPermissions("system:thumb:export")
    @Log(title = "视频缩略图", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MamVideoThumb mamVideoThumb)
    {
        List<MamVideoThumb> list = mamVideoThumbService.selectMamVideoThumbList(mamVideoThumb);
        ExcelUtil<MamVideoThumb> util = new ExcelUtil<MamVideoThumb>(MamVideoThumb.class);
        return util.exportExcel(list, "thumb");
    }

    /**
     * 新增视频缩略图
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存视频缩略图
     */
    @RequiresPermissions("system:thumb:add")
    @Log(title = "视频缩略图", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MamVideoThumb mamVideoThumb)
    {
        return toAjax(mamVideoThumbService.insertMamVideoThumb(mamVideoThumb));
    }

    /**
     * 修改视频缩略图
     */
    @GetMapping("/edit/{thumbId}")
    public String edit(@PathVariable("thumbId") Long thumbId, ModelMap mmap)
    {
        MamVideoThumb mamVideoThumb = mamVideoThumbService.selectMamVideoThumbById(thumbId);
        mmap.put("mamVideoThumb", mamVideoThumb);
        return prefix + "/edit";
    }

    /**
     * 修改保存视频缩略图
     */
    @RequiresPermissions("system:thumb:edit")
    @Log(title = "视频缩略图", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MamVideoThumb mamVideoThumb)
    {
        return toAjax(mamVideoThumbService.updateMamVideoThumb(mamVideoThumb));
    }

    /**
     * 删除视频缩略图
     */
    @RequiresPermissions("system:thumb:remove")
    @Log(title = "视频缩略图", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(mamVideoThumbService.deleteMamVideoThumbByIds(ids));
    }

    /**
     * 截图
     */
    @PostMapping( "/cutImage")
    @ResponseBody
    public AjaxResult cutImage(Long videoId,String fileName,String pointTime,String base64Stream)
    {
        MamVideo video = mamVideoService.selectMamVideoById(videoId);
        Map<String,Object> map = mamVideoThumbService.cutImage(video, fileName, pointTime, base64Stream);
        return AjaxResult.success(map);
    }

    /**
     * 设置封面
     */
    @PostMapping( "/setCover")
    @ResponseBody
    public AjaxResult setCover(Long thumbId)
    {
        MamVideoThumb mamVideoThumb = mamVideoThumbService.selectMamVideoThumbById(thumbId);
        MamVideo mamVideo = mamVideoService.selectMamVideoById(mamVideoThumb.getVideoId());

        String thumbPath = VideoUtils.getVideoPrivatePath(mamVideo.getCreateBy()) + mamVideoThumb.getSubpath() + File.separator+mamVideoThumb.getFilename();
        String thumbs = mamVideo.getThumbs();
        if(StringUtils.isNotBlank(thumbs)) {

            String[] thunmArray = thumbs.split(";");
            String thumbName = mamVideo.getThumbname();
            String thumbMiddleName = thumbName.replace(".jpg", thunmArray[0]);
            String thumbSmallName = thumbName.replace(".jpg", thunmArray[1]);

            String thumb =  VideoUtils.getVideoPrivatePath(mamVideo.getCreateBy()) + mamVideoThumb.getSubpath() + File.separator+thumbName;
            String thumbMiddlePath = VideoUtils.getVideoPrivatePath(mamVideo.getCreateBy()) + mamVideoThumb.getSubpath() + File.separator+thumbMiddleName;
            String thumbSmallPath = VideoUtils.getVideoPrivatePath(mamVideo.getCreateBy()) + mamVideoThumb.getSubpath() + File.separator+thumbSmallName;
            Map<String,String> configMap = mamVideoConfigService.selectVideoConfigMap(new MamVideoConfig());
            try {

                int thumbMiddleWidth = Convert.toInt(configMap.get("thumbMiddleWidth"));
                int thumbMiddleHeight = Convert.toInt(configMap.get("thumbMiddleHeight"));
                int thumbSmallWidth = Convert.toInt(configMap.get("thumbSmallWidth"));
                int thumbSmallHeight = Convert.toInt(configMap.get("thumbSmallHeight"));

                Thumbnails.of(thumbPath).scale(1f).outputQuality(1f).toFile(thumb);
                Thumbnails.of(thumbPath).size(thumbMiddleWidth, thumbMiddleHeight).toFile(thumbMiddlePath);
                Thumbnails.of(thumbPath).size(thumbSmallWidth, thumbSmallHeight).toFile(thumbSmallPath);
            } catch (IOException e) {
                return error(e.getMessage());
            }
        }
        return success();
    }
    /**
     * 添加描述
     */
    @PostMapping( "/addRemark")
    @ResponseBody
    public AjaxResult addRemark(Long thumbId,String remark)
    {
        MamVideoThumb mamVideoThumb = mamVideoThumbService.selectMamVideoThumbById(thumbId);
        mamVideoThumb.setRemark(remark);
        return toAjax(mamVideoThumbService.updateMamVideoThumb(mamVideoThumb));
    }
}
