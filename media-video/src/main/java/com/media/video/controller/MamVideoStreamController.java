package com.media.video.controller;

import com.media.common.annotation.Log;
import com.media.common.config.MediaConfig;
import com.media.common.core.controller.BaseController;
import com.media.common.core.domain.AjaxResult;
import com.media.common.core.domain.entity.SysUser;
import com.media.common.core.page.TableDataInfo;
import com.media.common.enums.BusinessType;
import com.media.common.exception.file.FileException;
import com.media.common.utils.ShiroUtils;
import com.media.common.utils.poi.ExcelUtil;
import com.media.video.domain.MamVideo;
import com.media.video.domain.MamVideoStream;
import com.media.video.service.IMamVideoLogService;
import com.media.video.service.IMamVideoService;
import com.media.video.service.IMamVideoStreamService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.List;

/**
 * 视频转码流对象Controller
 * 
 * @author liziye
 * @date 2022-03-15
 */
@Controller
@RequestMapping("/video/stream")
public class MamVideoStreamController extends BaseController
{
    private String prefix = "video/stream";

    @Autowired
    private IMamVideoStreamService mamVideoStreamService;

    @Autowired
    private IMamVideoService mamVideoService;

    @Autowired
    private IMamVideoLogService mamVideoLogService;


    @RequiresPermissions("video:stream:view")
    @GetMapping("/stream/{videoId}")
    public String videoStream(@PathVariable("videoId") Long videoId,ModelMap mmap)
    {
        mmap.put("videoId", videoId);
        return prefix + "/stream";
    }

    @RequiresPermissions("video:stream:view")
    @GetMapping()
    public String stream()
    {
        return prefix + "/stream";
    }

    /**
     * 查询视频转码流对象列表
     */
    @RequiresPermissions("video:stream:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MamVideoStream mamVideoStream)
    {
        startPage();
        List<MamVideoStream> list = mamVideoStreamService.selectMamVideoStreamList(mamVideoStream);
        return getDataTable(list);
    }

    /**
     * 导出视频转码流对象列表
     */
    @RequiresPermissions("video:stream:export")
    @Log(title = "视频转码流对象", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MamVideoStream mamVideoStream)
    {
        List<MamVideoStream> list = mamVideoStreamService.selectMamVideoStreamList(mamVideoStream);
        ExcelUtil<MamVideoStream> util = new ExcelUtil<MamVideoStream>(MamVideoStream.class);
        return util.exportExcel(list, "stream");
    }

    /**
     * 新增视频转码流对象
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存视频转码流对象
     */
    @RequiresPermissions("video:stream:add")
    @Log(title = "视频转码流对象", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MamVideoStream mamVideoStream)
    {
        return toAjax(mamVideoStreamService.insertMamVideoStream(mamVideoStream));
    }

    /**
     * 修改视频转码流对象
     */
    @GetMapping("/edit/{streamId}")
    public String edit(@PathVariable("streamId") Long streamId, ModelMap mmap)
    {
        MamVideoStream mamVideoStream = mamVideoStreamService.selectMamVideoStreamById(streamId);
        mmap.put("mamVideoStream", mamVideoStream);
        return prefix + "/edit";
    }

    /**
     * 修改保存视频转码流对象
     */
    @RequiresPermissions("video:stream:edit")
    @Log(title = "视频转码流对象", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MamVideoStream mamVideoStream)
    {
        return toAjax(mamVideoStreamService.updateMamVideoStream(mamVideoStream));
    }

    /**
     * 删除视频转码流对象
     */
    @RequiresPermissions("video:stream:remove")
    @Log(title = "视频转码流对象", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(mamVideoStreamService.deleteMamVideoStreamByIds(ids));
    }

    /**
     * 播放码流
     */
    @GetMapping("/play/{streamId}")
    public String play(@PathVariable("streamId") Long streamId, ModelMap mmap)
    {
        MamVideoStream videoStream = mamVideoStreamService.selectMamVideoStreamById(streamId);
        mmap.put("videoStream", videoStream);
        return prefix + "/play";
    }

    /**
     * 下载转码视频
     */
    @RequiresPermissions("video:stream:download")
    @Log(title = "下载视频转码流", businessType = BusinessType.EXPORT)
    @GetMapping("/download/{streamId}")
    public String download(@PathVariable("streamId") Long streamId, HttpServletRequest request, HttpServletResponse response) {
        MamVideoStream videoStream = mamVideoStreamService.selectMamVideoStreamById(streamId);
        if(videoStream == null) {
            throw new FileException("视频文件不存在!", null);
        }
        MamVideo video = mamVideoService.selectMamVideoById(videoStream.getVideoid());
        String filePath = MediaConfig.getProfile() + videoStream.getFilepath() + "/" + videoStream.getFilename();
        File file = new File(filePath);
        if (!file.exists()) {
            throw new FileException("视频不存在!", null);
        }
        SysUser currentUser = ShiroUtils.getSysUser();
        //开始下载
        // 告诉浏览器输出内容为流
        response.setHeader("Content-Type", "application/octet-stream;charset=utf-8");

        byte[] buffer = new byte[1024];
        FileInputStream fis = null;
        BufferedInputStream bis = null;
        try {
            response.addHeader("Content-Disposition", "attachment;fileName=" + URLEncoder.encode(videoStream.getFilename(),"UTF-8"));// 设置文件名
            fis = new FileInputStream(file);
            bis = new BufferedInputStream(fis);
            OutputStream os = response.getOutputStream();
            int i = bis.read(buffer);
            while (i != -1) {
                os.write(buffer, 0, i);
                i = bis.read(buffer);
            }
            String actionDes = "下载视频:"+currentUser.getLoginName()+"下载视频";
            mamVideoLogService.insertVideoLog(videoStream.getVideoid(), com.media.common.enums.MediaType.VIDEO.toString(), "download_video", "下载视频", actionDes, "MEDIA_CODE", "视频库-我的视频", "operation", "",currentUser.getUserId(),video.getCategoryId());
            return "下载成功";
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (bis != null) {
                try {
                    bis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return  "";
    }
}
