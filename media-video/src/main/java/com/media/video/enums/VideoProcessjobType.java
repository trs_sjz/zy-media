package com.media.video.enums;

/**
 *视频处理任务状态
 * @author liziye
 * @date 2022-03-15
 */
public enum VideoProcessjobType {
    STD("全新"), RE("重新");

    private final String info;

    private VideoProcessjobType(String info) {
        this.info = info;
    }

    public String getInfo() {
        return info;
    }
}
