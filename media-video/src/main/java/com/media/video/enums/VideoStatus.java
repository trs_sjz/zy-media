package com.media.video.enums;

/**
 * 视频状态
 *
 * @author liziye
 * @date 2022-03-15
 */
public enum VideoStatus {
	NEW(0, "待处理"), AVAILABLE(1, "待审"), PROCESS_FAILED(2, "审核退回"),SUC_DONE(9, "审核通过"),PUBLISHF_REVOKE(5,"发布撤回"),PUBLISHED(99,"已发布");

    private final int code;
    private final String info;

    VideoStatus(int code, String info)
    {
        this.code = code;
        this.info = info;
    }

    public int getCode()
    {
        return code;
    }

    public String getInfo()
    {
        return info;
    }

}
