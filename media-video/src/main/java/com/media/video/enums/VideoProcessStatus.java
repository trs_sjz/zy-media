package com.media.video.enums;

/**
 * 视频处理状态
 *
 * @author liziye
 * @date 2022-03-15
 */
public enum VideoProcessStatus {
	NEW("待处理"),  ACCEPT("处理任务排队中"),  ST_PROCESSING("处理中"),  AVAILABLE("待粗处理"),  PROCESSING_B("深度处理中"),  SUC_DONE("处理成功"),  FAIL_CONVERT("处理失败");

	private final String info;

	VideoProcessStatus( String info) {
		this.info = info;
	}


	public String getInfo() {
		return info;
	}
}
