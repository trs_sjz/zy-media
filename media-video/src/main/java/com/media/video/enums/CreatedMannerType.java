package com.media.video.enums;

/**
 *
 * @author liziye
 * @date 2022-03-15
 */
public enum CreatedMannerType {
	/**
     * 上传
     */
	UPLOAD,
	/**
     * 导入
     */
	IMPORT,

}
