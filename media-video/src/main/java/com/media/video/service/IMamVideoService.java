package com.media.video.service;

import com.media.video.domain.MamVideo;

import java.util.List;

/**
 * 视频信息Service接口
 * 
 * @author liziye
 * @date 2022-03-11
 */
public interface IMamVideoService 
{
    /**
     * 查询视频信息
     * 
     * @param videoId 视频信息ID
     * @return 视频信息
     */
    public MamVideo selectMamVideoById(Long videoId);

    /**
     * 查询视频信息
     *
     * @param sourceVideoId 源视频ID
     * @return 视频管理
     */
    public MamVideo selectMamVideoBySourceVideoId(Long sourceVideoId);

    /**
     * 查询视频信息列表
     * 
     * @param mamVideo 视频信息
     * @return 视频信息集合
     */
    public List<MamVideo> selectMamVideoList(MamVideo mamVideo);

    /**
     * 新增视频信息
     * 
     * @param mamVideo 视频信息
     * @return 结果
     */
    public int insertMamVideo(MamVideo mamVideo);

    /**
     * 修改视频信息
     * 
     * @param mamVideo 视频信息
     * @return 结果
     */
    public int updateMamVideo(MamVideo mamVideo);

    /**
     * 批量删除视频信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMamVideoByIds(String ids);

    /**
     * 删除视频信息信息
     * 
     * @param videoId 视频信息ID
     * @return 结果
     */
    public int deleteMamVideoById(Long videoId);

    /**
     * 修改视频加工状态
     *
     * @param mamVideo 视频管理
     * @return 结果
     */
    public int updateMamVideoProcessState(MamVideo mamVideo);

    /**
     * 定时任务修改视频管理
     *
     * @param mamVideo 视频管理
     * @return 结果
     */
    public int updateMamVideoProcessInfo(MamVideo mamVideo);

    /**
     * 视频回收站
     *
     * @param mamVideo 视频管理
     * @return 视频管理集合
     */
    public List<MamVideo> selectRecycleMamVideoList(MamVideo mamVideo);

    /**
     * 删除视频
     *
     * @param mamVideo 视频管理
     * @return 结果
     */
    public int deleteMamVideo(MamVideo mamVideo);

    /**
     * 批量还原视频管理
     *
     * @param ids 需要还原的数据ID
     * @return 结果
     */
    public int restoreMamVideoByIds(String ids);
}
