package com.media.video.service.impl;

import com.media.common.core.text.Convert;
import com.media.common.utils.DateUtils;
import com.media.video.domain.MamVideoProcessjob;
import com.media.video.mapper.MamVideoProcessjobMapper;
import com.media.video.service.IMamVideoProcessjobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 视频加工任务Service业务层处理
 * 
 * @author liziye
 * @date 2022-03-15
 */
@Service
public class MamVideoProcessjobServiceImpl implements IMamVideoProcessjobService 
{
    @Autowired
    private MamVideoProcessjobMapper mamVideoProcessjobMapper;

    /**
     * 查询视频加工任务
     * 
     * @param id 视频加工任务ID
     * @return 视频加工任务
     */
    @Override
    public MamVideoProcessjob selectMamVideoProcessjobById(Long id)
    {
        return mamVideoProcessjobMapper.selectMamVideoProcessjobById(id);
    }

    /**
     * 查询视频加工任务列表
     * 
     * @param mamVideoProcessjob 视频加工任务
     * @return 视频加工任务
     */
    @Override
    public List<MamVideoProcessjob> selectMamVideoProcessjobList(MamVideoProcessjob mamVideoProcessjob)
    {
        return mamVideoProcessjobMapper.selectMamVideoProcessjobList(mamVideoProcessjob);
    }

    /**
     * 新增视频加工任务
     * 
     * @param mamVideoProcessjob 视频加工任务
     * @return 结果
     */
    @Override
    public int insertMamVideoProcessjob(MamVideoProcessjob mamVideoProcessjob)
    {
        mamVideoProcessjob.setCreateTime(DateUtils.getNowDate());
        return mamVideoProcessjobMapper.insertMamVideoProcessjob(mamVideoProcessjob);
    }

    /**
     * 修改视频加工任务
     * 
     * @param mamVideoProcessjob 视频加工任务
     * @return 结果
     */
    @Override
    public int updateMamVideoProcessjob(MamVideoProcessjob mamVideoProcessjob)
    {
        mamVideoProcessjob.setUpdateTime(DateUtils.getNowDate());
        return mamVideoProcessjobMapper.updateMamVideoProcessjob(mamVideoProcessjob);
    }

    /**
     * 删除视频加工任务对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMamVideoProcessjobByIds(String ids)
    {
        return mamVideoProcessjobMapper.deleteMamVideoProcessjobByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除视频加工任务信息
     * 
     * @param id 视频加工任务ID
     * @return 结果
     */
    @Override
    public int deleteMamVideoProcessjobById(Long id)
    {
        return mamVideoProcessjobMapper.deleteMamVideoProcessjobById(id);
    }

    /**
     * 更新视频作业任务状态
     * @param mamVideoProcessjob
     */
    @Override
    public void updateMamVideoProcessjobState(MamVideoProcessjob mamVideoProcessjob) {
        mamVideoProcessjobMapper.updateMamVideoProcessjob(mamVideoProcessjob);
    }

    /**
     * 查询用户待处理任务列表
     *
     * @return 视频加工任务表集合
     */
    @Override
    public List<MamVideoProcessjob> selectMamProcessjobList() {
        System.out.println("11111");
        return mamVideoProcessjobMapper.selectMamProcessjobList();
    }
}
