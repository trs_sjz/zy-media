package com.media.video.service;

import com.media.video.domain.MamVideoConfig;

import java.util.List;
import java.util.Map;

/**
 * 视频转码配置Service接口
 * 
 * @author liziye
 * @date 2022-03-11
 */
public interface IMamVideoConfigService 
{
    /**
     * 查询视频转码配置
     * 
     * @param configId 视频转码配置ID
     * @return 视频转码配置
     */
    public MamVideoConfig selectMamVideoConfigById(Integer configId);

    /**
     * 查询视频转码配置列表
     * 
     * @param mamVideoConfig 视频转码配置
     * @return 视频转码配置集合
     */
    public List<MamVideoConfig> selectMamVideoConfigList(MamVideoConfig mamVideoConfig);

    /**
     * 新增视频转码配置
     * 
     * @param mamVideoConfig 视频转码配置
     * @return 结果
     */
    public int insertMamVideoConfig(MamVideoConfig mamVideoConfig);

    /**
     * 修改视频转码配置
     * 
     * @param mamVideoConfig 视频转码配置
     * @return 结果
     */
    public int updateMamVideoConfig(MamVideoConfig mamVideoConfig);

    /**
     * 批量删除视频转码配置
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMamVideoConfigByIds(String ids);

    /**
     * 删除视频转码配置信息
     * 
     * @param configId 视频转码配置ID
     * @return 结果
     */
    public int deleteMamVideoConfigById(Integer configId);

    /**
     * 获取视频转码配置信息
     *
     * @return 视频转码配置集合
     */
    public String selectVideoConfig();

    /**
     * 返回配置信息
     *
     * @param mamVideoConfig 视频转码配置
     * @return 视频转码配置集合
     */
    public Map<String,String> selectVideoConfigMap(MamVideoConfig mamVideoConfig);
}
