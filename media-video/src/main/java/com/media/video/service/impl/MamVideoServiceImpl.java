package com.media.video.service.impl;

import com.media.common.core.domain.entity.SysUser;
import com.media.common.core.text.Convert;
import com.media.common.enums.DelFlag;
import com.media.common.utils.DateUtils;
import com.media.common.utils.ShiroUtils;
import com.media.common.utils.file.FileUtils;
import com.media.video.domain.MamVideo;
import com.media.video.mapper.MamVideoMapper;
import com.media.video.service.*;
import com.media.video.utils.VideoUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;

/**
 * 视频信息Service业务层处理
 * 
 * @author liziye
 * @date 2022-03-11
 */
@Service
public class MamVideoServiceImpl implements IMamVideoService 
{
    @Autowired
    private MamVideoMapper mamVideoMapper;

    @Autowired
    private IMamVideoLogService iMamVideoLogService;      //媒资操作日志
    @Autowired
    private IMamVideoStreamService iMamVideoStreamService;     //视频转码码流
    @Autowired
    private IMamVideoThumbService iMamVideoThumbService;       //视频缩略图
    @Autowired
    private IMamVideoProcessjobService iMamVideoProcessjobService;     //转码任务
    @Autowired
    private IMamVideoLableService iMamVideoLableService;       //视频标签
    @Autowired
    private IMamVideoFramentService iMamVideoFramentService;     //视频片段
    @Autowired
    private IMamVideoSourceService iMamVideoSourceService;
    @Autowired
    private IMamVideoService iMamVideoService;

    /**
     * 查询视频信息
     * 
     * @param videoId 视频信息ID
     * @return 视频信息
     */
    @Override
    public MamVideo selectMamVideoById(Long videoId)
    {
        return mamVideoMapper.selectMamVideoById(videoId);
    }

    /**
     * 查询视频信息
     *
     * @param sourceVideoId 源视频ID
     * @return 视频信息
     */
    @Override
    public MamVideo selectMamVideoBySourceVideoId(Long sourceVideoId) {
        return mamVideoMapper.selectMamVideoBySourceVideoId(sourceVideoId);
    }

    /**
     * 查询视频信息列表
     * 
     * @param mamVideo 视频信息
     * @return 视频信息
     */
    @Override
    public List<MamVideo> selectMamVideoList(MamVideo mamVideo)
    {
        return mamVideoMapper.selectMamVideoList(mamVideo);
    }

    /**
     * 新增视频信息
     * 
     * @param mamVideo 视频信息
     * @return 结果
     */
    @Override
    public int insertMamVideo(MamVideo mamVideo)
    {
        mamVideo.setCreateTime(DateUtils.getNowDate());
        return mamVideoMapper.insertMamVideo(mamVideo);
    }

    /**
     * 修改视频信息
     * 
     * @param mamVideo 视频信息
     * @return 结果
     */
    @Override
    public int updateMamVideo(MamVideo mamVideo)
    {
        mamVideo.setUpdateTime(DateUtils.getNowDate());
        return mamVideoMapper.updateMamVideo(mamVideo);
    }

    /**
     * 删除视频信息对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMamVideoByIds(String ids)
    {
        SysUser  currentUser = ShiroUtils.getSysUser();
        Long[] idsArray = Convert.toLongArray(ids);
        int result = 0;
        MamVideo video = null;
        for(int i=0;i<idsArray.length;i++) {
            video = new MamVideo();
            video.setVideoId(idsArray[i]);
            //删除状态
            video.setDelFlag(DelFlag.DELETED.getCode());
            video.setDeleteBy(currentUser.getLoginName());
            video.setDeleteTime(DateUtils.getNowDate());
            result = mamVideoMapper.updateMamVideo(video);
        }
        return result;
    }

    /**
     * 删除视频信息信息
     * 
     * @param videoId 视频信息ID
     * @return 结果
     */
    @Override
    public int deleteMamVideoById(Long videoId)
    {
        return mamVideoMapper.deleteMamVideoById(videoId);
    }

    /**
     * 修改视频加工状态
     *
     * @param mamVideo 视频管理
     * @return 结果
     */
    @Override
    public int updateMamVideoProcessState(MamVideo mamVideo)
    {
        return mamVideoMapper.updateMamVideoProcessState(mamVideo);
    }

    /**
     * 视频加工加工管理
     *
     * @param mamVideo 视频管理
     * @return 结果
     */
    @Override
    public int updateMamVideoProcessInfo(MamVideo mamVideo)
    {
        return mamVideoMapper.updateMamVideo(mamVideo);
    }

    /**
     * 视频回收站
     *
     * @param mamVideo 视频管理
     * @return 视频管理
     */
    @Override
    public List<MamVideo> selectRecycleMamVideoList(MamVideo mamVideo)
    {
        return mamVideoMapper.selectRecycleMamVideoList(mamVideo);
    }

    /**
     * 物理删除视频
     */
    @Override
    public int deleteMamVideo(MamVideo mamVideo) {
        int result = 0;
        //删除视频码流
        iMamVideoStreamService.deleteMamVideoStreamById(mamVideo.getVideoId());
        //删除所有缩略图
        iMamVideoThumbService.deleteMamVideoThumbById(mamVideo.getVideoId());
        //删除转码任务
        iMamVideoProcessjobService.deleteMamVideoProcessjobById(mamVideo.getVideoId());
        //删除视频标签
        iMamVideoLableService.deleteMamVideoLableById(mamVideo.getVideoId());
        //删除视频片段
        iMamVideoFramentService.deleteMamVideoFramentById(mamVideo.getVideoId());
        //删除审核流程
        //videoProcessService.deleteVideoProcessByVideoId(mamVideo.getVideoId());

        //删除源视频
        iMamVideoSourceService.deleteMamVideoSourceById(mamVideo.getSourceVideoId());

        String thumbPath = VideoUtils.getVideoPrivatePath(mamVideo.getCreateBy()) + mamVideo.getSubpath() + File.separator + mamVideo.getFilenamebase();
        //删除视频缩略图地址
        FileUtils.deleteDir(thumbPath);
        //永久性删除视频
        mamVideoMapper.deleteMamVideoById(mamVideo.getVideoId());
        return result;
    }

    /**
     * 批量还原视频管理
     *
     * @param ids 需要还原的数据ID
     * @return 结果
     */
    @Override
    public int restoreMamVideoByIds(String ids) {
        SysUser currentUser = ShiroUtils.getSysUser();
        Long[] idsArray = Convert.toLongArray(ids);
        int result = 0;
        MamVideo mamVideo = null;
        for(int i=0;i<idsArray.length;i++) {
            mamVideo = new MamVideo();
            mamVideo.setVideoId(idsArray[i]);
            mamVideo.setDelFlag(DelFlag.OK.getCode());
            mamVideo.setUpdateBy(currentUser.getLoginName());
            mamVideo.setUpdateTime(DateUtils.getNowDate());
            mamVideoMapper.updateMamVideoDeleteStatus(mamVideo);
            result = 1;
        }
        return result;
    }
}
