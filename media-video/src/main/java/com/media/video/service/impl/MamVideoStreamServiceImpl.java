package com.media.video.service.impl;

import com.media.common.core.text.Convert;
import com.media.common.utils.DateUtils;
import com.media.video.domain.MamVideoStream;
import com.media.video.mapper.MamVideoStreamMapper;
import com.media.video.service.IMamVideoStreamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 视频转码流对象Service业务层处理
 * 
 * @author liziye
 * @date 2022-03-15
 */
@Service
public class MamVideoStreamServiceImpl implements IMamVideoStreamService 
{
    @Autowired
    private MamVideoStreamMapper mamVideoStreamMapper;

    /**
     * 查询视频转码流对象
     * 
     * @param streamId 视频转码流对象ID
     * @return 视频转码流对象
     */
    @Override
    public MamVideoStream selectMamVideoStreamById(Long streamId)
    {
        return mamVideoStreamMapper.selectMamVideoStreamById(streamId);
    }

    /**
     * 查询视频转码流对象列表
     * 
     * @param mamVideoStream 视频转码流对象
     * @return 视频转码流对象
     */
    @Override
    public List<MamVideoStream> selectMamVideoStreamList(MamVideoStream mamVideoStream)
    {
        return mamVideoStreamMapper.selectMamVideoStreamList(mamVideoStream);
    }

    /**
     * 新增视频转码流对象
     * 
     * @param mamVideoStream 视频转码流对象
     * @return 结果
     */
    @Override
    public int insertMamVideoStream(MamVideoStream mamVideoStream)
    {
        mamVideoStream.setCreateTime(DateUtils.getNowDate());
        return mamVideoStreamMapper.insertMamVideoStream(mamVideoStream);
    }

    /**
     * 修改视频转码流对象
     * 
     * @param mamVideoStream 视频转码流对象
     * @return 结果
     */
    @Override
    public int updateMamVideoStream(MamVideoStream mamVideoStream)
    {
        mamVideoStream.setUpdateTime(DateUtils.getNowDate());
        return mamVideoStreamMapper.updateMamVideoStream(mamVideoStream);
    }

    /**
     * 删除视频转码流对象对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMamVideoStreamByIds(String ids)
    {
        return mamVideoStreamMapper.deleteMamVideoStreamByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除视频转码流对象信息
     * 
     * @param streamId 视频转码流对象ID
     * @return 结果
     */
    @Override
    public int deleteMamVideoStreamById(Long streamId)
    {
        return mamVideoStreamMapper.deleteMamVideoStreamById(streamId);
    }
}
