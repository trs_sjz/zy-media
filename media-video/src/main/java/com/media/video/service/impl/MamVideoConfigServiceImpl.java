package com.media.video.service.impl;

import com.alibaba.druid.support.json.JSONUtils;
import com.media.common.core.text.Convert;
import com.media.common.utils.DateUtils;
import com.media.video.domain.MamVideoConfig;
import com.media.video.mapper.MamVideoConfigMapper;
import com.media.video.service.IMamVideoConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 视频转码配置Service业务层处理
 * 
 * @author liziye
 * @date 2022-03-11
 */
@Service
public class MamVideoConfigServiceImpl implements IMamVideoConfigService 
{
    @Autowired
    private MamVideoConfigMapper mamVideoConfigMapper;

    /**
     * 查询视频转码配置
     * 
     * @param configId 视频转码配置ID
     * @return 视频转码配置
     */
    @Override
    public MamVideoConfig selectMamVideoConfigById(Integer configId)
    {
        return mamVideoConfigMapper.selectMamVideoConfigById(configId);
    }

    /**
     * 查询视频转码配置列表
     * 
     * @param mamVideoConfig 视频转码配置
     * @return 视频转码配置
     */
    @Override
    public List<MamVideoConfig> selectMamVideoConfigList(MamVideoConfig mamVideoConfig)
    {
        return mamVideoConfigMapper.selectMamVideoConfigList(mamVideoConfig);
    }

    /**
     * 新增视频转码配置
     * 
     * @param mamVideoConfig 视频转码配置
     * @return 结果
     */
    @Override
    public int insertMamVideoConfig(MamVideoConfig mamVideoConfig)
    {
        mamVideoConfig.setCreateTime(DateUtils.getNowDate());
        return mamVideoConfigMapper.insertMamVideoConfig(mamVideoConfig);
    }

    /**
     * 修改视频转码配置
     * 
     * @param mamVideoConfig 视频转码配置
     * @return 结果
     */
    @Override
    public int updateMamVideoConfig(MamVideoConfig mamVideoConfig)
    {
        mamVideoConfig.setUpdateTime(DateUtils.getNowDate());
        return mamVideoConfigMapper.updateMamVideoConfig(mamVideoConfig);
    }

    /**
     * 删除视频转码配置对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMamVideoConfigByIds(String ids)
    {
        return mamVideoConfigMapper.deleteMamVideoConfigByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除视频转码配置信息
     * 
     * @param configId 视频转码配置ID
     * @return 结果
     */
    @Override
    public int deleteMamVideoConfigById(Integer configId)
    {
        return mamVideoConfigMapper.deleteMamVideoConfigById(configId);
    }

    /**
     * 获取视频转码配置信息
     *
     * @return 视频转码配置集合
     */
    @Override
    public String selectVideoConfig() {
        List<MamVideoConfig> list  = selectMamVideoConfigList(new MamVideoConfig());
        Map<String,String> config = new HashMap<String,String>();
        Map<String,String> map = new HashMap<String,String>();
        for(MamVideoConfig vConfig:list) {
            config.put(vConfig.getConfigKey(), vConfig.getConfigValue());
        }
        map.put("streamformat", config.get("streamformat"));
        map.put("streamquality", config.get("streamquality"));
        if(config.get("streamquality")!=null&&!config.get("streamquality").equals("0")) {
            String streamquality = config.get("streamquality");
            map.put("streambitrate", config.get("stream"+streamquality+"bitrate"));
            map.put("streamresolution", config.get("stream"+streamquality+"resolution"));
            map.put("streamaudioBitrate", config.get("stream"+streamquality+"audioBitrate"));
            map.put("streamaudioChannels", config.get("stream"+streamquality+"audioChannels"));
        }else {
            map.put("streambitrate", "");
            map.put("streamresolution", "");
            map.put("streamaudioBitrate", "");
            map.put("streamaudioChannels", "");
        }
        map.put("thumbMiddleWidth", config.get("thumbMiddleWidth"));
        map.put("thumbMiddleHeight", config.get("thumbMiddleHeight"));
        map.put("thumbSmallWidth", config.get("thumbSmallWidth"));
        map.put("thumbSmallHeight", config.get("thumbSmallHeight"));
        return JSONUtils.toJSONString(map);
    }

    /**
     * 返回配置信息
     *
     * @param mamVideoConfig 视频转码配置
     * @return 视频转码配置集合
     */
    @Override
    public Map<String, String> selectVideoConfigMap(MamVideoConfig mamVideoConfig) {
        List<MamVideoConfig> list  = selectMamVideoConfigList(mamVideoConfig);
        Map<String,String> config = new HashMap<String,String>();
        Map<String,String> map = new HashMap<String,String>();
        for(MamVideoConfig vConfig:list) {
            config.put(vConfig.getConfigKey(), vConfig.getConfigValue());
        }
        map.put("streamformat", config.get("streamformat"));
        map.put("streamquality", config.get("streamquality"));
        if(config.get("streamquality")!=null&&!config.get("streamquality").equals("0")) {
            String streamquality = config.get("streamquality");
            map.put("streambitrate", config.get("stream"+streamquality+"bitrate"));
            map.put("streamresolution", config.get("stream"+streamquality+"resolution"));
            map.put("streamaudioBitrate", config.get("stream"+streamquality+"audioBitrate"));
            map.put("streamaudioChannels", config.get("stream"+streamquality+"audioChannels"));
        }else {
            map.put("streambitrate", "");
            map.put("streamresolution", "");
            map.put("streamaudioBitrate", "");
            map.put("streamaudioChannels", "");
        }
        map.put("thumbMiddleWidth", config.get("thumbMiddleWidth"));
        map.put("thumbMiddleHeight", config.get("thumbMiddleHeight"));
        map.put("thumbSmallWidth", config.get("thumbSmallWidth"));
        map.put("thumbSmallHeight", config.get("thumbSmallHeight"));
        return map;
    }
}
