package com.media.video.service.impl;

import com.media.common.core.text.Convert;
import com.media.common.utils.DateUtils;
import com.media.video.domain.MamVideoLable;
import com.media.video.mapper.MamVideoLableMapper;
import com.media.video.service.IMamVideoLableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 视频标签Service业务层处理
 * 
 * @author liziye
 * @date 2022-03-16
 */
@Service
public class MamVideoLableServiceImpl implements IMamVideoLableService 
{
    @Autowired
    private MamVideoLableMapper mamVideoLableMapper;

    /**
     * 查询视频标签
     * 
     * @param lableId 视频标签ID
     * @return 视频标签
     */
    @Override
    public MamVideoLable selectMamVideoLableById(Long lableId)
    {
        return mamVideoLableMapper.selectMamVideoLableById(lableId);
    }

    /**
     * 查询视频标签列表
     * 
     * @param mamVideoLable 视频标签
     * @return 视频标签
     */
    @Override
    public List<MamVideoLable> selectMamVideoLableList(MamVideoLable mamVideoLable)
    {
        return mamVideoLableMapper.selectMamVideoLableList(mamVideoLable);
    }

    /**
     * 新增视频标签
     * 
     * @param mamVideoLable 视频标签
     * @return 结果
     */
    @Override
    public int insertMamVideoLable(MamVideoLable mamVideoLable)
    {
        mamVideoLable.setCreateTime(DateUtils.getNowDate());
        return mamVideoLableMapper.insertMamVideoLable(mamVideoLable);
    }

    /**
     * 修改视频标签
     * 
     * @param mamVideoLable 视频标签
     * @return 结果
     */
    @Override
    public int updateMamVideoLable(MamVideoLable mamVideoLable)
    {
        mamVideoLable.setUpdateTime(DateUtils.getNowDate());
        return mamVideoLableMapper.updateMamVideoLable(mamVideoLable);
    }

    /**
     * 删除视频标签对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMamVideoLableByIds(String ids)
    {
        return mamVideoLableMapper.deleteMamVideoLableByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除视频标签信息
     * 
     * @param lableId 视频标签ID
     * @return 结果
     */
    @Override
    public int deleteMamVideoLableById(Long lableId)
    {
        return mamVideoLableMapper.deleteMamVideoLableById(lableId);
    }
}
