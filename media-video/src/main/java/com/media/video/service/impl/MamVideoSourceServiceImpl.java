package com.media.video.service.impl;

import com.media.common.config.MediaConfig;
import com.media.common.constant.MediaConstrans;
import com.media.common.core.domain.entity.SysUser;
import com.media.common.core.text.Convert;
import com.media.common.enums.MediaType;
import com.media.common.utils.DateUtils;
import com.media.common.utils.IpUtils;
import com.media.common.utils.ShiroUtils;
import com.media.common.utils.file.FileUtils;
import com.media.common.utils.security.Md5Utils;
import com.media.framework.web.domain.response.CheckChunkResult;
import com.media.system.service.ISysConfigService;
import com.media.video.domain.MamVideoSource;
import com.media.video.enums.CreatedMannerType;
import com.media.video.enums.VideoProcessStatus;
import com.media.video.ffmepg.utils.FFprobeUtils;
import com.media.video.mapper.MamVideoSourceMapper;
import com.media.video.service.IMamVideoSourceService;
import com.media.video.utils.VideoUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.*;

/**
 * 源视频Service业务层处理
 * 
 * @author liziye
 * @date 2022-03-15
 */
@Service
public class MamVideoSourceServiceImpl implements IMamVideoSourceService 
{
    private static final Logger log = LoggerFactory.getLogger(MamVideoSourceServiceImpl.class);

    //上传文件数量
    private static int counter = 0;

    @Autowired
    private MamVideoSourceMapper mamVideoSourceMapper;

    @Autowired
    private ISysConfigService configService;

    /**
     * 查询源视频
     * 
     * @param id 源视频ID
     * @return 源视频
     */
    @Override
    public MamVideoSource selectMamVideoSourceById(Long id)
    {
        return mamVideoSourceMapper.selectMamVideoSourceById(id);
    }

    /**
     * 查询源视频列表
     * 
     * @param mamVideoSource 源视频
     * @return 源视频
     */
    @Override
    public List<MamVideoSource> selectMamVideoSourceList(MamVideoSource mamVideoSource)
    {
        return mamVideoSourceMapper.selectMamVideoSourceList(mamVideoSource);
    }

    /**
     * 新增源视频
     * 
     * @param mamVideoSource 源视频
     * @return 结果
     */
    @Override
    public int insertMamVideoSource(MamVideoSource mamVideoSource)
    {
        mamVideoSource.setCreateTime(DateUtils.getNowDate());
        return mamVideoSourceMapper.insertMamVideoSource(mamVideoSource);
    }

    /**
     * 修改源视频
     * 
     * @param mamVideoSource 源视频
     * @return 结果
     */
    @Override
    public int updateMamVideoSource(MamVideoSource mamVideoSource)
    {
        mamVideoSource.setUpdateTime(DateUtils.getNowDate());
        return mamVideoSourceMapper.updateMamVideoSource(mamVideoSource);
    }

    /**
     * 删除源视频对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMamVideoSourceByIds(String ids)
    {
        return mamVideoSourceMapper.deleteMamVideoSourceByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除源视频信息
     * 
     * @param id 源视频ID
     * @return 结果
     */
    @Override
    public int deleteMamVideoSourceById(Long id)
    {
        return mamVideoSourceMapper.deleteMamVideoSourceById(id);
    }

    /**
     * 检测块文件
     * @param name
     * @param chunkIndex
     * @param chunkSize
     * @return
     */
    @Override
    public CheckChunkResult checkChunk(String name, String blockmd5, Integer chunkIndex, Integer chunkSize) {
        //得到块文件所在路径
        String chunkFileFolderPath = getChunkFileFolderPath(name);
        //块文件的文件名称以1,2,3..序号命名，没有扩展名
        File chunkFile = new File(chunkFileFolderPath+chunkIndex+".part");
        if (chunkFile.exists()) {
            return new CheckChunkResult(1);
        } else {
            return new CheckChunkResult(0);
        }
    }

    /**
     * 分片上传视频,实现断点续传
     * @param file
     * @param name
     * @param chunks
     * @param chunk
     * @param size
     * @return
     */
    @Override
    public Map<String, Object> fileUploadChunk(MultipartFile file, String name, String type, Integer chunks, Integer chunk,
                                               Integer size) {
        SysUser currentUser = ShiroUtils.getSysUser();
        Map<String,Object> map = new HashMap<String,Object>();
        String fileMd5Path = DigestUtils.md5DigestAsHex((name+size+type + currentUser.getUserId()).getBytes());
        String chunkFileFolderPath = getChunkFileFolderPath(fileMd5Path);
        File desc = new File(chunkFileFolderPath + chunk+".part");
        try {
            file.transferTo(desc);
            map.put("status", 1);
        }catch (IOException e) {
            map.put("status", 0);
        }
        map.put("path", "");
        return map;
    }

    /**
     * 合并分片视频
     * @param name
     * @param chunks
     * @param ext
     * @return
     */
    @Override
    public Map<String,Object> mergeChunks(String name, String filename, Integer chunks, String ext, String type, Integer size){

        SysUser currentUser = ShiroUtils.getSysUser();
        name = DigestUtils.md5DigestAsHex((name+size+type+currentUser.getUserId()).getBytes());
        String ffmpegPath = configService.selectConfigByKey("sys.video.ffMpegPath"); // ffmpeg路径
        Map<String,Object> map = new HashMap<String,Object>();    //返回结果map

        String chunkFileFolderPath = getChunkFileFolderPath(name);
        File chunkFileFolder = new File(chunkFileFolderPath);
        String subPath = DateUtils.datePath();    //文件目录
        String newFileName = encodingFilename(name);    //新的编码文件
        String videoSourcePath = VideoUtils.getVideoSourcePath(subPath, currentUser.getLoginName());    //获取源视频上传目录
        String filePath = videoSourcePath+"/"+newFileName+"."+ext;
        //创建合并文件
        File mergeFile = createMergeFile(filePath);

        //获取块文件，此列表是已经排好序的列表
        List<File> chunkFiles = getChunkFiles(chunkFileFolder);
        //合并文件
        mergeFile = mergeFile(mergeFile, chunkFiles);

        FileUtils.deleteDir(chunkFileFolderPath);     //删除临时块文件


        MamVideoSource videoSource = new MamVideoSource();     //视频源数据
        //获取视频元数据
        Map<String,Map<String, Object>> mediaInfo = FFprobeUtils.getMediaInfo(ffmpegPath, filePath);
        if(mediaInfo!=null) {
            Map<String,Object> audioMap = (Map<String, Object>) mediaInfo.get("audio");
            Map<String,Object> videoMap = (Map<String, Object>) mediaInfo.get("video");
            Map<String,Object> formatMap = (Map<String, Object>) mediaInfo.get("format");

            if(audioMap!=null) {
                videoSource.setAudiobitrate(Long.valueOf(String.valueOf(audioMap.get("bit_rate"))));
                videoSource.setAudiochannels(Long.valueOf(String.valueOf(audioMap.get("channels"))));
                videoSource.setAudiocodec(String.valueOf(audioMap.get("codec_tag_string")));
                videoSource.setAudioformat(String.valueOf(audioMap.get("codec_name")));
                videoSource.setAudiosamplerate(Long.valueOf(String.valueOf(audioMap.get("sample_rate"))));
            }

            if(videoMap!=null) {
                videoSource.setFramerate(Long.valueOf(String.valueOf(videoMap.get("r_frame_rate"))));
                videoSource.setHeight(Long.valueOf(String.valueOf(videoMap.get("height"))));
                videoSource.setMediatype(MediaType.VIDEO.toString());
                videoSource.setNbframes(Long.valueOf(String.valueOf(videoMap.get("nb_frames"))));
                videoSource.setPixelformat(String.valueOf(videoMap.get("pix_fmt")));
                videoSource.setVideocodec(String.valueOf(videoMap.get("codec_tag_string")));
                videoSource.setVideoformat(String.valueOf(videoMap.get("codec_name")));
                videoSource.setVideolevel(String.valueOf(videoMap.get("level")));
                videoSource.setVideoprofile(String.valueOf(videoMap.get("profile")));
                videoSource.setWidth(Long.valueOf(String.valueOf(videoMap.get("width"))));
            }

            if(formatMap!=null&&formatMap.get("bit_rate")!=null) {
                videoSource.setBitrate(Long.valueOf(String.valueOf(formatMap.get("bit_rate"))));
            }

            videoSource.setDemuxer(String.valueOf(formatMap.get("format_name")));
            videoSource.setDuration(Long.valueOf(String.valueOf(formatMap.get("duration"))));
            videoSource.setFps(0L);

            videoSource.setAutosegment(0);
            videoSource.setCreatedip(IpUtils.getHostIp());
            videoSource.setCreatedmanner(CreatedMannerType.UPLOAD.toString());
            videoSource.setFileext(ext);
            videoSource.setFilename(newFileName+"."+ext);
            videoSource.setLang("中文");
            videoSource.setSize(Long.valueOf(String.valueOf(formatMap.get("size"))));
        }else {

        }
        String fileRelativePath = filePath.replace(MediaConfig.getProfile(), "");
        //文件路径
        videoSource.setFilepath(fileRelativePath);
        videoSource.setOriginname(filename);
        videoSource.setSampleid(0L);
        videoSource.setStatus(VideoProcessStatus.NEW.toString());
        videoSource.setSubpath(subPath);
        videoSource.setTitle(FileUtils.getFileNameWithoutSuffix(filename));
        videoSource.setYear(DateUtils.YYYY);
        videoSource.setCreateUserId(currentUser.getUserId());
        videoSource.setCreateBy(currentUser.getLoginName());
        videoSource.setCreateTime(DateUtils.getNowDate());
        videoSource.setUpdateBy(currentUser.getLoginName());
        videoSource.setUpdateTime(DateUtils.getNowDate());
        videoSource.setRemark(filename);
        insertMamVideoSource(videoSource);

        map.put("status", 1);
        map.put("sourceVideoId", videoSource.getId());
        map.put("path", fileRelativePath);

        return map;
    }

    /**
     * 创建合并文件
     * @return
     */
    private File createMergeFile(String filePath) {
        File mergeFile = new File(filePath);
        try {
            boolean newFile = mergeFile.createNewFile();   //创建一个新的空文件
            if(!newFile) {
                mergeFile = null;
            }
        } catch (IOException e) {
            e.printStackTrace();
            log.error("创建合并文件失败", e.getMessage());
            mergeFile = null;
        }

        return mergeFile;

    }




    /**
     * 得到块文件所在目录
     * @param fileMd5
     * @return
     */
    private String getChunkFileFolderPath(String fileMd5) {
        SysUser currentUser = ShiroUtils.getSysUser();
        String dirPath = MediaConfig.getProfile()+"/upload" + File.separator+currentUser.getLoginName()+File.separator+"video/"+ MediaConstrans.MEDIA_UPLOAD_CHUNKS_PACH+"/"+fileMd5+"/";
        File file = new File(dirPath);
        if (!file.exists()) {
            file.mkdirs();
        }
        return dirPath;
    }
    /**
     * 编码文件名
     */
    private static final String encodingFilename(String fileName)
    {
        fileName = fileName.replace("_", " ");
        fileName = Md5Utils.hash(fileName + System.nanoTime() + counter++);
        return fileName;
    }


    /**
     * 获取所有分片文件
     */
    private List<File> getChunkFiles(File chunkFileFolder) {
        //获取路径下的所有块文件
        File[] chunkFiles = chunkFileFolder.listFiles();
        //将文件数组转成list，并排序
        List<File> chunkFileList = new ArrayList<File>();
        chunkFileList.addAll(Arrays.asList(chunkFiles));
        //排序
        Collections.sort(chunkFileList, new Comparator<File>() {
            @Override
            public int compare(File o1, File o2) {
                if (Integer.parseInt(o1.getName().replace(".part", "")) > Integer.parseInt(o2.getName().replace(".part", ""))) {
                    return 1;
                }
                return -1;
            }
        });
        return chunkFileList;
    }
    /**
     * 合并块文件
     */
    private File mergeFile(File mergeFile, List<File> chunkFiles) {
        try {
            //创建写文件对象
            RandomAccessFile raf_write = new RandomAccessFile(mergeFile, "rw");
            //遍历分块文件开始合并
            //读取文件缓冲区
            byte[] b = new byte[1024];
            for (File chunkFile : chunkFiles) {
                RandomAccessFile raf_read = new RandomAccessFile(chunkFile, "r");
                int len = -1;
                //读取分块文件
                while ((len = raf_read.read(b)) != -1) {
                    //向合并文件中写数据
                    raf_write.write(b, 0, len);
                }
                raf_read.close();
            }
            raf_write.close();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("merge file error:{}", e.getMessage());
            return null;
        }
        return mergeFile;
    }
}
