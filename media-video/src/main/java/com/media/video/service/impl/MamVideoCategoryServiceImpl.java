package com.media.video.service.impl;

import com.media.common.core.domain.Ztree;
import com.media.common.core.text.Convert;
import com.media.common.utils.DateUtils;
import com.media.video.domain.MamVideoCategory;
import com.media.video.mapper.MamVideoCategoryMapper;
import com.media.video.service.IMamVideoCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 视频分类信息Service业务层处理
 *
 * @author liziye
 * @date 2021-12-17
 */
@Service
public class MamVideoCategoryServiceImpl implements IMamVideoCategoryService
{
    @Autowired
    private MamVideoCategoryMapper mamVideoCategoryMapper;

    /**
     * 查询视频分类信息
     *
     * @param categoryId 视频分类信息ID
     * @return 视频分类信息
     */
    @Override
    public MamVideoCategory selectMamVideoCategoryById(Long categoryId)
    {
        return mamVideoCategoryMapper.selectMamVideoCategoryById(categoryId);
    }

    /**
     * 查询视频分类信息列表
     *
     * @param mamVideoCategory 视频分类信息
     * @return 视频分类信息
     */
    @Override
    public List<MamVideoCategory> selectMamVideoCategoryList(MamVideoCategory mamVideoCategory)
    {
        return mamVideoCategoryMapper.selectMamVideoCategoryList(mamVideoCategory);
    }

    /**
     * 新增视频分类信息
     *
     * @param mamVideoCategory 视频分类信息
     * @return 结果
     */
    @Override
    public int insertMamVideoCategory(MamVideoCategory mamVideoCategory)
    {
        mamVideoCategory.setCreateTime(DateUtils.getNowDate());
        return mamVideoCategoryMapper.insertMamVideoCategory(mamVideoCategory);
    }

    /**
     * 修改视频分类信息
     *
     * @param mamVideoCategory 视频分类信息
     * @return 结果
     */
    @Override
    public int updateMamVideoCategory(MamVideoCategory mamVideoCategory)
    {
        mamVideoCategory.setUpdateTime(DateUtils.getNowDate());
        return mamVideoCategoryMapper.updateMamVideoCategory(mamVideoCategory);
    }

    /**
     * 删除视频分类信息对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMamVideoCategoryByIds(String ids)
    {
        return mamVideoCategoryMapper.deleteMamVideoCategoryByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除视频分类信息信息
     *
     * @param categoryId 视频分类信息ID
     * @return 结果
     */
    @Override
    public int deleteMamVideoCategoryById(Long categoryId)
    {
        return mamVideoCategoryMapper.deleteMamVideoCategoryById(categoryId);
    }

    /**
     * 查询视频分类信息树列表
     *
     * @return 所有视频分类信息信息
     */
    @Override
    public List<Ztree> selectMamVideoCategoryTree()
    {
        List<MamVideoCategory> mamVideoCategoryList = mamVideoCategoryMapper.selectMamVideoCategoryList(new MamVideoCategory());
        List<Ztree> ztrees = new ArrayList<Ztree>();
        for (MamVideoCategory mamVideoCategory : mamVideoCategoryList)
        {
            Ztree ztree = new Ztree();
            ztree.setId(mamVideoCategory.getCategoryId());
            ztree.setpId(mamVideoCategory.getParentId());
            ztree.setName(mamVideoCategory.getName());
            ztree.setTitle(mamVideoCategory.getName());
            ztrees.add(ztree);
        }
        return ztrees;
    }
}
