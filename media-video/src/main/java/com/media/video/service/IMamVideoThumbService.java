package com.media.video.service;

import com.media.video.domain.MamVideo;
import com.media.video.domain.MamVideoThumb;

import java.util.List;
import java.util.Map;

/**
 * 视频缩略图Service接口
 * 
 * @author liziye
 * @date 2022-03-15
 */
public interface IMamVideoThumbService 
{
    /**
     * 查询视频缩略图
     * 
     * @param thumbId 视频缩略图ID
     * @return 视频缩略图
     */
    public MamVideoThumb selectMamVideoThumbById(Long thumbId);

    /**
     * 查询视频缩略图列表
     * 
     * @param mamVideoThumb 视频缩略图
     * @return 视频缩略图集合
     */
    public List<MamVideoThumb> selectMamVideoThumbList(MamVideoThumb mamVideoThumb);

    /**
     * 新增视频缩略图
     * 
     * @param mamVideoThumb 视频缩略图
     * @return 结果
     */
    public int insertMamVideoThumb(MamVideoThumb mamVideoThumb);

    /**
     * 修改视频缩略图
     * 
     * @param mamVideoThumb 视频缩略图
     * @return 结果
     */
    public int updateMamVideoThumb(MamVideoThumb mamVideoThumb);

    /**
     * 批量删除视频缩略图
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMamVideoThumbByIds(String ids);

    /**
     * 删除视频缩略图信息
     * 
     * @param thumbId 视频缩略图ID
     * @return 结果
     */
    public int deleteMamVideoThumbById(Long thumbId);

    /**
     * 截图
     *
     * @return 结果
     */
    public Map<String,Object> cutImage(MamVideo video, String fileName, String pointTime, String base64Stream);
}
