package com.media.video.service;

import com.media.framework.web.domain.response.CheckChunkResult;
import com.media.video.domain.MamVideoSource;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * 源视频Service接口
 * 
 * @author liziye
 * @date 2022-03-15
 */
public interface IMamVideoSourceService 
{
    /**
     * 查询源视频
     * 
     * @param id 源视频ID
     * @return 源视频
     */
    public MamVideoSource selectMamVideoSourceById(Long id);

    /**
     * 查询源视频列表
     * 
     * @param mamVideoSource 源视频
     * @return 源视频集合
     */
    public List<MamVideoSource> selectMamVideoSourceList(MamVideoSource mamVideoSource);

    /**
     * 新增源视频
     * 
     * @param mamVideoSource 源视频
     * @return 结果
     */
    public int insertMamVideoSource(MamVideoSource mamVideoSource);

    /**
     * 修改源视频
     * 
     * @param mamVideoSource 源视频
     * @return 结果
     */
    public int updateMamVideoSource(MamVideoSource mamVideoSource);

    /**
     * 批量删除源视频
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMamVideoSourceByIds(String ids);

    /**
     * 删除源视频信息
     * 
     * @param id 源视频ID
     * @return 结果
     */
    public int deleteMamVideoSourceById(Long id);

    /**
     * 检测块文件
     * @param name
     * @param chunkIndex
     * @param chunkSize
     * @return
     */
    public CheckChunkResult checkChunk(String name, String blockmd5, Integer chunkIndex, Integer chunkSize);

    /**
     * 分片上传视频,实现断点续传
     * @param file
     * @param name
     * @param chunks
     * @param chunk
     * @param size
     * @return
     */
    public Map<String,Object> fileUploadChunk(MultipartFile file, String name, String type, Integer chunks, Integer chunk, Integer size);

    /**
     * 合并分片视频
     * @param name
     * @param chunks
     * @param ext
     * @return
     */
    public Map<String,Object> mergeChunks(String name,String filename,Integer chunks,String ext,String type, Integer size);
}
