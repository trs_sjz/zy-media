package com.media.video.service.impl;

import com.alibaba.fastjson.util.IOUtils;
import com.media.common.config.MediaConfig;
import com.media.common.core.domain.entity.SysUser;
import com.media.common.core.text.Convert;
import com.media.common.utils.DateUtils;
import com.media.common.utils.ShiroUtils;
import com.media.video.domain.MamVideo;
import com.media.video.domain.MamVideoThumb;
import com.media.video.mapper.MamVideoThumbMapper;
import com.media.video.service.IMamVideoThumbService;
import net.coobird.thumbnailator.Thumbnails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 视频缩略图Service业务层处理
 * 
 * @author liziye
 * @date 2022-03-15
 */
@Service
public class MamVideoThumbServiceImpl implements IMamVideoThumbService 
{
    @Autowired
    private MamVideoThumbMapper mamVideoThumbMapper;

    /**
     * 查询视频缩略图
     * 
     * @param thumbId 视频缩略图ID
     * @return 视频缩略图
     */
    @Override
    public MamVideoThumb selectMamVideoThumbById(Long thumbId)
    {
        return mamVideoThumbMapper.selectMamVideoThumbById(thumbId);
    }

    /**
     * 查询视频缩略图列表
     * 
     * @param mamVideoThumb 视频缩略图
     * @return 视频缩略图
     */
    @Override
    public List<MamVideoThumb> selectMamVideoThumbList(MamVideoThumb mamVideoThumb)
    {
        return mamVideoThumbMapper.selectMamVideoThumbList(mamVideoThumb);
    }

    /**
     * 新增视频缩略图
     * 
     * @param mamVideoThumb 视频缩略图
     * @return 结果
     */
    @Override
    public int insertMamVideoThumb(MamVideoThumb mamVideoThumb)
    {
        mamVideoThumb.setCreateTime(DateUtils.getNowDate());
        return mamVideoThumbMapper.insertMamVideoThumb(mamVideoThumb);
    }

    /**
     * 修改视频缩略图
     * 
     * @param mamVideoThumb 视频缩略图
     * @return 结果
     */
    @Override
    public int updateMamVideoThumb(MamVideoThumb mamVideoThumb)
    {
        mamVideoThumb.setUpdateTime(DateUtils.getNowDate());
        return mamVideoThumbMapper.updateMamVideoThumb(mamVideoThumb);
    }

    /**
     * 删除视频缩略图对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMamVideoThumbByIds(String ids)
    {
        return mamVideoThumbMapper.deleteMamVideoThumbByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除视频缩略图信息
     * 
     * @param thumbId 视频缩略图ID
     * @return 结果
     */
    @Override
    public int deleteMamVideoThumbById(Long thumbId)
    {
        return mamVideoThumbMapper.deleteMamVideoThumbById(thumbId);
    }

    /**
     * 上传截图
     * @return
     */
    @Override
    public Map<String, Object> cutImage(MamVideo video, String fileName, String pointTime, String base64Stream) {

        Map<String,Object> map =new HashMap<String,Object>();
        SysUser currentUser = ShiroUtils.getSysUser();

        Double pointTimeNum = Double.valueOf(pointTime);
        String filename =video.getVideoId() + "_" + Math.round(pointTimeNum/1000) + ".jpg";

        String subPath = video.getSubpath() + File.separator+video.getFilenamebase();
        String thumbPath = MediaConfig.getProfile()+video.getFilepathbase()+File.separator+ video.getFilenamebase()+File.separator+filename;

        MamVideoThumb videoThumb = new MamVideoThumb();
        Base64.Decoder decoder =  Base64.getDecoder();

        //FileOutputStream out = null;
        //字节输出流（写入到内存）
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            base64Stream = java.net.URLDecoder.decode(base64Stream,"UTF-8");
            byte[] bytes = decoder.decode(base64Stream);

            Thumbnails.of(new ByteArrayInputStream(bytes)).size(640, 360).toFile(thumbPath);

            videoThumb.setVideoId(video.getVideoId());
            videoThumb.setFilename(filename);
            videoThumb.setSubpath(subPath);
            videoThumb.setFilepath(video.getFilepathbase()+"/"+video.getFilenamebase());
            videoThumb.setInterceptTime(String.valueOf(pointTimeNum/1000));
            videoThumb.setCreateUserId(currentUser.getUserId());
            videoThumb.setCreateBy(currentUser.getLoginName());
            videoThumb.setCreateTime(DateUtils.getNowDate());
            videoThumb.setUpdateBy(currentUser.getLoginName());
            videoThumb.setUpdateTime(DateUtils.getNowDate());
            insertMamVideoThumb(videoThumb);
            map.put("thumbId", videoThumb.getThumbId());
            map.put("interceptTime", videoThumb.getInterceptTime());
            map.put("pontTime", DateUtils.formatSeconds(videoThumb.getInterceptTime().toString()));
            map.put("thumbSrc", "profile"+video.getFilepathbase()+"/"+video.getFilenamebase()+"/"+filename);
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            IOUtils.close(baos);
        }

        return map;
    }
}
