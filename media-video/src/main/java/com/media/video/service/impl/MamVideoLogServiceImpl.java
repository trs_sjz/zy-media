package com.media.video.service.impl;

import com.media.common.core.text.Convert;
import com.media.common.utils.DateUtils;
import com.media.common.utils.security.PermissionUtils;
import com.media.video.domain.MamVideoLog;
import com.media.video.mapper.MamVideoLogMapper;
import com.media.video.service.IMamVideoLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
 * 视频日志Service业务层处理
 * 
 * @author liziye
 * @date 2022-03-18
 */
@Service
public class MamVideoLogServiceImpl implements IMamVideoLogService 
{
    @Autowired
    private MamVideoLogMapper mamVideoLogMapper;

    /**
     * 查询视频日志
     * 
     * @param logId 视频日志ID
     * @return 视频日志
     */
    @Override
    public MamVideoLog selectMamVideoLogById(Long logId)
    {
        return mamVideoLogMapper.selectMamVideoLogById(logId);
    }

    /**
     * 查询视频日志列表
     * 
     * @param mamVideoLog 视频日志
     * @return 视频日志
     */
    @Override
    public List<MamVideoLog> selectMamVideoLogList(MamVideoLog mamVideoLog)
    {
        return mamVideoLogMapper.selectMamVideoLogList(mamVideoLog);
    }

    /**
     * 新增视频日志
     *
     * @return 结果
     */
    @Override
    public int insertVideoLog(Long objectId, String actionMediaType, String action, String actionName, String actionDes,
                               String appCode, String source, String type, String remark,Long createUserId,Long categoryId) {
        String loginName = (String) PermissionUtils.getPrincipalProperty("loginName");
        Long userId = (Long) PermissionUtils.getPrincipalProperty("userId");
        MamVideoLog mamVideoLog = new  MamVideoLog();
        mamVideoLog.setObjectId(objectId);
        mamVideoLog.setUserId(userId);
        mamVideoLog.setActionMediaType(actionMediaType);
        mamVideoLog.setAction(action);
        mamVideoLog.setActionName(actionName);
        mamVideoLog.setActionDes(actionDes);
        mamVideoLog.setAppCode(appCode);
        mamVideoLog.setSource(source);
        mamVideoLog.setType(type);
        mamVideoLog.setRemark(remark);
        mamVideoLog.setCreateBy(loginName);
        mamVideoLog.setCreateTime(DateUtils.getNowDate());
        mamVideoLog.setUpdateBy(loginName);
        mamVideoLog.setUpdateTime(DateUtils.getNowDate());
        mamVideoLog.setCreateUserId(createUserId);
        mamVideoLog.setCategoryId(categoryId);
        return mamVideoLogMapper.insertMamVideoLog(mamVideoLog);
    }

    /**
     * 修改视频日志
     * 
     * @param mamVideoLog 视频日志
     * @return 结果
     */
    @Override
    public int updateMamVideoLog(MamVideoLog mamVideoLog)
    {
        mamVideoLog.setUpdateTime(DateUtils.getNowDate());
        return mamVideoLogMapper.updateMamVideoLog(mamVideoLog);
    }

    /**
     * 删除视频日志对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMamVideoLogByIds(String ids)
    {
        return mamVideoLogMapper.deleteMamVideoLogByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除视频日志信息
     * 
     * @param logId 视频日志ID
     * @return 结果
     */
    @Override
    public int deleteMamVideoLogById(Long logId)
    {
        return mamVideoLogMapper.deleteMamVideoLogById(logId);
    }

    /**
     * 获取视频下载量(播放量)图表数据
     *
     * @param mamVideoLog 日志对象
     * @return 结果
     */
    @Override
    public List<HashMap<String, Object>> getVideoChartData(MamVideoLog mamVideoLog) {
        return mamVideoLogMapper.getVideoChartData(mamVideoLog);
    }
}
