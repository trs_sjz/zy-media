package com.media.video.service;

import com.media.video.domain.MamVideoProcessjob;

import java.util.List;

/**
 * 视频加工任务Service接口
 * 
 * @author liziye
 * @date 2022-03-15
 */
public interface IMamVideoProcessjobService 
{
    /**
     * 查询视频加工任务
     * 
     * @param id 视频加工任务ID
     * @return 视频加工任务
     */
    public MamVideoProcessjob selectMamVideoProcessjobById(Long id);

    /**
     * 查询视频加工任务列表
     * 
     * @param mamVideoProcessjob 视频加工任务
     * @return 视频加工任务集合
     */
    public List<MamVideoProcessjob> selectMamVideoProcessjobList(MamVideoProcessjob mamVideoProcessjob);

    /**
     * 新增视频加工任务
     * 
     * @param mamVideoProcessjob 视频加工任务
     * @return 结果
     */
    public int insertMamVideoProcessjob(MamVideoProcessjob mamVideoProcessjob);

    /**
     * 修改视频加工任务
     * 
     * @param mamVideoProcessjob 视频加工任务
     * @return 结果
     */
    public int updateMamVideoProcessjob(MamVideoProcessjob mamVideoProcessjob);

    /**
     * 批量删除视频加工任务
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMamVideoProcessjobByIds(String ids);

    /**
     * 删除视频加工任务信息
     * 
     * @param id 视频加工任务ID
     * @return 结果
     */
    public int deleteMamVideoProcessjobById(Long id);

    /**
     * 更新视频作业任务状态
     * @param mamVideoProcessjob
     */
    public void updateMamVideoProcessjobState(MamVideoProcessjob mamVideoProcessjob);

    /**
     * 查询用户待处理任务列表
     *
     * @return 视频加工任务表集合
     */
    public List<MamVideoProcessjob> selectMamProcessjobList();
}
