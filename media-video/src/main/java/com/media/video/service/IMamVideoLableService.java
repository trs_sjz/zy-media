package com.media.video.service;

import com.media.video.domain.MamVideoLable;

import java.util.List;

/**
 * 视频标签Service接口
 * 
 * @author liziye
 * @date 2022-03-16
 */
public interface IMamVideoLableService 
{
    /**
     * 查询视频标签
     * 
     * @param lableId 视频标签ID
     * @return 视频标签
     */
    public MamVideoLable selectMamVideoLableById(Long lableId);

    /**
     * 查询视频标签列表
     * 
     * @param mamVideoLable 视频标签
     * @return 视频标签集合
     */
    public List<MamVideoLable> selectMamVideoLableList(MamVideoLable mamVideoLable);

    /**
     * 新增视频标签
     * 
     * @param mamVideoLable 视频标签
     * @return 结果
     */
    public int insertMamVideoLable(MamVideoLable mamVideoLable);

    /**
     * 修改视频标签
     * 
     * @param mamVideoLable 视频标签
     * @return 结果
     */
    public int updateMamVideoLable(MamVideoLable mamVideoLable);

    /**
     * 批量删除视频标签
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMamVideoLableByIds(String ids);

    /**
     * 删除视频标签信息
     * 
     * @param lableId 视频标签ID
     * @return 结果
     */
    public int deleteMamVideoLableById(Long lableId);
}
