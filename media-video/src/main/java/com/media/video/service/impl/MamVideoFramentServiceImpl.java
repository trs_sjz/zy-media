package com.media.video.service.impl;

import com.media.common.core.text.Convert;
import com.media.common.utils.DateUtils;
import com.media.video.domain.MamVideoFrament;
import com.media.video.mapper.MamVideoFramentMapper;
import com.media.video.service.IMamVideoFramentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 视频片段Service业务层处理
 * 
 * @author liziye
 * @date 2022-03-18
 */
@Service
public class MamVideoFramentServiceImpl implements IMamVideoFramentService 
{
    @Autowired
    private MamVideoFramentMapper mamVideoFramentMapper;

    /**
     * 查询视频片段
     * 
     * @param framentId 视频片段ID
     * @return 视频片段
     */
    @Override
    public MamVideoFrament selectMamVideoFramentById(Long framentId)
    {
        return mamVideoFramentMapper.selectMamVideoFramentById(framentId);
    }

    /**
     * 查询视频片段列表
     * 
     * @param mamVideoFrament 视频片段
     * @return 视频片段
     */
    @Override
    public List<MamVideoFrament> selectMamVideoFramentList(MamVideoFrament mamVideoFrament)
    {
        return mamVideoFramentMapper.selectMamVideoFramentList(mamVideoFrament);
    }

    /**
     * 新增视频片段
     * 
     * @param mamVideoFrament 视频片段
     * @return 结果
     */
    @Override
    public int insertMamVideoFrament(MamVideoFrament mamVideoFrament)
    {
        mamVideoFrament.setCreateTime(DateUtils.getNowDate());
        return mamVideoFramentMapper.insertMamVideoFrament(mamVideoFrament);
    }

    /**
     * 修改视频片段
     * 
     * @param mamVideoFrament 视频片段
     * @return 结果
     */
    @Override
    public int updateMamVideoFrament(MamVideoFrament mamVideoFrament)
    {
        mamVideoFrament.setUpdateTime(DateUtils.getNowDate());
        return mamVideoFramentMapper.updateMamVideoFrament(mamVideoFrament);
    }

    /**
     * 删除视频片段对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMamVideoFramentByIds(String ids)
    {
        return mamVideoFramentMapper.deleteMamVideoFramentByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除视频片段信息
     * 
     * @param framentId 视频片段ID
     * @return 结果
     */
    @Override
    public int deleteMamVideoFramentById(Long framentId)
    {
        return mamVideoFramentMapper.deleteMamVideoFramentById(framentId);
    }
}
