package com.media.video.service;

import com.media.video.domain.MamVideoLog;

import java.util.HashMap;
import java.util.List;

/**
 * 视频日志Service接口
 * 
 * @author liziye
 * @date 2022-03-18
 */
public interface IMamVideoLogService 
{
    /**
     * 查询视频日志
     * 
     * @param logId 视频日志ID
     * @return 视频日志
     */
    public MamVideoLog selectMamVideoLogById(Long logId);

    /**
     * 查询视频日志列表
     * 
     * @param mamVideoLog 视频日志
     * @return 视频日志集合
     */
    public List<MamVideoLog> selectMamVideoLogList(MamVideoLog mamVideoLog);

    /**
     * 新增视频日志
     * @param objectId
     * @param actionMediaType
     * @param action
     * @param actionName
     * @param actionDes
     * @param appCode
     * @param source
     * @param type
     * @param remark
     * @return
     */
    public int insertVideoLog(Long objectId,String actionMediaType,String action,String actionName,String actionDes,String appCode,String source,String type,String remark,Long createUserId,Long categoryId);


    /**
     * 修改视频日志
     * 
     * @param mamVideoLog 视频日志
     * @return 结果
     */
    public int updateMamVideoLog(MamVideoLog mamVideoLog);

    /**
     * 批量删除视频日志
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMamVideoLogByIds(String ids);

    /**
     * 删除视频日志信息
     * 
     * @param logId 视频日志ID
     * @return 结果
     */
    public int deleteMamVideoLogById(Long logId);

    /**
     * 获取视频下载量(播放量)图表数据
     *
     * @param mamVideoLog
     * @return 结果
     */
    List<HashMap<String, Object>> getVideoChartData(MamVideoLog mamVideoLog);
}
