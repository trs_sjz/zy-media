<p align="center">
	<img alt="logo" src="https://oscimg.oschina.net/oscnet/up-dd77653d7c9f197dd9d93684f3c8dcfbab6.png">
</p>
<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">RuoYi v4.7.3</h1>
<h4 align="center">基于SpringBoot开发的轻量级Java快速开发框架</h4>
<p align="center">
	<a href="https://gitee.com/y_project/RuoYi/stargazers"><img src="https://gitee.com/y_project/RuoYi/badge/star.svg?theme=gvp"></a>
	<a href="https://gitee.com/y_project/RuoYi"><img src="https://img.shields.io/badge/RuoYi-v4.7.3-brightgreen.svg"></a>
	<a href="https://gitee.com/y_project/RuoYi/blob/master/LICENSE"><img src="https://img.shields.io/github/license/mashape/apistatus.svg"></a>
</p>

## 平台简介
首先特别感谢若依给我提供了一个快速开发平台，谢谢！

我爱人在工作单位负责宣传，工作内容就是对照片和视频进行拍摄和管理，看她在工作过程中很辛苦，就开发出了这么一套媒资管理系统来帮助她减轻工作压力~ 想到可能会有些小伙伴也需要类似的系统来解决自己所面临的问题，就将此系统开源出来，和大家一起分享学习~

目前系统已经初具雏形，但是由于工作原因没办法定时更新代码，不过我会利用业余时间继续更新

## 内置功能

1.  用户管理：用户是系统操作者，该功能主要完成系统用户配置。
2.  部门管理：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。
3.  岗位管理：配置系统用户所属担任职务。
4.  菜单管理：配置系统菜单，操作权限，按钮权限标识等。
5.  角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
6.  字典管理：对系统中经常使用的一些较为固定的数据进行维护。
7.  参数管理：对系统动态配置常用参数。
8.  通知公告：系统通知公告信息发布维护。
9.  操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
10. 登录日志：系统登录日志记录查询包含登录异常。
11. 在线用户：当前系统中活跃用户状态监控。
12. 定时任务：在线（添加、修改、删除)任务调度包含执行结果日志。
13. 代码生成：前后端代码的生成（java、html、xml、sql）支持CRUD下载 。
14. 系统接口：根据业务代码自动生成相关的api接口文档。
15. 服务监控：监视当前系统CPU、内存、磁盘、堆栈等相关信息。
16. 缓存监控：对系统的缓存查询，删除、清空等操作。
17. 在线构建器：拖动表单元素生成相应的HTML代码。
18. 连接池监视：监视当前系统数据库连接池状态，可进行分析SQL找出系统性能瓶颈。
19. 视频管理：整合FFmpeg框架，可根据个人所需（视频格式、视频比特率、音频比特率）进行编码。
20. 图片管理：对图片详细信息（分辨率、光圈值、曝光时间等）进行提取，并对图片进行分组管理。

注：【视频管理】模块已经初具雏形，各部分功能可正常使用；【图片管理】模块处于开发阶段，后续会进行更新；本项目采用的数据库为 mysql 5.7.29，对mysql8的适配可能会存在问题，后续会一一解决

在使用视频编码前需要在【系统管理】→【参数设置】中设置"ffmpeg和ffprobe所在目录"，例如 D:\win32\ffmpeg。ffmpeg工具在ffmpegUtil文件夹内，该工具可放在任意路径下。

## 访问地址及账号密码

- 访问地址：http://localhost/media/login
- 账号密码：admin/admin123  


## 互动交流
本系统中还存在部分待开发或未解决的Bug，如果您有什么好的意见建议，或者您想给本项目添砖加瓦，那么请您动动可爱的小手请在评论区留言，谢谢~

P.S. 系统的开发人员只有我自己一人，精力和时间都有限，如果系统中存在什么低端错误还请您见谅，谢谢各位！